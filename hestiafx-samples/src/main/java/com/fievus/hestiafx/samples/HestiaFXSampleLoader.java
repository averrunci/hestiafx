/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.control.Control;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

public class HestiaFXSampleLoader extends Service<ObservableList<HestiaFXSampleView>> {
    @Override
    protected Task<ObservableList<HestiaFXSampleView>> createTask() {
        return new Task<ObservableList<HestiaFXSampleView>>() {
            @Override
            protected ObservableList<HestiaFXSampleView> call() throws Exception {
                return load();
            }
        };
    }

    @SuppressWarnings("unchecked")
    public ObservableList<HestiaFXSampleView> load() {
        ObservableList<HestiaFXSampleView> contexts = FXCollections.observableArrayList();
        resolveClassPath().ifPresent(classPath -> {
            try {
                Files.walk(classPath)
                    .filter(path -> !Files.isDirectory(path))
                    .filter(path -> path.toString().endsWith(".class"))
                    .map(path -> classPath.relativize(path).toString().replace(File.separatorChar, '.'))
                    .map(classFile -> {
                        try {
                            return Class.forName(classFile.substring(0, classFile.length() - 6));
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                            return null;
                        }
                    })
                    .filter(c -> c != null)
                    .filter(Control.class::isAssignableFrom)
                    .filter(c -> c.isAnnotationPresent(HestiaFXSample.class))
                    .map(c -> HestiaFXSampleView.of(c.getDeclaredAnnotation(HestiaFXSample.class)).with((Class<? extends Control>)c))
                    .forEach(contexts::add);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        return contexts;
    }

    private Optional<Path> resolveClassPath() {
        try {
            return Optional.of(
                new File(getClass().getResource(getClass().getSimpleName() + ".class").toURI())
                    .toPath()
                    .resolve("../../../../..")
                    .normalize()
            );
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }
}
