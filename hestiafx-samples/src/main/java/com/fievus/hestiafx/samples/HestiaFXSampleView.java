/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;

import java.util.Objects;
import java.util.Optional;

public class HestiaFXSampleView extends Control {
    public ReadOnlyStringProperty titleProperty() { return title.getReadOnlyProperty(); }
    private final ReadOnlyStringWrapper title = new ReadOnlyStringWrapper(this, "title");
    public final String getTitle() { return title.get(); }
    protected final void setTitle(String title) { this.title.set(title); }

    public ReadOnlyBooleanProperty requireStageProperty() { return requireStage.getReadOnlyProperty(); }
    private final ReadOnlyBooleanWrapper requireStage = new ReadOnlyBooleanWrapper(this, "requireStage");
    public final boolean requireStage() { return requireStage.get(); }
    protected final void setRequireStage(boolean requireStage) { this.requireStage.set(requireStage); }

    public BooleanProperty selectedProperty() { return selected; }
    private final BooleanProperty selected = new SimpleBooleanProperty(this, "selected");
    public final boolean isSelected() { return selected.get(); }
    public final void setSelected(boolean selected) { this.selected.set(selected); }

    private final Class<? extends Control> hestiaFXSampleClass;

    protected HestiaFXSampleView(Builder builder) {
        Objects.requireNonNull(builder);

        setTitle(builder.title);
        setRequireStage(builder.requireStage);
        hestiaFXSampleClass = builder.hestiaFXSampleClass;
    }

    public static Builder of(HestiaFXSample hestiaFXSample) {
        return new Builder(Objects.requireNonNull(hestiaFXSample));
    }

    public Optional<Control> createHestiaFXSample() {
        try {
            return Optional.of(hestiaFXSampleClass.newInstance());
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new HestiaFXSampleViewSkin(this);
    }

    public static class Builder {
        private final String title;
        private final boolean requireStage;
        private Class<? extends Control> hestiaFXSampleClass;

        protected Builder(HestiaFXSample hestiaFXSample) {
            Objects.requireNonNull(hestiaFXSample);

            title = hestiaFXSample.title();
            requireStage = hestiaFXSample.requireStage();
        }

        public HestiaFXSampleView with(Class<? extends Control> hestiaFXSampleClass) {
            this.hestiaFXSampleClass = Objects.requireNonNull(hestiaFXSampleClass);
            return new HestiaFXSampleView(this);
        }
    }
}
