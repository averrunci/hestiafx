/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples;

import com.fievus.hestiafx.scene.control.FXMLSkin;
import com.fievus.hestiafx.scene.input.MouseOverZoomInOut;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class HestiaFXSampleViewSkin extends FXMLSkin<HestiaFXSampleView> {
    @FXML
    private Label titleLabel;

    protected HestiaFXSampleViewSkin(HestiaFXSampleView control) {
        super(control);

        consumeMouseEvents(false);
    }

    @FXML
    protected void initialize() {
        titleLabel.textProperty().bind(getSkinnable().titleProperty());
        MouseOverZoomInOut.of(getSkinnable())
            .zoomInXUntil(1.2)
            .zoomInYUntil(1.2)
            .with(node -> !((HestiaFXSampleView)node).isSelected())
            .enable();
    }
}
