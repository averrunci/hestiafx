/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples;

import javafx.application.Application;
import javafx.stage.Stage;

public class HestiaFXSamples extends Application {
    public static void main(String... args) {
        Application.launch(HestiaFXSamples.class, args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        HestiaFXSamplesMainView.performOn(primaryStage).show();
    }
}
