/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples;

import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.scene.Scene;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import javafx.scene.layout.Background;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class HestiaFXSamplesMainView extends Control {
    public ReadOnlyStringProperty titleProperty() { return title.getReadOnlyProperty(); }
    private final ReadOnlyStringWrapper title = new ReadOnlyStringWrapper(this, "title", "HestiaFX Samples");
    public final String getTitle() { return title.get(); }
    protected final void setTitle(String title) { this.title.set(title); }

    public HestiaFXSamplesMainView() {
        setBackground(Background.EMPTY);
        getStyleClass().add("hestia-fx-samples-main-view");
    }

    public static Stage performOn(Stage stage) {
        HestiaFXSamplesMainView view = new HestiaFXSamplesMainView();
        stage.setScene(new Scene(view, 1320, 990, null));
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.titleProperty().bind(view.titleProperty());
        stage.sizeToScene();
        stage.centerOnScreen();
        stage.show();
        return stage;
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new HestiaFXSamplesMainViewSkin(this);
    }
}
