/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples;

import com.fievus.hestiafx.scene.control.FXMLSkin;
import com.fievus.hestiafx.stage.WindowResizeThumb;
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;

public class HestiaFXSamplesMainViewSkin extends FXMLSkin<HestiaFXSamplesMainView> {
    @FXML
    private Pane headerRegion;
    @FXML
    private Button minimizeButton;
    @FXML
    private Button closeButton;
    @FXML
    private Label titleLabel;
    @FXML
    private ListView<HestiaFXSampleView> sampleContentListView;
    @FXML
    private Label sampleContentTitleLabel;
    @FXML
    private Pane sampleContentRegion;

    protected HestiaFXSamplesMainViewSkin(HestiaFXSamplesMainView control) {
        super(control);
    }

    @FXML
    protected void initialize() {
        WindowResizeThumb.to(getSkinnable()).enable();

        minimizeButton.setOnAction(e -> {
            Stage stage = (Stage)getSkinnable().getScene().getWindow();
            stage.setIconified(!stage.isIconified());
        });
        closeButton.setOnAction(e -> ((Stage)getSkinnable().getScene().getWindow()).close());
        titleLabel.textProperty().bind(getSkinnable().titleProperty());

        sampleContentListView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        sampleContentListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            deselect(oldValue);
            select(newValue);
        });

        HestiaFXSampleLoader loader = new HestiaFXSampleLoader();
        sampleContentListView.itemsProperty().bind(loader.valueProperty());
        loader.setOnSucceeded(e -> {
            if (sampleContentListView.getItems().isEmpty()) { return; }
            sampleContentListView.getSelectionModel().selectFirst();
        });
        loader.start();
    }

    private void select(HestiaFXSampleView view) {
        if (view == null) { return; }

        view.setScaleX(1.2);
        view.setScaleY(1.2);
        view.setSelected(true);

        show(view);
    }

    private void deselect(HestiaFXSampleView view) {
        if (view == null) { return; }

        view.setScaleX(1);
        view.setScaleY(1);
        view.setSelected(false);
    }

    private void show(HestiaFXSampleView view) {
        if (view == null) { return; }

        view.createHestiaFXSample().ifPresent(content -> {
            sampleContentTitleLabel.textProperty().bind(view.titleProperty());
            sampleContentRegion.getChildren().add(0, content);
            if (sampleContentRegion.getChildren().size() < 2) { return; }

            FadeTransition fadeOut = new FadeTransition(Duration.millis(600), sampleContentRegion.getChildren().get(1));
            fadeOut.setToValue(0);
            fadeOut.setOnFinished(e -> sampleContentRegion.getChildren().remove(1));

            FadeTransition fadeIn = new FadeTransition(Duration.millis(600), sampleContentRegion.getChildren().get(0));
            fadeIn.setFromValue(0);
            fadeIn.setToValue(1);

            new ParallelTransition(fadeOut, fadeIn).play();
        });
    }
}
