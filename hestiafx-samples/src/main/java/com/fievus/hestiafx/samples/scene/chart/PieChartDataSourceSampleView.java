/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples.scene.chart;

import com.fievus.hestiafx.samples.HestiaFXSample;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;

@HestiaFXSample(title = "PieChartDataSource Sample")
public class PieChartDataSourceSampleView extends Control {
    public ReadOnlyObjectProperty<ObservableList<Record>> recordsProperty() { return records.getReadOnlyProperty(); }
    private final ReadOnlyObjectWrapper<ObservableList<Record>> records = new ReadOnlyObjectWrapper<>(this, "records", FXCollections.observableArrayList());
    public final ObservableList<Record> getRecords() { return records.get(); }
    protected final void setRecords(ObservableList<Record> records) { this.records.set(records); }

    public PieChartDataSourceSampleView() {
        getStyleClass().add("pie-chart-data-source-sample-view");
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new PieChartDataSourceSampleViewSkin(this);
    }
}
