/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples.scene.chart;

import com.fievus.hestiafx.scene.chart.PieChartDataSource;
import com.fievus.hestiafx.scene.control.FXMLSkin;
import com.fievus.hestiafx.scene.control.TableColumnGeneration;
import javafx.fxml.FXML;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;

import java.util.Random;
import java.util.stream.IntStream;

public class PieChartDataSourceSampleViewSkin extends FXMLSkin<PieChartDataSourceSampleView> {
    @FXML
    private TableView<Record> recordTableView;
    @FXML
    private Button addRecordButton;
    @FXML
    private Button removeRecordButton;
    @FXML
    private ToggleGroup pieValuePropertyNameGroup;
    @FXML
    private PieChart recordPieChart;

    private final Random random = new Random();
    private final PieChartDataSource<Record> recordPieChartDataSource = new PieChartDataSource<>();
    private int lastRecordId = 0;

    protected PieChartDataSourceSampleViewSkin(PieChartDataSourceSampleView control) {
        super(control);
    }

    @FXML
    protected void initialize() {
        TableColumnGeneration.forTableView(recordTableView).itemTypeOf(Record.class).enable();
        recordTableView.itemsProperty().bind(getSkinnable().recordsProperty());

        recordPieChartDataSource.setPieChart(recordPieChart);
        recordPieChartDataSource.setNamePropertyStep("title");
        recordPieChartDataSource.itemsProperty().bind(getSkinnable().recordsProperty());

        pieValuePropertyNameGroup.selectedToggleProperty().addListener((observable, oldValue, newValue) ->
            recordPieChartDataSource.setPieValuePropertyStep(newValue.getUserData().toString())
        );
        pieValuePropertyNameGroup.selectToggle(pieValuePropertyNameGroup.getToggles().get(0));

        addRecordButton.setOnAction(e -> addRecord());

        removeRecordButton.setOnAction(e ->
            getSkinnable().getRecords().remove(recordTableView.getSelectionModel().getSelectedItem())
        );

        IntStream.rangeClosed(1, 5).forEach(index -> addRecord());
    }

    private void addRecord() {
        getSkinnable().getRecords().add(
            new Record(String.format("title-%1$s", ++lastRecordId), random.nextInt(20), random.nextInt(10))
        );
    }
}
