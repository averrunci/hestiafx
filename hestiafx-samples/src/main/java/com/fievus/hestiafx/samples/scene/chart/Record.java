/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples.scene.chart;

import com.fievus.hestiafx.scene.control.TableColumn;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Record {
    @TableColumn(order = 1, prefWidth = 120)
    public StringProperty titleProperty() { return title; }
    private final StringProperty title = new SimpleStringProperty(this, "title");
    public final String getTitle() { return title.get(); }
    public final void setTitle(String title) { this.title.set(title); }

    @TableColumn(order = 2)
    public DoubleProperty scoreProperty() { return score; }
    private final DoubleProperty score = new SimpleDoubleProperty(this, "score");
    public final double getScore() { return score.get(); }
    public final void setScore(double score) { this.score.set(score); }

    @TableColumn(order = 3)
    public IntegerProperty rankProperty() { return rank; }
    private final IntegerProperty rank = new SimpleIntegerProperty(this, "rank");
    public final int getRank() { return rank.get(); }
    public final void setRank(int rank) { this.rank.set(rank); }

    public Record(String title, double score, int rank) {
        setTitle(title);
        setScore(score);
        setRank(rank);
    }
}
