/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples.scene.chart;

import com.fievus.hestiafx.samples.HestiaFXSample;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;

@HestiaFXSample(title = "XYChartDataSource Sample")
public class XYChartDataSourceSampleView extends Control {
    public ReadOnlyObjectProperty<ObservableList<XYRecord>> recordsProperty() { return records.getReadOnlyProperty(); }
    private final ReadOnlyObjectWrapper<ObservableList<XYRecord>> records = new ReadOnlyObjectWrapper<>(this, "records", FXCollections.observableArrayList());
    public final ObservableList<XYRecord> getRecords() { return records.get(); }
    protected final void setRecords(ObservableList<XYRecord> records) { this.records.set(records); }

    public XYChartDataSourceSampleView() {
        getStyleClass().add("xy-chart-data-source-sample-view");
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new XYChartDataSourceSampleViewSkin(this);
    }
}
