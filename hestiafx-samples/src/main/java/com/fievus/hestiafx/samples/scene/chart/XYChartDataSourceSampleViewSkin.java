/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples.scene.chart;

import com.fievus.hestiafx.scene.chart.XYChartDataSource;
import com.fievus.hestiafx.scene.control.FXMLSkin;
import com.fievus.hestiafx.scene.control.TableColumnGeneration;
import javafx.fxml.FXML;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.BubbleChart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.ScatterChart;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;

import java.util.Random;
import java.util.stream.IntStream;

public class XYChartDataSourceSampleViewSkin extends FXMLSkin<XYChartDataSourceSampleView> {
    @FXML
    private TableView<XYRecord> recordTableView;
    @FXML
    private Button addRecordButton;
    @FXML
    private Button removeRecordButton;
    @FXML
    private AreaChart<String, Double> recordAreaChart;
    @FXML
    private BarChart<String, Double> recordBarChart;
    @FXML
    private BubbleChart<Integer, Double> recordBubbleChart;
    @FXML
    private LineChart<String, Double> recordLineChart;
    @FXML
    private ScatterChart<String, Double> recordScatterChart;

    private final Random random = new Random();

    private final XYChartDataSource<XYRecord, String, Double> recordAreaChartDataSource = new XYChartDataSource<>();
    private final XYChartDataSource<XYRecord, String, Double> recordBarChartDataSource = new XYChartDataSource<>();
    private final XYChartDataSource<XYRecord, Integer, Double> recordBubbleChartDataSource = new XYChartDataSource<>();
    private final XYChartDataSource<XYRecord, String, Double> recordLineChartDataSource = new XYChartDataSource<>();
    private final XYChartDataSource<XYRecord, String, Double> recordScatterChartDataSource = new XYChartDataSource<>();

    private int lastRecordId;

    protected XYChartDataSourceSampleViewSkin(XYChartDataSourceSampleView control) {
        super(control);
    }

    @FXML
    protected void initialize() {
        recordAreaChartDataSource.setXYChart(recordAreaChart);
        recordAreaChartDataSource.newSeries()
            .withName("Series #1")
            .bindItems(getSkinnable().recordsProperty())
            .withXValuePropertyStep("name")
            .withYValuePropertyStep("value1")
            .register();
        recordAreaChartDataSource.newSeries()
            .withName("Series #2")
            .bindItems(getSkinnable().recordsProperty())
            .withXValuePropertyStep("name")
            .withYValuePropertyStep("value2")
            .register();

        recordBarChartDataSource.setXYChart(recordBarChart);
        recordBarChartDataSource.newSeries()
            .withName("Series #1")
            .bindItems(getSkinnable().recordsProperty())
            .withXValuePropertyStep("name")
            .withYValuePropertyStep("value1")
            .register();
        recordBarChartDataSource.newSeries()
            .withName("Series #2")
            .bindItems(getSkinnable().recordsProperty())
            .withXValuePropertyStep("name")
            .withYValuePropertyStep("value2")
            .register();

        recordBubbleChartDataSource.setXYChart(recordBubbleChart);
        recordBubbleChartDataSource.newSeries()
            .withName("Series #1")
            .bindItems(getSkinnable().recordsProperty())
            .withXValuePropertyStep("value0")
            .withYValuePropertyStep("value1")
            .withExtraValuePropertyStep("value3")
            .register();
        recordBubbleChartDataSource.newSeries()
            .withName("Series #2")
            .bindItems(getSkinnable().recordsProperty())
            .withXValuePropertyStep("value0")
            .withYValuePropertyStep("value2")
            .withExtraValuePropertyStep("value3")
            .register();

        recordLineChartDataSource.setXYChart(recordLineChart);
        recordLineChartDataSource.newSeries()
            .withName("Series #1")
            .bindItems(getSkinnable().recordsProperty())
            .withXValuePropertyStep("name")
            .withYValuePropertyStep("value1")
            .register();
        recordLineChartDataSource.newSeries()
            .withName("Series #2")
            .bindItems(getSkinnable().recordsProperty())
            .withXValuePropertyStep("name")
            .withYValuePropertyStep("value2")
            .register();

        recordScatterChartDataSource.setXYChart(recordScatterChart);
        recordScatterChartDataSource.newSeries()
            .withName("Series #1")
            .bindItems(getSkinnable().recordsProperty())
            .withXValuePropertyStep("name")
            .withYValuePropertyStep("value1")
            .register();
        recordScatterChartDataSource.newSeries()
            .withName("Series #2")
            .bindItems(getSkinnable().recordsProperty())
            .withXValuePropertyStep("name")
            .withYValuePropertyStep("value2")
            .register();

        TableColumnGeneration.forTableView(recordTableView).itemTypeOf(XYRecord.class).enable();
        recordTableView.itemsProperty().bind(getSkinnable().recordsProperty());

        addRecordButton.setOnAction(e -> addRecord());

        removeRecordButton.setOnAction(e ->
            getSkinnable().getRecords().remove(recordTableView.getSelectionModel().getSelectedItem())
        );

        IntStream.rangeClosed(1, 5).forEach(index -> addRecord());
    }

    private void addRecord() {
        getSkinnable().getRecords().add(
            new XYRecord(
                String.format("Record-#%1$s", ++lastRecordId),
                random.nextInt(20), random.nextInt(10), random.nextInt(15), random.nextDouble() * 5
            )
        );
    }
}
