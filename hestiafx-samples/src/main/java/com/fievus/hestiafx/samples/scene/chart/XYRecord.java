/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples.scene.chart;

import com.fievus.hestiafx.scene.control.TableColumn;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class XYRecord {
    @TableColumn(order = 1, editable = false)
    public StringProperty nameProperty() { return name; }
    private final StringProperty name = new SimpleStringProperty(this, "name");
    public final String getName() { return name.get(); }
    public final void setName(String name) { this.name.set(name); }

    @TableColumn(order = 2, prefWidth = 70, editable = false)
    public IntegerProperty value0Property() { return value0; }
    private final IntegerProperty value0 = new SimpleIntegerProperty(this, "value0");
    public final int getValue0() { return value0.get(); }
    public final void setValue0(int value0) { this.value0.set(value0); }

    @TableColumn(order = 3, prefWidth = 70)
    public DoubleProperty value1Property() { return value1; }
    private final DoubleProperty value1 = new SimpleDoubleProperty(this, "value1");
    public final double getValue1() { return value1.get(); }
    public final void setValue1(double value1) { this.value1.set(value1); }

    @TableColumn(order = 4, prefWidth = 70)
    public DoubleProperty value2Property() { return value2; }
    private final DoubleProperty value2 = new SimpleDoubleProperty(this, "value2");
    public final double getValue2() { return value2.get(); }
    public final void setValue2(double value2) { this.value2.set(value2); }

    @TableColumn(order = 5, prefWidth = 70)
    public DoubleProperty value3Property() { return value3; }
    private final DoubleProperty value3 = new SimpleDoubleProperty(this, "value3");
    public final double getValue3() { return value3.get(); }
    public final void setValue3(double value3) { this.value3.set(value3); }

    public XYRecord(String name, int value0, double value1, double value2, double value3) {
        setName(name);
        setValue0(value0);
        setValue1(value1);
        setValue2(value2);
        setValue3(value3);
    }
}
