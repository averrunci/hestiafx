/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples.scene.control;

import com.fievus.hestiafx.scene.control.HierarchicalItem;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.shape.SVGPath;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;

public class FileItem implements HierarchicalItem<FileItem> {
    @Override
    public BooleanProperty expandedProperty() { return expanded; }
    private final BooleanProperty expanded = new SimpleBooleanProperty(this, "expanded");
    public final boolean getExpanded() { return expanded.get(); }
    public final void setExpanded(boolean expanded) { this.expanded.set(expanded); }

    @Override
    public ObjectProperty<Node> graphicProperty() { return graphic; }
    private final ObjectProperty<Node> graphic = new SimpleObjectProperty<>(this, "graphic");
    public final Node getGraphic() { return graphic.get(); }
    public final void setGraphic(Node graphic) { this.graphic.set(graphic); }

    private final File file;
    private final ObservableList<FileItem> items = FXCollections.observableArrayList();
    private final SVGPath folderGraphic = new SVGPath();
    private final SVGPath folderOpenGraphic = new SVGPath();

    private boolean itemBuilt;

    public FileItem(File[] roots) {
        if (roots.length > 1) {
            this.file = null;
            Arrays.stream(roots).forEach(root -> items.add(new FileItem(root)));
            itemBuilt = true;
        } else if (roots.length == 1) {
            this.file = Objects.requireNonNull(roots[0]);
            initializeGraphic();
        } else {
            throw new IllegalArgumentException();
        }
    }

    public FileItem(File file) {
        this.file = Objects.requireNonNull(file);
        initializeGraphic();
    }

    @Override
    public ObservableList<FileItem> getItems() {
        if (itemBuilt) { return items; }

        buildItems(items);
        itemBuilt = true;
        return items;
    }

    @Override
    public boolean hasItem() {
        return file == null || file.isDirectory();
    }

    @Override
    public String toString() {
        return file == null ? "" : file.getName();
    }

    protected void buildItems(ObservableList<FileItem> items) {
        if (!hasItem()) { return; }

        File[] files = file.listFiles();
        if (files == null) { return; }

        Arrays.stream(files).forEach(file -> items.add(new FileItem(file)));
    }

    private void initializeGraphic() {
        folderGraphic.setContent("M7 2l2 2h7v11h-16v-13z");
        folderOpenGraphic.setContent("M13 15l3-8h-13l-3 8zM2 6l-2 9v-13h4.5l2 2h6.5v2z");

        if (hasItem()) {
            setGraphic(folderGraphic);
        } else {
            SVGPath fileGraphic = new SVGPath();
            fileGraphic.setContent("M14.341 3.579c-0.347-0.473-0.831-1.027-1.362-1.558s-1.085-1.015-1.558-1.362c-0.806-0.591-1.197-0.659-1.421-0.659h-7.75c-0.689 0-1.25 0.561-1.25 1.25v13.5c0 0.689 0.561 1.25 1.25 1.25h11.5c0.689 0 1.25-0.561 1.25-1.25v-9.75c0-0.224-0.068-0.615-0.659-1.421zM12.271 2.729c0.48 0.48 0.856 0.912 1.134 1.271h-2.406v-2.405c0.359 0.278 0.792 0.654 1.271 1.134zM14 14.75c0 0.136-0.114 0.25-0.25 0.25h-11.5c-0.135 0-0.25-0.114-0.25-0.25v-13.5c0-0.135 0.115-0.25 0.25-0.25 0 0 7.749-0 7.75 0v3.5c0 0.276 0.224 0.5 0.5 0.5h3.5v9.75z");
            setGraphic(fileGraphic);
        }
        expandedProperty().addListener((observable, oldValue, newValue) -> {
            setGraphic(newValue ? folderOpenGraphic : folderGraphic);
        });
    }
}
