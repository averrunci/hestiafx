/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples.scene.control;

import com.fievus.hestiafx.samples.HestiaFXSample;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;

@HestiaFXSample(title = "TreeItemsSource Sample")
public class TreeItemsSourceSampleView extends Control {
    public ReadOnlyObjectProperty<FileItem> rootFileItemProperty() { return rootFileItem.getReadOnlyProperty(); }
    private final ReadOnlyObjectWrapper<FileItem> rootFileItem = new ReadOnlyObjectWrapper<>(this, "rootFileItem");
    public final FileItem getRootFileItem() { return rootFileItem.get(); }
    protected final void setRootFileItem(FileItem rootFileItem) { this.rootFileItem.set(rootFileItem); }

    public TreeItemsSourceSampleView() {
        getStyleClass().add("tree-items-source-sample-view");
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new TreeItemsSourceSampleViewSkin(this);
    }
}
