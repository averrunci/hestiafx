/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples.scene.control;

import com.fievus.hestiafx.scene.control.FXMLSkin;
import com.fievus.hestiafx.scene.control.TreeItemsSource;
import javafx.fxml.FXML;
import javafx.scene.control.TreeView;

import java.io.File;

public class TreeItemsSourceSampleViewSkin extends FXMLSkin<TreeItemsSourceSampleView> {
    @FXML
    private TreeView<FileItem> fileTreeView;

    private final TreeItemsSource<FileItem> source = new TreeItemsSource<>();

    protected TreeItemsSourceSampleViewSkin(TreeItemsSourceSampleView control) {
        super(control);
    }

    @FXML
    protected void initialize() {
        source.setTreeView(fileTreeView);
        source.rootItemProperty().bind(getSkinnable().rootFileItemProperty());

        getSkinnable().setRootFileItem(new FileItem(File.listRoots()));
        getSkinnable().getRootFileItem().setExpanded(true);
    }
}
