/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples.scene.control.cell;

import com.fievus.hestiafx.scene.control.FXMLSkin;
import com.fievus.hestiafx.scene.control.cell.ColorPickerListCell;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.StringConverter;

public class ColorPickerListCellSampleViewSkin extends FXMLSkin<ColorPickerListCellSampleView> {
    @FXML
    private CheckBox editableCheckBox;
    @FXML
    private ListView<Color> listView1;
    @FXML
    private ListView<ColorBag> listView2;
    @FXML
    private ListView<ColorBag> listView3;
    @FXML
    private ListView<Color> listView4;
    @FXML
    private ListView<ColorBag> listView5;

    protected ColorPickerListCellSampleViewSkin(ColorPickerListCellSampleView control) {
        super(control);
    }

    @FXML
    protected void initialize() {
        initializeForListView1();
        initializeForListView2();
        initializeForListView3();
        initializeForListView4();
        initializeForListView5();

        listView1.editableProperty().bind(editableCheckBox.selectedProperty());
        listView2.editableProperty().bind(editableCheckBox.selectedProperty());
        listView3.editableProperty().bind(editableCheckBox.selectedProperty());
        listView4.editableProperty().bind(editableCheckBox.selectedProperty());
        listView5.editableProperty().bind(editableCheckBox.selectedProperty());
    }

    private void initializeForListView1() {
        listView1.setCellFactory(ColorPickerListCell.forListView());
        initializeListItemWithColor(listView1);
    }

    private void initializeForListView2() {
        listView2.setCellFactory(ColorPickerListCell.forListView(ColorBag::colorProperty));
        initializeListItemWithColorBag(listView2);
    }

    private void initializeForListView3() {
        listView3.setCellFactory(ColorPickerListCell.forListView(ColorBag::colorProperty, new StringConverter<ColorBag>() {
            @Override
            public String toString(ColorBag object) {
                return object.getName();
            }

            @Override
            public ColorBag fromString(String string) {
                return null;
            }
        }));
        initializeListItemWithColorBag(listView3);
    }

    private void initializeForListView4() {
        listView4.setCellFactory(ColorPickerListCell.forListView(color -> new Circle(10), Circle::setFill));
        initializeListItemWithColor(listView4);
    }

    private void initializeForListView5() {
        listView5.setCellFactory(ColorPickerListCell.forListView(
            bag -> new Circle(10), Circle::setFill,
            ColorBag::colorProperty, new StringConverter<ColorBag>() {
            @Override
            public String toString(ColorBag object) {
                return object.getName();
            }

            @Override
            public ColorBag fromString(String string) {
                return null;
            }
        }));
        initializeListItemWithColorBag(listView5);
    }

    private void initializeListItemWithColor(ListView<Color> listView) {
        listView.setItems(FXCollections.observableArrayList(
            Color.RED,
            Color.GREEN,
            Color.BLUE
        ));
    }

    private void initializeListItemWithColorBag(ListView<ColorBag> listView) {
        listView.setItems(FXCollections.observableArrayList(
            new ColorBag("Red", Color.RED),
            new ColorBag("Green", Color.GREEN),
            new ColorBag("Blue", Color.BLUE)
        ));
    }
}
