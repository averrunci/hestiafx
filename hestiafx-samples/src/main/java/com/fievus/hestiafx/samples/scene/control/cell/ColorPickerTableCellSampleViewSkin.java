/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples.scene.control.cell;

import com.fievus.hestiafx.scene.control.FXMLSkin;
import com.fievus.hestiafx.scene.control.cell.ColorPickerTableCell;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.StringConverter;

public class ColorPickerTableCellSampleViewSkin extends FXMLSkin<ColorPickerTableCellSampleView> {
    @FXML
    private CheckBox editableCheckBox;

    @FXML
    private TableView<ColorBag> tableView1;
    @FXML
    private TableColumn<ColorBag, String> nameTableColumn1;
    @FXML
    private TableColumn<ColorBag, Color> colorTableColumn1;

    @FXML
    private TableView<TestItem> tableView2;
    @FXML
    private TableColumn<TestItem, String> nameTableColumn2;
    @FXML
    private TableColumn<TestItem, ColorBag> colorTableColumn2;

    @FXML
    private TableView<TestItem> tableView3;
    @FXML
    private TableColumn<TestItem, String> nameTableColumn3;
    @FXML
    private TableColumn<TestItem, ColorBag> colorTableColumn3;

    @FXML
    private TableView<ColorBag> tableView4;
    @FXML
    private TableColumn<ColorBag, String> nameTableColumn4;
    @FXML
    private TableColumn<ColorBag, Color> colorTableColumn4;

    @FXML
    private TableView<TestItem> tableView5;
    @FXML
    private TableColumn<TestItem, String> nameTableColumn5;
    @FXML
    private TableColumn<TestItem, ColorBag> colorTableColumn5;

    protected ColorPickerTableCellSampleViewSkin(ColorPickerTableCellSampleView control) {
        super(control);
    }

    @FXML
    protected void initialize() {
        initializeForTableView1();
        initializeForTableView2();
        initializeForTableView3();
        initializeForTableView4();
        initializeForTableView5();

        tableView1.editableProperty().bind(editableCheckBox.selectedProperty());
        tableView2.editableProperty().bind(editableCheckBox.selectedProperty());
        tableView3.editableProperty().bind(editableCheckBox.selectedProperty());
        tableView4.editableProperty().bind(editableCheckBox.selectedProperty());
        tableView5.editableProperty().bind(editableCheckBox.selectedProperty());
    }

    private void initializeForTableView1() {
        nameTableColumn1.setCellValueFactory(new PropertyValueFactory<>("name"));
        colorTableColumn1.setCellValueFactory(new PropertyValueFactory<>("color"));
        colorTableColumn1.setCellFactory(ColorPickerTableCell.forTableColumn());
        initializeTableItemWithColorBag(tableView1);
    }

    private void initializeForTableView2() {
        nameTableColumn2.setCellValueFactory(new PropertyValueFactory<>("name"));
        colorTableColumn2.setCellValueFactory(new PropertyValueFactory<>("color"));
        colorTableColumn2.setCellFactory(ColorPickerTableCell.forTableColumn(ColorBag::colorProperty));
        initializeTableItemWithTestItem(tableView2);
    }

    private void initializeForTableView3() {
        nameTableColumn3.setCellValueFactory(new PropertyValueFactory<>("name"));
        colorTableColumn3.setCellValueFactory(new PropertyValueFactory<>("color"));
        colorTableColumn3.setCellFactory(ColorPickerTableCell.forTableColumn(ColorBag::colorProperty, new StringConverter<ColorBag>() {
            @Override
            public String toString(ColorBag object) {
                return object.getName();
            }

            @Override
            public ColorBag fromString(String string) {
                return null;
            }
        }));
        initializeTableItemWithTestItem(tableView3);
    }

    private void initializeForTableView4() {
        nameTableColumn4.setCellValueFactory(new PropertyValueFactory<>("name"));
        colorTableColumn4.setCellValueFactory(new PropertyValueFactory<>("color"));
        colorTableColumn4.setCellFactory(ColorPickerTableCell.forTableColumn(
            color -> new Circle(10), Circle::setFill
        ));
        initializeTableItemWithColorBag(tableView4);
    }

    private void initializeForTableView5() {
        nameTableColumn5.setCellValueFactory(new PropertyValueFactory<>("name"));
        colorTableColumn5.setCellValueFactory(new PropertyValueFactory<>("color"));
        colorTableColumn5.setCellFactory(ColorPickerTableCell.forTableColumn(
            bag -> new Circle(10), Circle::setFill,
            ColorBag::colorProperty, new StringConverter<ColorBag>() {
                @Override
                public String toString(ColorBag object) {
                    return object.getName();
                }

                @Override
                public ColorBag fromString(String string) {
                    return null;
                }
            }
        ));
        initializeTableItemWithTestItem(tableView5);
    }

    private void initializeTableItemWithColorBag(TableView<ColorBag> tableView) {
        tableView.setItems(FXCollections.observableArrayList(
            new ColorBag("Red", Color.RED),
            new ColorBag("Green", Color.GREEN),
            new ColorBag("Blue", Color.BLUE)
        ));
    }

    private void initializeTableItemWithTestItem(TableView<TestItem> tableView) {
        tableView.setItems(FXCollections.observableArrayList(
            new TestItem("Red Item", new ColorBag("Red", Color.RED)),
            new TestItem("Green Item", new ColorBag("Green", Color.GREEN)),
            new TestItem("Blue Item", new ColorBag("Blue", Color.BLUE))
        ));
    }
}
