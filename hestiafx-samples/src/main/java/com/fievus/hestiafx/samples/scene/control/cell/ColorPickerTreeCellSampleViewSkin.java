/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples.scene.control.cell;

import com.fievus.hestiafx.scene.control.FXMLSkin;
import com.fievus.hestiafx.scene.control.cell.ColorPickerTreeCell;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.StringConverter;

public class ColorPickerTreeCellSampleViewSkin extends FXMLSkin<ColorPickerTreeCellSampleView> {
    @FXML
    private CheckBox editableCheckBox;
    @FXML
    private TreeView<Color> treeView1;
    @FXML
    private TreeView<ColorBag> treeView2;
    @FXML
    private TreeView<ColorBag> treeView3;
    @FXML
    private TreeView<Color> treeView4;
    @FXML
    private TreeView<ColorBag> treeView5;


    protected ColorPickerTreeCellSampleViewSkin(ColorPickerTreeCellSampleView control) {
        super(control);
    }

    @FXML
    protected void initialize() {
        initializeForTreeView1();
        initializeForTreeView2();
        initializeForTreeView3();
        initializeForTreeView4();
        initializeForTreeView5();

        treeView1.editableProperty().bind(editableCheckBox.selectedProperty());
        treeView2.editableProperty().bind(editableCheckBox.selectedProperty());
        treeView3.editableProperty().bind(editableCheckBox.selectedProperty());
        treeView4.editableProperty().bind(editableCheckBox.selectedProperty());
        treeView5.editableProperty().bind(editableCheckBox.selectedProperty());
    }

    private void initializeForTreeView1() {
        treeView1.setCellFactory(ColorPickerTreeCell.forTreeView());
        initializeTreeItemWithColor(treeView1);
    }

    private void initializeForTreeView2() {
        treeView2.setCellFactory(ColorPickerTreeCell.forTreeView(ColorBag::colorProperty));
        initializeTreeItemWithColorBag(treeView2);
    }

    private void initializeForTreeView3() {
        treeView3.setCellFactory(ColorPickerTreeCell.forTreeView(ColorBag::colorProperty, new StringConverter<ColorBag>() {
            @Override
            public String toString(ColorBag object) {
                return object.getName();
            }

            @Override
            public ColorBag fromString(String string) {
                return null;
            }
        }));
        initializeTreeItemWithColorBag(treeView3);
    }

    private void initializeForTreeView4() {
        treeView4.setCellFactory(ColorPickerTreeCell.forTreeView(color -> new Circle(10), Circle::setFill));
        initializeTreeItemWithColor(treeView4);
    }

    private void initializeForTreeView5() {
        treeView5.setCellFactory(ColorPickerTreeCell.forTreeView(
            color -> new Circle(10), Circle::setFill,
            ColorBag::colorProperty, new StringConverter<ColorBag>() {
            @Override
            public String toString(ColorBag object) {
                return object.getName();
            }

            @Override
            public ColorBag fromString(String string) {
                return null;
            }
        }));
        initializeTreeItemWithColorBag(treeView5);
    }

    @SuppressWarnings("unchecked")
    private void initializeTreeItemWithColor(TreeView<Color> treeView) {
        treeView.setRoot(new TreeItem<>(Color.BLACK));
        treeView.getRoot().getChildren().addAll(
            new TreeItem<>(Color.RED),
            new TreeItem<>(Color.GREEN),
            new TreeItem<>(Color.BLUE)
        );
        treeView.getRoot().getChildren().get(0).getChildren().addAll(
            new TreeItem<>(Color.ORANGE),
            new TreeItem<>(Color.DARKRED),
            new TreeItem<>(Color.PINK)
        );
        treeView.getRoot().getChildren().get(1).getChildren().addAll(
            new TreeItem<>(Color.LIME),
            new TreeItem<>(Color.DARKGREEN),
            new TreeItem<>(Color.LIGHTGREEN)
        );
        treeView.getRoot().getChildren().get(2).getChildren().addAll(
            new TreeItem<>(Color.SKYBLUE),
            new TreeItem<>(Color.DARKBLUE),
            new TreeItem<>(Color.LIGHTBLUE)
        );
    }

    @SuppressWarnings("unchecked")
    private void initializeTreeItemWithColorBag(TreeView<ColorBag> treeView) {
        treeView.setRoot(new TreeItem<>(new ColorBag("Black", Color.BLACK)));
        treeView.getRoot().getChildren().addAll(
            new TreeItem<>(new ColorBag("Red", Color.RED)),
            new TreeItem<>(new ColorBag("Green", Color.GREEN)),
            new TreeItem<>(new ColorBag("Blue", Color.BLUE))
        );
        treeView.getRoot().getChildren().get(0).getChildren().addAll(
            new TreeItem<>(new ColorBag("Orange", Color.ORANGE)),
            new TreeItem<>(new ColorBag("DarkRed", Color.DARKRED)),
            new TreeItem<>(new ColorBag("Pink", Color.PINK))
        );
        treeView.getRoot().getChildren().get(1).getChildren().addAll(
            new TreeItem<>(new ColorBag("Lime", Color.LIME)),
            new TreeItem<>(new ColorBag("DarkGreen", Color.DARKGREEN)),
            new TreeItem<>(new ColorBag("LightGreen", Color.LIGHTGREEN))
        );
        treeView.getRoot().getChildren().get(2).getChildren().addAll(
            new TreeItem<>(new ColorBag("SkyBlue", Color.SKYBLUE)),
            new TreeItem<>(new ColorBag("DarkBlue", Color.DARKBLUE)),
            new TreeItem<>(new ColorBag("LightBlue", Color.LIGHTBLUE))
        );
    }
}
