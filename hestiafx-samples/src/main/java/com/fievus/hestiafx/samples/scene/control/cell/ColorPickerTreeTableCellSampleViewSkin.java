/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples.scene.control.cell;

import com.fievus.hestiafx.scene.control.FXMLSkin;
import com.fievus.hestiafx.scene.control.cell.ColorPickerTreeTableCell;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.StringConverter;

public class ColorPickerTreeTableCellSampleViewSkin extends FXMLSkin<ColorPickerTreeTableCellSampleView> {
    @FXML
    private CheckBox editableCheckBox;

    @FXML
    private TreeTableView<ColorBag> treeTableView1;
    @FXML
    private TreeTableColumn<ColorBag, String> nameTreeTableColumn1;
    @FXML
    private TreeTableColumn<ColorBag, Color> colorTreeTableColumn1;

    @FXML
    private TreeTableView<TestItem> treeTableView2;
    @FXML
    private TreeTableColumn<TestItem, String> nameTreeTableColumn2;
    @FXML
    private TreeTableColumn<TestItem, ColorBag> colorTreeTableColumn2;

    @FXML
    private TreeTableView<TestItem> treeTableView3;
    @FXML
    private TreeTableColumn<TestItem, String> nameTreeTableColumn3;
    @FXML
    private TreeTableColumn<TestItem, ColorBag> colorTreeTableColumn3;

    @FXML
    private TreeTableView<ColorBag> treeTableView4;
    @FXML
    private TreeTableColumn<ColorBag, String> nameTreeTableColumn4;
    @FXML
    private TreeTableColumn<ColorBag, Color> colorTreeTableColumn4;

    @FXML
    private TreeTableView<TestItem> treeTableView5;
    @FXML
    private TreeTableColumn<TestItem, String> nameTreeTableColumn5;
    @FXML
    private TreeTableColumn<TestItem, ColorBag> colorTreeTableColumn5;
    
    protected ColorPickerTreeTableCellSampleViewSkin(ColorPickerTreeTableCellSampleView control) {
        super(control);
    }

    @FXML
    protected void initialize() {
        initializeForTreeTableView1();
        initializeForTreeTableView2();
        initializeForTreeTableView3();
        initializeForTreeTableView4();
        initializeForTreeTableView5();

        treeTableView1.editableProperty().bindBidirectional(editableCheckBox.selectedProperty());
        treeTableView2.editableProperty().bindBidirectional(editableCheckBox.selectedProperty());
        treeTableView3.editableProperty().bindBidirectional(editableCheckBox.selectedProperty());
        treeTableView4.editableProperty().bindBidirectional(editableCheckBox.selectedProperty());
        treeTableView5.editableProperty().bindBidirectional(editableCheckBox.selectedProperty());
    }

    private void initializeForTreeTableView1() {
        nameTreeTableColumn1.setCellValueFactory(new TreeItemPropertyValueFactory<>("name"));
        colorTreeTableColumn1.setCellValueFactory(new TreeItemPropertyValueFactory<>("color"));
        colorTreeTableColumn1.setCellFactory(ColorPickerTreeTableCell.forTreeTableColumn());

        initializeTreeItemWithColorBag(treeTableView1);
    }

    private void initializeForTreeTableView2() {
        nameTreeTableColumn2.setCellValueFactory(new TreeItemPropertyValueFactory<>("name"));
        colorTreeTableColumn2.setCellValueFactory(new TreeItemPropertyValueFactory<>("color"));
        colorTreeTableColumn2.setCellFactory(ColorPickerTreeTableCell.forTreeTableColumn(ColorBag::colorProperty));

        initializeTreeItemWithTestItem(treeTableView2);
    }

    private void initializeForTreeTableView3() {
        nameTreeTableColumn3.setCellValueFactory(new TreeItemPropertyValueFactory<>("name"));
        colorTreeTableColumn3.setCellValueFactory(new TreeItemPropertyValueFactory<>("color"));
        colorTreeTableColumn3.setCellFactory(ColorPickerTreeTableCell.forTreeTableColumn(ColorBag::colorProperty, new StringConverter<ColorBag>() {
            @Override
            public String toString(ColorBag object) {
                return object.getName();
            }

            @Override
            public ColorBag fromString(String string) {
                return null;
            }
        }));

        initializeTreeItemWithTestItem(treeTableView3);
    }

    private void initializeForTreeTableView4() {
        nameTreeTableColumn4.setCellValueFactory(new TreeItemPropertyValueFactory<>("name"));
        colorTreeTableColumn4.setCellValueFactory(new TreeItemPropertyValueFactory<>("color"));
        colorTreeTableColumn4.setCellFactory(ColorPickerTreeTableCell.forTreeTableColumn(color -> new Circle(10), Circle::setFill));

        initializeTreeItemWithColorBag(treeTableView4);
    }

    private void initializeForTreeTableView5() {
        nameTreeTableColumn5.setCellValueFactory(new TreeItemPropertyValueFactory<>("name"));
        colorTreeTableColumn5.setCellValueFactory(new TreeItemPropertyValueFactory<>("color"));
        colorTreeTableColumn5.setCellFactory(ColorPickerTreeTableCell.forTreeTableColumn(
            color -> new Circle(10), Circle::setFill, ColorBag::colorProperty,
            new StringConverter<ColorBag>() {
                @Override
                public String toString(ColorBag object) {
                    return object.getName();
                }

                @Override
                public ColorBag fromString(String string) {
                    return null;
                }
            }
        ));

        initializeTreeItemWithTestItem(treeTableView5);
    }

    @SuppressWarnings("unchecked")
    private void initializeTreeItemWithColorBag(TreeTableView<ColorBag> treeView) {
        treeView.setRoot(new TreeItem<>(new ColorBag("Black", Color.BLACK)));
        treeView.getRoot().getChildren().addAll(
            new TreeItem<>(new ColorBag("Red", Color.RED)),
            new TreeItem<>(new ColorBag("Green", Color.GREEN)),
            new TreeItem<>(new ColorBag("Blue", Color.BLUE))
        );
        treeView.getRoot().getChildren().get(0).getChildren().addAll(
            new TreeItem<>(new ColorBag("Orange", Color.ORANGE)),
            new TreeItem<>(new ColorBag("DarkRed", Color.DARKRED)),
            new TreeItem<>(new ColorBag("Pink", Color.PINK))
        );
        treeView.getRoot().getChildren().get(1).getChildren().addAll(
            new TreeItem<>(new ColorBag("Lime", Color.LIME)),
            new TreeItem<>(new ColorBag("DarkGreen", Color.DARKGREEN)),
            new TreeItem<>(new ColorBag("LightGreen", Color.LIGHTGREEN))
        );
        treeView.getRoot().getChildren().get(2).getChildren().addAll(
            new TreeItem<>(new ColorBag("SkyBlue", Color.SKYBLUE)),
            new TreeItem<>(new ColorBag("DarkBlue", Color.DARKBLUE)),
            new TreeItem<>(new ColorBag("LightBlue", Color.LIGHTBLUE))
        );
    }

    @SuppressWarnings("unchecked")
    private void initializeTreeItemWithTestItem(TreeTableView<TestItem> treeView) {
        treeView.setRoot(new TreeItem<>(new TestItem("Black Item", new ColorBag("Black", Color.BLACK))));
        treeView.getRoot().getChildren().addAll(
            new TreeItem<>(new TestItem("Red Item", new ColorBag("Red", Color.RED))),
            new TreeItem<>(new TestItem("Green Item", new ColorBag("Green", Color.GREEN))),
            new TreeItem<>(new TestItem("Blue Item", new ColorBag("Blue", Color.BLUE)))
        );
        treeView.getRoot().getChildren().get(0).getChildren().addAll(
            new TreeItem<>(new TestItem("Orange Item", new ColorBag("Orange", Color.ORANGE))),
            new TreeItem<>(new TestItem("DarkRed Item", new ColorBag("DarkRed", Color.DARKRED))),
            new TreeItem<>(new TestItem("Pink Item", new ColorBag("Pink", Color.PINK)))
        );
        treeView.getRoot().getChildren().get(1).getChildren().addAll(
            new TreeItem<>(new TestItem("Lime Item", new ColorBag("Lime", Color.LIME))),
            new TreeItem<>(new TestItem("DarkGreen Item", new ColorBag("DarkGreen", Color.DARKGREEN))),
            new TreeItem<>(new TestItem("LightGreen Item", new ColorBag("LightGreen", Color.LIGHTGREEN)))
        );
        treeView.getRoot().getChildren().get(2).getChildren().addAll(
            new TreeItem<>(new TestItem("SkyBlue Item", new ColorBag("SkyBlue", Color.SKYBLUE))),
            new TreeItem<>(new TestItem("DarkBlue Item", new ColorBag("DarkBlue", Color.DARKBLUE))),
            new TreeItem<>(new TestItem("LightBlue Item", new ColorBag("LightBlue", Color.LIGHTBLUE)))
        );
    }
}
