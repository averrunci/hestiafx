/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples.scene.control.cell;

import com.fievus.hestiafx.scene.control.FXMLSkin;
import com.fievus.hestiafx.scene.control.cell.DatePickerListCell;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.util.converter.LocalDateStringConverter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DatePickerListCellSampleViewSkin extends FXMLSkin<DatePickerListCellSampleView> {
    @FXML
    private CheckBox editableCheckBox;
    @FXML
    private ListView<LocalDate> listView1;
    @FXML
    private ListView<LocalDateBag> listView2;
    @FXML
    private ListView<LocalDateBag> listView3;
    @FXML
    private ListView<LocalDate> listView4;
    @FXML
    private ListView<LocalDateBag> listView5;

    protected DatePickerListCellSampleViewSkin(DatePickerListCellSampleView control) {
        super(control);
    }

    @FXML
    protected void initialize() {
        initializeForListView1();
        initializeForListView2();
        initializeForListView3();
        initializeForListView4();
        initializeForListView5();

        listView1.editableProperty().bind(editableCheckBox.selectedProperty());
        listView2.editableProperty().bind(editableCheckBox.selectedProperty());
        listView3.editableProperty().bind(editableCheckBox.selectedProperty());
        listView4.editableProperty().bind(editableCheckBox.selectedProperty());
        listView5.editableProperty().bind(editableCheckBox.selectedProperty());
    }

    private void initializeForListView1() {
        listView1.setCellFactory(DatePickerListCell.forListView());
        initializeListItemWithLocalDate(listView1);
    }

    private void initializeForListView2() {
        listView2.setCellFactory(DatePickerListCell.forListView(LocalDateBag::dateProperty));
        initializeListItemWithLocalDateBag(listView2);
    }

    private void initializeForListView3() {
        listView3.setCellFactory(DatePickerListCell.forListView(LocalDateBag::dateProperty, new LocalDateStringConverter(DateTimeFormatter.ofPattern("yyyy-MM-dd"), null)));
        initializeListItemWithLocalDateBag(listView3);
    }

    private void initializeForListView4() {
        listView4.setCellFactory(DatePickerListCell.forListView(() -> {
            DatePicker datePicker = new DatePicker();
            datePicker.setShowWeekNumbers(true);
            return datePicker;
        }));
        initializeListItemWithLocalDate(listView4);
    }

    private void initializeForListView5() {
        LocalDateStringConverter converter = new LocalDateStringConverter(DateTimeFormatter.ofPattern("yyyy-MM-dd"), null);
        listView5.setCellFactory(
            DatePickerListCell.forListView(() -> {
                DatePicker datePicker = new DatePicker();
                datePicker.setShowWeekNumbers(true);
                datePicker.setConverter(converter);
                return datePicker;
            }, LocalDateBag::dateProperty, converter)
        );
        initializeListItemWithLocalDateBag(listView5);
    }

    private void initializeListItemWithLocalDate(ListView<LocalDate> listView) {
        listView.setItems(FXCollections.observableArrayList(
            LocalDate.of(2016, 1, 1),
            LocalDate.of(2016, 2, 3),
            LocalDate.of(2016, 3, 20)
        ));
    }

    private void initializeListItemWithLocalDateBag(ListView<LocalDateBag> listView) {
        listView.setItems(FXCollections.observableArrayList(
            new LocalDateBag("New Year Day", LocalDate.of(2016, 1, 1)),
            new LocalDateBag("Special Day", LocalDate.of(2016, 2, 3)),
            new LocalDateBag("Another Day", LocalDate.of(2016, 3, 20))
        ));
    }
}
