/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples.scene.control.cell;

import com.fievus.hestiafx.samples.HestiaFXSample;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;

@HestiaFXSample(title = "DatePickerTableCell Sample")
public class DatePickerTableCellSampleView extends Control {
    public DatePickerTableCellSampleView() {
        getStyleClass().add("date-picker-table-cell-sample-view");
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new DatePickerTableCellSampleViewSkin(this);
    }
}
