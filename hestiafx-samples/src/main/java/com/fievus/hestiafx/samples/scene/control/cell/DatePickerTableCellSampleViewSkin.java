/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples.scene.control.cell;

import com.fievus.hestiafx.scene.control.FXMLSkin;
import com.fievus.hestiafx.scene.control.cell.DatePickerTableCell;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.converter.LocalDateStringConverter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DatePickerTableCellSampleViewSkin extends FXMLSkin<DatePickerTableCellSampleView> {
    @FXML
    private CheckBox editableCheckBox;

    @FXML
    private TableView<LocalDateBag> tableView1;
    @FXML
    private TableColumn<LocalDateBag, String> nameTableColumn1;
    @FXML
    private TableColumn<LocalDateBag, LocalDate> dateTableColumn1;

    @FXML
    private TableView<TestItem> tableView2;
    @FXML
    private TableColumn<TestItem, String> nameTableColumn2;
    @FXML
    private TableColumn<TestItem, LocalDateBag> dateTableColumn2;

    @FXML
    private TableView<TestItem> tableView3;
    @FXML
    private TableColumn<TestItem, String> nameTableColumn3;
    @FXML
    private TableColumn<TestItem, LocalDateBag> dateTableColumn3;

    @FXML
    private TableView<LocalDateBag> tableView4;
    @FXML
    private TableColumn<LocalDateBag, String> nameTableColumn4;
    @FXML
    private TableColumn<LocalDateBag, LocalDate> dateTableColumn4;

    @FXML
    private TableView<TestItem> tableView5;
    @FXML
    private TableColumn<TestItem, String> nameTableColumn5;
    @FXML
    private TableColumn<TestItem, LocalDateBag> dateTableColumn5;

    protected DatePickerTableCellSampleViewSkin(DatePickerTableCellSampleView control) {
        super(control);
    }

    @FXML
    protected void initialize() {
        initializeForTableView1();
        initializeForTableView2();
        initializeForTableView3();
        initializeForTableView4();
        initializeForTableView5();

        tableView1.editableProperty().bind(editableCheckBox.selectedProperty());
        tableView2.editableProperty().bind(editableCheckBox.selectedProperty());
        tableView3.editableProperty().bind(editableCheckBox.selectedProperty());
        tableView4.editableProperty().bind(editableCheckBox.selectedProperty());
        tableView5.editableProperty().bind(editableCheckBox.selectedProperty());
    }

    private void initializeForTableView1() {
        nameTableColumn1.setCellValueFactory(new PropertyValueFactory<>("name"));
        dateTableColumn1.setCellValueFactory(new PropertyValueFactory<>("date"));
        dateTableColumn1.setCellFactory(DatePickerTableCell.forTableColumn());
        initializeTableItemWithLocalDateBag(tableView1);
    }

    private void initializeForTableView2() {
        nameTableColumn2.setCellValueFactory(new PropertyValueFactory<>("name"));
        dateTableColumn2.setCellValueFactory(new PropertyValueFactory<>("date"));
        dateTableColumn2.setCellFactory(DatePickerTableCell.forTableColumn(LocalDateBag::dateProperty));
        initializeTableItemWithTestItem(tableView2);
    }

    private void initializeForTableView3() {
        nameTableColumn3.setCellValueFactory(new PropertyValueFactory<>("name"));
        dateTableColumn3.setCellValueFactory(new PropertyValueFactory<>("date"));
        dateTableColumn3.setCellFactory(DatePickerTableCell.forTableColumn(LocalDateBag::dateProperty, new LocalDateStringConverter(DateTimeFormatter.ofPattern("yyyy-MM-dd"), null)));
        initializeTableItemWithTestItem(tableView3);
    }

    private void initializeForTableView4() {
        nameTableColumn4.setCellValueFactory(new PropertyValueFactory<>("name"));
        dateTableColumn4.setCellValueFactory(new PropertyValueFactory<>("date"));
        dateTableColumn4.setCellFactory(DatePickerTableCell.forTableColumn(() -> {
            DatePicker datePicker = new DatePicker();
            datePicker.setShowWeekNumbers(true);
            return datePicker;
        }));
        initializeTableItemWithLocalDateBag(tableView4);
    }

    private void initializeForTableView5() {
        LocalDateStringConverter converter = new LocalDateStringConverter(DateTimeFormatter.ofPattern("yyyy-MM-dd"), null);
        nameTableColumn5.setCellValueFactory(new PropertyValueFactory<>("name"));
        dateTableColumn5.setCellValueFactory(new PropertyValueFactory<>("date"));
        dateTableColumn5.setCellFactory(
            DatePickerTableCell.forTableColumn(() -> {
                DatePicker datePicker = new DatePicker();
                datePicker.setShowWeekNumbers(true);
                datePicker.setConverter(converter);
                return datePicker;
            }, LocalDateBag::dateProperty, converter)
        );
        initializeTableItemWithTestItem(tableView5);
    }

    private void initializeTableItemWithLocalDateBag(TableView<LocalDateBag> tableView) {
        tableView.setItems(FXCollections.observableArrayList(
            new LocalDateBag("New Year Day", LocalDate.of(2016, 1, 1)),
            new LocalDateBag("Special Day", LocalDate.of(2016, 2, 3)),
            new LocalDateBag("Another Day", LocalDate.of(2016, 3, 20))
        ));
    }

    private void initializeTableItemWithTestItem(TableView<TestItem> tableView) {
        tableView.setItems(FXCollections.observableArrayList(
            new TestItem("New Year Day Item", new LocalDateBag("New Year Day", LocalDate.of(2016, 1, 1))),
            new TestItem("Special Day Item", new LocalDateBag("Special Day", LocalDate.of(2016, 2, 3))),
            new TestItem("Another Day Item", new LocalDateBag("Another Day", LocalDate.of(2016, 3, 20)))
        ));
    }
}
