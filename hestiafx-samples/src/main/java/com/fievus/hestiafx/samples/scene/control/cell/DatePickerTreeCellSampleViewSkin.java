/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples.scene.control.cell;

import com.fievus.hestiafx.scene.control.FXMLSkin;
import com.fievus.hestiafx.scene.control.cell.DatePickerTreeCell;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.util.converter.LocalDateStringConverter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DatePickerTreeCellSampleViewSkin extends FXMLSkin<DatePickerTreeCellSampleView> {
    @FXML
    private CheckBox editableCheckBox;
    @FXML
    private TreeView<LocalDate> treeView1;
    @FXML
    private TreeView<LocalDateBag> treeView2;
    @FXML
    private TreeView<LocalDateBag> treeView3;
    @FXML
    private TreeView<LocalDate> treeView4;
    @FXML
    private TreeView<LocalDateBag> treeView5;
    
    protected DatePickerTreeCellSampleViewSkin(DatePickerTreeCellSampleView control) {
        super(control);
    }

    @FXML
    protected void initialize() {
        initializeForTreeView1();
        initializeForTreeView2();
        initializeForTreeView3();
        initializeForTreeView4();
        initializeForTreeView5();

        treeView1.editableProperty().bind(editableCheckBox.selectedProperty());
        treeView2.editableProperty().bind(editableCheckBox.selectedProperty());
        treeView3.editableProperty().bind(editableCheckBox.selectedProperty());
        treeView4.editableProperty().bind(editableCheckBox.selectedProperty());
        treeView5.editableProperty().bind(editableCheckBox.selectedProperty());
    }

    private void initializeForTreeView1() {
        treeView1.setCellFactory(DatePickerTreeCell.forTreeView());
        initializeTreeItemWithLocalDate(treeView1);
    }

    private void initializeForTreeView2() {
        treeView2.setCellFactory(DatePickerTreeCell.forTreeView(LocalDateBag::dateProperty));
        initializeTreeItemWithLocalDateBag(treeView2);
    }

    private void initializeForTreeView3() {
        treeView3.setCellFactory(DatePickerTreeCell.forTreeView(LocalDateBag::dateProperty, new LocalDateStringConverter(DateTimeFormatter.ofPattern("yyyy-MM-dd"), null)));
        initializeTreeItemWithLocalDateBag(treeView3);
    }

    private void initializeForTreeView4() {
        treeView4.setCellFactory(DatePickerTreeCell.forTreeView(() -> {
            DatePicker datePicker = new DatePicker();
            datePicker.setShowWeekNumbers(true);
            return datePicker;
        }));
        initializeTreeItemWithLocalDate(treeView4);
    }

    private void initializeForTreeView5() {
        LocalDateStringConverter converter = new LocalDateStringConverter(DateTimeFormatter.ofPattern("yyyy-MM-dd"), null);
        treeView5.setCellFactory(
            DatePickerTreeCell.forTreeView(() -> {
                DatePicker datePicker = new DatePicker();
                datePicker.setShowWeekNumbers(true);
                datePicker.setConverter(converter);
                return datePicker;
            }, LocalDateBag::dateProperty, converter)
        );
        initializeTreeItemWithLocalDateBag(treeView5);
    }

    @SuppressWarnings("unchecked")
    private void initializeTreeItemWithLocalDate(TreeView<LocalDate> treeView) {
        treeView.setRoot(new TreeItem<>(LocalDate.of(2016, 1, 1)));
        treeView.getRoot().getChildren().addAll(
            new TreeItem<>(LocalDate.of(2016, 2, 3)),
            new TreeItem<>(LocalDate.of(2016, 2, 14)),
            new TreeItem<>(LocalDate.of(2016, 2, 28))
        );
        treeView.getRoot().getChildren().get(0).getChildren().addAll(
            new TreeItem<>(LocalDate.of(2016, 3, 4)),
            new TreeItem<>(LocalDate.of(2016, 3, 15)),
            new TreeItem<>(LocalDate.of(2016, 3, 31))
        );
        treeView.getRoot().getChildren().get(1).getChildren().addAll(
            new TreeItem<>(LocalDate.of(2016, 4, 1)),
            new TreeItem<>(LocalDate.of(2016, 4, 16)),
            new TreeItem<>(LocalDate.of(2016, 4, 30))
        );
        treeView.getRoot().getChildren().get(2).getChildren().addAll(
            new TreeItem<>(LocalDate.of(2016, 5, 6)),
            new TreeItem<>(LocalDate.of(2016, 5, 13)),
            new TreeItem<>(LocalDate.of(2016, 5, 22))
        );
    }

    @SuppressWarnings("unchecked")
    private void initializeTreeItemWithLocalDateBag(TreeView<LocalDateBag> treeView) {
        treeView.setRoot(new TreeItem<>(new LocalDateBag("New Year Day", LocalDate.of(2016, 1, 1))));
        treeView.getRoot().getChildren().addAll(
            new TreeItem<>(new LocalDateBag("February 3", LocalDate.of(2016, 2, 3))),
            new TreeItem<>(new LocalDateBag("February 14", LocalDate.of(2016, 2, 14))),
            new TreeItem<>(new LocalDateBag("February 28", LocalDate.of(2016, 2, 28)))
        );
        treeView.getRoot().getChildren().get(0).getChildren().addAll(
            new TreeItem<>(new LocalDateBag("March 4", LocalDate.of(2016, 3, 4))),
            new TreeItem<>(new LocalDateBag("March 15", LocalDate.of(2016, 3, 15))),
            new TreeItem<>(new LocalDateBag("March 31", LocalDate.of(2016, 3, 31)))
        );
        treeView.getRoot().getChildren().get(1).getChildren().addAll(
            new TreeItem<>(new LocalDateBag("April 1", LocalDate.of(2016, 4, 1))),
            new TreeItem<>(new LocalDateBag("April 16", LocalDate.of(2016, 4, 16))),
            new TreeItem<>(new LocalDateBag("April 30", LocalDate.of(2016, 4, 30)))
        );
        treeView.getRoot().getChildren().get(2).getChildren().addAll(
            new TreeItem<>(new LocalDateBag("May 6", LocalDate.of(2016, 5, 6))),
            new TreeItem<>(new LocalDateBag("May 13", LocalDate.of(2016, 5, 13))),
            new TreeItem<>(new LocalDateBag("May 22", LocalDate.of(2016, 5, 22)))
        );
    }
}
