/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples.scene.control.cell;

import com.fievus.hestiafx.scene.control.FXMLSkin;
import com.fievus.hestiafx.scene.control.cell.DatePickerTreeTableCell;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.util.converter.LocalDateStringConverter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DatePickerTreeTableCellSampleViewSkin extends FXMLSkin<DatePickerTreeTableCellSampleView> {
    @FXML
    private CheckBox editableCheckBox;

    @FXML
    private TreeTableView<LocalDateBag> treeTableView1;
    @FXML
    private TreeTableColumn<LocalDateBag, String> nameTreeTableColumn1;
    @FXML
    private TreeTableColumn<LocalDateBag, LocalDate> dateTreeTableColumn1;

    @FXML
    private TreeTableView<TestItem> treeTableView2;
    @FXML
    private TreeTableColumn<TestItem, String> nameTreeTableColumn2;
    @FXML
    private TreeTableColumn<TestItem, LocalDateBag> dateTreeTableColumn2;

    @FXML
    private TreeTableView<TestItem> treeTableView3;
    @FXML
    private TreeTableColumn<TestItem, String> nameTreeTableColumn3;
    @FXML
    private TreeTableColumn<TestItem, LocalDateBag> dateTreeTableColumn3;

    @FXML
    private TreeTableView<LocalDateBag> treeTableView4;
    @FXML
    private TreeTableColumn<LocalDateBag, String> nameTreeTableColumn4;
    @FXML
    private TreeTableColumn<LocalDateBag, LocalDate> dateTreeTableColumn4;

    @FXML
    private TreeTableView<TestItem> treeTableView5;
    @FXML
    private TreeTableColumn<TestItem, String> nameTreeTableColumn5;
    @FXML
    private TreeTableColumn<TestItem, LocalDateBag> dateTreeTableColumn5;
    
    protected DatePickerTreeTableCellSampleViewSkin(DatePickerTreeTableCellSampleView control) {
        super(control);
    }

    @FXML
    protected void initialize() {
        initializeForTreeTableView1();
        initializeForTreeTableView2();
        initializeForTreeTableView3();
        initializeForTreeTableView4();
        initializeForTreeTableView5();

        treeTableView1.editableProperty().bindBidirectional(editableCheckBox.selectedProperty());
        treeTableView2.editableProperty().bindBidirectional(editableCheckBox.selectedProperty());
        treeTableView3.editableProperty().bindBidirectional(editableCheckBox.selectedProperty());
        treeTableView4.editableProperty().bindBidirectional(editableCheckBox.selectedProperty());
        treeTableView5.editableProperty().bindBidirectional(editableCheckBox.selectedProperty());
    }

    private void initializeForTreeTableView1() {
        nameTreeTableColumn1.setCellValueFactory(new TreeItemPropertyValueFactory<>("name"));
        dateTreeTableColumn1.setCellValueFactory(new TreeItemPropertyValueFactory<>("date"));
        dateTreeTableColumn1.setCellFactory(DatePickerTreeTableCell.forTreeTableColumn());

        initializeTreeItemWithLocalDateBag(treeTableView1);
    }

    private void initializeForTreeTableView2() {
        nameTreeTableColumn2.setCellValueFactory(new TreeItemPropertyValueFactory<>("name"));
        dateTreeTableColumn2.setCellValueFactory(new TreeItemPropertyValueFactory<>("date"));
        dateTreeTableColumn2.setCellFactory(DatePickerTreeTableCell.forTreeTableColumn(LocalDateBag::dateProperty));

        initializeTreeItemWithTestItem(treeTableView2);
    }

    private void initializeForTreeTableView3() {
        nameTreeTableColumn3.setCellValueFactory(new TreeItemPropertyValueFactory<>("name"));
        dateTreeTableColumn3.setCellValueFactory(new TreeItemPropertyValueFactory<>("date"));
        dateTreeTableColumn3.setCellFactory(DatePickerTreeTableCell.forTreeTableColumn(LocalDateBag::dateProperty, new LocalDateStringConverter(DateTimeFormatter.ofPattern("yyyy-MM-dd"), null)));

        initializeTreeItemWithTestItem(treeTableView3);
    }

    private void initializeForTreeTableView4() {
        nameTreeTableColumn4.setCellValueFactory(new TreeItemPropertyValueFactory<>("name"));
        dateTreeTableColumn4.setCellValueFactory(new TreeItemPropertyValueFactory<>("date"));
        dateTreeTableColumn4.setCellFactory(DatePickerTreeTableCell.forTreeTableColumn(() -> {
            DatePicker datePicker = new DatePicker();
            datePicker.setShowWeekNumbers(true);
            return datePicker;
        }));

        initializeTreeItemWithLocalDateBag(treeTableView4);
    }

    private void initializeForTreeTableView5() {
        LocalDateStringConverter converter = new LocalDateStringConverter(DateTimeFormatter.ofPattern("yyyy-MM-dd"), null);
        nameTreeTableColumn5.setCellValueFactory(new TreeItemPropertyValueFactory<>("name"));
        dateTreeTableColumn5.setCellValueFactory(new TreeItemPropertyValueFactory<>("date"));
        dateTreeTableColumn5.setCellFactory(
            DatePickerTreeTableCell.forTreeTableColumn(() -> {
                DatePicker datePicker = new DatePicker();
                datePicker.setShowWeekNumbers(true);
                datePicker.setConverter(converter);
                return datePicker;
            }, LocalDateBag::dateProperty, converter)
        );

        initializeTreeItemWithTestItem(treeTableView5);
    }

    @SuppressWarnings("unchecked")
    private void initializeTreeItemWithLocalDateBag(TreeTableView<LocalDateBag> treeView) {
        treeView.setRoot(new TreeItem<>(new LocalDateBag("New Year Day", LocalDate.of(2016, 1, 1))));
        treeView.getRoot().getChildren().addAll(
            new TreeItem<>(new LocalDateBag("February 3", LocalDate.of(2016, 2, 3))),
            new TreeItem<>(new LocalDateBag("February 14", LocalDate.of(2016, 2, 14))),
            new TreeItem<>(new LocalDateBag("February 28", LocalDate.of(2016, 2, 28)))
        );
        treeView.getRoot().getChildren().get(0).getChildren().addAll(
            new TreeItem<>(new LocalDateBag("March 4", LocalDate.of(2016, 3, 4))),
            new TreeItem<>(new LocalDateBag("March 15", LocalDate.of(2016, 3, 15))),
            new TreeItem<>(new LocalDateBag("March 31", LocalDate.of(2016, 3, 31)))
        );
        treeView.getRoot().getChildren().get(1).getChildren().addAll(
            new TreeItem<>(new LocalDateBag("April 1", LocalDate.of(2016, 4, 1))),
            new TreeItem<>(new LocalDateBag("April 16", LocalDate.of(2016, 4, 16))),
            new TreeItem<>(new LocalDateBag("April 30", LocalDate.of(2016, 4, 30)))
        );
        treeView.getRoot().getChildren().get(2).getChildren().addAll(
            new TreeItem<>(new LocalDateBag("May 6", LocalDate.of(2016, 5, 6))),
            new TreeItem<>(new LocalDateBag("May 13", LocalDate.of(2016, 5, 13))),
            new TreeItem<>(new LocalDateBag("May 22", LocalDate.of(2016, 5, 22)))
        );
    }

    @SuppressWarnings("unchecked")
    private void initializeTreeItemWithTestItem(TreeTableView<TestItem> treeView) {
        treeView.setRoot(new TreeItem<>(new TestItem("New Year Day Item", new LocalDateBag("New Year Day", LocalDate.of(2016, 1, 1)))));
        treeView.getRoot().getChildren().addAll(
            new TreeItem<>(new TestItem("February 3 Item", new LocalDateBag("February 3", LocalDate.of(2016, 2, 3)))),
            new TreeItem<>(new TestItem("February 14 Item", new LocalDateBag("February 14", LocalDate.of(2016, 2, 14)))),
            new TreeItem<>(new TestItem("February 28 Item", new LocalDateBag("February 28", LocalDate.of(2016, 2, 28))))
        );
        treeView.getRoot().getChildren().get(0).getChildren().addAll(
            new TreeItem<>(new TestItem("March 4 Item", new LocalDateBag("March 4", LocalDate.of(2016, 3, 4)))),
            new TreeItem<>(new TestItem("March 15 Item", new LocalDateBag("March 15", LocalDate.of(2016, 3, 15)))),
            new TreeItem<>(new TestItem("March 31 Item", new LocalDateBag("March 31", LocalDate.of(2016, 3, 31))))
        );
        treeView.getRoot().getChildren().get(1).getChildren().addAll(
            new TreeItem<>(new TestItem("April 1 Item", new LocalDateBag("April 1", LocalDate.of(2016, 4, 1)))),
            new TreeItem<>(new TestItem("April 16 Item", new LocalDateBag("April 16", LocalDate.of(2016, 4, 16)))),
            new TreeItem<>(new TestItem("April 30 Item", new LocalDateBag("April 30", LocalDate.of(2016, 4, 30))))
        );
        treeView.getRoot().getChildren().get(2).getChildren().addAll(
            new TreeItem<>(new TestItem("May 6 Item", new LocalDateBag("May 6", LocalDate.of(2016, 5, 6)))),
            new TreeItem<>(new TestItem("May 13 Item", new LocalDateBag("May 13", LocalDate.of(2016, 5, 13)))),
            new TreeItem<>(new TestItem("May 22 Item", new LocalDateBag("May 22", LocalDate.of(2016, 5, 22))))
        );
    }
}
