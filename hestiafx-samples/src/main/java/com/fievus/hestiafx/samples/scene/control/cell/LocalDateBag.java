/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples.scene.control.cell;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.time.LocalDate;

public class LocalDateBag {
    public StringProperty nameProperty() { return name; }
    private final StringProperty name = new SimpleStringProperty(this, "name");
    public final String getName() { return name.get(); }
    public final void setName(String name) { this.name.set(name); }

    public ObjectProperty<LocalDate> dateProperty() { return date; }
    private final ObjectProperty<LocalDate> date = new SimpleObjectProperty<>(this, "date");
    public final LocalDate getDate() { return date.get(); }
    public final void setDate(LocalDate date) { this.date.set(date); }

    public LocalDateBag() {}

    public LocalDateBag(String name, LocalDate date) {
        setName(name);
        setDate(date);
    }
}
