/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples.scene.control.cell;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class TestItem {
    public StringProperty nameProperty() { return name; }
    private final StringProperty name = new SimpleStringProperty(this, "name");
    public final String getName() { return name.get(); }
    public final void setName(String name) { this.name.set(name); }

    public ObjectProperty<ColorBag> colorProperty() { return color; }
    private final ObjectProperty<ColorBag> color = new SimpleObjectProperty<>(this, "color");
    public final ColorBag getColor() { return color.get(); }
    public final void setColor(ColorBag color) { this.color.set(color); }

    public ObjectProperty<LocalDateBag> dateProperty() { return date; }
    private final ObjectProperty<LocalDateBag> date = new SimpleObjectProperty<>(this, "date");
    public final LocalDateBag getDate() { return date.get(); }
    public final void setDate(LocalDateBag date) { this.date.set(date); }

    public TestItem() {}

    public TestItem(String name, ColorBag color) {
        setName(name);
        setColor(color);
    }

    public TestItem(String name, LocalDateBag date) {
        setName(name);
        setDate(date);
    }
}
