/**
 * Copyright (c) 2015 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples.scene.input;

import com.fievus.hestiafx.samples.HestiaFXSample;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;

@HestiaFXSample(title = "Draggable Sample")
public class DraggableSampleView extends Control {
    public DraggableSampleView() {
        getStyleClass().add("draggable-sample-view");
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new DraggableSampleViewSkin(this);
    }
}
