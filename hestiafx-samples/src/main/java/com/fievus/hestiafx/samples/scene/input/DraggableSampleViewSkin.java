/**
 * Copyright (c) 2015 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.samples.scene.input;

import com.fievus.hestiafx.scene.control.FXMLSkin;
import com.fievus.hestiafx.scene.input.Draggable;
import javafx.fxml.FXML;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;

class DraggableSampleViewSkin extends FXMLSkin<DraggableSampleView> {
    @FXML
    private Circle draggedCircle;
    @FXML
    private Circle draggedCircleInParent;
    @FXML
    private Circle draggedCircleInBoundingRegion;
    @FXML
    private Pane boundingRegion;

    protected DraggableSampleViewSkin(DraggableSampleView control) {
        super(control);
    }

    @FXML
    protected void initialize() {
        Draggable.to(draggedCircle).enable();
        Draggable.to(draggedCircleInParent).boundToParent().enable();
        Draggable.to(draggedCircleInBoundingRegion).boundTo(boundingRegion).enable();
    }
}
