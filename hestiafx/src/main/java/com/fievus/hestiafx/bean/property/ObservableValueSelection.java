/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.bean.property;

import javafx.beans.value.ObservableValue;

/**
 * Defines the method to select an ObservableValue in a root object specified a step.
 * @param <T> the type of the root object
 * @param <E> the type of the value wrapped in ObservableValue
 */
@FunctionalInterface
public interface ObservableValueSelection<T, E> {
    /**
     * Selects an ObservableValue that is contained by the specified root object and is represented
     * by the specified step.
     * @param root the root object.
     * @param step the step to a property
     * @return an ObservableValue that is selected by the specified root object and step or
     *         null if an ObservableValue is not selected.
     */
    ObservableValue<E> select(T root, String step);
}
