/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.bean.property;

/**
 * Unchecked exception thrown when a property for the specified step is not found.
 */
public class PropertyNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 7052448243995875186L;

    private static final String DEFAULT_MESSAGE_FORMAT = "The property for the step(%1$s) is not found.";
    private final String step;

    /**
     * Constructs a new instance of this class with the specified step.
     * @param step the step to represent a property.
     */
    public PropertyNotFoundException(String step) {
        this(step, String.format(DEFAULT_MESSAGE_FORMAT, step));
    }

    /**
     * Constructs a new instance of this class with the specified step and detail message.
     * @param step
     * @param message the detail message.
     */
    public PropertyNotFoundException(String step, String message) {
        super(message);

        this.step = step;
    }

    /**
     * Constructs a new instance of this class with the specified step, detail message, and cause.
     * @param step the step to represent a property.
     * @param message the detail message.
     * @param cause the cause.
     */
    public PropertyNotFoundException(String step, String message, Throwable cause) {
        super(message, cause);

        this.step = step;
    }

    /**
     * Constructs a new instance of this class with the specified step and cause.
     * @param step the step to represent a property.
     * @param cause the cause.
     */
    public PropertyNotFoundException(String step, Throwable cause) {
        this(step, String.format(DEFAULT_MESSAGE_FORMAT, step), cause);
    }

    /**
     * Gets the step.
     * @return the step to represent a property.
     */
    public String getStep() { return step; }
}
