/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.bean.property;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;

/**
 * A Selector selects a property using the specified step to a property.
 * A step represents a path to a property separated with ".".
 * The target property is as follows:
 * <ul>
 *     <li>The return type is ReadOnlyProperty whose name property is equal to the property name of the specified step.</li>
 *     <li>The return type is ReadOnlyProperty and the name of the method ends with Property.</li>
 *     <li>The method name is "get" + the property name of the specified step.</li>
 *     <li>The method name is "is" + the property name of the specified step.</li>
 *     <li>The method name is the property name of the specified step.</li>
 * </ul>
 */
public final class PropertySelector {
    private static final String STEP_SEPARATOR_REGEX = "\\.";
    private static final String PROPERTY_METHOD_NAME_FORMAT = "%1$sProperty";
    private static final String GETTER_METHOD_NAME_FORMAT = "get%1$s";
    private static final String BOOLEAN_GETTER_METHOD_NAME_FORMAT="is%1$s";

    private PropertySelector() {}

    /**
     * Provides a PropertyValueFactory to extract the value from a given TableView row item reflectively,
     * using the specified step to a property.
     * @param step the step to a property.
     * @param <S> the type of the class contained within the TableView.items list.
     * @param <T> the type of the class contained within the TableColumn cells.
     * @return a Callback that can be inserted into the cell value factory property of a TableColumn.
     */
    public static <S, T> Callback<TableColumn.CellDataFeatures<S, T>, ObservableValue<T>> forTableColumn(String step) {
        return cellDataFeatures -> PropertySelector.select(cellDataFeatures.getValue(), step);
    }

    /**
     * Selects a property that is contained by the specified root object and is represented by the specified step.
     * @param root the object that contains a property.
     * @param step the step to a property.
     * @param <T> the type of the selected property.
     * @return a property that is selected by the specified root object and step or null if a property is not selected.
     */
    public static <T> ReadOnlyProperty<T> select(Object root, String step) {
        Objects.requireNonNull(root);
        if (step == null || step.trim().isEmpty()) { return null; }

        Object target = root;
        if (root instanceof ObservableValue) {
            target = ((ObservableValue)root).getValue();
        }

        ReadOnlyProperty<T> property = null;
        for (String propertyName : step.split(STEP_SEPARATOR_REGEX)) {
            property = retrieveProperty(target, propertyName);
            if (property == null) {
                return new ReadOnlyObjectWrapper<T>(target, propertyName).getReadOnlyProperty();
            }
            target = property.getValue();
        }

        return property;
    }

    private static <T> ReadOnlyProperty<T> retrieveProperty(Object target, String propertyName) {
        if (target == null) { return null; }

        ReadOnlyProperty<T> property = retrievePropertyFromProperty(target, propertyName);
        return property == null ? retrievePropertyFromGetterOfProperty(target, propertyName) : property;
    }

    @SuppressWarnings("unchecked")
    private static <T> ReadOnlyProperty<T> retrievePropertyFromProperty(Object target, String propertyName) {
        for (Method method : target.getClass().getMethods()) {
            if (!ReadOnlyProperty.class.isAssignableFrom(method.getReturnType())) { continue; }

            try {
                method.setAccessible(true);
                ReadOnlyProperty<T> property = null;
                Object actualProperty = method.invoke(target);
                if (actualProperty != null) { property = (ReadOnlyProperty<T>)actualProperty; }
                if (validateProperty(property, propertyName, method)) { return property; }
            } catch (IllegalAccessException | InvocationTargetException e) {
                return null;
            }
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    private static <T> ReadOnlyProperty<T> retrievePropertyFromGetterOfProperty(Object target, String propertyName) {
        for (Method method : target.getClass().getMethods()) {
            if (!validateGetterMethodOfProperty(method, propertyName)) { continue; }

            try {
                method.setAccessible(true);
                return new ReadOnlyObjectWrapper<>((T)method.invoke(target));
            } catch (IllegalAccessException | InvocationTargetException | ClassCastException e) {
                return null;
            }
        }
        return null;
    }

    private static boolean validateProperty(ReadOnlyProperty<?> property, String propertyName, Method method) {
        return (property != null && property.getName().equals(propertyName)) ||
            method.getName().equals(String.format(PROPERTY_METHOD_NAME_FORMAT, propertyName));
    }

    private static boolean validateGetterMethodOfProperty(Method method, String propertyName) {
        String methodName = method.getName();
        String capitalizedPropertyName = capitalize(propertyName);
        return methodName.equals(String.format(GETTER_METHOD_NAME_FORMAT, capitalizedPropertyName)) ||
            methodName.equals(String.format(BOOLEAN_GETTER_METHOD_NAME_FORMAT, capitalizedPropertyName)) ||
            methodName.equals(propertyName);
    }

    private static String capitalize(String value) {
        if (value == null) { return null; }
        if (value.isEmpty()) { return value; }

        String firstCharacter = Character.toString(Character.toUpperCase(value.charAt(0)));
        return value.length() == 1 ? firstCharacter : firstCharacter + value.substring(1);
    }
}
