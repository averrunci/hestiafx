/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.event;

import javafx.event.Event;

import java.lang.invoke.MethodHandles;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Logger;

/**
 * Utility class that contains of static methods for performing basic operations for the event.
 */
public final class EventExtensions {
    private static final Logger LOGGER = Logger.getLogger(MethodHandles.lookup().lookupClass().getCanonicalName());

    private EventExtensions() {}

    /**
     * Gets the event source from the specified event.
     * @param event the event.
     * @param <T> the type of the event source.
     * @return an Optional of the event source object or Optional.empty() if the event source is null or
     *         the type of the event source is different from the specified type.
     */
    @SuppressWarnings("unchecked")
    public static <T> Optional<T> getEventSourceFrom(Event event) {
        Object eventSource = Objects.requireNonNull(event).getSource();
        if (eventSource == null) { return Optional.empty(); }

        try {
            return Optional.of((T)eventSource);
        } catch (ClassCastException e) {
            LOGGER.severe(String.format("The event source(%1$s) of %2$s is not valid.", eventSource, event));
            return Optional.empty();
        }
    }
}
