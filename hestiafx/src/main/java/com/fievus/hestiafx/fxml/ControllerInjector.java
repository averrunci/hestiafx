/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.fxml;

/**
 * Defines the method to get an instance of a controller of the specified type.
 */
@FunctionalInterface
public interface ControllerInjector {
    /**
     * Gets an instance of the specified type or an instance the dependencies of which is injected
     * in the specified instance of the controller.
     * @param controllerClass the type of the controller.
     * @param controller the instance of the controller.
     * @param <C> the type of the controller
     * @return an instance of the specified type of the controller.
     */
    <C> C getInstanceOf(Class<C> controllerClass, C controller);
}
