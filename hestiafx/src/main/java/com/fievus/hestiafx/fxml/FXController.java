/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.fxml;

import com.fievus.hestiafx.resources.XmlResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * An FXController loads an FXML file of the specified location or the default location of the specified controller
 * and returns a controller.
 * @param <C> the type of the controller.
 */
public final class FXController<C> {
    private static final List<FXMLLocationResolver> DEFAULT_FXML_LOCATION_RESOLVERS = new ArrayList<>();
    private static ResourceBundle defaultResources;
    private static ControllerInjector defaultInjector;

    private final URL location;

    private C controller;
    private ResourceBundle resources;

    static {
        DEFAULT_FXML_LOCATION_RESOLVERS.add(new ControllerNameSuffixFXMLLocationResolver("Controller"));
        DEFAULT_FXML_LOCATION_RESOLVERS.add(new ControllerNameSuffixFXMLLocationResolver("Skin"));
    }

    private FXController(URL location) {
        this.location = location;
    }

    private FXController(Builder<C> builder) {
        this.location = builder.location;
        this.controller = builder.controller;
    }

    /**
     * Adds the specified FXMLLocationResolver.
     * @param resolver the resolver of the location of the FXML file.
     */
    public static void addFXMLLocationResolver(FXMLLocationResolver resolver) {
        DEFAULT_FXML_LOCATION_RESOLVERS.add(resolver);
    }

    /**
     * Removes the specified FXMLLocationResolver.
     * @param resolver the resolver of the location of the FXML file.
     */
    public static void removeFXMLLocationResolver(FXMLLocationResolver resolver) {
        DEFAULT_FXML_LOCATION_RESOLVERS.remove(resolver);
    }

    /**
     * Gets the ResourceBundle that is set as the default.
     * @return the ResourceBundle.
     */
    public static ResourceBundle getDefaultResource() {
        return defaultResources;
    }

    /**
     * Sets the ResourceBundle as the default.
     * @param resources the ResourceBundle.
     */
    public static void setDefaultResource(ResourceBundle resources) {
        defaultResources = resources;
    }

    /**
     * Sets the ResourceBundle as the default with the specified resource base name.
     * @param resourceBaseName the resource base name.
     */
    public static void setDefaultResourceOf(String resourceBaseName) {
        setDefaultResource(getResourceOf(resourceBaseName));
    }

    /**
     * Gets the ControllerInjector that is set as the default.
     * @return the ControllerInjector.
     */
    public static ControllerInjector getDefaultInjector() {
        return defaultInjector;
    }

    /**
     * Sets the ControllerInjector as the default.
     * @param injector the ControllerInjector.
     */
    public static void setDefaultInjector(ControllerInjector injector) {
        defaultInjector = injector;
    }

    /**
     * Constructs a new instance of this class with the specified location of the FXML file.
     * @param location the location of the FXML file.
     * @param <C> the type of the controller.
     * @return a new instance of this class.
     */
    public static <C> FXController<C> from(URL location) {
        return new FXController<>(Objects.requireNonNull(location));
    }

    /**
     * Constructs a new instance of Builder with the specified class of the controller.
     * @param controllerClass the class of the controller.
     * @param <C> the type of the controller.
     * @return a new instance of Builder.
     */
    public static <C> Builder<C> of(Class<C> controllerClass) {
        return new Builder<>(Objects.requireNonNull(controllerClass));
    }

    /**
     * Constructs a new instance of Builder with the specified instance of the controller.
     * @param controller the instance of the controller.
     * @param <C> the type of the controller.
     * @return a new instance of Builder.
     */
    public static <C> Builder<C> of(C controller) {
        return new Builder<>(Objects.requireNonNull(controller));
    }

    /**
     * Sets the ResourceBundle with the specified resource base name.
     * @param resourceBaseName the resource base name.
     * @return the instance of this class.
     */
    public FXController<C> withResourceOf(String resourceBaseName) {
        return with(getResourceOf(resourceBaseName));
    }

    /**
     * Sets the specified ResourceBundle
     * @param resources the ResourceBundle.
     * @return the instance of this class.
     */
    public FXController<C> with(ResourceBundle resources) {
        this.resources = resources;
        return this;
    }

    /**
     * Loads an FXML file from the specified location and returns an instance of a controller associated with it.
     * @return the instance of the controller associated with it.
     */
    public C load() {
        return loadWithFXMLLoader().getController();
    }

    /**
     * Loads contents of an FXML file from the specified location and returns its instance.
     * @param <T> the type of the class defined in the FXML file.
     * @return the instance of the class defined in the FXML file.
     */
    public <T> T loadContents() {
        return loadWithFXMLLoader().getRoot();
    }

    private FXMLLoader loadWithFXMLLoader() {
        FXMLLoader loader = new FXMLLoader(location, getResources().orElse(defaultResources));

        configure(loader);

        try {
            loader.load();
        } catch (IOException e) {
            throw new FXMLLoadException(location, e);
        }

        return loader;
    }

    private void configure(FXMLLoader loader) {
        Optional<C> controller = getController();
        controller.ifPresent(loader::setController);
        controller.filter(c -> c instanceof Node).ifPresent(loader::setRoot);
    }

    private Optional<C> getController() {
        return Optional.ofNullable(controller);
    }

    private Optional<ResourceBundle> getResources() {
        return Optional.ofNullable(resources);
    }

    private static ResourceBundle getResourceOf(String resourceBaseName) {
        //return ResourceBundle.getBundle(resourceBaseName, Locale.getDefault(), new XmlResourceBundle.Control());
        return ResourceBundle.getBundle(resourceBaseName);
    }

    /**
     * Constructs an FXController with the specified objects.
     * @param <C>
     */
    public static final class Builder<C> {
        private static final String FXML_FILE_NAME_FORMAT = "%1$s.fxml";

        private final Class<C> controllerClass;

        private URL location;
        private C controller;
        private ControllerInjector injector;

        private Builder(Class<C> controllerClass) {
            this.controllerClass = controllerClass;
        }

        @SuppressWarnings("unchecked")
        private Builder(C controller) {
            this.controller = controller;
            this.controllerClass = (Class<C>)controller.getClass();
        }

        /**
         * Sets the specified ControllerInjector to construct an FXController.
         * @param injector the ControllerInjector.
         * @return the instance of this class.
         */
        public Builder<C> using(ControllerInjector injector) {
            this.injector = injector;
            return this;
        }

        /**
         * Constructs a new instance of FXController with specified location of the FXML File.
         * @param location the location of the FXML file.
         * @return a new instance of FXController;
         */
        public FXController<C> from(URL location) {
            this.location = Objects.requireNonNull(location);
            return build();
        }

        /**
         * Constructs a new instance of FXController with the default location of the FXML file.
         * @return a new instance of FXController.
         */
        public FXController<C> fromDefaultLocation() {
            location = getDefaultFXMLLocationFromAnnotation();
            if (location == null) {
                location = getDefaultFXMLLocation();
            }
            if (location == null) {
                throw new IllegalFXMLDefaultLocationException(controllerClass.getName());
            }

            return build();
        }

        private FXController<C> build() {
            ensureController();
            return new FXController<>(this);
        }

        private Optional<ControllerInjector> injector() {
            return Optional.ofNullable(injector);
        }

        private void ensureController() {
            if (!injector().isPresent() && defaultInjector == null) { return; }
            ensureControllerWith(injector().orElse(defaultInjector));
        }

        private void ensureControllerWith(ControllerInjector injector) {
            controller = injector.getInstanceOf(controllerClass, controller);
        }

        private URL getDefaultFXMLLocationFromAnnotation() {
            FXMLLocation fxmlLocation = controllerClass.getAnnotation(FXMLLocation.class);
            return fxmlLocation == null ? null : controllerClass.getResource(fxmlLocation.value());
        }

        private URL getDefaultFXMLLocation() {
            return controllerClass.getResource(
                String.format(
                    FXML_FILE_NAME_FORMAT,
                    DEFAULT_FXML_LOCATION_RESOLVERS.stream()
                        .map(resolver -> resolver.resolveFXMLLocation(controllerClass))
                        .filter(fxmlLocation -> fxmlLocation != null)
                        .findFirst()
                        .orElse(controllerClass.getSimpleName())
                )
            );
        }
    }

    private static final class ControllerNameSuffixFXMLLocationResolver implements FXMLLocationResolver {
        private final String controllerNameSuffix;

        private ControllerNameSuffixFXMLLocationResolver(String controllerNameSuffix) {
            this.controllerNameSuffix = controllerNameSuffix;
        }

        @Override
        public String resolveFXMLLocation(Class<?> controllerClass) {
            String controllerName = Objects.requireNonNull(controllerClass).getSimpleName();
            if (!controllerName.endsWith(controllerNameSuffix)) { return null; }

            int controllerNameSuffixIndex = controllerName.lastIndexOf(controllerNameSuffix);
            if (controllerNameSuffixIndex < 0) {
                throw new IllegalStateException(
                    String.format("Controller name(%1$s) must end with %2$s", controllerName, controllerNameSuffix)
                );
            }

            return controllerName.substring(0, controllerNameSuffixIndex);
        }
    }
}
