/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.fxml;

import java.net.URL;

/**
 * Unchecked exception thrown when an error is encountered during a load operation of an FXML file.
 */
public class FXMLLoadException extends RuntimeException {
    private static final long serialVersionUID = -1182860066493174091L;

    private static final String DEFAULT_MESSAGE_FORMAT = "Cannot load from %1$s.";
    private final URL fxmlLocation;

    /**
     * Constructs a new instance of this class with the specified location of the FXML file.
     * @param location the location of the FXML file.
     */
    public FXMLLoadException(URL location) {
        this(location, String.format(DEFAULT_MESSAGE_FORMAT, location == null ? "null" : location.getPath()));
    }

    /**
     * Constructs a new instance of this class with the specified location of the FXML file and detail message.
     * @param location the location of the FXML file.
     * @param message the detail message.
     */
    public FXMLLoadException(URL location, String message) {
        super(message);
        this.fxmlLocation = location;
    }

    /**
     * Constructs a new instance of this class with the specified location of the FXML file and cause.
     * @param location the location of the FXML file.
     * @param cause the cause.
     */
    public FXMLLoadException(URL location, Throwable cause) {
        this(location, String.format(DEFAULT_MESSAGE_FORMAT, location == null ? "null" : location.getPath()), cause);
    }

    /**
     * Constructs a new instance of this class with the specified location of the FXML file, detail message, and cause.
     * @param location the location of the FXML file.
     * @param message the detail message.
     * @param cause the cause.
     */
    public FXMLLoadException(URL location, String message, Throwable cause) {
        super(message, cause);
        this.fxmlLocation = location;
    }

    /**
     * Constructs a new instance of this class with the specified location of the FXML file, detail message, cause,
     * suppression enabled or disabled, and writable stack trace enabled or disabled.
     * @param location the location of the FXML file.
     * @param message the detail message.
     * @param cause the cause.
     * @param enableSuppression whether or not suppression is enabled or disabled.
     * @param writableStackTrace whether or not the stack trace should be writable.
     */
    public FXMLLoadException(URL location, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.fxmlLocation = location;
    }

    /**
     * Gets the location of the FXML file.
     * @return the location of the FXML file.
     */
    public URL getFxmlLocation() {
        return fxmlLocation;
    }
}
