/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.fxml;

/**
 * Defines the method to resolve a default location of an fxml file.
 */
@FunctionalInterface
public interface FXMLLocationResolver {
    /**
     * Resolves the default location of the fxml file for the specified controller.
     * @param controllerClass the type of the controller.
     * @return the location of the fxml file.
     */
    String resolveFXMLLocation(Class<?> controllerClass);
}
