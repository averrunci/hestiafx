/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.fxml;

/**
 * Unchecked exception thrown when an default location of an FXML file is not found.
 */
public class IllegalFXMLDefaultLocationException extends RuntimeException {
    private static final long serialVersionUID = 1777918551192134548L;

    private static final String DEFAULT_MESSAGE_FORMAT = "Not found FXML file at the default location of %1$s.";
    private final String controllerClassName;

    /**
     * Constructs a new instance of this class with the specified class name of the controller.
     * @param controllerClassName the class name of the controller.
     */
    public IllegalFXMLDefaultLocationException(String controllerClassName) {
        this(controllerClassName, String.format(DEFAULT_MESSAGE_FORMAT, controllerClassName));
    }

    /**
     * Constructs a new instance of this class with the specified class name of the controller and detail message.
     * @param controllerClassName the class name of the controller.
     * @param message the detail message.
     */
    public IllegalFXMLDefaultLocationException(String controllerClassName, String message) {
        super(message);
        this.controllerClassName = controllerClassName;
    }

    /**
     * Constructs a new instance of this class with the specified class name of the controller and cause.
     * @param controllerClassName the class name of the controller.
     * @param cause the cause.
     */
    public IllegalFXMLDefaultLocationException(String controllerClassName, Throwable cause) {
        this(controllerClassName, String.format(DEFAULT_MESSAGE_FORMAT, controllerClassName), cause);
    }

    /**
     * Constructs a new instance of this class with the specified class name of the controller, detail message, and cause.
     * @param controllerClassName the class name of the controller.
     * @param message the detail message.
     * @param cause the cause.
     */
    public IllegalFXMLDefaultLocationException(String controllerClassName, String message, Throwable cause) {
        super(message, cause);
        this.controllerClassName = controllerClassName;
    }

    /**
     * Constructs a new instance of this class with the specified class name of the controller, detail message, cause,
     * suppression enabled or disabled, and writable stack trace enabled or disabled.
     * @param controllerClassName the class name of the controller.
     * @param message the detail message.
     * @param cause the cause.
     * @param enableSuppression whether or not suppression is enabled or disabled.
     * @param writableStackTrace whether or not the stack trace should be writable
     */
    public IllegalFXMLDefaultLocationException(String controllerClassName, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.controllerClassName = controllerClassName;
    }

    /**
     * Gets the class name of the controller.
     * @return the class name of the controller.
     */
    public String getControllerClassName() {
        return controllerClassName;
    }
}
