/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.resources;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * Represents XML-based resource bundles.
 */
public class XmlResourceBundle extends ResourceBundle {
    private final Properties properties = new Properties();

    /**
     * Constructs a new instance of this class with the specified input stream.
     * @param in the input stream from which to read the XML document.
     * @throws IOException if reading from the specified input stream results in an IOException.
     */
    protected XmlResourceBundle(InputStream in) throws IOException {
        properties.loadFromXML(Objects.requireNonNull(in));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Object handleGetObject(String key) {
        return properties.get(Objects.requireNonNull(key));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Enumeration<String> getKeys() {
        return Collections.enumeration(properties.stringPropertyNames());
    }

    /**
     * Represents a control of XML-based resource bundles.
     */
    public static class Control extends ResourceBundle.Control {
        private static final String XML = "xml";
        private static final List<String> FORMAT_XML = Collections.unmodifiableList(Collections.singletonList(XML));

        /**
         * {@inheritDoc}
         */
        @Override
        public List<String> getFormats(String baseName) {
            return FORMAT_XML;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public ResourceBundle newBundle(
            String baseName, Locale locale, String format, ClassLoader loader, boolean reload
        ) throws IllegalAccessException, InstantiationException, IOException {
            Objects.requireNonNull(baseName);
            Objects.requireNonNull(locale);
            Objects.requireNonNull(format);
            Objects.requireNonNull(loader);

            if (!format.equals(XML)) { return null; }

            String bundleName = toBundleName(baseName, locale);
            String resourceName = toResourceName(bundleName, format);
            InputStream stream = null;
            if (reload) {
                URL url = loader.getResource(resourceName);
                if (url != null) {
                    URLConnection connection = url.openConnection();
                    if (connection != null) {
                        connection.setUseCaches(false);
                        stream = connection.getInputStream();
                    }
                }
            } else {
                stream = loader.getResourceAsStream(resourceName);
            }

            if (stream == null) { return null; }

            try (BufferedInputStream in = new BufferedInputStream(stream)) {
                return new XmlResourceBundle(in);
            }
        }
    }
}
