/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene;

import javafx.scene.Node;

import java.lang.invoke.MethodHandles;
import java.util.Optional;
import java.util.logging.Logger;

/**
 * Utility class that contains of static methods for performing basic operations for the node.
 */
public final class NodeExtensions {
    private static final Logger LOGGER = Logger.getLogger(MethodHandles.lookup().lookupClass().getCanonicalName());

    private NodeExtensions() {}

    /**
     * Gets the property value of the specified node and property key.
     * @param node the node that contains the property.
     * @param key the key of the property.
     * @param <T> the type of the property.
     * @return the property value or Optional.empty() if the property is not found.
     */
    @SuppressWarnings("unchecked")
    public static <T> Optional<T> getProperty(Node node, String key) {
        if (!node.hasProperties()) { return Optional.empty(); }
        if (!node.getProperties().containsKey(key)) { return Optional.empty(); }

        Object property = node.getProperties().get(key);
        if (property == null) { return Optional.empty(); }

        try {
            return Optional.of((T)property);
        } catch (ClassCastException e) {
            LOGGER.severe(String.format("The property(key: %1$s, value: %2$s) of %3$s is not valid.", key, property, node));
            return Optional.empty();
        }
    }
}
