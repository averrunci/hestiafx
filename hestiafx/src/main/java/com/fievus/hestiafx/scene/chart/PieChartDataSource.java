/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.chart;

import com.fievus.hestiafx.bean.property.ObservableValueSelection;
import com.fievus.hestiafx.bean.property.PropertySelector;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.IntStream;

/**
 * Represents a data source of PieChart.
 * @param <T> the type of the item that is bound to data of PieChart.
 */
public class PieChartDataSource<T> {
    /**
     * The PieChart.
     * @return the PieChart.
     */
    public ObjectProperty<PieChart> pieChartProperty() { return pieChart; }
    private final ObjectProperty<PieChart> pieChart = new SimpleObjectProperty<PieChart>(this, "pieChart") {
        private PieChart currentPieChart;

        @Override
        protected void invalidated() {
            if (currentPieChart != null) { PieChartDataSource.this.unbindFrom(currentPieChart); }
            currentPieChart = get();
            if (currentPieChart != null) { PieChartDataSource.this.bindTo(currentPieChart); }
        }
    };

    /**
     * Gets the PieChart.
     * @return the PieChart.
     */
    public final PieChart getPieChart() { return pieChart.get(); }

    /**
     * Sets the PieChart.
     * @param pieChart the PieChart.
     */
    public final void setPieChart(PieChart pieChart) { this.pieChart.set(pieChart); }

    /**
     * The items that are bound to data of PieChart.
     * @return the items that.
     */
    public ObjectProperty<ObservableList<T>> itemsProperty() { return items; }
    private final ObjectProperty<ObservableList<T>> items = new SimpleObjectProperty<ObservableList<T>>(this, "items") {
        private ObservableList<T> currentItems;

        @Override
        protected void invalidated() {
            if (currentItems != null) { PieChartDataSource.this.unbind(currentItems); }
            currentItems = get();
            if (currentItems != null) { PieChartDataSource.this.bind(currentItems); }
        }
    };

    /**
     * Gets the items that are bound to data of PieChart.
     * @return the items that are bound to data of PieChart.
     */
    public final ObservableList<T> getItems() { return items.get(); }

    /**
     * Sets the items that are bound to data of PieChart.
     * @param items the items that are bound to data of PieChart.
     */
    public final void setItems(ObservableList<T> items) { this.items.set(items); }

    /**
     * The step to the name property, the value of which is the name of the pie slice.
     * @return the step to the name property.
     */
    public StringProperty namePropertyStepProperty() { return namePropertyStep; }
    private final StringProperty namePropertyStep = new SimpleStringProperty(this, "namePropertyStep") {
        @Override
        protected void invalidated() {
            rebind();
        }
    };

    /**
     * Gets the step to the name property, the value of which is the name of the pie slice.
     * @return the step to the name property.
     */
    public final String getNamePropertyStep() { return namePropertyStep.get(); }

    /**
     * Sets the step to the name property, the value of which is the name of the pie slice.
     * @param namePropertyStep the step to the name property.
     */
    public final void setNamePropertyStep(String namePropertyStep) { this.namePropertyStep.set(namePropertyStep); }

    /**
     * The step to the pieValue property, the value of which is the value of the pie slice.
     * @return the step to the pieValue property.
     */
    public StringProperty pieValuePropertyStepProperty() { return pieValuePropertyStep; }
    private final StringProperty pieValuePropertyStep = new SimpleStringProperty(this, "pieValuePropertyStep") {
        @Override
        protected void invalidated() {
            rebind();
        }
    };

    /**
     * Gets the step to the pieValue property, the value of which is the value of the pie slice.
     * @return the step to the pieValue property.
     */
    public final String getPieValuePropertyStep() { return pieValuePropertyStep.get(); }

    /**
     * Sets the step to the pieValue property, the value of which is the value of the pie slice.
     * @param pieValuePropertyStep the step to the pieValue property.
     */
    public final void setPieValuePropertyStep(String pieValuePropertyStep) { this.pieValuePropertyStep.set(pieValuePropertyStep); }

    /**
     * The ObservableValueSelection, which selects the name property.
     * @return the ObservableSelection, which selects the name property.
     */
    public ObjectProperty<ObservableValueSelection<T, String>> namePropertySelectionProperty() { return namePropertySelection; }
    private final ObjectProperty<ObservableValueSelection<T, String>> namePropertySelection = new SimpleObjectProperty<ObservableValueSelection<T, String>>(this, "namePropertySelection", PropertySelector::select) {
        @Override
        protected void invalidated() {
            rebind();
        }
    };

    /**
     * Gets the ObservableValueSelection, which selects the name property.
     * @return the ObservableValueSelection, which selects the name property.
     */
    public final ObservableValueSelection<T, String> getNamePropertySelection() { return namePropertySelection.get(); }

    /**
     * Sets the ObservableValueSelection, which selects the name property.
     * @param namePropertySelection
     */
    public final void setNamePropertySelection(ObservableValueSelection<T, String> namePropertySelection) { this.namePropertySelection.set(Objects.requireNonNull(namePropertySelection)); }

    /**
     * The ObservableValueSelection, which selects the pieValue property.
     * @return the ObservableValueSelection, which selects the pieValue property.
     */
    public ObjectProperty<ObservableValueSelection<T, Double>> pieValuePropertySelectionProperty() { return pieValuePropertySelection; }
    private final ObjectProperty<ObservableValueSelection<T, Double>> pieValuePropertySelection = new SimpleObjectProperty<ObservableValueSelection<T, Double>>(this, "pieValuePropertySelection", PropertySelector::select) {
        @Override
        protected void invalidated() {
            rebind();
        }
    };

    /**
     * Gets the ObservableValueSelection, which selects the pieValue property.
     * @return the ObservableValueSelection, which selects the pieValue property.
     */
    public final ObservableValueSelection<T, Double> getPieValuePropertySelection() { return pieValuePropertySelection.get(); }

    /**
     * Sets the ObservableValueSelection, which selects the pieValue property.
     * @param pieValuePropertySelection the ObservableValueSelection, which selects the pieValue property.
     */
    public final void setPieValuePropertySelection(ObservableValueSelection<T, Double> pieValuePropertySelection) { this.pieValuePropertySelection.set(Objects.requireNonNull(pieValuePropertySelection)); }

    private final ObjectProperty<ObservableList<PieChart.Data>> pieChartData = new SimpleObjectProperty<>(this, "pieChartData", FXCollections.observableArrayList());
    private final Map<T, PieChart.Data> itemPieChartDataMap = new HashMap<>();
    private final ListChangeListener<T> itemListChangeListener = change -> {
        while (change.next()) {
            if (change.wasPermutated()) {
                Map<Integer, PieChart.Data> permutationItems = new HashMap<>();
                for (int index = change.getTo() - 1; index >= change.getFrom(); --index) {
                    permutationItems.put(change.getPermutation(index), pieChartData.get().remove(index));
                }
                permutationItems.keySet().stream()
                    .sorted()
                    .forEach(index -> pieChartData.get().add(index, permutationItems.get(index)));
            } else {
                change.getRemoved().stream()
                    .filter(itemPieChartDataMap::containsKey)
                    .forEach(item -> pieChartData.get().remove(itemPieChartDataMap.remove(item)));
                IntStream.range(change.getFrom(), change.getTo())
                    .forEach(index ->
                        pieChartData.get().add(
                            index,
                            createPieChartDataFrom(change.getAddedSubList().get(index - change.getFrom()))
                        )
                    );
            }
        }
    };

    /**
     * Constructs a new instance of this class.
     */
    public PieChartDataSource() {}

    /**
     * Constructs a new instance of the class with the specified PieChart, step to the name property, and
     * step to the pieValue property.
     * @param pieChart the PieChart.
     * @param namePropertyStep the step to the name property, the value of which is the name of the pie slice.
     * @param pieValuePropertyStep the step to the pieValue property, the value of which is the value of the pie slice.
     */
    public PieChartDataSource(PieChart pieChart, String namePropertyStep, String pieValuePropertyStep) {
        setPieChart(pieChart);
        setNamePropertyStep(namePropertyStep);
        setPieValuePropertyStep(pieValuePropertyStep);
    }

    protected void bindTo(PieChart chart) {
        chart.dataProperty().bind(pieChartData);
    }

    protected void unbindFrom(PieChart chart) {
        chart.dataProperty().unbind();
    }

    protected void bind(ObservableList<T> items) {
        constructPieChartDataFrom(items);
    }

    protected void unbind(ObservableList<T> items) {
        pieChartData.get().clear();
        itemPieChartDataMap.clear();
        if (items != null) { items.removeListener(itemListChangeListener); }
    }

    protected void rebind() {
        unbind(getItems());
        bind(getItems());
    }

    protected void constructPieChartDataFrom(ObservableList<T> items) {
        if (items == null) { return; }

        items.forEach(item -> addPieChartData(createPieChartDataFrom(item)));
        items.addListener(itemListChangeListener);
    }

    protected void addPieChartData(PieChart.Data data) {
        pieChartData.get().add(data);
    }

    protected void setPieChartDataRelatedWithItem(PieChart.Data data, T item) {
        itemPieChartDataMap.put(item, data);
    }

    private PieChart.Data createPieChartDataFrom(T item) {
        PieChart.Data data = new PieChart.Data("", 0);

        ObservableValue<String> nameProperty = getNamePropertySelection().select(item, getNamePropertyStep());
        if (nameProperty != null) { data.nameProperty().bind(nameProperty); }

        ObservableValue<Double> pieValueProperty = getPieValuePropertySelection().select(item, getPieValuePropertyStep());
        if (pieValueProperty != null) { data.pieValueProperty().bind(pieValueProperty); }

        setPieChartDataRelatedWithItem(data, item);

        return data;
    }
}
