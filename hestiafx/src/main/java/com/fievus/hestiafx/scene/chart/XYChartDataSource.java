/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.chart;

import com.fievus.hestiafx.bean.property.ObservableValueSelection;
import com.fievus.hestiafx.bean.property.PropertySelector;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.chart.XYChart;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.IntStream;

/**
 * Represents a data source of XYChart.
 * @param <T> the type of the item that is bound to data of XYChart.
 * @param <X> the type of the value on the X axis.
 * @param <Y> the type of the value on the Y axis.
 */
public class XYChartDataSource<T, X, Y> {
    /**
     * The XYChart.
     * @return the XYChart.
     */
    public ObjectProperty<XYChart<X, Y>> xyChartProperty() { return xyChart; }
    private final ObjectProperty<XYChart<X, Y>> xyChart = new SimpleObjectProperty<XYChart<X, Y>>(this, "xyChart") {
        private XYChart<X, Y> currentXYChart;

        @Override
        protected void invalidated() {
            if (currentXYChart != null) { XYChartDataSource.this.unbindFrom(currentXYChart); }
            currentXYChart = get();
            if (currentXYChart != null) { XYChartDataSource.this.bindTo(currentXYChart); }
        }
    };

    /**
     * Gets the XYChart.
     * @return the XYChart.
     */
    public final XYChart<X, Y> getXYChart() { return xyChart.get(); }

    /**
     * Sets the XYChart.
     * @param xyChart the XYChart.
     */
    public final void setXYChart(XYChart<X, Y> xyChart) { this.xyChart.set(xyChart); }

    private final ObjectProperty<ObservableList<XYChart.Series<X, Y>>> xyChartData = new SimpleObjectProperty<>(this, "xyChartData", FXCollections.observableArrayList());
    private final Map<Context, XYChart.Series<X, Y>> contextXYChartDataMap = new HashMap<>();

    /**
     * Constructs a new instance of this class.
     */
    public XYChartDataSource() {}

    /**
     * Constructs a new instance of this class with the specified XYChart.
     * @param xyChart the XYChart.
     */
    public XYChartDataSource(XYChart<X, Y> xyChart) {
        setXYChart(xyChart);
    }

    /**
     * Creates an instance of a builder that builds a Context of a new series of XYChart.
     * @return the instance of the ContextBuilder.
     */
    public ContextBuilder newSeries() {
        return new ContextBuilder();
    }

    /**
     * Registers the specified Context of the series of XYChart.
     * @param context the Context of the series of XYChart.
     */
    public void register(Context<T, X, Y> context) {
        xyChartData.get().add(context.xyChartSeries);
        contextXYChartDataMap.put(context, context.xyChartSeries);
    }

    /**
     * Unregisters the specified Context of the series of XYChart.
     * @param context the Context of the series of XYChart.
     */
    public void unregister(Context<T, X, Y> context) {
        if (!contextXYChartDataMap.containsKey(context)) { return; }

        xyChartData.get().remove(contextXYChartDataMap.get(context));
    }

    /**
     * Clears all registered Context of the series of XYChart.
     */
    public void clear() {
        xyChartData.get().clear();
        contextXYChartDataMap.clear();
    }

    protected void bindTo(XYChart<X, Y> chart) {
        chart.dataProperty().bind(xyChartData);
    }

    protected void unbindFrom(XYChart<X, Y> chart) {
        chart.dataProperty().unbind();
    }

    /**
     * Represents a series of data items of XYChart.
     * @param <T> the type of the item that is bound to data of XYChart.
     * @param <X> the type of the value on the X axis.
     * @param <Y> the type of the value on the Y axis.
     */
    public static class Context<T, X, Y> {
        /**
         * The user displayable name for the series.
         * @return the user displayable name for the series.
         */
        public StringProperty nameProperty() { return name; }
        private final StringProperty name = new SimpleStringProperty(this, "name");

        /**
         * Gets the user displayable name for the series.
         * @return the user displayable name for the series.
         */
        public final String getName() { return name.get(); }

        /**
         * Sets the user displayable name for the series.
         * @param name the user displayable name for the series.
         */
        public final void setName(String name) { this.name.set(name); }

        /**
         * The node to display for the series.
         * @return the node to display for this series.
         */
        public ObjectProperty<Node> nodeProperty() { return node; }
        private final ObjectProperty<Node> node = new SimpleObjectProperty<>(this, "node");

        /**
         * Gets the node to display for the series.
         * @return the node to display for the series.
         */
        public final Node getNode() { return node.get(); }

        /**
         * Sets the node to display for the series.
         * @param node the node to display for the series.
         */
        public final void setNode(Node node) { this.node.set(node); }

        /**
         * The items that are bound to data of XYChart.
         * @return the items that are bound to data of XYChart.
         */
        public ObjectProperty<ObservableList<T>> itemsProperty() { return items; }
        private final ObjectProperty<ObservableList<T>> items = new SimpleObjectProperty<ObservableList<T>>(this, "items") {
            ObservableList<T> currentItems;

            @Override
            protected void invalidated() {
                if (currentItems != null) { Context.this.unbind(currentItems); }
                currentItems = get();
                if (currentItems != null) { Context.this.bind(currentItems); }
            }
        };

        /**
         * Gets the items that are bound to data of XYChart.
         * @return the items that are bound to data of XYChart.
         */
        public final ObservableList<T> getItems() { return items.get(); }

        /**
         * Sets the items that are bound to data of XYChart.
         * @param items the items that are bound to data of XYChart.
         */
        public final void setItems(ObservableList<T> items) { this.items.set(items); }

        /**
         * The step to the XValue property, the value of which is the data value on the X axis.
         * @return the step to the XValue property
         */
        public StringProperty xValuePropertyStepProperty() { return xValuePropertyStep; }
        private final StringProperty xValuePropertyStep = new SimpleStringProperty(this, "xValuePropertyStep") {
            @Override
            protected void invalidated() {
                rebind();
            }
        };

        /**
         * Gets the step to the XValue property, the value of which is the data value on the X axis.
         * @return the step to the XValue property.
         */
        public final String getXValuePropertyStep() { return xValuePropertyStep.get(); }

        /**
         * Sets the step to the XValue property, the value of which is the data value on the X axis.
         * @param xValuePropertyStep the step to the XValue property.
         */
        public final void setXValuePropertyStep(String xValuePropertyStep) { this.xValuePropertyStep.set(xValuePropertyStep); }

        /**
         * The step to the YValue property, the value of which is the data value on the Y axis.
         * @return the step to the YValue property.
         */
        public StringProperty yValuePropertyStepProperty() { return yValuePropertyStep; }
        private final StringProperty yValuePropertyStep = new SimpleStringProperty(this, "yValuePropertyStep") {
            @Override
            protected void invalidated() {
                rebind();
            }
        };

        /**
         * Gets the step to the YValue property, the value of which is the data value on the Y axis.
         * @return the step to the YValue property.
         */
        public final String getYValuePropertyStep() { return yValuePropertyStep.get(); }

        /**
         * Sets the step to the YValue property, the value of which is the data value on the Y axis.
         * @param yValuePropertyStep the step to the YValue property.
         */
        public final void setYValuePropertyStep(String yValuePropertyStep) { this.yValuePropertyStep.set(yValuePropertyStep); }

        /**
         * The step to the node property, the value of which is the node to display for the data item.
         * @return the step to the node property.
         */
        public StringProperty nodePropertyStepProperty() { return nodePropertyStep; }
        private final StringProperty nodePropertyStep = new SimpleStringProperty(this, "nodePropertyStep") {
            @Override
            protected void invalidated() {
                rebind();
            }
        };

        /**
         * Gets the step to the node property, the value of which is the node to display for the data item.
         * @return the step to the node property.
         */
        public final String getNodePropertyStep() { return nodePropertyStep.get(); }

        /**
         * Sets the step to the node property, the value of which is the node to display for the data item.
         * @param nodePropertyStep the step to the node property.
         */
        public final void setNodePropertyStep(String nodePropertyStep) { this.nodePropertyStep.set(nodePropertyStep); }

        /**
         * The step to the extraValue property, the value of which is the data value to be plotted in any way the chart needs.
         * @return the step to the extraValue property.
         */
        public StringProperty extraValuePropertyStepProperty() { return extraValuePropertyStep; }
        private final StringProperty extraValuePropertyStep = new SimpleStringProperty(this, "extraValuePropertyStep") {
            @Override
            protected void invalidated() {
                rebind();
            }
        };

        /**
         * Gets the step to the extraValue property, the value of which is the data value to be plotted in any way the chart needs.
         * @return the step to the extraValue property.
         */
        public final String getExtraValuePropertyStep() { return extraValuePropertyStep.get(); }

        /**
         * Sets the step to the extraValue property, the value of which is the data value to be plotted in any way the chart needs.
         * @param extraValuePropertyStep the step to the extraValue property.
         */
        public final void setExtraValuePropertyStep(String extraValuePropertyStep) { this.extraValuePropertyStep.set(extraValuePropertyStep); }

        /**
         * The ObservableValueSelection that selects the XValue property.
         * @return the ObservableSelection that selects the XValue property.
         */
        public ObjectProperty<ObservableValueSelection<T, X>> xValuePropertySelectionProperty() { return xValuePropertySelection; }
        private final ObjectProperty<ObservableValueSelection<T, X>> xValuePropertySelection = new SimpleObjectProperty<ObservableValueSelection<T, X>>(this, "xValuePropertySelection", PropertySelector::select) {
            @Override
            protected void invalidated() {
                rebind();
            }
        };

        /**
         * Gets the ObservableValueSelection that selects the XValue property.
         * @return the ObservableValueSelection that selects the XValue property.
         */
        public final ObservableValueSelection<T, X> getXValuePropertySelection() { return xValuePropertySelection.get(); }

        /**
         * Sets the ObservableValueSelection that selects the XValue property.
         * @param xValuePropertySelection the ObservableValueSelection that selects the XValue property.
         */
        public final void setXValuePropertySelection(ObservableValueSelection<T, X> xValuePropertySelection) { this.xValuePropertySelection.set(Objects.requireNonNull(xValuePropertySelection)); }

        /**
         * The ObservableValueSelection that selects the YValue property.
         * @return the ObservableValueSelection that selects the YValue property.
         */
        public ObjectProperty<ObservableValueSelection<T, Y>> yValuePropertySelectionProperty() { return yValuePropertySelection; }
        private final ObjectProperty<ObservableValueSelection<T, Y>> yValuePropertySelection = new SimpleObjectProperty<ObservableValueSelection<T, Y>>(this, "yValuePropertySelection", PropertySelector::select) {
            @Override
            protected void invalidated() {
                rebind();
            }
        };

        /**
         * Gets the ObservableValueSelection that selects the YValue property.
         * @return the ObservableValueSelection that selects the YValue property.
         */
        public final ObservableValueSelection<T, Y> getYValuePropertySelection() { return yValuePropertySelection.get(); }

        /**
         * Sets the ObservableValueSelection that selects the YValue property.
         * @param yValuePropertySelection the ObservableValueSelection that selects the YValue property.
         */
        public final void setYValuePropertySelection(ObservableValueSelection<T, Y> yValuePropertySelection) { this.yValuePropertySelection.set(Objects.requireNonNull(yValuePropertySelection)); }

        /**
         * The ObservableValueSelection that selects the node property.
         * @return the ObservableValueSelection that selects the node property.
         */
        public ObjectProperty<ObservableValueSelection<T, Node>> nodePropertySelectionProperty() { return nodePropertySelection; }
        private final ObjectProperty<ObservableValueSelection<T, Node>> nodePropertySelection = new SimpleObjectProperty<ObservableValueSelection<T, Node>>(this, "nodePropertySelection", PropertySelector::select) {
            @Override
            protected void invalidated() {
                rebind();
            }
        };

        /**
         * Gets the ObservableValueSelection that selects the node property.
         * @return the ObservableValueSelection that selects the node property.
         */
        public final ObservableValueSelection<T, Node> getNodePropertySelection() { return nodePropertySelection.get(); }

        /**
         * Sets the ObservableValueSelection that selects the node property.
         * @param nodePropertySelection the ObservableValueSelection that selects the node property.
         */
        public final void setNodePropertySelection(ObservableValueSelection<T, Node> nodePropertySelection) { this.nodePropertySelection.set(Objects.requireNonNull(nodePropertySelection)); }

        /**
         * The ObservableValueSelection that selects the extraValue property.
         * @return the ObservableValueSelection that selects the extraValue property.
         */
        public ObjectProperty<ObservableValueSelection<T, Object>> extraValuePropertySelectionProperty() { return extraValuePropertySelection; }
        private final ObjectProperty<ObservableValueSelection<T, Object>> extraValuePropertySelection = new SimpleObjectProperty<ObservableValueSelection<T, Object>>(this, "extraValuePropertySelection", PropertySelector::select) {
            @Override
            protected void invalidated() {
                rebind();
            }
        };

        /**
         * Gets the ObservableValueSelection that selects the extraValue property.
         * @return the ObservableValueSelection that selects the extraValue property.
         */
        public final ObservableValueSelection<T, Object> getExtraValuePropertySelection() { return extraValuePropertySelection.get(); }

        /**
         * Sets the ObservableValueSelection that selects the extraValue property.
         * @param extraValuePropertySelection the ObservableValueSelection that selects the extraValue property.
         */
        public final void setExtraValuePropertySelection(ObservableValueSelection<T, Object> extraValuePropertySelection) { this.extraValuePropertySelection.set(Objects.requireNonNull(extraValuePropertySelection)); }

        private final XYChart.Series<X, Y> xyChartSeries = new XYChart.Series<>();
        private final ReadOnlyObjectProperty<ObservableList<XYChart.Data<X, Y>>> xyChartData = new ReadOnlyObjectWrapper<>(this, "xyChartData", FXCollections.observableArrayList());
        private final Map<T, XYChart.Data<X, Y>> itemXYChartDataMap = new HashMap<>();
        private final ListChangeListener<T> itemListChangeListener = change -> {
            while (change.next()) {
                if (change.wasPermutated()) {
                    Map<Integer, XYChart.Data<X, Y>> permutationItems = new HashMap<>();
                    for (int index = change.getTo() - 1; index >= change.getFrom(); --index) {
                        permutationItems.put(change.getPermutation(index), xyChartData.get().remove(index));
                    }
                    permutationItems.keySet().stream()
                        .sorted()
                        .forEach(index -> xyChartData.get().add(index, permutationItems.get(index)));
                } else {
                    change.getRemoved().stream()
                        .filter(itemXYChartDataMap::containsKey)
                        .forEach(item -> xyChartData.get().remove(itemXYChartDataMap.remove(item)));
                    IntStream.range(change.getFrom(), change.getTo())
                        .forEach(index ->
                            xyChartData.get().add(
                                index,
                                createXYChartDataFrom(change.getAddedSubList().get(index - change.getFrom()))
                            )
                        );
                }
            }
        };
        private final Map<T, DataValueChangeListener<X>> xValueChangeListeners = new HashMap<>();
        private final Map<T, DataValueChangeListener<Y>> yValueChangeListeners = new HashMap<>();

        /**
         * Constructs a new instance of this class.
         */
        public Context() {
            this(null, null, null);
        }

        /**
         * Constructs a new instance of this class with the specified name, step to the XValue property, and
         * step to the YValue property
         * @param name the user displayable name for the series.
         * @param xValuePropertyStep the step to the XValue property.
         * @param yValuePropertyStep the step to the YValue property.
         */
        public Context(String name, String xValuePropertyStep, String yValuePropertyStep) {
            setName(name);
            setXValuePropertyStep(xValuePropertyStep);
            setYValuePropertyStep(yValuePropertyStep);

            xyChartSeries.nameProperty().bind(nameProperty());
            nodeProperty().bindBidirectional(xyChartSeries.nodeProperty());
            xyChartSeries.dataProperty().bind(xyChartData);
        }

        protected void bind(ObservableList<T> items) {
            constructXYChartDataFrom(items);
        }

        protected void unbind(ObservableList<T> items) {
            xyChartData.get().clear();
            itemXYChartDataMap.clear();
            if (items != null) {
                items.removeListener(itemListChangeListener);
                items.forEach(this::removeDataValueChangeListener);
            }
            xValueChangeListeners.clear();
            yValueChangeListeners.clear();
        }

        protected void rebind() {
            unbind(getItems());
            bind(getItems());
        }

        protected void constructXYChartDataFrom(ObservableList<T> items) {
            if (items == null) { return; }

            items.forEach(item -> addXYChartData(createXYChartDataFrom(item)));
            items.addListener(itemListChangeListener);
        }

        protected void addXYChartData(XYChart.Data<X, Y> data) {
            xyChartData.get().add(data);
        }

        protected XYChart.Data<X, Y> createXYChartDataFrom(T item) {
            XYChart.Data<X, Y> data = new XYChart.Data<>();

           addDataValueChangeListener(item, data);

            ObservableValue<Node> nodeProperty = getNodePropertySelection().select(item, getNodePropertyStep());
            if (nodeProperty != null) { data.nodeProperty().bind(nodeProperty); }

            ObservableValue<Object> extraValueProperty = getExtraValuePropertySelection().select(item, getExtraValuePropertyStep());
            if (extraValueProperty != null) { data.extraValueProperty().bind(extraValueProperty); }

            setXYChartDataRelatedWithItem(data, item);

            return data;
        }

        protected void setXYChartDataRelatedWithItem(XYChart.Data<X, Y> data, T item) {
            itemXYChartDataMap.put(item, data);
        }

        private void addDataValueChangeListener(T item, XYChart.Data<X, Y> data) {
            ObservableValue<X> xValueProperty = getXValuePropertySelection().select(item, getXValuePropertyStep());
            if (xValueProperty != null) {
                DataValueChangeListener<X> xValueChangeListener = new DataValueChangeListener<>(data::setXValue);
                xValueProperty.addListener(xValueChangeListener);
                xValueChangeListeners.put(item, xValueChangeListener);
                data.setXValue(xValueProperty.getValue());
            }

            ObservableValue<Y> yValueProperty = getYValuePropertySelection().select(item, getYValuePropertyStep());
            if (yValueProperty != null) {
                DataValueChangeListener<Y> yValueChangeListener = new DataValueChangeListener<>(data::setYValue);
                yValueProperty.addListener(yValueChangeListener);
                yValueChangeListeners.put(item, yValueChangeListener);
                data.setYValue(yValueProperty.getValue());
            }
        }

        private void removeDataValueChangeListener(T item) {
            if (xValueChangeListeners.containsKey(item)) {
                ObservableValue<X> xValueProperty = getXValuePropertySelection().select(item, getXValuePropertyStep());
                if (xValueProperty != null) {
                    xValueProperty.removeListener(xValueChangeListeners.get(item));
                }

                ObservableValue<Y> yValueProperty = getYValuePropertySelection().select(item, getYValuePropertyStep());
                if (yValueProperty != null) {
                    yValueProperty.removeListener(yValueChangeListeners.get(item));
                }
            }
        }

        private class DataValueChangeListener<T> implements ChangeListener<T> {
            private final Consumer<T> consumer;

            public DataValueChangeListener(Consumer<T> consumer) {
                this.consumer = Objects.requireNonNull(consumer);
            }

            @Override
            public void changed(ObservableValue<? extends T> observable, T oldValue, T newValue) {
                consumer.accept(newValue);
            }
        }
    }

    /**
     * Builds a Context of a series of XYChart.
     */
    public class ContextBuilder {
        private final Context<T, X, Y> context = new Context<>();

        protected ContextBuilder() {}

        /**
         * Sets the user displayable name for the series.
         * @param name the user displayable name for the series.
         * @return the instance of this class.
         */
        public ContextBuilder withName(String name) {
            context.setName(name);
            return this;
        }

        /**
         * Binds the specified user displayable name for the series to the name property.
         * @param name the use displayable name for the series.
         * @return the instance of this class.
         */
        public ContextBuilder bindName(ObservableValue<? extends String> name) {
            context.nameProperty().bind(Objects.requireNonNull(name));
            return this;
        }

        /**
         * Sets the specified items that are bound to data of XYChart.
         * @param items the items that are bound to data of XYChart.
         * @return the instance of this class.
         */
        public ContextBuilder withItems(ObservableList<T> items) {
            context.setItems(items);
            return this;
        }

        /**
         * Binds the specified items that are bound to data of XYChart.
         * @param items the items that are bound to data of XYChart.
         * @return the instance of this class.
         */
        public ContextBuilder bindItems(ObservableValue<? extends ObservableList<T>> items) {
            context.itemsProperty().bind(Objects.requireNonNull(items));
            return this;
        }

        /**
         * Sets the specified step to the XValue property.
         * @param xValuePropertyStep the step to the XValue property.
         * @return the instance of this class.
         */
        public ContextBuilder withXValuePropertyStep(String xValuePropertyStep) {
            context.setXValuePropertyStep(xValuePropertyStep);
            return this;
        }

        /**
         * Binds the specified step to the XValue property.
         * @param xValuePropertyStep the step to the XValue property.
         * @return the instance of this class.
         */
        public ContextBuilder bindXValuePropertyStep(ObservableValue<? extends String> xValuePropertyStep) {
            context.xValuePropertyStepProperty().bind(Objects.requireNonNull(xValuePropertyStep));
            return this;
        }

        /**
         * Sets the specified step to the YValue property.
         * @param yValuePropertyStep the step to the YValue property.
         * @return the instance of this class.
         */
        public ContextBuilder withYValuePropertyStep(String yValuePropertyStep) {
            context.setYValuePropertyStep(yValuePropertyStep);
            return this;
        }

        /**
         * Binds the specified step to the YValue property.
         * @param yValuePropertyStep the step to the YValue property.
         * @return the instance of this class.
         */
        public ContextBuilder bindYValuePropertyStep(ObservableValue<? extends String> yValuePropertyStep) {
            context.yValuePropertyStepProperty().bind(Objects.requireNonNull(yValuePropertyStep));
            return this;
        }

        /**
         * Sets the specified step to the node property.
         * @param nodePropertyStep the step to the node property.
         * @return the instance of this class.
         */
        public ContextBuilder withNodePropertyStep(String nodePropertyStep) {
            context.setNodePropertyStep(nodePropertyStep);
            return this;
        }

        /**
         * Binds the specified step to the node property.
         * @param nodePropertyStep the step to the node property.
         * @return the instance of this class.
         */
        public ContextBuilder bindNodePropertyStep(ObservableValue<? extends String> nodePropertyStep) {
            context.nodePropertyStepProperty().bind(Objects.requireNonNull(nodePropertyStep));
            return this;
        }

        /**
         * Sets the specified step to the extraValue property.
         * @param extraValuePropertyStep the step to the extraValue property.
         * @return the instance of this class.
         */
        public ContextBuilder withExtraValuePropertyStep(String extraValuePropertyStep) {
            context.setExtraValuePropertyStep(extraValuePropertyStep);
            return this;
        }

        /**
         * Binds the specified step to the extraValue property.
         * @param extraValuePropertyStep the step to the extraValue property.
         * @return the instance of this class.
         */
        public ContextBuilder bindExtraValuePropertyStep(ObservableValue<? extends String> extraValuePropertyStep) {
            context.extraValuePropertyStepProperty().bind(Objects.requireNonNull(extraValuePropertyStep));
            return this;
        }

        /**
         * Sets the specified ObservableValueSelection that selects the XValue property.
         * @param xValuePropertySelection the ObservableValueSelection that selects the XValue property.
         * @return the instance of this class.
         */
        public ContextBuilder withXValuePropertySelection(ObservableValueSelection<T, X> xValuePropertySelection) {
            context.setXValuePropertySelection(Objects.requireNonNull(xValuePropertySelection));
            return this;
        }

        /**
         * Binds the specified ObservableValueSelection that selects the XValue property
         * @param xValuePropertySelection the ObservableValueSelection that selects the XValue property.
         * @return the instance of this class.
         */
        public ContextBuilder bindXValuePropertySelection(ObservableValue<? extends ObservableValueSelection<T, X>> xValuePropertySelection) {
            context.xValuePropertySelectionProperty().bind(Objects.requireNonNull(xValuePropertySelection));
            return this;
        }

        /**
         * Sets the specified ObservableValueSelection that selects the YValue property.
         * @param yValuePropertySelection the ObservableValueSelection that selects the YValue property.
         * @return the instance of this class.
         */
        public ContextBuilder withYValuePropertySelection(ObservableValueSelection<T, Y> yValuePropertySelection) {
            context.setYValuePropertySelection(Objects.requireNonNull(yValuePropertySelection));
            return this;
        }

        /**
         * Binds the specified ObservableValueSelection that selects the YValue property.
         * @param yValuePropertySelection the ObservableValueSelection that selects the YValue property.
         * @return the instance of this class.
         */
        public ContextBuilder bindYValuePropertySelection(ObservableValue<? extends ObservableValueSelection<T, Y>> yValuePropertySelection) {
            context.yValuePropertySelectionProperty().bind(Objects.requireNonNull(yValuePropertySelection));
            return this;
        }

        /**
         * Sets the specified ObservableValueSelection that selects the node property.
         * @param nodePropertySelection the ObservableValueSelection that selects the node property.
         * @return the instance of this class.
         */
        public ContextBuilder withNodePropertySelection(ObservableValueSelection<T, Node> nodePropertySelection) {
            context.setNodePropertySelection(Objects.requireNonNull(nodePropertySelection));
            return this;
        }

        /**
         * Binds the specified ObservableValueSelection that selects the node property.
         * @param nodePropertySelection the ObservableValueSelection that selects the node property.
         * @return the instance of this class.
         */
        public ContextBuilder bindNodePropertySelection(ObservableValue<? extends ObservableValueSelection<T, Node>> nodePropertySelection) {
            context.nodePropertySelectionProperty().bind(Objects.requireNonNull(nodePropertySelection));
            return this;
        }

        /**
         * Sets the specified ObservableValueSelection that selects the extraValue property.
         * @param extraValuePropertySelection the ObservableValueSelection that selects the extraValue property.
         * @return the instance of this class.
         */
        public ContextBuilder withExtraValuePropertySelection(ObservableValueSelection<T, Object> extraValuePropertySelection) {
            context.setExtraValuePropertySelection(Objects.requireNonNull(extraValuePropertySelection));
            return this;
        }

        /**
         * Binds the specified ObservableValueSelection that selects the extraValue property.
         * @param extraValuePropertySelection the ObservableValueSelection that selects the extraValue property.
         * @return the instance of this class.
         */
        public ContextBuilder bindExtraValuePropertySelection(ObservableValue<? extends ObservableValueSelection<T, Object>> extraValuePropertySelection) {
            context.extraValuePropertySelectionProperty().bind(Objects.requireNonNull(extraValuePropertySelection));
            return this;
        }

        /**
         * Registers an context that is built by this class to the data source.
         * @return the context registered to the data source.
         */
        public Context<T, X, Y> register() {
            XYChartDataSource.this.register(context);
            return context;
        }
    }
}
