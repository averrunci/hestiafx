/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.control;

import com.fievus.hestiafx.fxml.FXController;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.SkinBase;

/**
 * Represents the user interface controls by defining a scene graph of nodes
 * using FXML file.
 * The FXML file location is defined using naming convention or FXMLLocation annotation.
 * The naming convention is as follows:
 * <ul>
 *     <li>The package is the same as the Skin class.</li>
 *     <li>The FXML file name is the Skin class name except the suffix "Skin".</li>
 * </ul>
 * @param <C> the type of the Skinnable.
 */
public class FXMLSkin<C extends Control> extends SkinBase<C> {
    private boolean childrenInitialized = false;

    protected FXMLSkin(C control) {
        super(control);
    }

    @Override
    protected void layoutChildren(double contentX, double contentY, double contentWidth, double contentHeight) {
        if (!childrenInitialized) {
            initializeChildren();
            childrenInitialized = true;
        }

        super.layoutChildren(contentX, contentY, contentWidth, contentHeight);
    }

    protected void initializeChildren() {
        getChildren().setAll(FXController.of(this).fromDefaultLocation().<Node>loadContents());
    }
}
