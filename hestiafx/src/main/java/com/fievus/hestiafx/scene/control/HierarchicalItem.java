/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.control;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.Node;

/**
 * Defines the methods to perform the operation of TreeItem.
 * @param <T> the type of the value property within TreeItem.
 */
public interface HierarchicalItem<T extends HierarchicalItem<T>> {
    /**
     * The expanded state of the TreeItem.
     * @return the expanded state of the TreeItem.
     */
    BooleanProperty expandedProperty();

    /**
     * The node that is generally shown to the left of the value property.
     * @return the node that is generally shown to the left of the value property.
     */
    ObjectProperty<Node> graphicProperty();

    /**
     * Gets the items this Item has. These items are used as the children of the TreeItem.
     * @return the sub items of this Item.
     */
    ObservableList<T> getItems();

    /**
     * Gets the value to indicate whether this item has a child item.
     * @return true if this item has a child item, otherwise false.
     */
    boolean hasItem();
}
