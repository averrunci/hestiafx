/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.control;

import javafx.scene.control.TableColumn.SortType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a method as to indicate an item of a TableColumn.
 */
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface TableColumn {
    /**
     * Get the id of the TableColumn.
     * @return the id of the TableColumn.
     */
    String id() default "";

    /**
     * Get the name of the header of the TableColumn.
     * @return the name of the header of the TableColumn.
     */
    String name() default "";

    /**
     * Gets the order to show the TableColumn.
     * @return the order to show the TableColumn.
     */
    int order() default Integer.MAX_VALUE;

    /**
     * Gets the value to indicate whether the TableColumn allows editing.
     * @return true if the TableColumn allows editing, otherwise false.
     */
    boolean editable() default true;

    /**
     * Gets the preferred width of the TableColumn.
     * @return the preferred width of the TableColumn.
     */
    double prefWidth() default 80;

    /**
     * Gets the maximum width the TableColumn is permitted to be resized to.
     * @return the maximum width the TableColumn is permitted to be resized to.
     */
    double maxWidth() default 5000;

    /**
     * Gets the minimum width the TableColumn is permitted to be resized to.
     * @return the minimum width the TableColumn is permitted to be resized to.
     */
    double minWidth() default 10;

    /**
     * Gets the visibility of the TableColumn.
     * @return true if the TableColumn is visible, otherwise false.
     */
    boolean visible() default true;

    /**
     * Gets the value to indicate whether the width of the TableColumn can change.
     * @return true if the width of the TableColumn can change, otherwise false.
     */
    boolean resizable() default true;

    /**
     * Gets the value to indicate whether the TableColumn can be sorted.
     * @return true if the TableColumn can be sorted, otherwise false.
     */
    boolean sortable() default true;

    /**
     * Gets the value to indicate whether the TableColumn should be sorted in an ascending or a descending order.
     * @return SortType.ASCENDING if the TableColumn is sorted in an ascending order,
     *         SortType.DESCENDING if the TableColumn is sorted in a descending order.
     */
    SortType sortType() default SortType.ASCENDING;

    /**
     * Gets the string representation of the CSS style associated with the TableColumn instance.
     * @return the string representation of the CSS style associated with the TableColumn instance.
     */
    String style() default "";

    /**
     * Gets the CSS style class associated with the TableColumn instance.
     * @return the CSS style class associated with the TableColumn instance.
     */
    String[] styleClass() default "";

    /**
     * Gets the CSS style class associated with the TableCell instance in the TableColumn instance.
     * @return the CSS style class associated with the TableCell instance in the TableColumn instance.
     */
    String[] cellStyleClass() default "";
}
