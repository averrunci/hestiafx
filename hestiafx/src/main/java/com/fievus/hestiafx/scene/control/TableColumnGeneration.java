/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.control;

import com.fievus.hestiafx.bean.property.PropertySelector;
import com.fievus.hestiafx.scene.control.cell.ColorPickerTableCell;
import com.fievus.hestiafx.scene.control.cell.DatePickerTableCell;
import javafx.beans.InvalidationListener;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ReadOnlyProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import javafx.util.converter.DateTimeStringConverter;
import javafx.util.converter.DefaultStringConverter;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.FloatStringConverter;
import javafx.util.converter.IntegerStringConverter;
import javafx.util.converter.LongStringConverter;

import java.lang.invoke.MethodHandles;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Generates a TableColumn using the specified item annotated with TableColumn annotation.
 * The generated TableCell is created with registered CellFactories.
 * The registered CellFactories are as follows:
 * <dl>
 *     <dt>boolean (Boolean)</dt>
 *     <dd>CheckBoxTableCell</dd>
 *
 *     <dt>int (Integer)</dt>
 *     <dd>TextFieldTableCell with IntegerStringConverter</dd>
 *
 *     <dt>long (Long)</dt>
 *     <dd>TextFieldTableCell with LongStringConverter</dd>
 *
 *     <dt>float (Float)</dt>
 *     <dd>TextFieldTableCell with FloatStringConverter</dd>
 *
 *     <dt>double (Double)</dt>
 *     <dd>TextFieldTableCell with DoubleStringConverter</dd>
 *
 *     <dt>String</dt>
 *     <dd>TextFieldTableCell with DefaultStringConverter</dd>
 *
 *     <dt>Date</dt>
 *     <dd>TextFieldTableCell with DateTimeStringConverter (a date format is yyyy/MM/dd HH:mm:ss)</dd>
 *
 *     <dt>Color</dt>
 *     <dd>ColorPickerTableCell</dd>
 *
 *     <dt>LocalDate</dt>
 *     <dd>DatePickerTableCell</dd>
 * </dl>
 */
public final class TableColumnGeneration {
    private static final Logger LOGGER = Logger.getLogger(MethodHandles.lookup().lookupClass().getCanonicalName());

    private static final String TABLE_COLUMN_GENERATION_ENABLE = "table-column-generation-enable";
    private static final String TABLE_COLUMN_GENERATION_ITEM_TYPE = "table-column-generation-item-type";
    private static final String TABLE_COLUMN_GENERATION_CELL_FACTORIES = "table-column-generation-cell-factories";
    private static final String TABLE_COLUMN_GENERATION_LIST_CHANGE_LISTENER = "table-column-generation-list-change-listener";
    private static final String TABLE_COLUMN_GENERATION_OLD_ITEMS = "table-column-generation-old-items";

    private static final String DEFAULT_DATE_FORMAT = "yyyy/MM/dd HH:mm:ss";
    private static final String GETTER_METHOD_NAME_PREFIX = "get";
    private static final String BOOLEAN_GETTER_METHOD_NAME_PREFIX ="is";
    private static final String PROPERTY_METHOD_NAME_SUFFIX = "Property";

    private static final List<PrimitiveTypePropertyEntry> PRIMITIVE_TYPE_PROPERTY_ENTRIES = new ArrayList<>();
    private static final TableCellFactories DEFAULT_CELL_FACTORIES = new TableCellFactories();
    private static final TableCellFactories CELL_FACTORIES = new TableCellFactories();
    private static final Callback<javafx.scene.control.TableColumn, TableCell> DEFAULT_CELL_FACTORY = column -> new DefaultTableCell();

    private static final InvalidationListener TABLE_VIEW_ITEMS_INVALIDATION_LISTENER = observable -> {
        ReadOnlyProperty itemsProperty = (ReadOnlyProperty)observable;
        TableView tableView = (TableView)itemsProperty.getBean();
        TableColumnGeneration.generateTableColumns(tableView, (ObservableList)tableView.getProperties().getOrDefault(TABLE_COLUMN_GENERATION_OLD_ITEMS, null), (ObservableList)itemsProperty.getValue());
        tableView.getProperties().put(TABLE_COLUMN_GENERATION_OLD_ITEMS, itemsProperty.getValue());
    };

    static {
        PRIMITIVE_TYPE_PROPERTY_ENTRIES.add(new PrimitiveTypePropertyEntry(StringProperty.class, String.class));
        PRIMITIVE_TYPE_PROPERTY_ENTRIES.add(new PrimitiveTypePropertyEntry(BooleanProperty.class, boolean.class));
        PRIMITIVE_TYPE_PROPERTY_ENTRIES.add(new PrimitiveTypePropertyEntry(IntegerProperty.class, int.class));
        PRIMITIVE_TYPE_PROPERTY_ENTRIES.add(new PrimitiveTypePropertyEntry(LongProperty.class, long.class));
        PRIMITIVE_TYPE_PROPERTY_ENTRIES.add(new PrimitiveTypePropertyEntry(FloatProperty.class, float.class));
        PRIMITIVE_TYPE_PROPERTY_ENTRIES.add(new PrimitiveTypePropertyEntry(DoubleProperty.class, double.class));

        DEFAULT_CELL_FACTORIES.register(boolean.class, column -> new CheckBoxTableCell<>());
        DEFAULT_CELL_FACTORIES.register(int.class, column -> new TextFieldTableCell<>(new IntegerStringConverter()));
        DEFAULT_CELL_FACTORIES.register(long.class, column -> new TextFieldTableCell<>(new LongStringConverter()));
        DEFAULT_CELL_FACTORIES.register(float.class, column -> new TextFieldTableCell<>(new FloatStringConverter()));
        DEFAULT_CELL_FACTORIES.register(double.class, column -> new TextFieldTableCell<>(new DoubleStringConverter()));
        DEFAULT_CELL_FACTORIES.register(String.class, column -> new TextFieldTableCell<>(new DefaultStringConverter()));
        DEFAULT_CELL_FACTORIES.register(Boolean.class, column -> new CheckBoxTableCell<>());
        DEFAULT_CELL_FACTORIES.register(Integer.class, column -> new TextFieldTableCell<>(new IntegerStringConverter()));
        DEFAULT_CELL_FACTORIES.register(Long.class, column -> new TextFieldTableCell<>(new LongStringConverter()));
        DEFAULT_CELL_FACTORIES.register(Float.class, column -> new TextFieldTableCell<>(new FloatStringConverter()));
        DEFAULT_CELL_FACTORIES.register(Double.class, column -> new TextFieldTableCell<>(new DoubleStringConverter()));
        DEFAULT_CELL_FACTORIES.register(Date.class, column -> new TextFieldTableCell<>(new DateTimeStringConverter(DEFAULT_DATE_FORMAT)));
        DEFAULT_CELL_FACTORIES.register(Color.class, column -> new ColorPickerTableCell<>());
        DEFAULT_CELL_FACTORIES.register(LocalDate.class, column -> new DatePickerTableCell<>());
    }

    private TableColumnGeneration() {}

    /**
     * Registers the specified CellFactory for the specified class.
     * @param cellValueClass the class of the cell value.
     * @param cellFactory the CellFactory to create the TableCell for the class of the cell value.
     */
    public static void registerCellFactory(Class<?> cellValueClass, Callback<javafx.scene.control.TableColumn, TableCell> cellFactory) {
        CELL_FACTORIES.register(Objects.requireNonNull(cellValueClass), Objects.requireNonNull(cellFactory));
    }

    /**
     * Unregisters the registered cellFactory for the specified class.
     * @param cellValueClass the class of the cell value associated with the registered cellFactory.
     */
    public static void unregisterCellFactory(Class<?> cellValueClass) {
        CELL_FACTORIES.unregister(Objects.requireNonNull(cellValueClass));
    }

    /**
     * Clears all registered cellFactories.
     */
    public static void clearCellFactory() {
        CELL_FACTORIES.clear();
    }

    /**
     * Constructs a new instance of Builder with the specified TableView that contains the generated TableColumn.
     * @param tableView the TableView that contains the generated TableColumn.
     * @param <S> the type of the the objects contained within the TableView items list.
     * @return the instance of Builder.
     */
    public static <S> Builder<S> forTableView(TableView<S> tableView) {
        return new Builder<>(Objects.requireNonNull(tableView));
    }

    /**
     * Gets the value to indicate whether the generation of the TableColumn of the specified TableView is enable.
     * @param tableView the TableView that contains the generated TableColumn.
     * @param <S> the type of the objects contained within the TableView items list.
     * @return true if the generation of the TableColumn of the specified TableView is enable, otherwise false.
     */
    public static <S> boolean isEnable(TableView<S> tableView) {
        return getPropertyValue(Objects.requireNonNull(tableView), TABLE_COLUMN_GENERATION_ENABLE, false);
    }

    /**
     * Sets the value to indicate whether the generation of the TableColumn of the specified TableView is enable.
     * @param tableView the TableView that contains the generated TableColumn.
     * @param enable true if the generation of the TableColumn of the specified TableView is enable, otherwise false.
     * @param <S> the type of the objects contained within the TableView items list.
     */
    public static <S> void setEnable(TableView<S> tableView, boolean enable) {
        setEnable(Objects.requireNonNull(tableView), null, enable);
    }

    /**
     * Sets the value to indicate whether the generation of the TableColumn of the specified TableView is enable
     * with the specified class of the TableView items list.
     * @param tableView the TableView that contains the generated TableColumn.
     * @param itemType the class of the TableView items list.
     * @param enable true if the generation of the TableColumn of the specified TableView is enable, otherwise false.
     * @param <S> the type of the objects contained within the TableView items list.
     */
    public static <S> void setEnable(TableView<S> tableView, Class<S> itemType, boolean enable) {
        setEnable(Objects.requireNonNull(tableView), itemType, TableCellFactories.emptyCellFactories(), enable);
    }

    private static <S> void setEnable(TableView<S> tableView, Class<S> itemType, TableCellFactories cellFactories, boolean enable) {
        tableView.getProperties().put(TABLE_COLUMN_GENERATION_ENABLE, enable);

        if (enable) {
            tableView.getProperties().put(TABLE_COLUMN_GENERATION_ITEM_TYPE, itemType);
            tableView.getProperties().put(TABLE_COLUMN_GENERATION_CELL_FACTORIES, cellFactories);
            tableView.getProperties().put(TABLE_COLUMN_GENERATION_OLD_ITEMS, tableView.getItems());
            tableView.itemsProperty().addListener(TABLE_VIEW_ITEMS_INVALIDATION_LISTENER);

            generateTableColumns(tableView, null, tableView.getItems());
        } else {
            tableView.getProperties().remove(TABLE_COLUMN_GENERATION_ITEM_TYPE);
            tableView.getProperties().remove(TABLE_COLUMN_GENERATION_CELL_FACTORIES);
            tableView.itemsProperty().removeListener(TABLE_VIEW_ITEMS_INVALIDATION_LISTENER);
            tableView.getProperties().remove(TABLE_COLUMN_GENERATION_OLD_ITEMS);
        }
    }

    @SuppressWarnings("unchecked")
    private static <T> T getPropertyValue(TableView tableView, String propertyName, T defaultValue) {
        if (!tableView.hasProperties()) { return defaultValue; }

        Object propertyValue = tableView.getProperties().get(propertyName);
        if (propertyValue == null) { return defaultValue; }

        try {
            return (T)propertyValue;
        } catch (ClassCastException e) {
            LOGGER.severe(
                String.format(
                    "The property(key: %1$s, value: %2$s) of %3$s is not valid.",
                    propertyName, propertyValue, tableView)
            );
            return defaultValue;
        }
    }

    private static void generateTableColumns(TableView tableView, ObservableList oldItems, ObservableList newItems) {
        removeListChangeListener(tableView, oldItems);
        tableView.getColumns().clear();
        generateTableColumn(tableView, newItems);
        addListChangeListener(tableView, newItems);
    }

    @SuppressWarnings("unchecked")
    private static void generateTableColumn(TableView tableView, ObservableList items) {
        Class<?> itemType = getItemType(tableView);
        if (itemType == null) {
            if (items == null || items.isEmpty()) { return; }
            itemType = items.get(0).getClass();
        }

        retrievePropertiesFrom(itemType)
            .forEach(context -> tableView.getColumns().add(context.createTableColumn(getCellFactories(tableView))));
    }

    @SuppressWarnings("unchecked")
    private static void addListChangeListener(TableView tableView, ObservableList items) {
        if (items == null || !items.isEmpty()) { return; }

        ListChangeListener listener = c -> {
            if (tableView.getColumns().isEmpty() && !c.getList().isEmpty()) {
                generateTableColumn(tableView, c.getList());
                removeListChangeListener(tableView, c.getList());
            }
        };
        items.addListener(listener);
        tableView.getProperties().put(TABLE_COLUMN_GENERATION_LIST_CHANGE_LISTENER, listener);
    }

    @SuppressWarnings("unchecked")
    private static void removeListChangeListener(TableView tableView, ObservableList items) {
        if (items == null) { return; }

        ListChangeListener listener = getPropertyValue(tableView, TABLE_COLUMN_GENERATION_LIST_CHANGE_LISTENER, null);
        if (listener != null) { items.removeListener(listener); }
    }

    private static Class<?> getItemType(TableView tableView) {
        return getPropertyValue(tableView, TABLE_COLUMN_GENERATION_ITEM_TYPE, null);
    }

    private static TableCellFactories getCellFactories(TableView tableView) {
        return getPropertyValue(tableView, TABLE_COLUMN_GENERATION_CELL_FACTORIES, TableCellFactories.emptyCellFactories());
    }

    private static List<PropertyContext> retrievePropertiesFrom(Class<?> itemType) {
        List<PropertyContext> contexts = new ArrayList<>();

        Arrays.stream(itemType.getMethods())
            .filter(method -> method.isAnnotationPresent(TableColumn.class))
            .forEach(method -> createPropertyContext(method).ifPresent(contexts::add));
        contexts.sort((context1, context2) -> context1.order() - context2.order());

        return contexts;
    }

    private static Optional<PropertyContext> createPropertyContext(Method method) {
        String propertyName = resolvePropertyName(method.getName());
        Type propertyType = method.getReturnType();
        if (ReadOnlyProperty.class.isAssignableFrom(method.getReturnType())) {
            propertyType = method.getGenericReturnType();
        }

        return Optional.of(
            new PropertyContext(propertyName, getPropertyValueClass(propertyType), method.getAnnotation(TableColumn.class))
        );
    }

    private static Class<?> getPropertyValueClass(Type propertyType) {
        if ((propertyType instanceof ParameterizedType)) {
            return getPropertyValueClass((ParameterizedType)propertyType);
        }

        return PRIMITIVE_TYPE_PROPERTY_ENTRIES.stream()
            .filter(entry -> entry.accepts((Class<?>)propertyType))
            .map(PrimitiveTypePropertyEntry::propertyValueClass)
            .findFirst()
            .orElse((Class<?>)propertyType);
    }

    private static Class<?> getPropertyValueClass(ParameterizedType propertyType) {
        if (propertyType.getActualTypeArguments().length == 0) { return null; }

        Type actualTypeArgument = propertyType.getActualTypeArguments()[0];
        try {
            return (Class<?>)actualTypeArgument;
        } catch (ClassCastException e) {
            LOGGER.severe(String.format("The actual type argument(%1$s) of %2$s is not valid", actualTypeArgument, propertyType));
            return null;
        }
    }

    private static String resolvePropertyName(String methodName) {
        int beginIndex = getPropertyNameBeginIndex(methodName, GETTER_METHOD_NAME_PREFIX) + getPropertyNameBeginIndex(methodName, BOOLEAN_GETTER_METHOD_NAME_PREFIX);
        int endIndex = getPropertyNameEndIndex(methodName, PROPERTY_METHOD_NAME_SUFFIX);

        String propertyName = methodName.substring(beginIndex, endIndex);
        String firstCharacter = Character.toString(Character.toLowerCase(propertyName.charAt(0)));
        return propertyName.length() == 1 ? firstCharacter : firstCharacter + propertyName.substring(1);
    }

    private static int getPropertyNameBeginIndex(String methodName, String prefix) {
        return methodName.startsWith(prefix) && Character.isUpperCase(methodName.charAt(prefix.length())) ? prefix.length() : 0;
    }

    private static int getPropertyNameEndIndex(String methodName, String suffix) {
        return methodName.endsWith(suffix) ? methodName.length() - suffix.length() : methodName.length();
    }

    /**
     * Builds properties of the TableColumnGeneration.
     * @param <S> the type of the objects contained within the TableView items list.
     */
    public static final class Builder<S> {
        private final TableView<S> tableView;
        private TableCellFactories cellFactories = new TableCellFactories();
        private Class<S> itemType;

        private Builder(TableView<S> tableView) {
            this.tableView = tableView;
        }

        /**
         * Sets the specified class of the TableView items list.
         * @param itemType the class of the TableView items list.
         * @return the instance of this class.
         */
        public Builder<S> itemTypeOf(Class<S> itemType) {
            this.itemType = itemType;
            return this;
        }

        /**
         * Sets the specified CellFactory for the specified class.
         * @param cellValueClass the class of the cell value.
         * @param cellFactory the CellFactory to create the TableCell for the classof the cell value.
         * @return the instance of this class.
         */
        public Builder<S> with(Class<?> cellValueClass, Callback<javafx.scene.control.TableColumn, TableCell> cellFactory) {
            cellFactories.register(Objects.requireNonNull(cellValueClass), Objects.requireNonNull(cellFactory));
            return this;
        }

        /**
         * Enables the generation of TableColumn.
         */
        public void enable() {
            setEnable(tableView, itemType, cellFactories, true);
        }
    }

    private static final class PrimitiveTypePropertyEntry {
        private final Class<?> propertyClass;
        private final Class<?> propertyValueClass;

        private PrimitiveTypePropertyEntry(Class<?> propertyClass, Class<?> propertyValueClass) {
            this.propertyClass = Objects.requireNonNull(propertyClass);
            this.propertyValueClass = Objects.requireNonNull(propertyValueClass);
        }

        private boolean accepts(Class<?> targetPropertyClass) {
            return propertyClass.isAssignableFrom(targetPropertyClass);
        }

        private Class<?> propertyValueClass() {
            return propertyValueClass;
        }
    }

    private static final class TableCellFactories {
        private static final TableCellFactories EMPTY_CELL_FACTORIES = new TableCellFactories();

        private final Map<Class<?>, Callback<javafx.scene.control.TableColumn, TableCell>> cellFactories = new HashMap<>();

        private TableCellFactories() {}

        private static TableCellFactories emptyCellFactories() {
            return EMPTY_CELL_FACTORIES;
        }

        private void register(Class<?> cellValueClass, Callback<javafx.scene.control.TableColumn, TableCell> cellFactory) {
            cellFactories.put(cellValueClass, cellFactory);
        }

        private void unregister(Class<?> cellValueClass) {
            cellFactories.remove(cellValueClass);
        }

        private void clear() {
            cellFactories.clear();
        }

        private Optional<Callback<javafx.scene.control.TableColumn, TableCell>> getCellFactory(PropertyContext context) {
            if (!cellFactories.containsKey(context.propertyValueClass())) { return Optional.empty(); }
            if (context.propertyValueClass() == null) { return Optional.empty(); }

            return Optional.of(context.createCellFactory(cellFactories.get(context.propertyValueClass())));
        }
    }

    private static final class DefaultTableCell extends TableCell {
        @SuppressWarnings("unchecked")
        @Override
        protected void updateItem(Object item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else if (item instanceof Node) {
                setText(null);
                setGraphic((Node) item);
            } else if (item instanceof ReadOnlyProperty) {
                setText(((ReadOnlyProperty)item).getValue().toString());
                setGraphic(null);
            } else {
                setText(item.toString());
                setGraphic(null);
            }
        }
    }

    private static final class PropertyContext {
        private final String propertyName;
        private final Class<?> propertyValueClass;
        private final TableColumn tableColumn;

        private PropertyContext(String propertyName, Class<?> propertyValueClass, TableColumn tableColumn) {
            this.propertyName = Objects.requireNonNull(propertyName);
            this.propertyValueClass = propertyValueClass;
            this.tableColumn = Objects.requireNonNull(tableColumn);
        }

        private Class<?> propertyValueClass() {
            return propertyValueClass;
        }

        private int order() {
            return tableColumn.order();
        }

        private Callback<javafx.scene.control.TableColumn, TableCell> createCellFactory(Callback<javafx.scene.control.TableColumn, TableCell> cellFactory) {
            return column -> applyStyleClass(cellFactory.call(column));
        }

        @SuppressWarnings("unchecked")
        private javafx.scene.control.TableColumn createTableColumn(TableCellFactories cellFactories) {
            javafx.scene.control.TableColumn column = new javafx.scene.control.TableColumn();

            applyTableColumnProperty(column);
            column.setCellValueFactory(PropertySelector.forTableColumn(propertyName));
            column.setCellFactory(getCellFactory(cellFactories));

            return column;
        }

        private Callback<javafx.scene.control.TableColumn, TableCell> getCellFactory(TableCellFactories cellFactories) {
            return cellFactories.getCellFactory(this)
                .orElseGet(() -> CELL_FACTORIES.getCellFactory(this)
                    .orElseGet(() -> DEFAULT_CELL_FACTORIES.getCellFactory(this)
                        .orElseGet(() -> createCellFactory(DEFAULT_CELL_FACTORY))
                    ));
        }

        private TableCell applyStyleClass(TableCell cell) {
            cell.getStyleClass().addAll(Arrays.stream(tableColumn.cellStyleClass()).filter(c -> !c.isEmpty()).collect(Collectors.toList()));
            return cell;
        }

        @SuppressWarnings("unchecked")
        private void applyTableColumnProperty(javafx.scene.control.TableColumn column) {
            column.setText(tableColumn.name().isEmpty() ? capitalize(propertyName) : tableColumn.name());
            column.setId(tableColumn.id().isEmpty() ? null : tableColumn.id());
            column.setEditable(tableColumn.editable());
            column.setPrefWidth(tableColumn.prefWidth());
            column.setMaxWidth(tableColumn.maxWidth());
            column.setMinWidth(tableColumn.minWidth());
            column.setVisible(tableColumn.visible());
            column.setResizable(tableColumn.resizable());
            column.setSortable(tableColumn.sortable());
            column.setSortType(tableColumn.sortType());
            column.setStyle(tableColumn.style());
            column.getStyleClass().addAll(Arrays.stream(tableColumn.styleClass()).filter(c -> !c.isEmpty()).toArray());
        }

        private String capitalize(String value) {
            if (value == null) { return null; }
            if (value.isEmpty()) { return value; }

            String firstCharacter = Character.toString(Character.toUpperCase(value.charAt(0)));
            return value.length() == 1 ? firstCharacter : firstCharacter + value.substring(1);
        }
    }
}
