/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.control;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.IntStream;

/**
 * Represents a source of TreeItem.
 * @param <T> the type of the item contained within the TreeItem value property for all tree items in the TreeView.
 */
public class TreeItemsSource<T extends HierarchicalItem<T>> {
    /**
     * The TreeView.
     * @return the TreeView.
     */
    public ObjectProperty<TreeView<T>> treeViewProperty() { return treeView; }
    private final ObjectProperty<TreeView<T>> treeView = new SimpleObjectProperty<TreeView<T>>(this, "treeView") {
        private TreeView<T> currentTreeView;

        @Override
        protected void invalidated() {
            if (currentTreeView != null) { TreeItemsSource.this.unbindFrom(currentTreeView); }
            currentTreeView = get();
            if (currentTreeView != null) { TreeItemsSource.this.bindTo(currentTreeView); }
        }
    };

    /**
     * Gets the TreeView.
     * @return the TreeView.
     */
    public final TreeView<T> getTreeView() { return treeView.get(); }

    /**
     * Sets the TreeView.
     * @param treeView the TreeView.
     */
    public final void setTreeView(TreeView<T> treeView) { this.treeView.set(treeView); }

    /**
     * The root item of the TreeView.
     * @return the root item of the TreeView.
     */
    public ObjectProperty<T> rootItemProperty() { return rootItem; }
    private final ObjectProperty<T> rootItem = new SimpleObjectProperty<T>(this, "rootItem") {
        private T currentRootItem;

        @Override
        protected void invalidated() {
            if (currentRootItem != null) { TreeItemsSource.this.unbind(currentRootItem); }
            currentRootItem = get();
            if (currentRootItem != null) { TreeItemsSource.this.bind(currentRootItem); }
        }
    };

    /**
     * Gets the root item of the TreeView.
     * @return the root item of the TreeView.
     */
    public final T getRootItem() { return rootItem.get(); }

    /**
     * Sets the root item of the TreeView.
     * @param rootItem the root item of the TreeView.
     */
    public final void setRootItem(T rootItem) { this.rootItem.set(rootItem); }

    private final ObjectProperty<TreeItem<T>> root = new SimpleObjectProperty<>(this, "root");

    /**
     * Constructs a new instance of this class.
     */
    public TreeItemsSource() {}

    /**
     * Constructs a new instance of this class with the specified TreeView.
     * @param treeView the TreeView.
     */
    public TreeItemsSource(TreeView<T> treeView) {
        this(treeView, null);
    }

    /**
     * Constructs a new instance of this class with the specified TreeView and root item of the TreeView.
     * @param treeView the TreeView.
     * @param rootItem the root item of the TreeView.
     */
    public TreeItemsSource(TreeView<T> treeView, T rootItem) {
        setTreeView(treeView);
        setRootItem(rootItem);
    }

    protected void bindTo(TreeView<T> treeView) {
        treeView.rootProperty().bind(root);
    }

    protected void unbindFrom(TreeView<T> treeView) {
        treeView.rootProperty().unbind();
    }

    protected void bind(T rootItem) {
        root.set(createTreeItem(rootItem));
    }

    protected void unbind(T rootItem) {
        root.set(null);
    }

    protected TreeItem<T> createTreeItem(T item) {
        return new TreeItem<T>() {
            {
                valueProperty().addListener(new InvalidationListener() {
                    private T currentValue;

                    @Override
                    public void invalidated(Observable observable) {
                        if (currentValue != null) { unbind(currentValue); }
                        currentValue = getValue();
                        if (currentValue != null) { bind(currentValue); }
                    }
                });
                setValue(item);
            }

            private final ListChangeListener<T> listChangeListener = change -> {
                while (change.next()) {
                    if (change.wasPermutated()) {
                        Map<Integer, TreeItem<T>> permutationItems = new HashMap<>();
                        for (int index = change.getTo() - 1; index >= change.getFrom(); --index) {
                            permutationItems.put(change.getPermutation(index), getChildren().get(index));
                        }
                        permutationItems.forEach(getChildren()::set);
                    } else {
                        change.getRemoved()
                            .forEach(removedItem -> getChildren().remove(createTreeItem(removedItem)));
                        IntStream.range(change.getFrom(), change.getTo())
                            .forEach(index ->
                                getChildren().add(
                                    index,
                                    createTreeItem(change.getAddedSubList().get(index - change.getFrom()))
                                )
                            );
                    }
                }
            };

            private boolean childrenCreated;

            @Override
            public ObservableList<TreeItem<T>> getChildren() {
                if (!childrenCreated) {
                    super.getChildren().setAll(createChildren());
                    childrenCreated = true;
                }

                return super.getChildren();
            }

            @Override
            public boolean isLeaf() {
                return getValue() == null ? super.isLeaf() : !getValue().hasItem();
            }

            @SuppressWarnings("unchecked")
            @Override
            public boolean equals(Object obj) {
                return obj != null && (
                    obj == this || (
                        obj.getClass() == getClass() &&
                        Objects.equals(((TreeItem<T>)obj).getValue(), getValue())
                    )
                );
            }

            @Override
            public int hashCode() {
                return Objects.hashCode(getValue());
            }

            @Override
            public String toString() {
                return Objects.toString(getValue());
            }

            @Override
            protected void finalize() throws Throwable {
                clearChildren();
                super.finalize();
            }

            private void bind(T value) {
                expandedProperty().bindBidirectional(value.expandedProperty());
                graphicProperty().bindBidirectional(value.graphicProperty());
            }

            private void unbind(T value) {
                expandedProperty().unbindBidirectional(value.expandedProperty());
                graphicProperty().unbindBidirectional(value.graphicProperty());

                clearChildren();
            }

            private List<TreeItem<T>> createChildren() {
                List<TreeItem<T>> children = new ArrayList<>();
                ObservableList<T> items = getValue().getItems();
                if (items == null) { return children; }

                items.forEach(item -> children.add(createTreeItem(item)));
                items.addListener(listChangeListener);
                return children;
            }

            private void clearChildren() {
                getChildren().clear();
                if (getValue().getItems() != null) {
                    getValue().getItems().removeListener(listChangeListener);
                }
                childrenCreated = false;
            }
        };
    }
}
