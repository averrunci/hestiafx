/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.control.cell;

import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.Cell;
import javafx.scene.control.ColorPicker;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Callback;
import javafx.util.StringConverter;

import java.util.Objects;
import java.util.function.BiConsumer;

class ColorPickerCellBehavior<T, G extends Node> {
    @SuppressWarnings("unchecked")
    private static <T, G> Callback<T, G> defaultGraphicFactory() { return (Callback<T, G>)DEFAULT_GRAPHIC_FACTORY; }
    private static final Callback<?, Rectangle> DEFAULT_GRAPHIC_FACTORY = item -> new Rectangle(30, 30);
    @SuppressWarnings("unchecked")
    private static <G> BiConsumer<G, Color> defaultGraphicColorUpdater() { return (BiConsumer<G, Color>) DEFAULT_GRAPHIC_COLOR_UPDATER; }
    private static final BiConsumer<Rectangle, Color> DEFAULT_GRAPHIC_COLOR_UPDATER = Rectangle::setFill;

    private final Cell<T> cell;
    private final Callback<T, G> graphicFactory;
    private final BiConsumer<G, Color> graphicColorUpdater;

    private ObservableValue<Color> colorProperty;
    private G graphic;
    private ColorPicker colorPicker;

    ColorPickerCellBehavior(Cell<T> cell, Callback<T, G> graphicFactory, BiConsumer<G, Color> graphicColorUpdater) {
        this.cell = Objects.requireNonNull(cell);
        if (graphicFactory == null && graphicColorUpdater == null) {
            this.graphicFactory = defaultGraphicFactory();
            this.graphicColorUpdater = defaultGraphicColorUpdater();
        } else if (graphicFactory != null && graphicColorUpdater != null) {
            this.graphicFactory = graphicFactory;
            this.graphicColorUpdater = graphicColorUpdater;
        } else {
            throw new IllegalArgumentException("Both graphicFactory and graphicColorUpdater must be specified.");
        }
    }

    void updateItem(T item, boolean empty, Callback<T, ObservableValue<Color>> getColorProperty, StringConverter<T> converter) {
        if (empty) {
            updateEmptyItem();
            return;
        }

        if (cell.isEditing()) {
            updateEditingItem();
        } else {
            updateItem(item, getColorProperty, converter);
        }
    }

    void startEdit() {
        updateEditingItem();
    }

    void cancelEdit(Callback<T, ObservableValue<Color>> getColorProperty, StringConverter<T> converter) {
        updateItem(cell.getItem(), getColorProperty, converter);
    }

    private void updateEmptyItem() {
        cell.setGraphic(null);
        cell.setText(null);
    }

    private void updateEditingItem() {
        cell.setGraphic(ensureColorPicker());
        cell.setText(null);
    }

    @SuppressWarnings("unchecked")
    private ColorPicker ensureColorPicker() {
        if (colorPicker != null) {
            return colorPicker;
        }

        colorPicker = new ColorPicker();
        if (colorProperty != null) { colorPicker.setValue(colorProperty.getValue()); }
        colorPicker.setOnAction(event -> {
            if (colorProperty == null) { return; }

            if (colorProperty instanceof Property) {
                ((Property)colorProperty).setValue(((ColorPicker)event.getSource()).getValue());
            }

            T currentItem = cell.getItem();
            cell.commitEdit(currentItem instanceof Color ? (T)colorProperty.getValue() : currentItem);
        });
        colorPicker.setOnHidden(event -> cell.cancelEdit());

        return colorPicker;
    }

    private void updateItem(T item, Callback<T, ObservableValue<Color>> getColorProperty, StringConverter<T> converter) {
        ensureGraphic();

        updateColorProperty(item, getColorProperty);
        if (colorProperty == null) {
            updateEmptyItem();
            return;
        }

        graphicColorUpdater.accept(graphic, colorProperty.getValue());
        cell.setGraphic(graphic);

        cell.setText(converter == null ? null : converter.toString(item));
    }

    private void ensureGraphic() {
        if (graphic == null) {
            graphic = graphicFactory.call(cell.getItem());
        }
    }

    @SuppressWarnings("unchecked")
    private void updateColorProperty(T item, Callback<T, ObservableValue<Color>> getColorProperty) {
        if (getColorProperty != null) {
            colorProperty = getColorProperty.call(item);
        } else if (item instanceof Color) {
            if (!(colorProperty instanceof Property)) { colorProperty = new SimpleObjectProperty<>(this, "color"); }
            ((Property)colorProperty).setValue(item);
        }
    }
}
