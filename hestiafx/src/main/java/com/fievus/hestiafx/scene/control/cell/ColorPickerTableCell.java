/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.control.cell;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import javafx.util.StringConverter;

import java.util.function.BiConsumer;

/**
 * A class containing a TableCell implementation that draws a ColorPicker node inside the cell.
 * @param <S> the type of the objects contained within the TableView items list.
 * @param <T> the type of the elements contained within the TableColumn.
 * @param <G> the type of the graphic that draws a color.
 */
public class ColorPickerTableCell<S, T, G extends Node> extends TableCell<S, T> {
    /**
     * Provides a CellFactory for use in a TableColumn cell factory.
     * @param <S> the type of the objects contained within the TableView items list.
     * @param <T> the type of the elements contained within the TableColumn.
     * @return a Callback that will return a TableCell that is able to work on the type of the element contained
     *         within the TableColumn.
     */
    public static <S, T> Callback<TableColumn<S, T>, TableCell<S, T>> forTableColumn() {
        return forTableColumn((StringConverter<T>)null);
    }

    /**
     * Provides a CellFactory for use in a TableColumn cell factory with the specified GraphicFactory and GraphicColorUpdater.
     * @param graphicFactory the factory to create the graphic that draws a color.
     * @param graphicColorUpdater the updater to update a color of the graphic.
     * @param <S> the type of the objects contained within the TableView items. list.
     * @param <T> the type of the elements contained within the TableColumn.
     * @param <G> the type of the graphic that draws a color.
     * @return a Callback that will return a TableCell that is able to work on the type of the element contained
     *         within the TableColumn.
     */
    public static <S, T, G extends Node> Callback<TableColumn<S, T>, TableCell<S, T>> forTableColumn(Callback<T, G> graphicFactory, BiConsumer<G, Color> graphicColorUpdater) {
        return forTableColumn(graphicFactory, graphicColorUpdater, null, null);
    }

    /**
     * Provides a CellFactory for use in a TableColumn cell factory with the specified Callback to get the color property.
     * @param getColorProperty the Callback to get the color property.
     * @param <S> the type of the objects contained within the TableView items list.
     * @param <T> the type of the elements contained within the TableColumn.
     * @return a Callback that will return a TableCell that is able to work on the type of the element contained
     *         within the TableColumn.
     */
    public static <S, T> Callback<TableColumn<S, T>, TableCell<S, T>> forTableColumn(Callback<T, ObservableValue<Color>> getColorProperty) {
        return forTableColumn(getColorProperty, null);
    }

    /**
     * Provides a CellFactory for use in a TableColumn cell factory with the specified converter that will return a string
     * that can be used to represent the object visually.
     * @param converter the StringConverter that will return a String that can be used to represent the object visually.
     * @param <S> the type of the objects contained within the TableView items list.
     * @param <T> the type of the elements contained within the TableColumn.
     * @return a Callback that will return a TableCell that is able to work on the type of the element contained
     *         within the TableColumn.
     */
    public static <S, T> Callback<TableColumn<S, T>, TableCell<S, T>> forTableColumn(StringConverter<T> converter) {
        return forTableColumn(null, converter);
    }

    /**
     * Provides a CellFactory for use in a TableColumn cell factory with the specified Callback to get the color property and
     * converter that will return a String that can be used to represent the object visually.
     * @param getColorProperty the Callback to get the color property.
     * @param converter the StringConverter that will return a String that can be used to represent the object visually.
     * @param <S> the type of the objects contained within the TableView items list.
     * @param <T> the type of the elements contained within the TableColumn.
     * @return a Callback that will return a TableCell that is able to work on the type of the element contained
     *         within the TableColumn.
     */
    public static <S, T> Callback<TableColumn<S, T>, TableCell<S, T>> forTableColumn(Callback<T, ObservableValue<Color>> getColorProperty, StringConverter<T> converter) {
        return forTableColumn(null, null, getColorProperty, converter);
    }

    /**
     * Provides a CellFactory for use in a TableColumn cell factory with the specified GraphicFactory, GraphicColorUpdater,
     * Callback to get the color property, and converter that will return a String that can be used to represent
     * the object visually.
     * @param graphicFactory the factory to create the graphic that draws a color.
     * @param graphicColorUpdater the updater to update a color of the graphic.
     * @param getColorProperty the Callback to get the color property.
     * @param converter the StringConverter that will return a String that can be used to represent the object visually.
     * @param <S> the type of the objects contained within the TableView items list.
     * @param <T> the type of the elements contained within the TableColumn.
     * @param <G> the type of the graphic that draws a color.
     * @return a Callback that will return a TableCell that is able to work on the type of the element contained
     *         within the TableColumn.
     */
    public static <S, T, G extends Node> Callback<TableColumn<S, T>, TableCell<S, T>> forTableColumn(Callback<T, G> graphicFactory, BiConsumer<G, Color> graphicColorUpdater, Callback<T, ObservableValue<Color>> getColorProperty, StringConverter<T> converter) {
        return column -> new ColorPickerTableCell<>(graphicFactory, graphicColorUpdater, getColorProperty, converter);
    }

    /**
     * The Callback to get the color property.
     * @return the Callback to get the color property.
     */
    public ObjectProperty<Callback<T, ObservableValue<Color>>> colorCallbackProperty() { return colorCallback; }
    private final ObjectProperty<Callback<T, ObservableValue<Color>>> colorCallback = new SimpleObjectProperty<>(this, "colorCallback");

    /**
     * Gets the Callback to get the color property.
     * @return the Callback to get the color property.
     */
    public final Callback<T, ObservableValue<Color>> getColorCallback() { return colorCallback.get(); }

    /**
     * Sets the Callback to get the color property.
     * @param colorCallback the Callback to get the color property.
     */
    public final void setColorCallback(Callback<T, ObservableValue<Color>> colorCallback) { this.colorCallback.set(colorCallback); }

    /**
     * The converter that will return a String that can be used to represent the object visually.
     * @return the StringConverter.
     */
    public ObjectProperty<StringConverter<T>> converterProperty() { return converter; }
    private final ObjectProperty<StringConverter<T>> converter = new SimpleObjectProperty<>(this, "converter");

    /**
     * Gets the converter that will return a String that can be used to represent the object visually.
     * @return the StringConverter.
     */
    public final StringConverter<T> getConverter() { return converter.get(); }

    /**
     * Sets the converter that will return a String that can be used to represent the object visually.
     * @param converter the StringConverter.
     */
    public final void setConverter(StringConverter<T> converter) { this.converter.set(converter); }

    private static final Insets DEFAULT_PADDING = new Insets(5);

    private final ColorPickerCellBehavior<T, G> behavior;

    /**
     * Constructs a new instance of this class.
     */
    public ColorPickerTableCell() {
        this((StringConverter<T>)null);
    }

    /**
     * Constructs a new instance of this class with the specified GraphicFactory and GraphicColorUpdater.
     * @param graphicFactory the factory to create the graphic that draws a color.
     * @param graphicColorUpdater the updater to update a color of the graphic.
     */
    public ColorPickerTableCell(Callback<T, G> graphicFactory, BiConsumer<G, Color> graphicColorUpdater) {
        this(graphicFactory, graphicColorUpdater, null, null);
    }

    /**
     * Constructs a new instance of this class with the specified Callback to get the color property.
     * @param getColorProperty the Callback to get the color property.
     */
    public ColorPickerTableCell(Callback<T, ObservableValue<Color>> getColorProperty) {
        this(getColorProperty, null);
    }

    /**
     * Constructs a new instance of this class with the specified converter that will return a String that
     * can be used to represent the object visually.
     * @param converter the StringConverter that will return a String that can be used to represent the object visually.
     */
    public ColorPickerTableCell(StringConverter<T> converter) {
        this(null, converter);
    }

    /**
     * Constructs a new instance of this class with the specified Callback to get the color property and
     * converter that will return a String that can be used to represent the object visually.
     * @param getColorProperty the Callback to get the color property.
     * @param converter the StringConverter that will return a String that can be used to represent the object visually.
     */
    public ColorPickerTableCell(Callback<T, ObservableValue<Color>> getColorProperty, StringConverter<T> converter) {
        this(null, null, getColorProperty, converter);
    }

    /**
     * Constructs a new instance of this class with the specified GraphicFactory, GraphicColorUpdater,
     * Callback to get the color property, and converter that will return a String that can be used to represent
     * the object visually.
     * @param graphicFactory the factory to create the graphic that draws a color.
     * @param graphicColorUpdater the updater to update a color of the graphic.
     * @param getColorProperty the Callback to get the color property.
     * @param converter the StringConverter that will return a String that can be used to represent the object visually.
     */
    public ColorPickerTableCell(Callback<T, G> graphicFactory, BiConsumer<G, Color> graphicColorUpdater, Callback<T, ObservableValue<Color>> getColorProperty, StringConverter<T> converter) {
        behavior = new ColorPickerCellBehavior<>(this, graphicFactory, graphicColorUpdater);

        setColorCallback(getColorProperty);
        setConverter(converter);

        setContentDisplay(converter == null ? ContentDisplay.GRAPHIC_ONLY : ContentDisplay.LEFT);
        setAlignment(Pos.CENTER);
        setPadding(DEFAULT_PADDING);
        getStyleClass().add("color-picker-table-cell");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void startEdit() {
        if (!isEditable()) { return; }
        if (!getTableView().isEditable() || !getTableColumn().isEditable()) { return; }

        super.startEdit();
        behavior.startEdit();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void cancelEdit() {
        if (!isEditing()) { return; }

        super.cancelEdit();
        behavior.cancelEdit(getColorCallback(), getConverter());
    }

    @Override
    protected void updateItem(T item, boolean empty) {
        super.updateItem(item, empty);
        behavior.updateItem(item, empty, getColorCallback(), getConverter());
    }
}
