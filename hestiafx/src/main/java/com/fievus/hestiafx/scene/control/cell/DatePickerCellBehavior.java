/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.control.cell;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Cell;
import javafx.scene.control.DatePicker;
import javafx.util.Callback;
import javafx.util.converter.LocalDateStringConverter;

import java.time.LocalDate;
import java.util.Objects;
import java.util.function.Supplier;

class DatePickerCellBehavior<T> {
    private static final LocalDateStringConverter DEFAULT_CONVERTER = new LocalDateStringConverter();

    private final Cell<T> cell;
    private final Supplier<DatePicker> datePickerFactory;
    private final ObjectProperty<LocalDateStringConverter> converter = new SimpleObjectProperty<>(this, "converter");

    private ObservableValue<LocalDate> dateProperty;
    private DatePicker datePicker;

    DatePickerCellBehavior(Cell<T> cell, ObjectProperty<LocalDateStringConverter> converter, Supplier<DatePicker> datePickerFactory) {
        this.cell = Objects.requireNonNull(cell);
        this.converter.bind(Objects.requireNonNull(converter));
        if (datePickerFactory == null) {
            this.datePickerFactory = () -> {
                DatePicker datePicker = new DatePicker();
                datePicker.setEditable(false);
                datePicker.converterProperty().bind(converter);
                return datePicker;
            };
        } else {
            this.datePickerFactory = datePickerFactory;
        }
    }

    void updateItem(T item, boolean empty, Callback<T, ObservableValue<LocalDate>> getDateProperty) {
        if (empty) {
            updateEmptyItem();
            return;
        }

        if (cell.isEditing()) {
            updateEditingItem();
        } else {
            updateItem(item, getDateProperty);
        }
    }

    void startEdit() {
        updateEditingItem();
    }

    void cancelEdit(Callback<T, ObservableValue<LocalDate>> getDateProperty) {
        updateItem(cell.getItem(), getDateProperty);
    }

    private void updateEmptyItem() {
        cell.setGraphic(null);
        cell.setText(null);
    }

    private void updateEditingItem() {
        cell.setGraphic(ensureDatePicker());
        cell.setText(null);
    }

    @SuppressWarnings("unchecked")
    private DatePicker ensureDatePicker() {
        if (datePicker != null) {
            return datePicker;
        }

        datePicker = datePickerFactory.get();
        if (dateProperty != null) { datePicker.setValue(dateProperty.getValue()); }
        datePicker.setOnAction(event -> {
            if (dateProperty == null) { return; }

            if (dateProperty instanceof Property) {
                ((Property)dateProperty).setValue(((DatePicker)event.getSource()).getValue());
            }

            T currentItem = cell.getItem();
            cell.commitEdit(currentItem instanceof LocalDate ? (T)dateProperty.getValue() : currentItem);
        });
        datePicker.setOnHidden(event -> cell.cancelEdit());

        return datePicker;
    }

    private void updateItem(T item, Callback<T, ObservableValue<LocalDate>> getDateProperty) {
        updateDateProperty(item, getDateProperty);
        if (dateProperty == null) {
            updateEmptyItem();
            return;
        }

        cell.setGraphic(null);
        cell.setText(ensureConverter(converter.get()).toString(dateProperty.getValue()));
    }

    @SuppressWarnings("unchecked")
    private void updateDateProperty(T item, Callback<T, ObservableValue<LocalDate>> getDateProperty) {
        if (getDateProperty != null) {
            dateProperty = getDateProperty.call(item);
        } else if (item instanceof LocalDate) {
            if (!(dateProperty instanceof Property)) { dateProperty = new SimpleObjectProperty<>(this, "date"); }
            ((Property)dateProperty).setValue(item);
        }
    }

    private LocalDateStringConverter ensureConverter(LocalDateStringConverter converter) {
        return converter == null ? DEFAULT_CONVERTER : converter;
    }
}
