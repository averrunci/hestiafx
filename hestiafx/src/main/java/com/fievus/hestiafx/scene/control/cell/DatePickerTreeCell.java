/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.control.cell;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeView;
import javafx.util.Callback;
import javafx.util.converter.LocalDateStringConverter;

import java.time.LocalDate;
import java.util.function.Supplier;

/**
 * A class containing a TreeCell implementation that draws a DatePicker node inside the cell.
 * @param <T> the type of the item contained within the TreeItem in the TreeView.
 */
public class DatePickerTreeCell<T> extends TreeCell<T> {
    /**
     * Provides a CellFactory for use in a TreeView cell factory.
     * @param <T> the type of the item contained within the TreeItem in the TreeView.
     * @return a Callback that will return a TreeCell that is able to work on the type of the element contained
     *         within the TreeView root and all of its children.
     */
    public static <T> Callback<TreeView<T>, TreeCell<T>> forTreeView() {
        return forTreeView((LocalDateStringConverter)null);
    }

    /**
     * Provides a CellFactory for use in a TreeView cell factory with the specified factory of the DatePicker.
     * @param datePickerFactory the factory to create a DatePicker.
     * @param <T> the type of the item contained within the TreeItem in the TreeView.
     * @return a Callback that will return a TreeCell that is able to work on the type of the element contained
     *         within the TreeView root and all of its children.
     */
    public static <T> Callback<TreeView<T>, TreeCell<T>> forTreeView(Supplier<DatePicker> datePickerFactory) {
        return forTreeView(datePickerFactory, null, null);
    }

    /**
     * Provides a CellFactory for use in a TreeView cell factory with the specified Callback to get the date property.
     * @param getDateProperty the Callback to get the date property.
     * @param <T> the type of the item contained within the TreeItem in the TreeView.
     * @return a Callback that will return a TreeCell that is able to work on the type of the element contained
     *         within the TreeView root and all of its children.
     */
    public static <T> Callback<TreeView<T>, TreeCell<T>> forTreeView(Callback<T, ObservableValue<LocalDate>> getDateProperty) {
        return forTreeView(getDateProperty, null);
    }

    /**
     * Provides a CellFactory for use in a TreeView cell factory with the specified converter
     * that will return a String that can be used to represent the object visually.
     * @param converter the LocalDateStringConverter that will return a String that can be used to represent the object visually.
     * @param <T> the type of the item contained within the TreeItem in the TreeView.
     * @return a Callback that will return a TreeCell that is able to work on the type of the element contained
     *         within the TreeView root and all of its children.
     */
    public static <T> Callback<TreeView<T>, TreeCell<T>> forTreeView(LocalDateStringConverter converter) {
        return forTreeView(null, converter);
    }

    /**
     * Provides a CellFactory for use in a TreeView cell factory with the specified Callback to get the date property
     * and converter that will return a String that can be used to represent the object visually.
     * @param getDateProperty the Callback to get the date property.
     * @param converter the LocalDateStringConverter that will return a String that can be used to represent the object visually.
     * @param <T> the type of the item contained within the TreeItem in the TreeView.
     * @return a Callback that will return a TreeCell that is able to work on the type of the element contained
     *         within the TreeView root and all of its children.
     */
    public static <T> Callback<TreeView<T>, TreeCell<T>> forTreeView(Callback<T, ObservableValue<LocalDate>> getDateProperty, LocalDateStringConverter converter) {
        return forTreeView(null, getDateProperty, converter);
    }

    /**
     * Provides a CellFactory for use in a TreeView cell factory with the specified factory of the DatePicker,
     * Callback to get the date property, and converter that will return a String that can be used to represent
     * the object visually.
     * @param datePickerFactory the factory to create a DatePicker.
     * @param getDateProperty the Callback to get the date property.
     * @param converter the LocalDateStringConverter that will return a String that can be used to represent the object visually.
     * @param <T> the type of the item contained within the TreeItem in the TreeView.
     * @return a Callback that will return a TreeCell that is able to work on the type of the element contained
     *         within the TreeView root and all of its children.
     */
    public static <T> Callback<TreeView<T>, TreeCell<T>> forTreeView(Supplier<DatePicker> datePickerFactory, Callback<T, ObservableValue<LocalDate>> getDateProperty, LocalDateStringConverter converter) {
        return column -> new DatePickerTreeCell<>(datePickerFactory, getDateProperty, converter);
    }

    /**
     * The Callback to get the date property.
     * @return the Callback to the date property.
     */
    public ObjectProperty<Callback<T, ObservableValue<LocalDate>>> dateCallbackProperty() { return dateCallback; }
    private final ObjectProperty<Callback<T, ObservableValue<LocalDate>>> dateCallback = new SimpleObjectProperty<>(this, "dateCallback");

    /**
     * Gets the Callback to the date property.
     * @return the Callback to the date property.
     */
    public final Callback<T, ObservableValue<LocalDate>> getDateCallback() { return dateCallback.get(); }

    /**
     * Sets the Callback to the date property.
     * @param dateCallback the Callback to get the date property.
     */
    public final void setDateCallback(Callback<T, ObservableValue<LocalDate>> dateCallback) { this.dateCallback.set(dateCallback); }

    /**
     * The converter that will return a String that can be used to represent the object visually.
     * @return the LocalDateStringConverter.
     */
    public ObjectProperty<LocalDateStringConverter> converterProperty() { return converter; }
    private final ObjectProperty<LocalDateStringConverter> converter = new SimpleObjectProperty<>(this, "converter");

    /**
     * Gets the converter that will return a String that can be used to represent the object visually.
     * @return the LocalDateStringConverter.
     */
    public final LocalDateStringConverter getConverter() { return converter.get(); }

    /**
     * Sets the converter that will return a String that can be used to represent the object visually.
     * @param converter the LocalDateStringConverter.
     */
    public final void setConverter(LocalDateStringConverter converter) { this.converter.set(converter); }

    private final DatePickerCellBehavior<T> behavior;

    /**
     * Constructs a new instance of this class.
     */
    public DatePickerTreeCell() {
        this((LocalDateStringConverter)null);
    }

    /**
     * Constructs a new instance of this class with the specified factory of the DatePicker.
     * @param datePickerFactory the factory to create the DatePicker.
     */
    public DatePickerTreeCell(Supplier<DatePicker> datePickerFactory) {
        this(datePickerFactory, null, null);
    }

    /**
     * Constructs a new instance of this class with the specified Callback to get the date property.
     * @param getDateProperty the Callback to get the date property.
     */
    public DatePickerTreeCell(Callback<T, ObservableValue<LocalDate>> getDateProperty) {
        this(getDateProperty, null);
    }

    /**
     * Constructs a new instance of this class with the specified converter that will return a String that
     * can be used to represent the object visually.
     * @param converter the LocalDateStringConverter that will return a String that can be used to represent the object visually.
     */
    public DatePickerTreeCell(LocalDateStringConverter converter) {
        this(null, converter);
    }

    /**
     * Constructs a new instance of this class with the specified Callback to get the date property and
     * converter that will return a String that can be used to represent the object visually.
     * @param getDateProperty the Callback to get the date property.
     * @param converter the LocalDateStringConverter that will return a String that can be used to represent the object visually.
     */
    public DatePickerTreeCell(Callback<T, ObservableValue<LocalDate>> getDateProperty, LocalDateStringConverter converter) {
        this(null, getDateProperty, converter);
    }

    /**
     * Constructs a new instance of this class with the specified factory of the DatePicker, Callback to get
     * the date property, and converter that will return a String that can be used to represent the object visually.
     * @param datePickerFactory the factory to create a DatePicker.
     * @param getDateProperty the Callback to get the date property.
     * @param converter the LocalDateStringConverter that will return a String that can be used to represent the object visually.
     */
    public DatePickerTreeCell(Supplier<DatePicker> datePickerFactory, Callback<T, ObservableValue<LocalDate>> getDateProperty, LocalDateStringConverter converter) {
        setDateCallback(getDateProperty);
        setConverter(converter == null ? new LocalDateStringConverter() : converter);

        behavior = new DatePickerCellBehavior<>(this, converterProperty(), datePickerFactory);

        getStyleClass().add("date-picker-tree-cell");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void startEdit() {
        if (!isEditable()) { return; }
        if (!getTreeView().isEditable()) { return; }

        super.startEdit();
        behavior.startEdit();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void cancelEdit() {
        if (!isEditing()) { return; }

        super.cancelEdit();
        behavior.cancelEdit(getDateCallback());
    }

    @Override
    protected void updateItem(T item, boolean empty) {
        super.updateItem(item, empty);
        behavior.updateItem(item, empty, getDateCallback());
    }
}
