/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.input;

import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.Scene;

import java.util.Objects;

/**
 * Performs the dragging operation of the node to the specified location.
 */
public final class DragContext {
    private final Node dragNode;
    private final Node boundingNode;
    private final double dragAnchorX;
    private final double dragAnchorY;

    private DragContext(Builder builder) {
        dragNode = builder.dragNode;
        boundingNode = builder.boundingNode;
        dragAnchorX = builder.dragAnchorX;
        dragAnchorY = builder.dragAnchorY;
    }

    /**
     * Constructs a new instance of the Builder that builds a DragContext with the specified dragged node.
     * @param node the dragged node.
     * @return an instance of the Builder.
     */
    public static Builder of(Node node) {
        return new Builder(Objects.requireNonNull(node));
    }

    /**
     * Drags the node to the specified location.
     * @param sceneX the X coordinate location on a Scene.
     * @param sceneY the Y coordinate location on a Scene.
     */
    public void dragNodeTo(double sceneX, double sceneY) {
        if (!canDragNodeTo(sceneX, sceneY)) { return; }

        dragNode.setLayoutX(sceneX - dragAnchorX);
        dragNode.setLayoutY(sceneY - dragAnchorY);
    }

    private boolean canDragNodeTo(double sceneX, double sceneY) {
        if (boundingNode != null) {
            return boundingNode.getLayoutBounds().contains(boundingNode.sceneToLocal(sceneX, sceneY));
        }

        Scene scene = dragNode.getScene();
        Bounds sceneBounds = new BoundingBox(0, 0, scene.getWidth(), scene.getHeight());
        return sceneBounds.contains(sceneX, sceneY);
    }

    /**
     * Builds a DragContext with the specified properties.
     */
    public final static class Builder {
        private final Node dragNode;

        private Node boundingNode;
        private double dragAnchorX;
        private double dragAnchorY;

        private Builder(Node node) {
            dragNode = node;
        }

        /**
         * Sets the node that contains the dragged node.
         * The node can be dragged within the specified node.
         * @param node the node that contains the dragged node.
         * @return the instance of this class.
         */
        public Builder boundTo(Node node) {
            boundingNode = node;
            return this;
        }

        /**
         * Constructs a new instance of the DragContext with the specified mouse location.
         * @param sceneX the X coordinate location of the mouse on a Scene.
         * @param sceneY the Y coordinate location of the mouse on a Scene.
         * @return an instance of the DragContext.
         */
        public DragContext atMouseLocation(double sceneX, double sceneY) {
            dragAnchorX = sceneX - dragNode.getLayoutX();
            dragAnchorY = sceneY - dragNode.getLayoutY();
            return new DragContext(this);
        }
    }
}
