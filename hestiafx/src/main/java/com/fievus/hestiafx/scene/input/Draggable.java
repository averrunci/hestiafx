/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.input;

import com.fievus.hestiafx.event.EventExtensions;
import com.fievus.hestiafx.scene.NodeExtensions;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

import java.util.Objects;

/**
 * Provides the function of dragging for a node.
 */
public final class Draggable {
    private static final String DRAGGABLE_ENABLE = "draggable-enable";
    private static final String DRAGGABLE_BOUNDING_NODE = "draggable-bounding-node";
    private static final String DRAGGABLE_DRAG_CONTEXT = "draggable-drag-context";

    private static final EventHandler<MouseEvent> MOUSE_PRESSED_HANDLER = mouseEvent ->
        EventExtensions.<Node>getEventSourceFrom(mouseEvent).ifPresent(node -> {
            node.getProperties().put(
                DRAGGABLE_DRAG_CONTEXT,
                DragContext.of(node)
                    .boundTo(getBoundingNode(node))
                    .atMouseLocation(mouseEvent.getSceneX(), mouseEvent.getSceneY())
            );
            mouseEvent.consume();
        });

    private static final EventHandler<MouseEvent> MOUSE_DRAGGED_HANDLER = mouseEvent ->
        EventExtensions.<Node>getEventSourceFrom(mouseEvent).ifPresent(node -> {
            NodeExtensions.<DragContext>getProperty(node, DRAGGABLE_DRAG_CONTEXT)
                .ifPresent(context -> context.dragNodeTo(mouseEvent.getSceneX(), mouseEvent.getSceneY()));
            mouseEvent.consume();
        });

    private static final EventHandler<MouseEvent> MOUSE_RELEASED_HANDLER = mouseEvent ->
        EventExtensions.<Node>getEventSourceFrom(mouseEvent).ifPresent(node -> {
            node.getProperties().remove(DRAGGABLE_DRAG_CONTEXT);
            mouseEvent.consume();
        });

    private Draggable() {}

    /**
     * Gets the value to indicate whether the specified node can be dragged.
     * @param node the dragged node.
     * @return true if the specified node can be dragged, otherwise false.
     */
    public static boolean isEnable(Node node) {
        return NodeExtensions.<Boolean>getProperty(Objects.requireNonNull(node), DRAGGABLE_ENABLE).orElse(false);
    }

    /**
     * Sets the value to indicate whether the specified node can be dragged.
     * @param node the dragged node.
     * @param enable true if the specified node can be dragged, otherwise false.
     */
    public static void setEnable(Node node, boolean enable) {
        Objects.requireNonNull(node).getProperties().put(DRAGGABLE_ENABLE, enable);

        if (enable) {
            node.addEventHandler(MouseEvent.MOUSE_PRESSED, MOUSE_PRESSED_HANDLER);
            node.addEventHandler(MouseEvent.MOUSE_DRAGGED, MOUSE_DRAGGED_HANDLER);
            node.addEventHandler(MouseEvent.MOUSE_RELEASED, MOUSE_RELEASED_HANDLER);
        } else {
            node.removeEventHandler(MouseEvent.MOUSE_PRESSED, MOUSE_PRESSED_HANDLER);
            node.removeEventHandler(MouseEvent.MOUSE_DRAGGED, MOUSE_DRAGGED_HANDLER);
            node.removeEventHandler(MouseEvent.MOUSE_RELEASED, MOUSE_RELEASED_HANDLER);
        }
    }

    /**
     * Gets the bounding node within which the specified node can be dragged.
     * @param node the dragged node.
     * @return the bounding node within which the specified node can be dragged.
     */
    public static Node getBoundingNode(Node node) {
        return NodeExtensions.<Node>getProperty(Objects.requireNonNull(node), DRAGGABLE_BOUNDING_NODE).orElse(null);
    }

    /**
     * Sets the bounding node within which the specified node can be dragged.
     * @param node the dragged node.
     * @param boundingNode the bounding node within which the specified node can be dragged.
     */
    public static void setBoundingNode(Node node, Node boundingNode) {
        Objects.requireNonNull(node).getProperties().put(DRAGGABLE_BOUNDING_NODE, boundingNode);
    }

    /**
     * Constructs a new instance of the Builder with the specified dragged node.
     * @param node the dragged node.
     * @return a instance of the Builder.
     */
    public static Builder to(Node node) {
        return new Builder(Objects.requireNonNull(node));
    }

    /**
     * Builds properties of the Draggable.
     */
    public static final class Builder {
        private final Node dragNode;
        private Node boundingNode;

        private Builder(Node node) {
            dragNode = node;
        }

        /**
         * Sets the node that contains the dragged node.
         * The dragged node can be dragged within the specified node.
         * @param node the node that contains the dragged node.
         * @return the instance of this class.
         */
        public Builder boundTo(Node node) {
            boundingNode = node;
            return this;
        }

        /**
         * Sets the parent node to a bounding node within which the node can be dragged.
         * @return the instance of this class.
         */
        public Builder boundToParent() {
            boundingNode = dragNode.getParent();
            return this;
        }

        /**
         * Enables the dragging of the node.
         */
        public void enable() {
            Draggable.setBoundingNode(dragNode, boundingNode);
            Draggable.setEnable(dragNode, true);
        }
    }
}
