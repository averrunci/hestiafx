/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.input;

import com.fievus.hestiafx.event.EventExtensions;
import com.fievus.hestiafx.scene.NodeExtensions;
import javafx.animation.FadeTransition;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;

import java.util.Objects;
import java.util.function.Predicate;

/**
 * Provides the function of fade-in when the mouse is entered into the node and
 * fade-out when the mouse is exited from the node.
 */
public final class MouseOverFadeInOut {
    private static final String MOUSE_OVER_FADE_IN_OUT_ENABLE = "mouse-over-fade-in-out-enable";
    private static final String MOUSE_OVER_FADE_IN_OUT_FADE_IN_DURATION = "mouse-over-fade-in-out-fade-in-duration";
    private static final String MOUSE_OVER_FADE_IN_OUT_FADE_OUT_DURATION = "mouse-over-fade-in-out-fade-out-duration";
    private static final String MOUSE_OVER_FADE_IN_OUT_OPACITY_FADED_IN = "mouse-over-fade-in-out-opacity-faded-in";
    private static final String MOUSE_OVER_FADE_IN_OUT_OPACITY_FADED_OUT = "mouse-over-fade-in-out-opacity-faded-out";
    private static final String MOUSE_OVER_FADE_IN_OUT_FILTER = "mouse-over-fade-in-out-filter";

    private static final Duration DEFAULT_FADE_IN_DURATION = Duration.millis(200);
    private static final Duration DEFAULT_FADE_OUT_DURATION = Duration.millis(400);
    private static final double DEFAULT_OPACITY_FADED_IN = 1;
    private static final double DEFAULT_OPACITY_FADED_OUT = 0;
    private static Predicate<? super Node> DEFAULT_FILTER = node -> true;

    private static final EventHandler<MouseEvent> MOUSE_ENTERED_HANDLER = mouseEvent ->
        EventExtensions.<Node>getEventSourceFrom(mouseEvent)
            .filter(node -> getFilter(node).test(node))
            .ifPresent(node -> {
                FadeTransition transition = new FadeTransition(getFadeInDuration(node), node);
                transition.setToValue(getOpacityFadedIn(node));
                transition.play();
            });

    private static final EventHandler<MouseEvent> MOUSE_EXITED_HANDLER = mouseEvent ->
        EventExtensions.<Node>getEventSourceFrom(mouseEvent)
            .filter(node -> getFilter(node).test(node))
            .ifPresent(node -> {
                FadeTransition transition = new FadeTransition(getFadeOutDuration(node), node);
                transition.setToValue(getOpacityFadedOut(node));
                transition.play();
            });

    private MouseOverFadeInOut() {}

    /**
     * Gets the value to indicate whether the function of fade-in/fade-out over the mouse is enable.
     * @param node the target node.
     * @return true if the function of fade-in/fade-out over the mouse is enable, otherwise false.
     */
    public static boolean isEnable(Node node) {
        return NodeExtensions.<Boolean>getProperty(Objects.requireNonNull(node), MOUSE_OVER_FADE_IN_OUT_ENABLE).orElse(false);
    }

    /**
     * Sets the value to indicate whether the function of fade-in/fade-out over the mouse is enable.
     * @param node the target node.
     * @param enable true if the function of fade-in/fade-out over the mouse is enable, otherwise false.
     */
    public static void setEnable(Node node, boolean enable) {
        Objects.requireNonNull(node).getProperties().put(MOUSE_OVER_FADE_IN_OUT_ENABLE, enable);

        if (enable) {
            node.addEventFilter(MouseEvent.MOUSE_ENTERED, MOUSE_ENTERED_HANDLER);
            node.addEventFilter(MouseEvent.MOUSE_EXITED, MOUSE_EXITED_HANDLER);
        } else {
            node.removeEventFilter(MouseEvent.MOUSE_ENTERED, MOUSE_ENTERED_HANDLER);
            node.removeEventFilter(MouseEvent.MOUSE_EXITED, MOUSE_EXITED_HANDLER);
        }
    }

    /**
     * Gets the duration for which the node fades in.
     * @param node the target node.
     * @return the duration for which the node fades in.
     */
    public static Duration getFadeInDuration(Node node) {
        return NodeExtensions.<Duration>getProperty(Objects.requireNonNull(node), MOUSE_OVER_FADE_IN_OUT_FADE_IN_DURATION).orElse(DEFAULT_FADE_IN_DURATION);
    }

    /**
     * Sets the duration for which the node fades in.
     * @param node the target node.
     * @param duration the duration for which the node fades in.
     */
    public static void setFadeInDuration(Node node, Duration duration) {
        Objects.requireNonNull(node).getProperties().put(MOUSE_OVER_FADE_IN_OUT_FADE_IN_DURATION, duration);
    }

    /**
     * Gets the duration for which the node fades out.
     * @param node the target node.
     * @return the duration for which the node fades out.
     */
    public static Duration getFadeOutDuration(Node node) {
        return NodeExtensions.<Duration>getProperty(Objects.requireNonNull(node), MOUSE_OVER_FADE_IN_OUT_FADE_OUT_DURATION).orElse(DEFAULT_FADE_OUT_DURATION);
    }

    /**
     * Sets the duration for which the node fades out.
     * @param node the target node.
     * @param duration the duration for which the node fades out.
     */
    public static void setFadeOutDuration(Node node, Duration duration) {
        Objects.requireNonNull(node).getProperties().put(MOUSE_OVER_FADE_IN_OUT_FADE_OUT_DURATION, duration);
    }

    /**
     * Gets the opacity of the specified node when the fade-in is finished.
     * @param node the target node.
     * @return the opacity of the specified node when the fade-in is finished.
     */
    public static double getOpacityFadedIn(Node node) {
        return NodeExtensions.<Double>getProperty(Objects.requireNonNull(node), MOUSE_OVER_FADE_IN_OUT_OPACITY_FADED_IN).orElse(DEFAULT_OPACITY_FADED_IN);
    }

    /**
     * Sets the opacity of the specified node when the fade-in is finished.
     * @param node the target node.
     * @param opacity the opacity of the specified node when the fade-in is finished.
     */
    public static void setOpacityFadedIn(Node node, double opacity) {
        Objects.requireNonNull(node).getProperties().put(MOUSE_OVER_FADE_IN_OUT_OPACITY_FADED_IN, opacity);
    }

    /**
     * Gets the opacity of the specified node when the fade-out is finished.
     * @param node the target node.
     * @return the opacity of the specified node when the fade-out is finished.
     */
    public static double getOpacityFadedOut(Node node) {
        return NodeExtensions.<Double>getProperty(Objects.requireNonNull(node), MOUSE_OVER_FADE_IN_OUT_OPACITY_FADED_OUT).orElse(DEFAULT_OPACITY_FADED_OUT);
    }

    /**
     * Sets the opacity of the specified node when the fade-out is finished.
     * @param node the target node.
     * @param opacity the opacity of the specified node when the fade-out is finished.
     */
    public static void setOpacityFadedOut(Node node, double opacity) {
        Objects.requireNonNull(node).getProperties().put(MOUSE_OVER_FADE_IN_OUT_OPACITY_FADED_OUT, opacity);
    }

    /**
     * Gets the filter to decide whether the fade-in/fade-out can be executed.
     * @param node the target node.
     * @return the filter to decide whether the fade-in/fade-out can be executed.
     */
    public static Predicate<? super Node> getFilter(Node node) {
        return NodeExtensions.<Predicate<? super Node>>getProperty(Objects.requireNonNull(node), MOUSE_OVER_FADE_IN_OUT_FILTER).orElse(DEFAULT_FILTER);
    }

    /**
     * Sets the filter to decide whether the fade-in/fade-out can be executed.
     * @param node the target node.
     * @param filter the filter to decide whether the fade-in/fade-out can be executed.
     */
    public static void setFilter(Node node, Predicate<? super Node> filter) {
        Objects.requireNonNull(node).getProperties().put(MOUSE_OVER_FADE_IN_OUT_FILTER, filter);
    }

    /**
     * Constructs a new instance of the Builder with the specified node.
     * @param node the target node.
     * @return a instance of the Builder.
     */
    public static Builder of(Node node) {
        return new Builder(Objects.requireNonNull(node));
    }

    /**
     * Builds properties of the MouseOverFadeInOut.
     */
    public static final class Builder {
        private final Node node;

        private Duration fadeInDuration;
        private Duration fadeOutDuration;
        private double opacityFadedIn = Double.NaN;
        private double opacityFadedOut = Double.NaN;
        private Predicate<? super Node> filter;

        private Builder(Node node) {
            this.node = node;
        }

        /**
         * Sets the duration for which the node fades in.
         * @param duration the duration for which the node fades in.
         * @return the instance of this class.
         */
        public Builder fadeInFor(Duration duration) {
            this.fadeInDuration = duration;
            return this;
        }

        /**
         * Sets the duration for which the node fades out.
         * @param duration the duration for which the node fades out.
         * @return the instance of this class.
         */
        public Builder fadeOutFor(Duration duration) {
            this.fadeOutDuration = duration;
            return this;
        }

        /**
         * Sets the opacity of the node when the fade-in is finished.
         * @param opacity the opacity of the node when the fade-in is finished.
         * @return the instance of this class.
         */
        public Builder fadeInUntil(double opacity) {
            this.opacityFadedIn = opacity;
            return this;
        }

        /**
         * Sets the opacity of the node when the fade-out is finished.
         * @param opacity the opacity of the node when the fade-ou is finished.
         * @return the instance of this class.
         */
        public Builder fadeOutUntil(double opacity) {
            this.opacityFadedOut = opacity;
            return this;
        }

        /**
         * Sets the filter to decide whether the fade-in/fade-out can be executed.
         * @param filter the filter to decide whether the fade-in/fade-out can be executed.
         * @return the instance of this class.
         */
        public Builder with(Predicate<? super Node> filter) {
            this.filter = filter;
            return this;
        }

        /**
         * Enables the function of fade-in/fade-out.
         */
        public void enable() {
            if (fadeInDuration != null) { MouseOverFadeInOut.setFadeInDuration(node, fadeInDuration); }
            if (fadeOutDuration != null) { MouseOverFadeInOut.setFadeInDuration(node, fadeOutDuration); }
            if (!Double.isNaN(opacityFadedIn)) { MouseOverFadeInOut.setOpacityFadedIn(node, opacityFadedIn); }
            if (!Double.isNaN(opacityFadedOut)) { MouseOverFadeInOut.setOpacityFadedOut(node, opacityFadedOut); }
            if (filter != null) { MouseOverFadeInOut.setFilter(node, filter); }
            MouseOverFadeInOut.setEnable(node, true);
        }
    }
}
