/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.input;

import com.fievus.hestiafx.event.EventExtensions;
import com.fievus.hestiafx.scene.NodeExtensions;
import javafx.animation.ScaleTransition;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;

import java.util.Objects;
import java.util.function.Predicate;

/**
 * Provides the function of zoom-in when the mouse is entered into the node and
 * zoom-out when the mouse is exited from the node.
 */
public final class MouseOverZoomInOut {
    private static final String MOUSE_OVER_ZOOM_IN_OUT_ENABLE = "mouse-over-zoom-in-out-enable";
    private static final String MOUSE_OVER_ZOOM_IN_OUT_ZOOM_IN_DURATION = "mouse-over-zoom-in-out-zoom-in-duration";
    private static final String MOUSE_OVER_ZOOM_IN_OUT_ZOOM_OUT_DURATION = "mouse-over-zoom-in-out-zoom-out-duration";
    private static final String MOUSE_OVER_ZOOM_IN_OUT_SCALE_X_ZOOMED_IN = "mouse-over-zoom-in-out-scale-x-zoomed-in";
    private static final String MOUSE_OVER_ZOOM_IN_OUT_SCALE_Y_ZOOMED_IN = "mouse-over-zoom-in-out-scale-y-zoomed-in";
    private static final String MOUSE_OVER_ZOOM_IN_OUT_SCALE_Z_ZOOMED_IN = "mouse-over-zoom-in-out-scale-z-zoomed-in";
    private static final String MOUSE_OVER_ZOOM_IN_OUT_SCALE_X_ZOOMED_OUT = "mouse-over-zoom-in-out-scale-x-zoomed-out";
    private static final String MOUSE_OVER_ZOOM_IN_OUT_SCALE_Y_ZOOMED_OUT = "mouse-over-zoom-in-out-scale-y-zoomed-out";
    private static final String MOUSE_OVER_ZOOM_IN_OUT_SCALE_Z_ZOOMED_OUT = "mouse-over-zoom-in-out-scale-z-zoomed-out";
    private static final String MOUSE_OVER_ZOOM_IN_OUT_FILTER = "mouse-over-zoom-in-out-filter";

    private static final Duration DEFAULT_ZOOM_IN_DURATION = Duration.millis(200);
    private static final Duration DEFAULT_ZOOM_OUT_DURATION = Duration.millis(400);
    private static final double DEFAULT_SCALE_ZOOMED_IN = 1.5;
    private static final double DEFAULT_SCALE_ZOOMED_OUT = 1;
    private static final Predicate<? super Node> DEFAULT_FILTER = n -> true;

    private static final EventHandler<MouseEvent> MOUSE_ENTERED_HANDLER = mouseEvent ->
        EventExtensions.<Node>getEventSourceFrom(mouseEvent)
            .filter(node -> getFilter(node).test(node))
            .ifPresent(node -> {
                ScaleTransition transition = new ScaleTransition(getZoomInDuration(node), node);
                transition.setToX(getScaleXZoomedIn(node));
                transition.setToY(getScaleYZoomedIn(node));
                transition.setToZ(getScaleZZoomedIn(node));
                transition.play();
            });

    private static final EventHandler<MouseEvent> MOUSE_EXITED_HANDLER = mouseEvent ->
        EventExtensions.<Node>getEventSourceFrom(mouseEvent)
            .filter(node -> getFilter(node).test(node))
            .ifPresent(node -> {
                ScaleTransition transition = new ScaleTransition(getZoomOutDuration(node), node);
                transition.setToX(getScaleXZoomedOut(node));
                transition.setToY(getScaleYZoomedOut(node));
                transition.setToZ(getScaleZZoomedOut(node));
                transition.play();
            });

    private MouseOverZoomInOut() {}

    /**
     * Gets the value to indicate whether the function of zoom-in/zoom-out over the mouse is enable.
     * @param node the target node.
     * @return true if the function of zoom-in/zoom-out over the mouse is enable, otherwise false.
     */
    public static boolean isEnable(Node node) {
        return NodeExtensions.<Boolean>getProperty(Objects.requireNonNull(node), MOUSE_OVER_ZOOM_IN_OUT_ENABLE).orElse(false);
    }

    /**
     * Sets the value to indicate whether the function of zoom-in/zoom-out over the mouse is enable.
     * @param node the target node.
     * @param enable true if the function of zoom-in/zoom-out over the mouse is enable, otherwise false.
     */
    public static void setEnable(Node node, boolean enable) {
        Objects.requireNonNull(node).getProperties().put(MOUSE_OVER_ZOOM_IN_OUT_ENABLE, enable);

        if (enable) {
            node.addEventFilter(MouseEvent.MOUSE_ENTERED, MOUSE_ENTERED_HANDLER);
            node.addEventFilter(MouseEvent.MOUSE_EXITED, MOUSE_EXITED_HANDLER);
        } else {
            node.removeEventFilter(MouseEvent.MOUSE_ENTERED, MOUSE_ENTERED_HANDLER);
            node.removeEventFilter(MouseEvent.MOUSE_EXITED, MOUSE_EXITED_HANDLER);
        }
    }

    /**
     * Gets the duration for which the node zooms in.
     * @param node the target node.
     * @return the duration for which the node zooms in.
     */
    public static Duration getZoomInDuration(Node node) {
        return NodeExtensions.<Duration>getProperty(Objects.requireNonNull(node), MOUSE_OVER_ZOOM_IN_OUT_ZOOM_IN_DURATION).orElse(DEFAULT_ZOOM_IN_DURATION);
    }

    /**
     * Sets the duration for which the node zooms in.
     * @param node the target node.
     * @param duration the duration for which the node zooms in.
     */
    public static void setZoomInDuration(Node node, Duration duration) {
        Objects.requireNonNull(node).getProperties().put(MOUSE_OVER_ZOOM_IN_OUT_ZOOM_IN_DURATION, duration);
    }

    /**
     * Gets the duration for which the node zooms out.
     * @param node the target node.
     * @return the duration for which the node zooms out.
     */
    public static Duration getZoomOutDuration(Node node) {
        return NodeExtensions.<Duration>getProperty(Objects.requireNonNull(node), MOUSE_OVER_ZOOM_IN_OUT_ZOOM_OUT_DURATION).orElse(DEFAULT_ZOOM_OUT_DURATION);
    }

    /**
     * Sets the duration for which the node zooms out.
     * @param node the target node.
     * @param duration the duration for which the node zooms out.
     */
    public static void setZoomOutDuration(Node node, Duration duration) {
        Objects.requireNonNull(node).getProperties().put(MOUSE_OVER_ZOOM_IN_OUT_ZOOM_OUT_DURATION, duration);
    }

    /**
     * Gets the scaleX of the specified node when the zoom-in is finished.
     * @param node the target node.
     * @return the scaleX of the specified node when the zoom-in is finished.
     */
    public static double getScaleXZoomedIn(Node node) {
        return NodeExtensions.<Double>getProperty(Objects.requireNonNull(node), MOUSE_OVER_ZOOM_IN_OUT_SCALE_X_ZOOMED_IN).orElse(DEFAULT_SCALE_ZOOMED_IN);
    }

    /**
     * Sets the scaleX of the specified node when the zoom-in is finished.
     * @param node the target node.
     * @param scaleX the scaleX of the specified node when the zoom-in is finished.
     */
    public static void setScaleXZoomedIn(Node node, double scaleX) {
        Objects.requireNonNull(node).getProperties().put(MOUSE_OVER_ZOOM_IN_OUT_SCALE_X_ZOOMED_IN, scaleX);
    }

    /**
     * Gets the scaleY of the specified node when the zoom-in is finished.
     * @param node the target node.
     * @return the scaleY of the specified node when the zoom-in is finished.
     */
    public static double getScaleYZoomedIn(Node node) {
        return NodeExtensions.<Double>getProperty(Objects.requireNonNull(node), MOUSE_OVER_ZOOM_IN_OUT_SCALE_Y_ZOOMED_IN).orElse(DEFAULT_SCALE_ZOOMED_IN);
    }

    /**
     * Sets the scaleY of the specified node when the zoom-in is finished.
     * @param node the target node.
     * @param scaleY the scaleY of the specified node when the zoom-in is finished.
     */
    public static void setScaleYZoomedIn(Node node, double scaleY) {
        Objects.requireNonNull(node).getProperties().put(MOUSE_OVER_ZOOM_IN_OUT_SCALE_Y_ZOOMED_IN, scaleY);
    }

    /**
     * Gets the scaleZ of the specified node when the zoom-in is finished.
     * @param node the target node.
     * @return the scaleZ of the specified node when the zoom-in is finished.
     */
    public static double getScaleZZoomedIn(Node node) {
        return NodeExtensions.<Double>getProperty(Objects.requireNonNull(node), MOUSE_OVER_ZOOM_IN_OUT_SCALE_Z_ZOOMED_IN).orElse(DEFAULT_SCALE_ZOOMED_IN);
    }

    /**
     * Sets the scaleZ of the specified node when the zoom-in is finished.
     * @param node the target node.
     * @param scaleZ the scaleZ of the specified node when the zoom-in is finished.
     */
    public static void setScaleZZoomedIn(Node node, double scaleZ) {
        Objects.requireNonNull(node).getProperties().put(MOUSE_OVER_ZOOM_IN_OUT_SCALE_Z_ZOOMED_IN, scaleZ);
    }

    /**
     * Gets the scaleX of the specified node when the zoom-out is finished.
     * @param node the target node.
     * @return the scaleX of the specified node when the zoom-out is finished.
     */
    public static double getScaleXZoomedOut(Node node) {
        return NodeExtensions.<Double>getProperty(Objects.requireNonNull(node), MOUSE_OVER_ZOOM_IN_OUT_SCALE_X_ZOOMED_OUT).orElse(DEFAULT_SCALE_ZOOMED_OUT);
    }

    /**
     * Sets the scaleX of the specified node when the zoom-out is finished.
     * @param node the target node.
     * @param scaleX the scaleX of the specified node when the zoom-out is finished.
     */
    public static void setScaleXZoomedOut(Node node, double scaleX) {
        Objects.requireNonNull(node).getProperties().put(MOUSE_OVER_ZOOM_IN_OUT_SCALE_X_ZOOMED_OUT, scaleX);
    }

    /**
     * Gets the scaleY of the specified node when the zoom-out is finished.
     * @param node the target node.
     * @return the scaleY of the specified node when the zoom-out is finished.
     */
    public static double getScaleYZoomedOut(Node node) {
        return NodeExtensions.<Double>getProperty(Objects.requireNonNull(node), MOUSE_OVER_ZOOM_IN_OUT_SCALE_Y_ZOOMED_OUT).orElse(DEFAULT_SCALE_ZOOMED_OUT);
    }

    /**
     * Sets the scaleY of the specified node when the zoom-out is finished.
     * @param node the target node.
     * @param scaleY the scaleY of the specified node when the zoom-out is finished.
     */
    public static void setScaleYZoomedOut(Node node, double scaleY) {
        Objects.requireNonNull(node).getProperties().put(MOUSE_OVER_ZOOM_IN_OUT_SCALE_Y_ZOOMED_OUT, scaleY);
    }

    /**
     * Gets the scaleZ of the specified node when the zoom-out is finished.
     * @param node the target node.
     * @return the scaleZ of the specified node when the zoom-out is finished.
     */
    public static double getScaleZZoomedOut(Node node) {
        return NodeExtensions.<Double>getProperty(Objects.requireNonNull(node), MOUSE_OVER_ZOOM_IN_OUT_SCALE_Z_ZOOMED_OUT).orElse(DEFAULT_SCALE_ZOOMED_OUT);
    }

    /**
     * Sets the scaleZ of the specified node when the zoom-out is finished.
     * @param node the target node.
     * @param scaleZ the scaleZ of the specified node when the zoom-out is finished.
     */
    public static void setScaleZZoomedOut(Node node, double scaleZ) {
        Objects.requireNonNull(node).getProperties().put(MOUSE_OVER_ZOOM_IN_OUT_SCALE_Z_ZOOMED_OUT, scaleZ);
    }

    /**
     * Gets the filter to decide whether the zoom-in/zoom-out can be executed.
     * @param node the target node.
     * @return the filter to decide whether the zoom-in/zoom-out can be executed.
     */
    public static Predicate<? super Node> getFilter(Node node) {
        return NodeExtensions.<Predicate<? super Node>>getProperty(Objects.requireNonNull(node), MOUSE_OVER_ZOOM_IN_OUT_FILTER).orElse(DEFAULT_FILTER);
    }

    /**
     * Sets the filter to decide whether the zoom-in/zoom-out can be executed.
     * @param node the target node.
     * @param filter the filter to decide whether the zoom-in/zoom-out can be executed.
     */
    public static void setFilter(Node node, Predicate<? super Node> filter) {
        Objects.requireNonNull(node).getProperties().put(MOUSE_OVER_ZOOM_IN_OUT_FILTER, filter);
    }

    /**
     * Constructs a new instance of the Builder with the specified node.
     * @param node the target node.
     * @return a instance of the Builder.
     */
    public static Builder of(Node node) {
        return new Builder(Objects.requireNonNull(node));
    }

    /**
     * Builds properties of the MouseOverZoomInOut.
     */
    public static final class Builder {
        private final Node node;

        private Duration zoomInDuration;
        private Duration zoomOutDuration;
        private double scaleXZoomedIn = Double.NaN;
        private double scaleYZoomedIn = Double.NaN;
        private double scaleZZoomedIn = Double.NaN;
        private double scaleXZoomedOut = Double.NaN;
        private double scaleYZoomedOut = Double.NaN;
        private double scaleZZoomedOut = Double.NaN;
        private Predicate<? super Node> filter;

        private Builder(Node node) {
            this.node = node;
        }

        /**
         * Sets the duration for which the node zooms in.
         * @param duration the duration for which the node zooms in.
         * @return the instance of this class.
         */
        public Builder zoomInFor(Duration duration) {
            this.zoomInDuration = duration;
            return this;
        }

        /**
         * Sets the duration for which the node zooms out.
         * @param duration the duration for which the node zooms out.
         * @return the instance of this class.
         */
        public Builder zoomOutFor(Duration duration) {
            this.zoomOutDuration = duration;
            return this;
        }

        /**
         * Sets the scaleX of the node when the zoom-in is finished.
         * @param scale the scaleX of the node when the zoom-in is finished.
         * @return the instance of this class.
         */
        public Builder zoomInXUntil(double scale) {
            this.scaleXZoomedIn = scale;
            return this;
        }

        /**
         * Sets the scaleY of the node when the zoom-in is finished.
         * @param scale the scaleY of the node when the zoom-in is finished.
         * @return the instance of this class.
         */
        public Builder zoomInYUntil(double scale) {
            this.scaleYZoomedIn = scale;
            return this;
        }

        /**
         * Sets the scaleZ of the node when the zoom-in is finished.
         * @param scale the scaleZ of the node when the zoom-in is finished.
         * @return the instance of this class.
         */
        public Builder zoomInZUntil(double scale) {
            this.scaleZZoomedIn = scale;
            return this;
        }

        /**
         * Sets the scaleX of the node when the zoom-out is finished.
         * @param scale the scaleX of the node when the zoom-out is finished.
         * @return the instance of this class.
         */
        public Builder zoomOutXUntil(double scale) {
            this.scaleXZoomedOut = scale;
            return this;
        }

        /**
         * Sets the scaleY of the node when the zoom-out is finished.
         * @param scale the scaleY of the node when the zoom-out is finished.
         * @return the instance of this class.
         */
        public Builder zoomOutYUntil(double scale) {
            this.scaleYZoomedOut = scale;
            return this;
        }

        /**
         * Sets the scaleZ of the node when the zoom-out is finished.
         * @param scale the scaleZ of the node when the zoom-out is finished.
         * @return the instance of this class.
         */
        public Builder zoomOutZUntil(double scale) {
            this.scaleZZoomedOut = scale;
            return this;
        }

        /**
         * Sets the filter to decide whether the zoom-in/zoom-out can be executed.
         * @param filter the filter to decide whether the zoom-in/zoom-out can be executed.
         * @return the instance of this class.
         */
        public Builder with(Predicate<? super Node> filter) {
            this.filter = filter;
            return this;
        }

        /**
         * Enables the function of zoom-in/zoom-out.
         */
        public void enable() {
            if (zoomInDuration != null) { MouseOverZoomInOut.setZoomInDuration(node, zoomInDuration); }
            if (zoomOutDuration != null) { MouseOverZoomInOut.setZoomOutDuration(node, zoomOutDuration); }
            if (!Double.isNaN(scaleXZoomedIn)) { MouseOverZoomInOut.setScaleXZoomedIn(node, scaleXZoomedIn); }
            if (!Double.isNaN(scaleYZoomedIn)) { MouseOverZoomInOut.setScaleYZoomedIn(node, scaleYZoomedIn); }
            if (!Double.isNaN(scaleZZoomedIn)) { MouseOverZoomInOut.setScaleZZoomedIn(node, scaleZZoomedIn); }
            if (!Double.isNaN(scaleXZoomedOut)) { MouseOverZoomInOut.setScaleXZoomedOut(node, scaleXZoomedOut); }
            if (!Double.isNaN(scaleYZoomedOut)) { MouseOverZoomInOut.setScaleYZoomedOut(node, scaleYZoomedOut); }
            if (!Double.isNaN(scaleZZoomedOut)) { MouseOverZoomInOut.setScaleZZoomedOut(node, scaleZZoomedOut); }
            if (filter != null) { MouseOverZoomInOut.setFilter(node, filter); }
            MouseOverZoomInOut.setEnable(node, true);
        }
    }
}
