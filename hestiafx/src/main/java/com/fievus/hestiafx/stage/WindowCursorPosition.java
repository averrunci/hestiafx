/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.stage;

import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.stage.Window;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * Represents a cursor position to indicate the direction of a resize on a window.
 */
public enum WindowCursorPosition {
    /**
     * The north cursor position.
     */
    NORTH {
        @Override
        public Cursor getCursor() {
            return Cursor.N_RESIZE;
        }

        @Override
        public Optional<WindowResizer.Builder> windowResizerOf(Window window) {
            return Optional.of(WindowNorthResizer.of(Objects.requireNonNull(window)));
        }
    },
    /**
     * The east cursor position.
     */
    EAST {
        @Override
        public Cursor getCursor() {
            return Cursor.E_RESIZE;
        }

        @Override
        public Optional<WindowResizer.Builder> windowResizerOf(Window window) {
            return Optional.of(WindowEastResizer.of(Objects.requireNonNull(window)));
        }
    },
    /**
     * The south cursor position.
     */
    SOUTH {
        @Override
        public Cursor getCursor() {
            return Cursor.S_RESIZE;
        }

        @Override
        public Optional<WindowResizer.Builder> windowResizerOf(Window window) {
            return Optional.of(WindowSouthResizer.of(Objects.requireNonNull(window)));
        }
    },
    /**
     * The west cursor position.
     */
    WEST {
        @Override
        public Cursor getCursor() {
            return Cursor.W_RESIZE;
        }

        @Override
        public Optional<WindowResizer.Builder> windowResizerOf(Window window) {
            return Optional.of(WindowWestResizer.of(Objects.requireNonNull(window)));
        }
    },
    /**
     * The north-east cursor position.
     */
    NORTH_EAST {
        @Override
        public Cursor getCursor() {
            return Cursor.NE_RESIZE;
        }

        @Override
        public Optional<WindowResizer.Builder> windowResizerOf(Window window) {
            return Optional.of(WindowNorthEastResizer.of(Objects.requireNonNull(window)));
        }
    },
    /**
     * The south-east cursor position.
     */
    SOUTH_EAST {
        @Override
        public Cursor getCursor() {
            return Cursor.SE_RESIZE;
        }

        @Override
        public Optional<WindowResizer.Builder> windowResizerOf(Window window) {
            return Optional.of(WindowSouthEastResizer.of(Objects.requireNonNull(window)));
        }
    },
    /**
     * The south-west cursor position.
     */
    SOUTH_WEST {
        @Override
        public Cursor getCursor() {
            return Cursor.SW_RESIZE;
        }

        @Override
        public Optional<WindowResizer.Builder> windowResizerOf(Window window) {
            return Optional.of(WindowSouthWestResizer.of(Objects.requireNonNull(window)));
        }
    },
    /**
     * The north-west cursor position.
     */
    NORTH_WEST {
        @Override
        public Cursor getCursor() {
            return Cursor.NW_RESIZE;
        }

        @Override
        public Optional<WindowResizer.Builder> windowResizerOf(Window window) {
            return Optional.of(WindowNorthWestResizer.of(Objects.requireNonNull(window)));
        }
    },
    /**
     * The center cursor position.
     */
    CENTER {
        @Override
        public Cursor getCursor() {
            return Cursor.DEFAULT;
        }

        @Override
        public Optional<WindowResizer.Builder> windowResizerOf(Window window) {
            return Optional.empty();
        }
    };

    /**
     * Constructs a new instance of the Builder with the specified window.
     * @param window the window.
     * @return an instance of the Builder.
     */
    public static Builder of(Window window) {
        return new Builder(Objects.requireNonNull(window));
    }

    /**
     * Gets the mouse cursor depending on the cursor position.
     * @return the mouse cursor depending on the cursor position.
     */
    public abstract Cursor getCursor();

    /**
     * Constructs a new instance of the WindowResizer.Builder with the specified window.
     * @param window the window.
     * @return an instance of the WindowResizer.Builder to build the WindowResizer or
     *         Optional.empty() if the window can not be resized at the cursor position,
     *         that is WindowCursorPosition.CENTER.
     */
    public abstract Optional<WindowResizer.Builder> windowResizerOf(Window window);

    /**
     * Builds the WindowCursorPosition with the specified properties.
     */
    public static final class Builder {
        private static final int NORTH_INDEX = 1;
        private static final int EAST_INDEX = 2;
        private static final int SOUTH_INDEX = 4;
        private static final int WEST_INDEX = 8;

        private static final Map<Integer, WindowCursorPosition> windowCursorPositions = new HashMap<>();

        private final double windowMinX;
        private final double windowMinY;
        private final double windowMaxX;
        private final double windowMaxY;

        private Insets resizeEdge = new Insets(10);

        static {
            windowCursorPositions.put(NORTH_INDEX, NORTH);
            windowCursorPositions.put(EAST_INDEX, EAST);
            windowCursorPositions.put(NORTH_INDEX | EAST_INDEX, NORTH_EAST);
            windowCursorPositions.put(SOUTH_INDEX, SOUTH);
            windowCursorPositions.put(SOUTH_INDEX | EAST_INDEX, SOUTH_EAST);
            windowCursorPositions.put(WEST_INDEX, WEST);
            windowCursorPositions.put(NORTH_INDEX | WEST_INDEX, NORTH_WEST);
            windowCursorPositions.put(SOUTH_INDEX | WEST_INDEX, SOUTH_WEST);
        }

        private Builder(Window window) {
            windowMinX = window.getX();
            windowMinY = window.getY();
            windowMaxX = window.getX() + window.getWidth();
            windowMaxY = window.getY() + window.getHeight();
        }

        /**
         * Sets the edge in which the window can be resized.
         * The window can be resized when the mouse cursor is in it.
         * @param resizeEdge the edge in which the window can be resized.
         * @return the instance of this class.
         */
        public Builder in(Insets resizeEdge) {
            this.resizeEdge = Objects.requireNonNull(resizeEdge);
            return this;
        }

        /**
         * Gets the instance of the WindowCursorPosition with the specified mouse location on a screen.
         * @param screenX the X coordinate location of the mouse on a screen.
         * @param screenY the Y coordinate location of the mouse on a screen.
         * @return the instance of the WindowCursorPosition.
         */
        public WindowCursorPosition at(double screenX, double screenY) {
            int position = 0;

            if (inNorthZone(screenX, screenY)) { position |= NORTH_INDEX; }
            if (inEastZone(screenX, screenY)) { position |= EAST_INDEX; }
            if (inSouthZone(screenX, screenY)) { position |= SOUTH_INDEX; }
            if (inWestZone(screenX, screenY)) { position |= WEST_INDEX; }

            return windowCursorPositions.getOrDefault(position, CENTER);
        }

        private boolean inWindow(double x, double y) {
            return windowMinX <= x && x <= windowMaxX && windowMinY <= y && y <= windowMaxY;
        }

        private boolean inNorthZone(double x, double y) {
            return inWindow(x, y) && windowMinY <= y && y <= windowMinY + resizeEdge.getTop();
        }

        private boolean inEastZone(double x, double y) {
            return inWindow(x, y) && windowMaxX - resizeEdge.getRight() <= x && x <= windowMaxX;
        }

        private boolean inSouthZone(double x, double y) {
            return inWindow(x, y) && windowMaxY - resizeEdge.getBottom() <= y && y <= windowMaxY;
        }

        private boolean inWestZone(double x, double y) {
            return inWindow(x, y) && windowMinX <= x && x <= windowMinX + resizeEdge.getLeft();
        }
    }
}
