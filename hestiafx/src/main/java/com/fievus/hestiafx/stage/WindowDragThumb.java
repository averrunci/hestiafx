/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.stage;

import com.fievus.hestiafx.event.EventExtensions;
import com.fievus.hestiafx.scene.NodeExtensions;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

import java.util.Objects;

/**
 * Provides the function of dragging for a window when a target node is dragged.
 */
public final class WindowDragThumb {
    private static final String WINDOW_DRAG_THUMB_ENABLE = "window-drag-thumb-enable";
    private static final String WINDOW_DRAG_THUMB_WINDOW_RELOCATOR = "window-drag-thumb-window-relocator";
    private static final String WINDOW_DRAG_THUMB_PADDING = "window-drag-thumb-padding";

    private static final EventHandler<MouseEvent> MOUSE_PRESSED_HANDLER = mouseEvent ->
        EventExtensions.<Node>getEventSourceFrom(mouseEvent)
            .filter(node ->
                isEnable(node) && canRelocate(node, mouseEvent.getX(), mouseEvent.getY()) &&
                node.getScene() != null && node.getScene().getWindow() != null
            )
            .ifPresent(node -> {
                node.getProperties().put(
                    WINDOW_DRAG_THUMB_WINDOW_RELOCATOR,
                    WindowRelocator.of(node.getScene().getWindow())
                        .at(mouseEvent.getScreenX(), mouseEvent.getScreenY())
                );
                mouseEvent.consume();
            });

    private static final EventHandler<MouseEvent> MOUSE_DRAGGED_HANDLER = mouseEvent ->
        EventExtensions.<Node>getEventSourceFrom(mouseEvent).ifPresent(node ->
            NodeExtensions.<WindowRelocator>getProperty(node, WINDOW_DRAG_THUMB_WINDOW_RELOCATOR)
                .ifPresent(relocator -> {
                    relocator.relocateFor(mouseEvent.getScreenX(), mouseEvent.getScreenY());
                    mouseEvent.consume();
                })
        );

    private static final EventHandler<MouseEvent> MOUSE_RELEASED_HANDLER = mouseEvent ->
        EventExtensions.<Node>getEventSourceFrom(mouseEvent).ifPresent(node ->
            NodeExtensions.<WindowRelocator>getProperty(node, WINDOW_DRAG_THUMB_WINDOW_RELOCATOR)
                .ifPresent( relocator -> {
                    node.getProperties().remove(WINDOW_DRAG_THUMB_WINDOW_RELOCATOR);
                    mouseEvent.consume();
                })
        );

    private WindowDragThumb() {}

    /**
     * Gets the value to indicate whether the dragging of the window is enable when the specified node is dragged.
     * @param node the target node.
     * @return true if the dragging of the window is enable when the specified node is dragged, otherwise false.
     */
    public static boolean isEnable(Node node) {
        return NodeExtensions.<Boolean>getProperty(Objects.requireNonNull(node), WINDOW_DRAG_THUMB_ENABLE).orElse(false);
    }

    /**
     * Sets the value to indicate whether the dragging of the window is enable when the specified node is dragged.
     * @param node the target node.
     * @param enable true if the dragging of the window is enable when the specified node is dragged, otherwise false.
     */
    public static void setEnable(Node node, boolean enable) {
        Objects.requireNonNull(node).getProperties().put(WINDOW_DRAG_THUMB_ENABLE, enable);

        if (enable) {
            node.addEventHandler(MouseEvent.MOUSE_PRESSED, MOUSE_PRESSED_HANDLER);
            node.addEventHandler(MouseEvent.MOUSE_DRAGGED, MOUSE_DRAGGED_HANDLER);
            node.addEventHandler(MouseEvent.MOUSE_RELEASED, MOUSE_RELEASED_HANDLER);
        } else {
            node.removeEventHandler(MouseEvent.MOUSE_PRESSED, MOUSE_PRESSED_HANDLER);
            node.removeEventHandler(MouseEvent.MOUSE_DRAGGED, MOUSE_DRAGGED_HANDLER);
            node.removeEventHandler(MouseEvent.MOUSE_RELEASED, MOUSE_RELEASED_HANDLER);
        }
    }

    /**
     * Gets the padding in which the window cannot be dragged.
     * @param node the target node.
     * @return the padding in which the window cannot be dragged.
     */
    public static Insets getPadding(Node node) {
        return NodeExtensions.<Insets>getProperty(Objects.requireNonNull(node), WINDOW_DRAG_THUMB_PADDING).orElse(Insets.EMPTY);
    }

    /**
     * Sets the padding in which the window cannot be dragged.
     * @param node the target node.
     * @param padding the padding in which the window cannot be dragged.
     */
    public static void setPadding(Node node, Insets padding) {
        Objects.requireNonNull(node).getProperties().put(WINDOW_DRAG_THUMB_PADDING, padding);
    }

    /**
     * Constructs a new instance of the Builder with the specified target node.
     * @param node the target node.
     * @return an instance of the Builder.
     */
    public static Builder to(Node node) {
        return new Builder(Objects.requireNonNull(node));
    }

    private static boolean canRelocate(Node node, double x, double y) {
        Bounds nodeBounds = node.getLayoutBounds();
        Insets padding = getPadding(node);
        return nodeBounds.getMinX() + padding.getLeft() < x && x < nodeBounds.getMaxX() - padding.getRight() &&
            nodeBounds.getMinY() + padding.getTop() < y && y < nodeBounds.getMaxY() - padding.getBottom();
    }

    /**
     * Builds properties of the WindowDragThumb.
     */
    public static final class Builder {
        private final Node node;
        private Insets padding;

        private Builder(Node node) {
            this.node = node;
        }

        /**
         * Sets the padding in which the window cannot be dragged.
         * @param padding the padding in which the window cannot be dragged.
         * @return the instance of this class.
         */
        public Builder padBy(Insets padding) {
            this.padding = Objects.requireNonNull(padding);
            return this;
        }

        /**
         * Enables the dragging of the window when the target node is dragged.
         */
        public void enable() {
            if (padding != null) { WindowDragThumb.setPadding(node, padding); }
            WindowDragThumb.setEnable(node, true);
        }
    }
}
