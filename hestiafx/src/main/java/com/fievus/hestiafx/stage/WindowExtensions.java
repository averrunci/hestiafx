/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.stage;

import javafx.stage.Window;

import java.lang.invoke.MethodHandles;
import java.util.Optional;
import java.util.logging.Logger;

/**
 * Utility class that contains of static methods for performing the basic operations for the window.
 */
public final class WindowExtensions {
    private static final Logger LOGGER = Logger.getLogger(MethodHandles.lookup().lookupClass().getCanonicalName());

    private WindowExtensions() {}

    /**
     * Gets the property value of the specified window and property key.
     * @param window the window that contains the property.
     * @param key the key of the property.
     * @param <T> the type of the property.
     * @return the property value or Optional.empty() if the property is not found.
     */
    @SuppressWarnings("unchecked")
    public static <T> Optional<T> getProperty(Window window, String key) {
        if (!window.hasProperties()) { return Optional.empty(); }
        if (!window.getProperties().containsKey(key)) { return Optional.empty(); }

        Object property = window.getProperties().get(key);
        if (property == null) { return Optional.empty(); }

        try {
            return Optional.of((T)property);
        } catch (ClassCastException e) {
            LOGGER.severe(String.format("The property(key: %1$s, value: %2$s) of %3$s is not valid.", key, property, window));
            return Optional.empty();
        }
    }
}
