/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.stage;

import javafx.stage.Window;

import java.util.Objects;

/**
 * Relocates the window.
 */
final class WindowRelocator {
    private final Window window;

    private double currentScreenX;
    private double currentScreenY;

    private WindowRelocator(Builder builder) {
        window = Objects.requireNonNull(builder).window;
        currentScreenX = builder.screenX;
        currentScreenY = builder.screenY;
    }

    /**
     * Constructs a new instance of the Builder with the specified target window.
     * @param window the target window.
     * @return an instance of the Builder.
     */
    public static Builder of(Window window) {
        return new Builder(Objects.requireNonNull(window));
    }

    /**
     * Relocates the window with the specified location on a screen.
     * @param screenX the X coordinate location on a screen.
     * @param screenY the Y coordinate location on a screen.
     */
    public void relocateFor(double screenX, double screenY) {
        relocateWindowBy(screenX - currentScreenX, screenY - currentScreenY);
        updateLocation(screenX, screenY);
    }

    private void relocateWindowBy(double incrementedX, double incrementedY) {
        relocateWindowTo(window.getX() + incrementedX, window.getY() + incrementedY);
    }

    private void relocateWindowTo(double newX, double newY) {
        window.setX(newX);
        window.setY(newY);
    }

    private void updateLocation(double screenX, double screenY) {
        currentScreenX = screenX;
        currentScreenY = screenY;
    }

    /**
     * Builds an instance of the WindowRelocator.
     */
    public static final class Builder {
        private final Window window;

        private double screenX;
        private double screenY;

        private Builder(Window window) {
            this.window = Objects.requireNonNull(window);
        }

        /**
         * Constructs a new instance of the WindowRelocator with the specified location on a screen.
         * @param screenX the X coordinate location on a screen.
         * @param screenY the Y coordinate location on a screen.
         * @return an instance of the WindowRelocator.
         */
        public WindowRelocator at(double screenX, double screenY) {
            this.screenX = screenX;
            this.screenY = screenY;
            return new WindowRelocator(this);
        }
    }
}
