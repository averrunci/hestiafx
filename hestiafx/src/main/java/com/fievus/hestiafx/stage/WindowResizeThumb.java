/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.stage;

import com.fievus.hestiafx.event.EventExtensions;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.stage.Window;

import java.util.Objects;

/**
 * Provides the function of resizing the window.
 */
public final class WindowResizeThumb {
    private static final String WINDOW_RESIZE_THUMB_ENABLE = "window-resize-thumb-enable";
    private static final String WINDOW_RESIZE_THUMB_WINDOW_RESIZER = "window-drag-thumb-window-resizer";
    private static final String WINDOW_RESIZE_THUMB_RESIZE_EDGE = "window-drag-thumb-resize-edge";

    private static final Insets DEFAULT_RESIZE_EDGE = new Insets(10);

    private static final EventHandler<MouseEvent> MOUSE_MOVED_HANDLER = mouseEvent ->
        EventExtensions.<Window>getEventSourceFrom(mouseEvent).ifPresent(window ->
            window.getScene().setCursor(
                WindowCursorPosition.of(window)
                    .in(getResizeEdge(window))
                    .at(mouseEvent.getScreenX(), mouseEvent.getScreenY())
                    .getCursor()
            )
        );

    private static final EventHandler<MouseEvent> MOUSE_EXITED_TARGET_HANDLER = mouseEvent ->
        EventExtensions.<Window>getEventSourceFrom(mouseEvent).ifPresent(window ->
            window.getScene().setCursor(Cursor.DEFAULT)
        );

    private static final EventHandler<MouseEvent> MOUSE_PRESSED_HANDLER = mouseEvent ->
        EventExtensions.<Window>getEventSourceFrom(mouseEvent).ifPresent(window ->
            WindowCursorPosition.of(window)
                .in(getResizeEdge(window))
                .at(mouseEvent.getScreenX(), mouseEvent.getScreenY())
                .windowResizerOf(window)
                .ifPresent(resizer -> {
                    window.getProperties().put(
                        WINDOW_RESIZE_THUMB_WINDOW_RESIZER,
                        resizer.at(mouseEvent.getScreenX(), mouseEvent.getScreenY())
                    );
                    mouseEvent.consume();
                })
        );

    private static final EventHandler<MouseEvent> MOUSE_DRAGGED_HANDLER = mouseEvent ->
        EventExtensions.<Window>getEventSourceFrom(mouseEvent).ifPresent(window ->
            WindowExtensions.<WindowResizer>getProperty(window, WINDOW_RESIZE_THUMB_WINDOW_RESIZER)
                .ifPresent(resizer -> {
                    resizer.resizeFor(mouseEvent.getScreenX(), mouseEvent.getScreenY());
                    mouseEvent.consume();
                })
        );

    private static final EventHandler<MouseEvent> MOUSE_RELEASED_HANDLER = mouseEvent ->
        EventExtensions.<Window>getEventSourceFrom(mouseEvent).ifPresent(window ->
            WindowExtensions.<WindowResizer>getProperty(window, WINDOW_RESIZE_THUMB_WINDOW_RESIZER)
                .ifPresent(resizer -> {
                    window.getProperties().remove(WINDOW_RESIZE_THUMB_WINDOW_RESIZER);
                    mouseEvent.consume();
                })
        );

    private WindowResizeThumb() {}

    /**
     * Gets the value to indicate whether the resizing the window that contains the specified node is enable.
     * @param node the target node.
     * @return true if the resizing the window that contains the specified node is enable, otherwise false.
     */
    public static boolean isEnable(Node node) {
        return isEnable(requireWindow(node));
    }

    /**
     * Gets the value to indicate whether the resizing the specified window is enable.
     * @param window the target window.
     * @return true if the resizing the specified window is enable, otherwise false.
     */
    public static boolean isEnable(Window window) {
        return WindowExtensions.<Boolean>getProperty(Objects.requireNonNull(window), WINDOW_RESIZE_THUMB_ENABLE).orElse(false);
    }

    /**
     * Sets the value to indicate whether the resizing the window that contains the specified node is enable.
     * @param node the target node.
     * @param enable true if the resizing the window that contains the specified node is enable, otherwise false.
     */
    public static void setEnable(Node node, boolean enable) {
        setEnable(requireWindow(node), enable);
    }

    /**
     * Sets the value to indicate whether the resizing the specified window is enable.
     * @param window the target window.
     * @param enable true if the resizing the specified window is enable, otherwise false.
     */
    public static void setEnable(Window window, boolean enable) {
        Objects.requireNonNull(window).getProperties().put(WINDOW_RESIZE_THUMB_ENABLE, enable);

        if (enable) {
            window.addEventFilter(MouseEvent.MOUSE_MOVED, MOUSE_MOVED_HANDLER);
            window.addEventFilter(MouseEvent.MOUSE_EXITED_TARGET, MOUSE_EXITED_TARGET_HANDLER);
            window.addEventFilter(MouseEvent.MOUSE_PRESSED, MOUSE_PRESSED_HANDLER);
            window.addEventFilter(MouseEvent.MOUSE_DRAGGED, MOUSE_DRAGGED_HANDLER);
            window.addEventFilter(MouseEvent.MOUSE_RELEASED, MOUSE_RELEASED_HANDLER);
        } else {
            window.removeEventFilter(MouseEvent.MOUSE_MOVED, MOUSE_MOVED_HANDLER);
            window.removeEventFilter(MouseEvent.MOUSE_EXITED_TARGET, MOUSE_EXITED_TARGET_HANDLER);
            window.removeEventFilter(MouseEvent.MOUSE_PRESSED, MOUSE_PRESSED_HANDLER);
            window.removeEventFilter(MouseEvent.MOUSE_DRAGGED, MOUSE_DRAGGED_HANDLER);
            window.removeEventFilter(MouseEvent.MOUSE_RELEASED, MOUSE_RELEASED_HANDLER);
        }
    }

    /**
     * Gets the edge in which the window that contains the specified node can be resized.
     * @param node the target node
     * @return the edge in which the window that contains the specified node can be resized.
     */
    public static Insets getResizeEdge(Node node) {
        return getResizeEdge(requireWindow(node));
    }

    /**
     * Gets the edge in which the specified window can be resized.
     * @param window the target window.
     * @return the edge in which the specified window can be resized.
     */
    public static Insets getResizeEdge(Window window) {
        return WindowExtensions.<Insets>getProperty(window, WINDOW_RESIZE_THUMB_RESIZE_EDGE).orElse(DEFAULT_RESIZE_EDGE);
    }

    /**
     * Sets the edge in which the window that contains the specified node can be resized.
     * @param node the target node.
     * @param resizeEdge the edge in which the window that contains the specified node can be resized.
     */
    public static void setResizeEdge(Node node, Insets resizeEdge) {
        setResizeEdge(requireWindow(node), resizeEdge);
    }

    /**
     * Sets the edge in which the specified window can be resized.
     * @param window the target window.
     * @param resizeEdge the edge in which the specified window can be resized.
     */
    public static void setResizeEdge(Window window, Insets resizeEdge) {
        Objects.requireNonNull(window).getProperties().put(WINDOW_RESIZE_THUMB_RESIZE_EDGE, resizeEdge);
    }

    /**
     * Constructs a new instance of the Builder with the specified node that is on the target window.
     * @param node the target node.
     * @return an instance of the Builder.
     */
    public static Builder to(Node node) {
        return new Builder(Objects.requireNonNull(node));
    }

    /**
     * Constructs a new instance of the Builder with the specified target window.
     * @param window the target window.
     * @return an instance of the Builder.
     */
    public static Builder to(Window window) {
        return new Builder(Objects.requireNonNull(window));
    }

    private static Window requireWindow(Node node) {
        Objects.requireNonNull(node);
        if (node.getScene() == null) {
            throw new IllegalArgumentException("The node must be part of a scene.");
        }
        if (node.getScene().getWindow() == null) {
            throw new IllegalArgumentException("The window for the scene that the node is part of must be specified.");
        }
        return node.getScene().getWindow();
    }

    /**
     * Builds properties of the WindowResizeThumb.
     */
    public static final class Builder {
        private final Node node;
        private final Window window;

        private Insets resizeEdge;

        private Builder(Node node) {
            this.node = node;
            this.window = null;
        }

        private Builder(Window window) {
            this.node = null;
            this.window = window;
        }

        /**
         * Sets the edge in which the window can be resized.
         * @param resizeEdge the edge in which the window can be resized.
         * @return the instance of this class.
         */
        public Builder with(Insets resizeEdge) {
            this.resizeEdge = resizeEdge;
            return this;
        }

        /**
         * Enables the resizing of the window.
         */
        public void enable() {
            if (node != null) {
                if (resizeEdge != null) { WindowResizeThumb.setResizeEdge(node, resizeEdge); }
                WindowResizeThumb.setEnable(node, true);
            } else if (window != null) {
                if (resizeEdge != null) { WindowResizeThumb.setResizeEdge(window, resizeEdge); }
                WindowResizeThumb.setEnable(window, true);
            } else {
                throw new IllegalStateException("The node or window must not be null.");
            }
        }
    }
}
