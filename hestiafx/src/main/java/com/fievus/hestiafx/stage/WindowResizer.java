/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.stage;

import javafx.scene.control.PopupControl;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.util.Objects;

/**
 * Resizes the window.
 */
abstract class WindowResizer {
    private final Window window;

    private double currentScreenX;
    private double currentScreenY;

    protected WindowResizer(Builder builder) {
        window = Objects.requireNonNull(builder).window;
        currentScreenX = builder.screenX;
        currentScreenY = builder.screenY;
    }

    /**
     * Resizes the window with the specified location on a screen.
     * @param screenX the X coordinate location on a screen.
     * @param screenY the Y coordinate location on a screen.
     */
    public void resizeFor(double screenX, double screenY) {
        resizeWindowBy(screenX - currentScreenX, screenY - currentScreenY);
        updateLocation(screenX, screenY);
    }

    protected abstract void resizeWindowBy(double incrementedX, double incrementedY);

    protected void resizeWindowBy(double incrementedX, double incrementedY, double incrementedWidth, double incrementedHeight) {
        resizeWindowWidthTo(window.getX() + incrementedX, window.getWidth() + incrementedWidth);
        resizeWindowHeightTo(window.getY() + incrementedY, window.getHeight() + incrementedHeight);
    }

    protected void resizeWindowWidthTo(double newX, double newWidth) {
        if (!canResizeWidthTo(newWidth)) { return; }

        window.setX(newX);
        window.setWidth(newWidth);
    }

    protected void resizeWindowHeightTo(double newY, double newHeight) {
        if (!canResizeHeightTo(newHeight)) { return; }

        window.setY(newY);
        window.setHeight(newHeight);
    }

    protected boolean canResizeWidthTo(double newWidth) {
        if (window instanceof Stage) {
            Stage stage = (Stage)window;
            return stage.getMinWidth() < newWidth && newWidth < stage.getMaxWidth();
        }

        if (window instanceof PopupControl) {
            PopupControl popupControl = (PopupControl)window;
            return popupControl.getMinWidth() < newWidth && newWidth < popupControl.getMaxWidth();
        }

        return false;
    }

    protected boolean canResizeHeightTo(double newHeight) {
        if (window instanceof Stage) {
            Stage stage = (Stage)window;
            return stage.getMinHeight() < newHeight && newHeight < stage.getMaxHeight();
        }

        if (window instanceof PopupControl) {
            PopupControl popupControl = (PopupControl)window;
            return popupControl.getMinHeight() < newHeight && newHeight < popupControl.getMaxHeight();
        }

        return false;
    }

    protected void updateLocation(double screenX, double screenY) {
        this.currentScreenX = screenX;
        this.currentScreenY = screenY;
    }

    /**
     * Builds an instance of the WindowResizer.
     */
    protected static abstract class Builder {
        private final Window window;

        private double screenX;
        private double screenY;

        protected Builder(Window window) {
            this.window = Objects.requireNonNull(window);
        }

        /**
         * Constructs a new instance of the WindowResizer with the specified location on a screen.
         * @param screenX the X coordinate location on a screen.
         * @param screenY the Y coordinate location on a screen.
         * @return
         */
        public WindowResizer at(double screenX, double screenY) {
            this.screenX = screenX;
            this.screenY = screenY;
            return build();
        }

        protected abstract WindowResizer build();
    }
}
