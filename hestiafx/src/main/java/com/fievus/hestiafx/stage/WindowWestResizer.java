/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.stage;

import javafx.stage.Window;

import java.util.Objects;

/**
 * Resizes the window in the direction of the west.
 */
final class WindowWestResizer extends WindowResizer {
    private WindowWestResizer(Builder builder) {
        super(builder);
    }

    public static Builder of(Window window) {
        return new Builder(Objects.requireNonNull(window));
    }

    @Override
    protected void resizeWindowBy(double incrementedX, double incrementedY) {
        resizeWindowBy(incrementedX, 0, -incrementedX, 0);
    }

    public static final class Builder extends WindowResizer.Builder {
        private Builder(Window window) {
            super(window);
        }

        @Override
        protected WindowResizer build() {
            return new WindowWestResizer(this);
        }
    }
}
