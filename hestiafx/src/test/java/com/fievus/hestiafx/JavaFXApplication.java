/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

public class JavaFXApplication extends Application implements TestRule {
    private static final AtomicBoolean STARTED = new AtomicBoolean(false);
    private static final CountDownLatch START_SIGNAL = new CountDownLatch(1);

    @Override
    public Statement apply(Statement base, Description description) {
        return new JavaFXThreadStatement(base);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        START_SIGNAL.countDown();
    }

    private class JavaFXThreadStatement extends Statement {
        private final Statement statement;

        private Throwable rethrownException;

        public JavaFXThreadStatement(Statement statement) {
            this.statement = statement;
        }

        @Override
        public void evaluate() throws Throwable {
            if (!STARTED.getAndSet(true)) {
                launchJavaFXApplication();
            }

            CountDownLatch performSignal = new CountDownLatch(1);
            Platform.runLater(() -> {
                try {
                    statement.evaluate();
                } catch (Throwable e) {
                    rethrownException = e;
                }

                performSignal.countDown();
            });

            performSignal.await();

            if (rethrownException != null) { throw rethrownException; }
        }

        private void launchJavaFXApplication() throws InterruptedException {
            ExecutorService executor = Executors.newFixedThreadPool(1);
            executor.submit(() -> {
                try {
                    Application.launch(JavaFXApplication.class);
                } catch (Throwable e) {
                    e.printStackTrace();
                    START_SIGNAL.countDown();
                }
            });
            executor.shutdown();

            START_SIGNAL.await();
        }
    }
}
