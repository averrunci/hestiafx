/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.bean.property;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleObjectProperty;

final class DoubleNestedPlainObject {
    public ObjectProperty<NestedPlainObject> propertyWithNameProperty() { return propertyWithName; }
    private final ObjectProperty<NestedPlainObject> propertyWithName = new SimpleObjectProperty<>(this, "propertyWithName", new NestedPlainObject());

    public ObjectProperty<NestedPlainObject> propertyWithoutNameProperty() { return propertyWithoutName; }
    private final ObjectProperty<NestedPlainObject> propertyWithoutName = new SimpleObjectProperty<>(new NestedPlainObject());

    public ReadOnlyObjectProperty<NestedPlainObject> readOnlyPropertyWithNameProperty() { return readOnlyPropertyWithName.getReadOnlyProperty(); }
    private final ReadOnlyObjectWrapper<NestedPlainObject> readOnlyPropertyWithName = new ReadOnlyObjectWrapper<>(this, "readOnlyPropertyWithName", new NestedPlainObject());

    public ReadOnlyObjectProperty<NestedPlainObject> readOnlyPropertyWithoutNameProperty() { return readOnlyPropertyWithoutName.getReadOnlyProperty(); }
    private final ReadOnlyObjectWrapper<NestedPlainObject> readOnlyPropertyWithoutName = new ReadOnlyObjectWrapper<>(new NestedPlainObject());

    public NestedPlainObject getNestedPlainObject() { return nestedPlainObject; }
    private final NestedPlainObject nestedPlainObject = new NestedPlainObject();
}
