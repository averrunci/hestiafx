/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.bean.property;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleObjectProperty;

final class NestedPlainObject {
    public ObjectProperty<PlainObject> propertyWithNameProperty() { return propertyWithName; }
    private final ObjectProperty<PlainObject> propertyWithName = new SimpleObjectProperty<>(this, "propertyWithName", new PlainObject());

    public ObjectProperty<PlainObject> propertyWithoutNameProperty() { return propertyWithoutName; }
    private final ObjectProperty<PlainObject> propertyWithoutName = new SimpleObjectProperty<>(new PlainObject());

    public ReadOnlyObjectProperty<PlainObject> readOnlyPropertyWithNameProperty() { return readOnlyPropertyWithName.getReadOnlyProperty(); }
    private final ReadOnlyObjectWrapper<PlainObject> readOnlyPropertyWithName = new ReadOnlyObjectWrapper<>(this, "readOnlyPropertyWithName", new PlainObject());

    public ReadOnlyObjectProperty<PlainObject> readOnlyPropertyWithoutNameProperty() { return readOnlyPropertyWithoutName.getReadOnlyProperty(); }
    private final ReadOnlyObjectWrapper<PlainObject> readOnlyPropertyWithoutName = new ReadOnlyObjectWrapper<>(new PlainObject());

    public PlainObject getPlainObject() { return plainObject; }
    private final PlainObject plainObject = new PlainObject();
}
