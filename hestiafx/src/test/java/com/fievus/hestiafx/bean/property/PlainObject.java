/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.bean.property;

import javafx.beans.property.*;
import javafx.geometry.Orientation;

final class PlainObject {
    // ---------------------------------------------------------------------------------------------
    // property with name
    public StringProperty stringPropertyWithNameProperty() { return stringPropertyWithName; }
    private final StringProperty stringPropertyWithName = new SimpleStringProperty(this, "stringPropertyWithName", "String Property with Name");

    public BooleanProperty booleanPropertyWithNameProperty() { return booleanPropertyWithName; }
    private final BooleanProperty booleanPropertyWithName = new SimpleBooleanProperty(this, "booleanPropertyWithName", true);

    public IntegerProperty integerPropertyWithNameProperty() { return integerPropertyWithName; }
    private final IntegerProperty integerPropertyWithName = new SimpleIntegerProperty(this, "integerPropertyWithName", 777);

    public LongProperty longPropertyWithNameProperty() { return longPropertyWithName; }
    private final LongProperty longPropertyWithName = new SimpleLongProperty(this, "longPropertyWithName", 77777);

    public FloatProperty floatPropertyWithNameProperty() { return floatPropertyWithName; }
    private final FloatProperty floatPropertyWithName = new SimpleFloatProperty(this, "floatPropertyWithName", 3.1415f);

    public DoubleProperty doublePropertyWithNameProperty() { return doublePropertyWithName; }
    private final DoubleProperty doublePropertyWithName = new SimpleDoubleProperty(this, "doublePropertyWithName", 2.71828182846);

    public ObjectProperty<Orientation> objectPropertyWithNameProperty() { return objectPropertyWithName; }
    private final ObjectProperty<Orientation> objectPropertyWithName = new SimpleObjectProperty<>(this, "objectPropertyWithName", Orientation.HORIZONTAL);

    // ---------------------------------------------------------------------------------------------
    // read only property with name
    public ReadOnlyStringProperty readOnlyStringPropertyWithNameProperty() { return readOnlyStringPropertyWithName.getReadOnlyProperty(); }
    private final ReadOnlyStringWrapper readOnlyStringPropertyWithName = new ReadOnlyStringWrapper(this, "readOnlyStringPropertyWithName", "Read Only String Property with Name");

    public ReadOnlyBooleanProperty readOnlyBooleanPropertyWithNameProperty() { return readOnlyBooleanPropertyWithName.getReadOnlyProperty(); }
    private final ReadOnlyBooleanWrapper readOnlyBooleanPropertyWithName = new ReadOnlyBooleanWrapper(this, "readOnlyBooleanPropertyWithName", true);

    public ReadOnlyIntegerProperty readOnlyIntegerPropertyWithNameProperty() { return readOnlyIntegerPropertyWithName.getReadOnlyProperty(); }
    private final ReadOnlyIntegerWrapper readOnlyIntegerPropertyWithName = new ReadOnlyIntegerWrapper(this, "readOnlyIntegerPropertyWithName", 777);

    public ReadOnlyLongProperty readOnlyLongPropertyWithNameProperty() { return readOnlyLongPropertyWithName.getReadOnlyProperty(); }
    private final ReadOnlyLongWrapper readOnlyLongPropertyWithName = new ReadOnlyLongWrapper(this, "readOnlyLongPropertyWithName", 77777);

    public ReadOnlyFloatProperty readOnlyFloatPropertyWithNameProperty() { return readOnlyFloatPropertyWithName.getReadOnlyProperty(); }
    private final ReadOnlyFloatWrapper readOnlyFloatPropertyWithName = new ReadOnlyFloatWrapper(this, "readOnlyFloatPropertyWithName", 3.1415f);

    public ReadOnlyDoubleProperty readOnlyDoublePropertyWithNameProperty() { return readOnlyDoublePropertyWithName.getReadOnlyProperty(); }
    private final ReadOnlyDoubleWrapper readOnlyDoublePropertyWithName = new ReadOnlyDoubleWrapper(this, "readOnlyDoublePropertyWithName", 2.71828182846);

    public ReadOnlyObjectProperty<Orientation> readOnlyObjectPropertyWithNameProperty() { return readOnlyObjectPropertyWithName.getReadOnlyProperty(); }
    private final ReadOnlyObjectWrapper<Orientation> readOnlyObjectPropertyWithName = new ReadOnlyObjectWrapper<>(this, "readOnlyObjectPropertyWithName", Orientation.HORIZONTAL);

    // ---------------------------------------------------------------------------------------------
    // property without name
    public StringProperty stringPropertyWithoutNameProperty() { return stringPropertyWithoutName; }
    private final StringProperty stringPropertyWithoutName = new SimpleStringProperty("String Property without Name");

    public BooleanProperty booleanPropertyWithoutNameProperty() { return booleanPropertyWithoutName; }
    private final BooleanProperty booleanPropertyWithoutName = new SimpleBooleanProperty(true);

    public IntegerProperty integerPropertyWithoutNameProperty() { return integerPropertyWithoutName; }
    private final IntegerProperty integerPropertyWithoutName = new SimpleIntegerProperty(777);

    public LongProperty longPropertyWithoutNameProperty() { return longPropertyWithoutName; }
    private final LongProperty longPropertyWithoutName = new SimpleLongProperty(77777);

    public FloatProperty floatPropertyWithoutNameProperty() { return floatPropertyWithoutName; }
    private final FloatProperty floatPropertyWithoutName = new SimpleFloatProperty(3.1415f);

    public DoubleProperty doublePropertyWithoutNameProperty() { return doublePropertyWithoutName; }
    private final DoubleProperty doublePropertyWithoutName = new SimpleDoubleProperty(2.71828182846);

    public ObjectProperty<Orientation> objectPropertyWithoutNameProperty() { return objectPropertyWithoutName; }
    private final ObjectProperty<Orientation> objectPropertyWithoutName = new SimpleObjectProperty<>(Orientation.HORIZONTAL);

    // ---------------------------------------------------------------------------------------------
    // read only property without name
    public ReadOnlyStringProperty readOnlyStringPropertyWithoutNameProperty() { return readOnlyStringPropertyWithoutName.getReadOnlyProperty(); }
    private final ReadOnlyStringWrapper readOnlyStringPropertyWithoutName = new ReadOnlyStringWrapper("Read Only String Property without Name");

    public ReadOnlyBooleanProperty readOnlyBooleanPropertyWithoutNameProperty() { return readOnlyBooleanPropertyWithoutName.getReadOnlyProperty(); }
    private final ReadOnlyBooleanWrapper readOnlyBooleanPropertyWithoutName = new ReadOnlyBooleanWrapper(true);

    public ReadOnlyIntegerProperty readOnlyIntegerPropertyWithoutNameProperty() { return readOnlyIntegerPropertyWithoutName.getReadOnlyProperty(); }
    private final ReadOnlyIntegerWrapper readOnlyIntegerPropertyWithoutName = new ReadOnlyIntegerWrapper(777);

    public ReadOnlyLongProperty readOnlyLongPropertyWithoutNameProperty() { return readOnlyLongPropertyWithoutName.getReadOnlyProperty(); }
    private final ReadOnlyLongWrapper readOnlyLongPropertyWithoutName = new ReadOnlyLongWrapper(77777);

    public ReadOnlyFloatProperty readOnlyFloatPropertyWithoutNameProperty() { return readOnlyFloatPropertyWithoutName.getReadOnlyProperty(); }
    private final ReadOnlyFloatWrapper readOnlyFloatPropertyWithoutName = new ReadOnlyFloatWrapper(3.1415f);

    public ReadOnlyDoubleProperty readOnlyDoublePropertyWithoutNameProperty() { return readOnlyDoublePropertyWithoutName.getReadOnlyProperty(); }
    private final ReadOnlyDoubleWrapper readOnlyDoublePropertyWithoutName = new ReadOnlyDoubleWrapper(2.71828182846);

    public ReadOnlyObjectProperty<Orientation> readOnlyObjectPropertyWithoutNameProperty() { return readOnlyObjectPropertyWithoutName.getReadOnlyProperty(); }
    private final ReadOnlyObjectWrapper<Orientation> readOnlyObjectPropertyWithoutName = new ReadOnlyObjectWrapper<>(Orientation.HORIZONTAL);

    // ---------------------------------------------------------------------------------------------
    // primitive or reference type property
    public String getStringType() { return stringType; }
    private final String stringType = "String Type";

    public boolean isBooleanType() { return booleanType; }
    private final boolean booleanType = true;

    public int getIntegerType() { return integerType; }
    private final int integerType = 777;

    public long getLongType() { return longType; }
    private final long longType = 77777;

    public float getFloatType() { return floatType; }
    private final float floatType = 3.1415f;

    public double getDoubleType() { return doubleType; }
    private final double doubleType = 2.71828182846;

    public Orientation referenceType() { return referenceType; }
    private final Orientation referenceType = Orientation.HORIZONTAL.HORIZONTAL;

    public String getP() { return p; }
    private final String p = "One Character Property";
}
