/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.fxml;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

@FXMLLocation("/com/fievus/hestiafx/fxml/views/SimpleTestView.fxml")
public final class AbsoluteAnnotatedSimpleTestViewController {
    @FXML
    private Label messageLabel;

    public AbsoluteAnnotatedSimpleTestViewController() {}

    public String getMessage() { return messageLabel.getText(); }
}
