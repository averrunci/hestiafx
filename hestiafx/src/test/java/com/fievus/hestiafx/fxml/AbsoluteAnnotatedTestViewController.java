/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.fxml;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

@FXMLLocation("/com/fievus/hestiafx/fxml/views/AnnotatedTestView.fxml")
public final class AbsoluteAnnotatedTestViewController {
    @FXML
    private Label messageLabel;

    public AbsoluteAnnotatedTestViewController() {}

    public String getMessage() { return messageLabel.getText(); }
}
