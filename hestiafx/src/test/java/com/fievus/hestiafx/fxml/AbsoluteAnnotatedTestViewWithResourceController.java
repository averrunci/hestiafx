/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.fxml;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

@FXMLLocation("/com/fievus/hestiafx/fxml/views/AnnotatedTestViewWithResource.fxml")
public class AbsoluteAnnotatedTestViewWithResourceController {
    @FXML
    private Label messageLabel;
    @FXML
    private Label resourceMessageLabel;

    public AbsoluteAnnotatedTestViewWithResourceController() {}

    public String getMessage() { return messageLabel.getText(); }
    public String getResourceMessage() { return resourceMessageLabel.getText(); }
}
