/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.fxml;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;

public final class DynamicRootControl extends StackPane {
    @FXML
    private Label messageLabel;

    public DynamicRootControl() {}

    public String getMessage() { return messageLabel.getText(); }
}
