/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.fxml;


import javafx.fxml.FXML;
import javafx.scene.control.Label;

@FXMLLocation("SimpleTestView.fxml")
public final class RelativeAnnotatedSimpleTestViewController {
    @FXML
    private Label messageLabel;

    public RelativeAnnotatedSimpleTestViewController() {}

    public String getMessage() { return messageLabel.getText(); }
}
