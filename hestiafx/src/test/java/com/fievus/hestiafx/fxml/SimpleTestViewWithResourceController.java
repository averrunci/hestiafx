/**
 * Copyright (c) 2015 koji
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.fxml;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

public final class SimpleTestViewWithResourceController {
    @FXML
    private Label messageLabel;
    @FXML
    private Label resourceMessageLabel;

    public SimpleTestViewWithResourceController() {}

    public String getMessage() { return messageLabel.getText(); }
    public String getResourceMessage() { return resourceMessageLabel.getText(); }
}
