/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.chart;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.Node;

public class ChartDataItem {
    public StringProperty nameProperty() { return name; }
    private final StringProperty name = new SimpleStringProperty(this, "name");
    public final String getName() { return name.get(); }
    public final void setName(String name) { this.name.set(name); }

    public DoubleProperty pieValueProperty() { return pieValue; }
    private final DoubleProperty pieValue = new SimpleDoubleProperty(this, "pieValue");
    public final double getPieValue() { return pieValue.get(); }
    public final void setPieValue(double pieValue) { this.pieValue.set(pieValue); }

    public StringProperty titleProperty() { return title; }
    private final StringProperty title = new SimpleStringProperty(this, "title");
    public final String getTitle() { return title.get(); }
    public final void setTitle(String title) { this.title.set(title); }

    public DoubleProperty scoreProperty() { return score; }
    private final DoubleProperty score = new SimpleDoubleProperty(this, "score");
    public final double getScore() { return score.get(); }
    public final void setScore(double score) { this.score.set(score); }

    public DoubleProperty xValueProperty() { return xValue; }
    private final DoubleProperty xValue = new SimpleDoubleProperty(this, "xValue");
    public final double getXValue() { return xValue.get(); }
    public final void setXValue(double xValue) { this.xValue.set(xValue); }

    public DoubleProperty yValueProperty() { return yValue; }
    private final DoubleProperty yValue = new SimpleDoubleProperty(this, "yValue");
    public final double getYValue() { return yValue.get(); }
    public final void setYValue(double yValue) { this.yValue.set(yValue); }

    public ObjectProperty<Node> nodeProperty() { return node; }
    private final ObjectProperty<Node> node = new SimpleObjectProperty<>(this, "node");
    public final Node getNode() { return node.get(); }
    public final void setNode(Node node) { this.node.set(node); }

    public ObjectProperty<Node> nextNodeProperty() { return nextNode; }
    private final ObjectProperty<Node> nextNode = new SimpleObjectProperty<>(this, "nextNode");
    public final Node getNextNode() { return nextNode.get(); }
    public final void setNextNode(Node nextNode) { this.nextNode.set(nextNode); }
}
