/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.control;

import javafx.scene.control.Control;
import javafx.scene.control.Skin;

public class NotFoundTestView extends Control {
    public NotFoundTestView() {
        getStyleClass().add("not-found-test-view");
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new NotFoundTestViewSkin(this);
    }
}
