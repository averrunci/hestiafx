/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.control;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.paint.Color;

import java.time.LocalDate;
import java.util.Date;

public class TableColumnGenerationTestModels {
    private TableColumnGenerationTestModels() {}

    public static TestModel newTestModel() { return new TestModel(); }

    public static StringPropertyModel newStringPropertyModel() { return new StringPropertyModel(); }
    public static BooleanPropertyModel newBooleanPropertyModel() { return new BooleanPropertyModel(); }
    public static IntegerPropertyModel newIntegerPropertyModel() { return new IntegerPropertyModel(); }
    public static LongPropertyModel newLongPropertyModel() { return new LongPropertyModel(); }
    public static FloatPropertyModel newFloatPropertyModel() { return new FloatPropertyModel(); }
    public static DoublePropertyModel newDoublePropertyModel() { return new DoublePropertyModel(); }

    public static ObjectPropertyStringModel newObjectPropertyStringModel() { return new ObjectPropertyStringModel(); }
    public static ObjectPropertyBooleanModel newObjectPropertyBooleanModel() { return new ObjectPropertyBooleanModel(); }
    public static ObjectPropertyIntegerModel newObjectPropertyIntegerModel() { return new ObjectPropertyIntegerModel(); }
    public static ObjectPropertyLongModel newObjectPropertyLongModel() { return new ObjectPropertyLongModel();}
    public static ObjectPropertyFloatModel newObjectPropertyFloatModel() { return new ObjectPropertyFloatModel(); }
    public static ObjectPropertyDoubleModel newObjectPropertyDoubleModel() { return new ObjectPropertyDoubleModel(); }
    public static ObjectPropertyDateModel newObjectPropertyDateModel() { return new ObjectPropertyDateModel(); }
    public static ObjectPropertyColorModel newObjectPropertyColorModel() { return new ObjectPropertyColorModel(); }
    public static ObjectPropertyLocalDateModel newObjectPropertyLocalDateModel() { return new ObjectPropertyLocalDateModel(); }

    public static StringItemModel newStringItemModel() { return new StringItemModel(); }
    public static BooleanItemModel newBooleanItemModel() { return new BooleanItemModel(); }
    public static IntegerItemModel newIntegerItemModel() { return new IntegerItemModel(); }
    public static LongItemModel newLongItemModel() { return new LongItemModel(); }
    public static FloatItemModel newFloatItemModel() { return new FloatItemModel(); }
    public static DoubleItemModel newDoubleItemModel() { return new DoubleItemModel(); }
    public static DateItemModel newDateItemModel() { return new DateItemModel(); }
    public static ColorItemModel newColorItemModel() { return new ColorItemModel(); }
    public static LocalDateItemModel newLocalDateItemModel() { return new LocalDateItemModel(); }

    public static CustomItemModel newCustomItemModel() { return new CustomItemModel(); }

    public static TableColumnDefaultPropertyTestModel newTableColumnDefaultPropertyTestModel() { return new TableColumnDefaultPropertyTestModel(); }
    public static TableColumnCustomPropertyTestModel newTableColumnCustomPropertyTestModel() { return new TableColumnCustomPropertyTestModel(); }
    public static TableColumnOrderTestMode newTableColumnOrderTestModel() { return new TableColumnOrderTestMode(); }

    public static class TestModel {
        @TableColumn
        public StringProperty stringItemProperty() { return stringItem; }
        private final StringProperty stringItem = new SimpleStringProperty(this, "stringItem");
        public final String getStringItem() { return stringItem.get(); }
        public final void setStringItem(String stringItem) { this.stringItem.set(stringItem); }

        @TableColumn
        public BooleanProperty booleanItemProperty() { return booleanItem; }
        private final BooleanProperty booleanItem = new SimpleBooleanProperty(this, "booleanItem");
        public final boolean getBooleanItem() { return booleanItem.get(); }
        public final void setBooleanItem(boolean booleanItem) { this.booleanItem.set(booleanItem); }

        @TableColumn
        public IntegerProperty integerItemProperty() { return integerItem; }
        private final IntegerProperty integerItem = new SimpleIntegerProperty(this, "integerItem");
        public final int getIntegerItem() { return integerItem.get(); }
        public final void setIntegerItem(int integerItem) { this.integerItem.set(integerItem); }

        @TableColumn
        public LongProperty longItemProperty() { return longItem; }
        private final LongProperty longItem = new SimpleLongProperty(this, "longItem");
        public final long getLongItem() { return longItem.get(); }
        public final void setLongItem(long longItem) { this.longItem.set(longItem); }

        @TableColumn
        public FloatProperty floatItemProperty() { return floatItem; }
        private final FloatProperty floatItem = new SimpleFloatProperty(this, "floatItem");
        public final float getFloatItem() { return floatItem.get(); }
        public final void setFloatItem(float floatItem) { this.floatItem.set(floatItem); }

        @TableColumn
        public DoubleProperty doubleItemProperty() { return doubleItem; }
        private final DoubleProperty doubleItem = new SimpleDoubleProperty(this, "doubleItem");
        public final double getDoubleItem() { return doubleItem.get(); }
        public final void setDoubleItem(double doubleItem) { this.doubleItem.set(doubleItem); }

        @TableColumn
        public ObjectProperty<Date> dateItemProperty() { return dateItem; }
        private final ObjectProperty<Date> dateItem = new SimpleObjectProperty<Date>(this, "dateItem");
        public final Date getDate() { return dateItem.get(); }
        public final void setDate(Date dateItem) { this.dateItem.set(dateItem); }
    }

    public static class StringPropertyModel {
        @TableColumn
        public StringProperty itemProperty() { return item; }
        private final StringProperty item = new SimpleStringProperty(this, "item");
        public final String getItem() { return item.get(); }
        public final void setItem(String item) { this.item.set(item); }
    }

    public static class BooleanPropertyModel {
        @TableColumn
        public BooleanProperty itemProperty() { return item; }
        private final BooleanProperty item = new SimpleBooleanProperty(this, "item");
        public final Boolean getItem() { return item.get(); }
        public final void setItem(Boolean item) { this.item.set(item); }
    }

    public static class IntegerPropertyModel {
        @TableColumn
        public IntegerProperty itemProperty() { return item; }
        private final IntegerProperty item = new SimpleIntegerProperty(this, "item");
        public final int getItem() { return item.get(); }
        public final void setItem(int item) { this.item.set(item); }
    }

    public static class LongPropertyModel {
        @TableColumn
        public LongProperty itemProperty() { return item; }
        private final LongProperty item = new SimpleLongProperty(this, "item");
        public final long getItem() { return item.get(); }
        public final void setItem(long item) { this.item.set(item); }
    }

    public static class FloatPropertyModel {
        @TableColumn
        public FloatProperty itemProperty() { return item; }
        private final FloatProperty item = new SimpleFloatProperty(this, "item");
        public final float getItem() { return item.get(); }
        public final void setItem(float item) { this.item.set(item); }
    }

    public static class DoublePropertyModel {
        @TableColumn
        public DoubleProperty itemProperty() { return item; }
        private final DoubleProperty item = new SimpleDoubleProperty(this, "item");
        public final double getItem() { return item.get(); }
        public final void setItem(double item) { this.item.set(item); }
    }

    public static class ObjectPropertyStringModel {
        @TableColumn
        public ObjectProperty<String> itemProperty() { return item; }
        private final ObjectProperty<String> item = new SimpleObjectProperty<>(this, "item");
        public final String getItem() { return item.get(); }
        public final void setItem(String item) { this.item.set(item); }
    }

    public static class ObjectPropertyBooleanModel {
        @TableColumn
        public ObjectProperty<Boolean> itemProperty() { return item; }
        private final ObjectProperty<Boolean> item = new SimpleObjectProperty<>(this, "item");
        public final boolean getItem() { return item.get(); }
        public final void setItem(boolean item) { this.item.set(item); }
    }

    public static class ObjectPropertyIntegerModel {
        @TableColumn
        public ObjectProperty<Integer> itemProperty() { return item; }
        private final ObjectProperty<Integer> item = new SimpleObjectProperty<>(this, "item");
        public final int getItem() { return item.get(); }
        public final void setItem(int item) { this.item.set(item); }
    }

    public static class ObjectPropertyLongModel {
        @TableColumn
        public ObjectProperty<Long> itemProperty() { return item; }
        private final ObjectProperty<Long> item = new SimpleObjectProperty<>(this, "item");
        public final long getItem() { return item.get(); }
        public final void setItem(long item) { this.item.set(item); }
    }

    public static class ObjectPropertyFloatModel {
        @TableColumn
        public ObjectProperty<Float> itemProperty() { return item; }
        private final ObjectProperty<Float> item = new SimpleObjectProperty<>(this, "item");
        public final float getItem() { return item.get(); }
        public final void setItem(float item) { this.item.set(item); }
    }

    public static class ObjectPropertyDoubleModel {
        @TableColumn
        public ObjectProperty<Double> itemProperty() { return item; }
        private final ObjectProperty<Double> item = new SimpleObjectProperty<>(this, "item");
        public final double getItem() { return item.get(); }
        public final void setItem(double item) { this.item.set(item); }
    }

    public static class ObjectPropertyDateModel {
        @TableColumn
        public ObjectProperty<Date> itemProperty() { return item; }
        private final ObjectProperty<Date> item = new SimpleObjectProperty<>(this, "item");
        public final Date getItem() { return item.get(); }
        public final void setItem(Date item) { this.item.set(item); }
    }

    public static class ObjectPropertyColorModel {
        @TableColumn
        public ObjectProperty<Color> itemProperty() { return item; }
        private final ObjectProperty<Color> item = new SimpleObjectProperty<>(this, "item");
        public final Color getItem() { return item.get(); }
        public final void setItem(Color item) { this.item.set(item); }
    }

    public static class ObjectPropertyLocalDateModel {
        @TableColumn
        public ObjectProperty<LocalDate> itemProperty() { return item; }
        private final ObjectProperty<LocalDate> item = new SimpleObjectProperty<>(this, "item");
        public final LocalDate getItem() { return item.get(); }
        public final void setItem(LocalDate item) { this.item.set(item); }
    }

    public static class StringItemModel {
        @TableColumn
        public String getItem() { return item; }
        public void setItem(String item) { this.item = item; }
        private String item;
    }

    public static class BooleanItemModel {
        @TableColumn
        public boolean isItem() { return item; }
        public void setItem(boolean item) { this.item = item; }
        private boolean item;
    }

    public static class IntegerItemModel {
        @TableColumn
        public int getItem() { return item; }
        public void setItem(int item) { this.item = item; }
        private int item;
    }

    public static class LongItemModel {
        @TableColumn
        public long getItem() { return item; }
        public void setItem(long item) { this.item = item; }
        private long item;
    }

    public static class FloatItemModel {
        @TableColumn
        public float getItem() { return item; }
        public void setItem(float item) { this.item = item; }
        private float item;
    }

    public static class DoubleItemModel {
        @TableColumn
        public double getItem() { return item; }
        public void setItem(double item) { this.item = item; }
        private double item;
    }

    public static class DateItemModel {
        @TableColumn
        public Date getItem() { return item; }
        public void setItem(Date item) { this.item = item; }
        private Date item;
    }

    public static class ColorItemModel {
        @TableColumn
        public Color getItem() { return item; }
        public void setItem(Color item) { this.item = item; }
        private Color item;
    }

    public static class LocalDateItemModel {
        @TableColumn
        public LocalDate getItem() { return item; }
        public void setItem(LocalDate item) { this.item = item; }
        private LocalDate item;
    }

    public static class CustomItem {}

    public static class CustomItemModel {
        @TableColumn
        public ObjectProperty<CustomItem> itemProperty() { return item; }
        private final ObjectProperty<CustomItem> item = new SimpleObjectProperty<>(this, "item");
        public final CustomItem getItem() { return item.get(); }
        public final void setItem(CustomItem item) { this.item.set(item); }

        @TableColumn
        public CustomItem getCustomItem() { return customItem; }
        public void setCustomItem(CustomItem customItem) { this.customItem = customItem; }
        private CustomItem customItem;
    }

    public static class TableColumnDefaultPropertyTestModel {
        @TableColumn
        public StringProperty defaultItemProperty() { return defaultItem; }
        private final StringProperty defaultItem = new SimpleStringProperty(this, "defaultItem");
        public final String getDefaultItem() { return defaultItem.get(); }
        public final void setDefaultItem(String defaultItem) { this.defaultItem.set(defaultItem); }
    }

    public static class TableColumnCustomPropertyTestModel {
        @TableColumn(
            name = "Custom Item", id = "customItemTableColumn", editable = false,
            prefWidth = 250, maxWidth = 500, minWidth = 100,
            visible = false, resizable = false, sortable = false, sortType = javafx.scene.control.TableColumn.SortType.DESCENDING,
            style = "-fx-background-color:red;", styleClass = {"column", "warn"}, cellStyleClass = {"cell", "remark"}
        )
        public StringProperty customItemProperty() { return customItem; }
        private final StringProperty customItem = new SimpleStringProperty(this, "customItem");
        public final String getCustomItem() { return customItem.get(); }
        public final void setCustomItem(String customItem) { this.customItem.set(customItem); }
    }

    public static class TableColumnOrderTestMode {
        @TableColumn
        public String getItem1() { return item1; }
        public void setItem1(String item1) { this.item1 = item1; }
        private String item1;

        @TableColumn(order = 20)
        public String getItem2() { return item2; }
        public void setItem2(String item2) { this.item2 = item2; }
        private String item2;

        @TableColumn(order = 0)
        public String getItem3() { return item3; }
        public void setItem3(String item3) { this.item3 = item3; }
        private String item3;

        @TableColumn(order = 10)
        public String getItem4() { return item4; }
        public void setItem4(String item4) { this.item4 = item4; }
        private String item4;

        @TableColumn(order = 1)
        public String getItem5() { return item5; }
        public void setItem5(String item5) { this.item5 = item5; }
        private String item5;
    }
}
