/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.control;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;

public class TestHierarchicalItem implements HierarchicalItem<TestHierarchicalItem> {
    @Override
    public BooleanProperty expandedProperty() { return expanded; }
    private final BooleanProperty expanded = new SimpleBooleanProperty(this, "expanded");
    public final boolean getExpanded() { return expanded.get(); }
    public final void setExpanded(boolean expanded) { this.expanded.set(expanded); }

    @Override
    public ObjectProperty<Node> graphicProperty() { return graphic; }
    private final ObjectProperty<Node> graphic = new SimpleObjectProperty<>(this, "graphic");
    public final Node getGraphic() { return graphic.get(); }
    public final void setGraphic(Node graphic) { this.graphic.set(graphic); }

    private final boolean isLeafItem;
    private ObservableList<TestHierarchicalItem> children;

    protected TestHierarchicalItem(boolean isLeafItem) {
        this.isLeafItem = isLeafItem;
    }

    public static TestHierarchicalItem asInternal() {
        return new TestHierarchicalItem(false);
    }

    public static TestHierarchicalItem asLeaf() {
        return new TestHierarchicalItem(true);
    }

    @Override
    public ObservableList<TestHierarchicalItem> getItems() {
        if (children != null) { return children; }
        if (isLeafItem) { return null; }

        children = FXCollections.observableArrayList(
            TestHierarchicalItem.asInternal(), TestHierarchicalItem.asLeaf()
        );
        return children;
    }

    @Override
    public boolean hasItem() {
        return !isLeafItem;
    }
}
