/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.control;

public class TestViewSkin extends FXMLSkin<TestView> {
    protected TestViewSkin(TestView control) {
        super(control);
    }
}
