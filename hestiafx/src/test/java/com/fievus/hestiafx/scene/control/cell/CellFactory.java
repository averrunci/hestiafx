/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.control.cell;

import javafx.scene.Node;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;

import java.lang.reflect.Field;
import java.util.function.Supplier;

public class CellFactory {
    private CellFactory() {}

    public static <S, T, G extends Node> ColorPickerTableCell<S, T, G> createEditableColorPickerTableCell() {
        return applyTableCellEditable(new ColorPickerTableCell<>());
    }

    public static <S, T> DatePickerTableCell<S, T> createEditableDatePickerTableCell() {
        return createEditableDatePickerTableCell(null);
    }

    public static <S, T> DatePickerTableCell<S, T> createEditableDatePickerTableCell(Supplier<DatePicker> datePickerFactory) {
        return applyTableCellEditable(new DatePickerTableCell<>(datePickerFactory));
    }

    private static <S, T, C extends TableCell<S, T>> C applyTableCellEditable(C cell) {
        cell.setEditable(true);
        updateEditableTableView(cell);
        updateEditableTableColumn(cell);
        lockItemOnEdit(TableCell.class, cell);
        return cell;
    }

    private static <S, T> void updateEditableTableView(TableCell<S, T> cell) {
        TableView<S> tableView = new TableView<>();
        tableView.setEditable(true);
        cell.updateTableView(tableView);
    }

    private static <S, T> void updateEditableTableColumn(TableCell<S, T> cell) {
        TableColumn<S, T> tableColumn = new TableColumn<>();
        tableColumn.setEditable(true);
        cell.updateTableColumn(tableColumn);
    }

    public static <S, T, G extends Node> ColorPickerTreeTableCell<S, T, G> createEditableColorPickerTreeTableCell() {
        return applyTreeTableCellEditable(new ColorPickerTreeTableCell<>());
    }

    public static <S, T> DatePickerTreeTableCell<S, T> createEditableDatePickerTreeTableCell() {
        return createEditableDatePickerTreeTableCell(null);
    }

    public static <S, T> DatePickerTreeTableCell<S, T> createEditableDatePickerTreeTableCell(Supplier<DatePicker> datePickerFactory) {
        return applyTreeTableCellEditable(new DatePickerTreeTableCell<>(datePickerFactory));
    }

    private static <S, T, C extends TreeTableCell<S, T>> C applyTreeTableCellEditable(C cell) {
        cell.setEditable(true);
        updateEditableTreeTableView(cell);
        updateEditableTreeTableColumn(cell);
        lockItemOnEdit(TreeTableCell.class, cell);
        return cell;
    }

    private static <S, T> void updateEditableTreeTableView(TreeTableCell<S, T> cell) {
        TreeTableView<S> treeTableView = new TreeTableView<>();
        treeTableView.setEditable(true);
        cell.updateTreeTableView(treeTableView);
    }

    private static <S, T> void updateEditableTreeTableColumn(TreeTableCell<S, T> cell) {
        TreeTableColumn<S, T> treeTableColumn = new TreeTableColumn<>();
        treeTableColumn.setEditable(true);
        cell.updateTreeTableColumn(treeTableColumn);
    }

    private static void lockItemOnEdit(Class<?> cellClass, Object cell) {
        try {
            Field field = cellClass.getDeclaredField("lockItemOnEdit");
            field.setAccessible(true);
            field.set(cell, true);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
