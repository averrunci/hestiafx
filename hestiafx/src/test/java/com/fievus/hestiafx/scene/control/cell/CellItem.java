/**
 * Copyright (c) 2016 Fievus
 * <p>
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.control.cell;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.paint.Color;

import java.time.LocalDate;

public class CellItem {
    public StringProperty nameProperty() { return name; }
    private final StringProperty name = new SimpleStringProperty(this, "name");
    public final String getName() { return name.get(); }
    public final void setName(String name) { this.name.set(name); }

    public ObjectProperty<Color> colorProperty() { return color; }
    private final ObjectProperty<Color> color = new SimpleObjectProperty<>(this, "color");
    public final Color getColor() { return color.get(); }
    public final void setColor(Color color) { this.color.set(color); }

    public ObjectProperty<LocalDate> dateProperty() { return date; }
    private final ObjectProperty<LocalDate> date = new SimpleObjectProperty<>(this, "date");
    public final LocalDate getDate() { return date.get(); }
    public final void setDate(LocalDate date) { this.date.set(date); }

    public CellItem() {}

    public CellItem(String name, Color color) {
        setName(name);
        setColor(color);
    }

    public CellItem(String name, LocalDate date) {
        setName(name);
        setDate(date);
    }
}
