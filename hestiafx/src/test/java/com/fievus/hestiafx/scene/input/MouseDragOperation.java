/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.input;

import javafx.event.EventType;
import javafx.scene.Node;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Window;

import java.util.Objects;

public class MouseDragOperation {
    private final EventFirer eventFirer;
    private final double sceneOffsetX;
    private final double sceneOffsetY;

    private double currentSceneX;
    private double currentSceneY;

    private MouseDragOperation(Builder builder) {
        eventFirer = builder.eventFirer;
        sceneOffsetX = builder.sceneOffsetX;
        sceneOffsetY = builder.sceneOffsetY;
        currentSceneX = builder.sceneX;
        currentSceneY = builder.sceneY;

        startDrag();
    }

    public static Builder forNode(Node node) {
        return new Builder(Objects.requireNonNull(node));
    }

    public static Builder forWindow(Window window) { return new Builder(Objects.requireNonNull(window)); }

    public MouseDragOperation dragTo(double sceneX, double sceneY) {
        currentSceneX = sceneX;
        currentSceneY = sceneY;
        eventFirer.fireEvent(createMouseEvent(MouseEvent.MOUSE_DRAGGED));
        return this;
    }

    public void release() {
        eventFirer.fireEvent(createMouseEvent(MouseEvent.MOUSE_RELEASED));
    }

    private void startDrag() {
        eventFirer.fireEvent(createMouseEvent(MouseEvent.MOUSE_PRESSED));
        eventFirer.fireEvent(createMouseEvent(MouseEvent.DRAG_DETECTED));
    }

    private MouseEvent createMouseEvent(EventType<MouseEvent> eventType) {
        return new MouseEvent(
            eventType,
            currentSceneX, currentSceneY, currentSceneX + sceneOffsetX, currentSceneY + sceneOffsetY,
            MouseButton.PRIMARY, 1,
            false, false, false, false, true, false, false, false, false, false, null
        );
    }

    public static class Builder {
        private final EventFirer eventFirer;
        private double sceneOffsetX;
        private double sceneOffsetY;
        private double sceneX;
        private double sceneY;

        private Builder(Node node) {
            this.eventFirer = new EventFirer(node);
        }

        private Builder(Window window) {
            this.eventFirer = new EventFirer(window);
        }

        public Builder sceneOffsetX(double sceneOffsetX) {
            this.sceneOffsetX = sceneOffsetX;
            return this;
        }

        public Builder sceneOffsetY(double sceneOffsetY) {
            this.sceneOffsetY = sceneOffsetY;
            return this;
        }

        public MouseDragOperation startDragAt(double sceneX, double sceneY) {
            this.sceneX = sceneX;
            this.sceneY = sceneY;
            return new MouseDragOperation(this);
        }
    }

    private static class EventFirer {
        private final Node node;
        private final Window window;

        private EventFirer(Node node) {
            this.node = node;
            this.window = null;
        }

        private EventFirer(Window window) {
            this.node = null;
            this.window = window;
        }

        public void fireEvent(MouseEvent mouseEvent) {
            if (node != null) {
                node.fireEvent(mouseEvent);
            } else if (window != null) {
                window.fireEvent(mouseEvent);
            }
        }
    }
}
