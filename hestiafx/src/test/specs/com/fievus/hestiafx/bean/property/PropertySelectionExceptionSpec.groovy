/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.bean.property

import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(PropertySelector)
class PropertySelectionExceptionSpec extends Specification {
    def 'throws an exception when null is specified for a root object'() {
        given:
        def root = null
        and:
        def step = 'property'

        when:
        PropertySelector.select root, step

        then:
        thrown NullPointerException
    }

    def 'returns null when null is specified for a step'() {
        given:
        def root = new PlainObject()
        and:
        def step = null

        when:
        def property =PropertySelector.select root, step

        then:
        expect property, is(nullValue())
    }

    def 'returns null when an empty string is specified for a step'() {
        given:
        def root = new PlainObject()
        and:
        def step = ''

        when:
        def property =PropertySelector.select root, step

        then:
        expect property, is(nullValue())
    }

    def 'returns null when a whitespace is specified for a step'() {
        given:
        def root = new PlainObject()
        and:
        def step = '      '

        when:
        def property =PropertySelector.select root, step

        then:
        expect property, is(nullValue())
    }
}