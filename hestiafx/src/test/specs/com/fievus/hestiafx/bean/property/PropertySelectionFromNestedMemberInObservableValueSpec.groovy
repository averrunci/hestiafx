/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.bean.property

import javafx.beans.property.BooleanProperty
import javafx.beans.property.DoubleProperty
import javafx.beans.property.IntegerProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.StringProperty
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(PropertySelector)
class PropertySelectionFromNestedMemberInObservableValueSpec extends Specification {
    def 'gets Property defined as Property of Property the name of which is specified'() {
        given:
        def root = new SimpleObjectProperty<>(new NestedPlainObject())
        and:
        def step = 'propertyWithName.stringPropertyWithName'

        when:
        def property = PropertySelector.<String>select root, step

        then:
        expect property, is(instanceOf(StringProperty))
        and:
        that property.value, is('String Property with Name')
    }

    def 'gets Property defined as Property of Property the name of which is not specified'() {
        given:
        def root = new SimpleObjectProperty<>(new NestedPlainObject())
        and:
        def step = 'propertyWithoutName.stringPropertyWithoutName'

        when:
        def property = PropertySelector.<String>select root, step

        then:
        expect property, is(instanceOf(StringProperty))
        and:
        that property.value, is('String Property without Name')
    }

    def 'gets Property defined as Property of ReadOnlyProperty the name of which is specified'() {
        given:
        def root = new SimpleObjectProperty<>(new NestedPlainObject())
        and:
        def step = 'readOnlyPropertyWithName.integerPropertyWithName'

        when:
        def property = PropertySelector.<Integer>select root, step

        then:
        expect property, is(instanceOf(IntegerProperty))
        and:
        that property.value, is(777)
    }

    def 'gets Property defined as Property of ReadOnlyProperty the name of which is not specified'() {
        given:
        def root = new SimpleObjectProperty<>(new NestedPlainObject())
        and:
        def step = 'readOnlyPropertyWithoutName.doublePropertyWithoutName'

        when:
        def property = PropertySelector.<Double>select root, step

        then:
        expect property, is(instanceOf(DoubleProperty))
        and:
        that property.value, is(2.71828182846d)
    }

    def 'gets Property defined as Property of Property which is a reference type'() {
        given:
        def root = new SimpleObjectProperty<>(new NestedPlainObject())
        and:
        def step = 'plainObject.booleanPropertyWithName'

        when:
        def property = PropertySelector.<Boolean>select root, step

        then:
        expect property, is(instanceOf(BooleanProperty))
        and:
        that property.value, is(true)
    }

    def 'gets Property defined as Property of Property of Property the name of which is specified'() {
        given:
        def root = new SimpleObjectProperty<>(new DoubleNestedPlainObject())
        and:
        def step = 'propertyWithName.propertyWithoutName.stringPropertyWithName'

        when:
        def property = PropertySelector.<String>select root, step

        then:
        expect property, is(instanceOf(StringProperty))
        and:
        that property.value, is('String Property with Name')
    }

    def 'gets Property defined as Property of Property of Property the name of which is not specified'() {
        given:
        def root = new SimpleObjectProperty<>(new DoubleNestedPlainObject())
        and:
        def step = 'propertyWithoutName.readOnlyPropertyWithName.stringPropertyWithoutName'

        when:
        def property = PropertySelector.<String>select root, step

        then:
        expect property, is(instanceOf(StringProperty))
        and:
        that property.value, is('String Property without Name')
    }

    def 'gets Property defined as Property of Property of ReadOnlyProperty the name of which is specified'() {
        given:
        def root = new SimpleObjectProperty<>(new DoubleNestedPlainObject())
        and:
        def step = 'readOnlyPropertyWithName.plainObject.integerPropertyWithName'

        when:
        def property = PropertySelector.<Integer>select root, step

        then:
        expect property, is(instanceOf(IntegerProperty))
        and:
        that property.value, is(777)
    }

    def 'gets Property defined as Property of Property of ReadOnlyProperty the name of which is not specified'() {
        given:
        def root = new SimpleObjectProperty<>(new DoubleNestedPlainObject())
        and:
        def step = 'readOnlyPropertyWithoutName.readOnlyPropertyWithoutName.doublePropertyWithoutName'

        when:
        def property = PropertySelector.<Double>select root, step

        then:
        expect property, is(instanceOf(DoubleProperty))
        and:
        that property.value, is(2.71828182846d)
    }

    def 'gets Property defined as Property of Property of Property which is a reference type'() {
        given:
        def root = new SimpleObjectProperty<>(new DoubleNestedPlainObject())
        and:
        def step = 'nestedPlainObject.propertyWithName.booleanPropertyWithName'

        when:
        def property = PropertySelector.<Boolean>select root, step

        then:
        expect property, is(instanceOf(BooleanProperty))
        and:
        that property.value, is(true)
    }
}