/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.bean.property

import javafx.beans.property.ReadOnlyObjectProperty
import javafx.geometry.Orientation
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(PropertySelector)
class PropertySelectionFromPrimitiveOrReferenceTypeInPlainObjectSpec extends Specification {
    def root

    def setup() {
        root = new PlainObject()
    }

    def 'gets Property defined as String'() {
        given:
        def step = 'stringType'

        when:
        def property = PropertySelector.<String>select root, step

        then:
        expect property, is(instanceOf(ReadOnlyObjectProperty))
        and:
        that property.value, is('String Type')
    }

    def 'gets Property defined as boolean'() {
        given:
        def step = 'booleanType'

        when:
        def property = PropertySelector.<Boolean>select root, step

        then:
        expect property, is(instanceOf(ReadOnlyObjectProperty))
        and:
        that property.value, is(true)
    }

    def 'gets Property defined as int'() {
        given:
        def step = 'integerType'

        when:
        def property = PropertySelector.<Integer>select root, step

        then:
        expect property, is(instanceOf(ReadOnlyObjectProperty))
        and:
        that property.value, is(777)
    }

    def 'gets Property defined as long'() {
        given:
        def step = 'longType'

        when:
        def property = PropertySelector.<Long>select root, step

        then:
        expect property, is(instanceOf(ReadOnlyObjectProperty))
        and:
        that property.value, is(77777L)
    }

    def 'gets Property defined as float'() {
        given:
        def step = 'floatType'

        when:
        def property = PropertySelector.<Float>select root, step

        then:
        expect property, is(instanceOf(ReadOnlyObjectProperty))
        and:
        that property.value, is(3.1415f)
    }

    def 'gets Property defined as double'() {
        given:
        def step = 'doubleType'

        when:
        def property = PropertySelector.<Double>select root, step

        then:
        expect property, is(instanceOf(ReadOnlyObjectProperty))
        and:
        that property.value, is(2.71828182846d)
    }

    def 'gets Property defined as a reference type'() {
        given:
        def step = 'referenceType'

        when:
        def property = PropertySelector.<Orientation>select root, step

        then:
        expect property, is(instanceOf(ReadOnlyObjectProperty))
        and:
        that property.value, is(Orientation.HORIZONTAL)
    }

    def 'gets Property the name of which has one character'() {
        given:
        def step = 'p'

        when:
        def property = PropertySelector.<String>select root, step

        then:
        expect property, is(instanceOf(ReadOnlyObjectProperty))
        and:
        that property.value, is('One Character Property')
    }

    def 'gets Property the value of which is null when the specified Property does not exist'() {
        given:
        def step = 'notFoundProperty'

        when:
        def property = PropertySelector.select root, step

        then:
        expect property, is(instanceOf(ReadOnlyObjectProperty))
        and:
        that property.value, is(nullValue())
    }
}