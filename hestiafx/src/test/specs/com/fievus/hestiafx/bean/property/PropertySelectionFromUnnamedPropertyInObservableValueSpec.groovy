/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.bean.property

import javafx.beans.property.BooleanProperty
import javafx.beans.property.DoubleProperty
import javafx.beans.property.FloatProperty
import javafx.beans.property.IntegerProperty
import javafx.beans.property.LongProperty
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.StringProperty
import javafx.geometry.Orientation
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(PropertySelector)
class PropertySelectionFromUnnamedPropertyInObservableValueSpec extends Specification {
    def root

    def setup() {
        root = new SimpleObjectProperty<>(new PlainObject())
    }

    def 'gets Property defined as StringProperty the name of which is not specified'() {
        given:
        def step = 'stringPropertyWithoutName'

        when:
        def property = PropertySelector.<String>select root, step

        then:
        expect property, is(instanceOf(StringProperty))
        and:
        that property.value, is('String Property without Name')
    }

    def 'gets Property defined as BooleanProperty the name of which is not specified'() {
        given:
        def step = 'booleanPropertyWithoutName'

        when:
        def property = PropertySelector.<Boolean>select root, step

        then:
        expect property, is(instanceOf(BooleanProperty))
        and:
        that property.value, is(true)
    }

    def 'gets Property defined as IntegerProperty the name of which is not specified'() {
        given:
        def step = 'integerPropertyWithoutName'

        when:
        def property = PropertySelector.<Integer>select root, step

        then:
        expect property, is(instanceOf(IntegerProperty))
        and:
        that property.value, is(777)
    }

    def 'gets Property defined as LongProperty the name of which is not specified'() {
        given:
        def step = 'longPropertyWithoutName'

        when:
        def property = PropertySelector.<Long>select root, step

        then:
        expect property, is(instanceOf(LongProperty))
        and:
        that property.value, is(77777L)
    }

    def 'gets Property defined as FloatProperty the name of which is not specified'() {
        given:
        def step = 'floatPropertyWithoutName'

        when:
        def property = PropertySelector.<Float>select root, step

        then:
        expect property, is(instanceOf(FloatProperty))
        and:
        that property.value, is(3.1415f)
    }

    def 'gets Property defined as DoubleProperty the name of which is not specified'() {
        given:
        def step = 'doublePropertyWithoutName'

        when:
        def property = PropertySelector.<Double>select root, step

        then:
        expect property, is(instanceOf(DoubleProperty))
        and:
        that property.value, is(2.71828182846d)
    }

    def 'gets Property defined as ObjectProperty the name of which is not specified'() {
        given:
        def step = 'objectPropertyWithoutName'

        when:
        def property = PropertySelector.<Orientation>select root, step

        then:
        expect property, is(instanceOf(ObjectProperty))
        and:
        that property.value, is(Orientation.HORIZONTAL)
    }
}