/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.bean.property

import javafx.beans.property.ReadOnlyBooleanProperty
import javafx.beans.property.ReadOnlyDoubleProperty
import javafx.beans.property.ReadOnlyFloatProperty
import javafx.beans.property.ReadOnlyIntegerProperty
import javafx.beans.property.ReadOnlyLongProperty
import javafx.beans.property.ReadOnlyObjectProperty
import javafx.beans.property.ReadOnlyStringProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.geometry.Orientation
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(PropertySelector)
class PropertySelectionFromUnnamedReadOnlyPropertyInObservableValueSpec extends Specification {
    def root

    def setup() {
        root = new SimpleObjectProperty<>(new PlainObject())
    }

    def 'gets Property defined as ReadOnlyStringProperty the name of which is not specified'() {
        given:
        def step = 'readOnlyStringPropertyWithoutName'

        when:
        def property = PropertySelector.<String>select root, step

        then:
        expect property, is(instanceOf(ReadOnlyStringProperty))
        and:
        that property.value, is('Read Only String Property without Name')
    }

    def 'gets Property defined as ReadOnlyBooleanProperty the name of which is not specified'() {
        given:
        def step = 'readOnlyBooleanPropertyWithoutName'

        when:
        def property = PropertySelector.<Boolean>select root, step

        then:
        expect property, is(instanceOf(ReadOnlyBooleanProperty))
        and:
        that property.value, is(true)
    }

    def 'gets Property defined as ReadOnlyIntegerProperty the name of which is not specified'() {
        given:
        def step = 'readOnlyIntegerPropertyWithoutName'

        when:
        def property = PropertySelector.<Integer>select root, step

        then:
        expect property, is(instanceOf(ReadOnlyIntegerProperty))
        and:
        that property.value, is(777)
    }

    def 'gets Property defined as ReadOnlyLongProperty the name of which is not specified'() {
        given:
        def step = 'readOnlyLongPropertyWithoutName'

        when:
        def property = PropertySelector.<Long>select root, step

        then:
        expect property, is(instanceOf(ReadOnlyLongProperty))
        and:
        that property.value, is(77777L)
    }

    def 'gets Property defined as ReadOnlyFloatProperty the name of which is not specified'() {
        given:
        def step = 'readOnlyFloatPropertyWithoutName'

        when:
        def property = PropertySelector.<Float>select root, step

        then:
        expect property, is(instanceOf(ReadOnlyFloatProperty))
        and:
        that property.value, is(3.1415f)
    }

    def 'gets Property defined as ReadOnlyDoubleProperty the name of which is not specified'() {
        given:
        def step = 'readOnlyDoublePropertyWithoutName'

        when:
        def property = PropertySelector.<Double>select root, step

        then:
        expect property, is(instanceOf(ReadOnlyDoubleProperty))
        and:
        that property.value, is(2.71828182846d)
    }

    def 'gets Property defined as ReadOnlyObjectProperty the name of which is not specified'() {
        given:
        def step = 'readOnlyObjectPropertyWithoutName'

        when:
        def property = PropertySelector.<Orientation>select root, step

        then:
        expect property, is(instanceOf(ReadOnlyObjectProperty))
        and:
        that property.value, is(Orientation.HORIZONTAL)
    }
}