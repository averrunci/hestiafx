/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.event

import javafx.event.Event
import javafx.scene.layout.StackPane
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(EventExtensions)
class EventExtensionsSpec extends Specification {
    def 'gets the event source object from the specified event'() {
        given:
        def source = new StackPane()
        and:
        def event = new Event(source, null, null)

        when:
        def actualSource = EventExtensions.<StackPane>getEventSourceFrom event

        then:
        expect actualSource.isPresent(), is(true)
        and:
        that actualSource.get(), is(source)
    }

    def 'throws an exception when null is specified for an event'() {
        given:
        def event = null

        when:
        EventExtensions.getEventSourceFrom event

        then:
        thrown NullPointerException
    }
}
