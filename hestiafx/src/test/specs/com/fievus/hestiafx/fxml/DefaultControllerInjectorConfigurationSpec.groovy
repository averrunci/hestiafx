/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.fxml

import com.fievus.hestiafx.JavaFXApplication
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(FXController)
class DefaultControllerInjectorConfigurationSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    ControllerInjector initialDefaultInjector

    def setup() {
        initialDefaultInjector = FXController.defaultInjector
    }

    def cleanup() {
        FXController.setDefaultInjector initialDefaultInjector
    }

    def 'gets null as the default ControllerInjector when nothing is set'() {
        expect:
        that FXController.defaultInjector, is(nullValue())
    }

    def 'gets the default ControllerInjector which is set as the default Injector'() {
        given:
        def defaultInjector = Stub ControllerInjector

        when:
        FXController.setDefaultInjector defaultInjector

        then:
        expect FXController.defaultInjector, is(defaultInjector)
    }

    def 'loads with the specified ControllerInjector which is set as the default Injector'() {
        given:
        def defaultInjector = Mock ControllerInjector
        FXController.setDefaultInjector defaultInjector

        when:
        def controller = FXController.of(SimpleTestViewController)
            .fromDefaultLocation()
            .load()

        then:
        expect controller.message, is('Simple Label Message')
        and:
        1 * defaultInjector.getInstanceOf(SimpleTestViewController, null) >> new SimpleTestViewController()
    }

    def 'prefers ControllerInjector which is set with [using] method to the default Injector'() {
        given:
        def defaultInjector = Mock ControllerInjector
        FXController.setDefaultInjector defaultInjector
        and:
        def injector = Mock ControllerInjector

        when:
        def controller = FXController.of(SimpleTestViewController)
            .using(injector)
            .fromDefaultLocation()
            .load()

        then:
        expect controller.message, is('Simple Label Message')
        and:
        0 * defaultInjector.getInstanceOf(SimpleTestViewController, null)
        1 * injector.getInstanceOf(SimpleTestViewController, null) >> new SimpleTestViewController()
    }
}