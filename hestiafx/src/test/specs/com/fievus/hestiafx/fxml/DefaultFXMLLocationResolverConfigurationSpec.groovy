/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.fxml

import com.fievus.hestiafx.JavaFXApplication
import javafx.fxml.FXML
import javafx.scene.control.Label
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(FXController)
class DefaultFXMLLocationResolverConfigurationSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    def 'loads with the specified FXMLLocationResolver'() {
        given:
        def resolver = {controllerClass -> 'TestView'}

        when:
        FXController.addFXMLLocationResolver resolver
        and:
        def controller = FXController.of Test fromDefaultLocation() load()

        then:
        expect controller.message, is('Label Message')

        cleanup:
        FXController.removeFXMLLocationResolver resolver
    }

    def 'removes the added FXMLLocationResolver'() {
        given:
        def resolver = {controllerClass -> 'TestView'}
        FXController.addFXMLLocationResolver resolver

        expect:
        that FXController.of(Test).fromDefaultLocation().load().message, is('Label Message')

        when:
        FXController.removeFXMLLocationResolver resolver
        and:
        FXController.of Test fromDefaultLocation() load()

        then:
        thrown IllegalFXMLDefaultLocationException
    }

    private final class Test {
        @FXML
        private Label messageLabel;
        def String getMessage() { messageLabel.text }
    }
}
