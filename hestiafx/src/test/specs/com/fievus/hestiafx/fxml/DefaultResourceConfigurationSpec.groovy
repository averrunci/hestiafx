/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.fxml

import com.fievus.hestiafx.JavaFXApplication
import com.fievus.hestiafx.resources.XmlResourceBundle
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(FXController)
class DefaultResourceConfigurationSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    ResourceBundle initialDefaultResource

    def setup() {
        initialDefaultResource = FXController.defaultResource
    }

    def cleanup() {
        FXController.setDefaultResource initialDefaultResource
    }

    def 'gets null as the default ResourceBundle when nothing is set'() {
        expect:
        that FXController.defaultResource, is(nullValue())
    }

    def 'gets the default ResourceBundle which is set as the default Resource'() {
        given:
        ResourceBundle defaultResource = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.default_resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        FXController.setDefaultResource defaultResource

        then:
        expect FXController.defaultResource, is(defaultResource)
    }

    def 'loads with the specified ResourceBundle which is set as the default Resource'() {
        given:
        FXController.setDefaultResource ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.default_resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        def controller = FXController.of(TestViewWithResourceController)
            .fromDefaultLocation()
            .load()

        then:
        expect controller.message, is('Label Message')
        and:
        that controller.resourceMessage, is('Default Resource Message')
    }

    def 'loads with the specified resource base name which is set as the default resource'() {
        given:
        FXController.setDefaultResourceOf 'com.fievus.hestiafx.fxml.default_prop_resources'

        when:
        def controller = FXController.of(TestViewWithResourceController)
            .fromDefaultLocation()
            .load()

        then:
        expect controller.message, is('Label Message')
        and:
        that controller.resourceMessage, is('Default Resource Message from properties file')
    }

    def 'prefers ResourceBundle which is set with [with] method to the default resource'() {
        given:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )
        and:
        FXController.setDefaultResourceOf 'com.fievus.hestiafx.fxml.default_prop_resources'

        when:
        def controller = FXController.of(TestViewWithResourceController)
            .fromDefaultLocation()
            .with(resources)
            .load()

        then:
        expect controller.message, is('Label Message')
        and:
        that controller.resourceMessage, is('Resource Message')
    }

    def 'prefers ResourceBundle which is set with [withResourceOf] method to the default resource'() {
        given:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'
        and:
        FXController.setDefaultResourceOf 'com.fievus.hestiafx.fxml.default_prop_resources'

        when:
        def controller = FXController.of(TestViewWithResourceController)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect controller.message, is('Label Message')
        and:
        that controller.resourceMessage, is('Resource Message from properties file')
    }
}