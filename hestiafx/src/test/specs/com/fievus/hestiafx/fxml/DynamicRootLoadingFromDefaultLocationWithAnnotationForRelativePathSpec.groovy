/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.fxml

import com.fievus.hestiafx.JavaFXApplication
import com.fievus.hestiafx.resources.XmlResourceBundle
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(FXController)
class DynamicRootLoadingFromDefaultLocationWithAnnotationForRelativePathSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    // =============================================================================================
    // FXController.of(control)
    //     .fromDefaultLocation()
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads contents to the specified dynamic root controller from the default location annotated with the relative path'() {
        given:
        def control = new RelativeAnnotatedDynamicRootControl()

        when:
        FXController.of(control)
            .fromDefaultLocation()
            .load()

        then:
        expect control.message, is('Label Message')
    }

    def 'loads contents to the specified dynamic root controller from the default location annotated with the relative path with the specified ResourceBundle'() {
        given:
        def control = new RelativeAnnotatedDynamicRootControlWithResource()
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        FXController.of(control)
            .fromDefaultLocation()
            .with(resources)
            .load()

        then:
        expect control.message, is('Label Message')
        and:
        that control.resourceMessage, is('Resource Message')
    }

    def 'loads contents to the specified dynamic root controller from the default location annotated with the relative path with the specified resource base name'() {
        given:
        def control = new RelativeAnnotatedDynamicRootControlWithResource()
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        FXController.of(control)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect control.message, is('Label Message')
        and:
        that control.resourceMessage, is('Resource Message from properties file')
    }

    // =============================================================================================
    // FXController.of(scene)
    //     .fromDefaultLocation()
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads contents to the specified Scene from the default location annotated with the relative path'() {
        given:
        def scene = new RelativeAnnotatedTestScene()

        when:
        FXController.of(scene)
            .fromDefaultLocation()
            .load()

        then:
        expect scene.message, is('Label Message')
    }

    def 'loads contents to the specified Scene from the default location annotated with the relative path with the specified ResourceBundle'() {
        given:
        def scene = new RelativeAnnotatedTestSceneWithResource()
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        FXController.of(scene)
            .fromDefaultLocation()
            .with(resources)
            .load()

        then:
        expect scene.message, is('Label Message')
        and:
        that scene.resourceMessage, is('Resource Message')
    }

    def 'loads contents to the specified Scene from the default location annotated with the relative path with the specified resource base name'() {
        given:
        def scene = new RelativeAnnotatedTestSceneWithResource()
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        FXController.of(scene)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect scene.message, is('Label Message')
        and:
        that scene.resourceMessage, is('Resource Message from properties file')
    }

    // =============================================================================================
    // FXController.of(control)
    //     .using(injector)
    //     .from(location)
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads contents specified in the FXML as the dynamic root control with the specified control class and ControllerInjector from the default location annotated with the relative path'() {
        given:
        def injector = Mock ControllerInjector

        when:
        def control = FXController.of(RelativeAnnotatedDynamicRootControl)
            .using(injector)
            .fromDefaultLocation()
            .load()

        then:
        expect control.message, is('Label Message')
        and:
        1 * injector.getInstanceOf(RelativeAnnotatedDynamicRootControl, null) >> new RelativeAnnotatedDynamicRootControl()
    }

    def 'loads contents specified in the FXML as the dynamic root control with the specified control class, ControllerInjector, and ResourceBundle from the default location annotated with the relative path'() {
        given:
        def injector = Mock ControllerInjector
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        def control = FXController.of(RelativeAnnotatedDynamicRootControlWithResource)
            .using(injector)
            .fromDefaultLocation()
            .with(resources)
            .load()

        then:
        expect control.message, is('Label Message')
        and:
        that control.resourceMessage, is('Resource Message')
        and:
        1 * injector.getInstanceOf(RelativeAnnotatedDynamicRootControlWithResource, null) >> new RelativeAnnotatedDynamicRootControlWithResource()
    }

    def 'loads contents specified in the FXML as the dynamic root control with the specified control class, ControllerInjector, and resource base name from the default location annotated with the relative path'() {
        given:
        def injector = Mock ControllerInjector
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        def control = FXController.of(RelativeAnnotatedDynamicRootControlWithResource)
            .using(injector)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect control.message, is('Label Message')
        and:
        that control.resourceMessage, is('Resource Message from properties file')
        and:
        1 * injector.getInstanceOf(RelativeAnnotatedDynamicRootControlWithResource, null) >> new RelativeAnnotatedDynamicRootControlWithResource()
    }

    // =============================================================================================
    // FXController.of(scene)
    //     .using(injector)
    //     .from(location)
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads contents specified in the FXML as Scene with the specified Scene class and ControllerInjector from the default location annotated with the relative path'() {
        given:
        def injector = Mock ControllerInjector

        when:
        def scene = FXController.of(RelativeAnnotatedTestScene)
            .using(injector)
            .fromDefaultLocation()
            .load()

        then:
        expect scene.message, is('Label Message')
        and:
        1 * injector.getInstanceOf(RelativeAnnotatedTestScene, null) >> new RelativeAnnotatedTestScene()
    }

    def 'loads contents specified in the FXML as Scene with the specified Scene class, ControllerInjector, and ResourceBundle from the default location annotated with the relative path'() {
        given:
        def injector = Mock ControllerInjector
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        def scene = FXController.of(RelativeAnnotatedTestSceneWithResource)
            .using(injector)
            .fromDefaultLocation()
            .with(resources)
            .load()

        then:
        expect scene.message, is('Label Message')
        and:
        that scene.resourceMessage, is('Resource Message')
        and:
        1 * injector.getInstanceOf(RelativeAnnotatedTestSceneWithResource, null) >> new RelativeAnnotatedTestSceneWithResource()
    }

    def 'loads contents specified in the FXML as Scene with the specified Scene class, ControllerInjector, and resource base name from the default location annotated with the relative path'() {
        given:
        def injector = Mock ControllerInjector
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        def scene = FXController.of(RelativeAnnotatedTestSceneWithResource)
            .using(injector)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect scene.message, is('Label Message')
        and:
        that scene.resourceMessage, is('Resource Message from properties file')
        and:
        1 * injector.getInstanceOf(RelativeAnnotatedTestSceneWithResource, null) >> new RelativeAnnotatedTestSceneWithResource()
    }
}