/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.fxml

import com.fievus.hestiafx.JavaFXApplication
import com.fievus.hestiafx.resources.XmlResourceBundle
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(FXController)
class DynamicRootLoadingFromDefaultLocationWithNamingConventionSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    // =============================================================================================
    // FXController.of(control)
    //     .fromDefaultLocation()
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads contents to the specified dynamic root controller from the default location resolved from the controller class name'() {
        given:
        def control = new DynamicRootControl()

        when:
        FXController.of(control)
            .fromDefaultLocation()
            .load()

        then:
        expect control.message, is('Label Message')
    }

    def 'loads contents to the specified dynamic root controller from the default location resolved from the controller class name with the specified ResourceBundle'() {
        given:
        def control = new DynamicRootControlWithResource()
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        FXController.of(control)
            .fromDefaultLocation()
            .with(resources)
            .load()

        then:
        expect control.message, is('Label Message')
        and:
        that control.resourceMessage, is('Resource Message')
    }

    def 'loads contents to the specified dynamic root controller from the default location resolved from the controller class name with the specified resource base name'() {
        given:
        def control = new DynamicRootControlWithResource()
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        FXController.of(control)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect control.message, is('Label Message')
        and:
        that control.resourceMessage, is('Resource Message from properties file')
    }

    // =============================================================================================
    // FXController.of(scene)
    //     .fromDefaultLocation()
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads contents to the specified Scene from the default location resolved from the controller class name with the specified resource base name'() {
        given:
        def scene = new TestScene()

        when:
        FXController.of(scene)
            .fromDefaultLocation()
            .load()

        then:
        expect scene.message, is('Label Message')
    }

    def 'loads contents to the specified Scene with the specified ResourceBundle from the default location resolved from the controller class name with the specified resource base name'() {
        given:
        def scene = new TestSceneWithResource()
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        FXController.of(scene)
            .fromDefaultLocation()
            .with(resources)
            .load()

        then:
        expect scene.message, is('Label Message')
        and:
        that scene.resourceMessage, is('Resource Message')
    }

    def 'loads contents to the specified Scene with the specified resource base name from the default location resolved from the controller class name with the specified resource base name'() {
        given:
        def scene = new TestSceneWithResource()
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        FXController.of(scene)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect scene.message, is('Label Message')
        and:
        that scene.resourceMessage, is('Resource Message from properties file')
    }

    // =============================================================================================
    // FXController.of(control)
    //     .using(injector)
    //     .fromDefaultLocation()
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads contents specified in the FXML as the dynamic root control with the specified control class and ControllerInjector from the default location resolved from the controller class name with the specified resource base name'() {
        given:
        def injector = Mock ControllerInjector

        when:
        def control = FXController.of(DynamicRootControl)
            .using(injector)
            .fromDefaultLocation()
            .load()

        then:
        expect control.message, is('Label Message')
        and:
        1 * injector.getInstanceOf(DynamicRootControl, null) >> new DynamicRootControl()
    }

    def 'loads contents specified in the FXML as the dynamic root control with the specified control class, ControllerInjector, and ResourceBundle from the default location resolved from the controller class name with the specified resource base name'() {
        given:
        def injector = Mock ControllerInjector
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        def control = FXController.of(DynamicRootControlWithResource)
            .using(injector)
            .fromDefaultLocation()
            .with(resources)
            .load()

        then:
        expect control.message, is('Label Message')
        and:
        that control.resourceMessage, is('Resource Message')
        and:
        1 * injector.getInstanceOf(DynamicRootControlWithResource, null) >> new DynamicRootControlWithResource()
    }

    def 'loads contents specified in the FXML as the dynamic root control with the specified control class, ControllerInjector, and resource base name from the default location resolved from the controller class name with the specified resource base name'() {
        given:
        def injector = Mock ControllerInjector
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        def control = FXController.of(DynamicRootControlWithResource)
            .using(injector)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect control.message, is('Label Message')
        and:
        that control.resourceMessage, is('Resource Message from properties file')
        and:
        1 * injector.getInstanceOf(DynamicRootControlWithResource, null) >> new DynamicRootControlWithResource()
    }

    // =============================================================================================
    // FXController.of(scene)
    //     .using(injector)
    //     .fromDefaultLocation()
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads contents specified in the FXML as Scene with the specified Scene class and ControllerInjector from the default location resolved from the controller class name with the specified resource base name'() {
        given:
        def injector = Mock ControllerInjector

        when:
        def scene = FXController.of(TestScene)
            .using(injector)
            .fromDefaultLocation()
            .load()

        then:
        expect scene.message, is('Label Message')
        and:
        1 * injector.getInstanceOf(TestScene, null) >> new TestScene()
    }

    def 'loads contents specified in the FXML as Scene with the specified Scene class, ControllerInjector, and ResourceBundle from the default location resolved from the controller class name with the specified resource base name'() {
        given:
        def injector = Mock ControllerInjector
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        def scene = FXController.of(TestSceneWithResource)
            .using(injector)
            .fromDefaultLocation()
            .with(resources)
            .load()

        then:
        expect scene.message, is('Label Message')
        and:
        that scene.resourceMessage, is('Resource Message')
        and:
        1 * injector.getInstanceOf(TestSceneWithResource, null) >> new TestSceneWithResource()
    }

    def 'loads contents specified in the FXML as Scene with the specified Scene class, ControllerInjector, and resource base name from the default location resolved from the controller class name with the specified resource base name'() {
        given:
        def injector = Mock ControllerInjector
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        def scene = FXController.of(TestSceneWithResource)
            .using(injector)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect scene.message, is('Label Message')
        and:
        that scene.resourceMessage, is('Resource Message from properties file')
        and:
        1 * injector.getInstanceOf(TestSceneWithResource, null) >> new TestSceneWithResource()
    }
}
