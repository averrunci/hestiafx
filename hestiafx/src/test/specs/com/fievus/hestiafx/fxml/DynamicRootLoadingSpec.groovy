/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.fxml

import com.fievus.hestiafx.JavaFXApplication
import com.fievus.hestiafx.resources.XmlResourceBundle
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(FXController)
class DynamicRootLoadingSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    // =============================================================================================
    // FXController.of(control)
    //     .from(location)
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads contents to the specified dynamic root controller with the specified URL'() {
        given:
        def control = new DynamicRootControl()
        and:
        def location = getClass().getResource 'DynamicRootControl.fxml'

        when:
        FXController.of(control)
            .from(location)
            .load()

        then:
        expect control.message, is('Label Message')
    }

    def 'loads contents to the specified dynamic root controller with the specified URL and ResourceBundle'() {
        given:
        def control = new DynamicRootControlWithResource()
        and:
        def location = getClass().getResource 'DynamicRootControlWithResource.fxml'
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        FXController.of(control)
            .from(location)
            .with(resources)
            .load()

        then:
        expect control.message, is('Label Message')
        and:
        that control.resourceMessage, is('Resource Message')
    }

    def 'loads contents to the specified dynamic root controller with the specified URL and resource base name'() {
        given:
        def control = new DynamicRootControlWithResource()
        and:
        def location = getClass().getResource 'DynamicRootControlWithResource.fxml'
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        FXController.of(control)
            .from(location)
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect control.message, is('Label Message')
        and:
        that control.resourceMessage, is('Resource Message from properties file')
    }

    // =============================================================================================
    // FXController.of(scene)
    //     .from(location)
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads contents to the specified Scene with the specified URL'() {
        given:
        def scene = new TestScene()
        and:
        def location = getClass().getResource 'TestScene.fxml'

        when:
        FXController.of(scene)
            .from(location)
            .load()

        then:
        expect scene.message, is('Label Message')
    }

    def 'loads contents to the specified Scene with the specified URL and ResourceBundle'() {
        given:
        def scene = new TestSceneWithResource()
        and:
        def location = getClass().getResource 'TestSceneWithResource.fxml'
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        FXController.of(scene)
            .from(location)
            .with(resources)
            .load()

        then:
        expect scene.message, is('Label Message')
        and:
        that scene.resourceMessage, is('Resource Message')
    }

    def 'loads contents to the specified Scene with the specified URL and resource base name'() {
        given:
        def scene = new TestSceneWithResource()
        and:
        def location = getClass().getResource 'TestSceneWithResource.fxml'
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        FXController.of(scene)
            .from(location)
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect scene.message, is('Label Message')
        and:
        that scene.resourceMessage, is('Resource Message from properties file')
    }

    // =============================================================================================
    // FXController.of(control)
    //     .using(injector)
    //     .from(location)
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads contents specified in the FXML as the dynamic root control with the specified control class, ControllerInjector, and URL'() {
        given:
        def location = getClass().getResource 'DynamicRootControl.fxml'
        and:
        def injector = Mock ControllerInjector

        when:
        def control = FXController.of(DynamicRootControl)
            .using(injector)
            .from(location)
            .load()

        then:
        expect control.message, is('Label Message')
        and:
        1 * injector.getInstanceOf(DynamicRootControl, null) >> new DynamicRootControl()
    }

    def 'loads contents specified in the FXML as the dynamic root control with the specified control class, ControllerInjector, URL, and ResourceBundle'() {
        given:
        def location = getClass().getResource 'DynamicRootControlWithResource.fxml'
        and:
        def injector = Mock ControllerInjector
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        def control = FXController.of(DynamicRootControlWithResource)
            .using(injector)
            .from(location)
            .with(resources)
            .load()

        then:
        expect control.message, is('Label Message')
        and:
        that control.resourceMessage, is('Resource Message')
        and:
        1 * injector.getInstanceOf(DynamicRootControlWithResource, null) >> new DynamicRootControlWithResource()
    }

    def 'loads contents specified in the FXML as the dynamic root control with the specified control class, ControllerInjector, URL, and resource base name'() {
        given:
        def location = getClass().getResource 'DynamicRootControlWithResource.fxml'
        and:
        def injector = Mock ControllerInjector
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        def control = FXController.of(DynamicRootControlWithResource)
            .using(injector)
            .from(location)
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect control.message, is('Label Message')
        and:
        that control.resourceMessage, is('Resource Message from properties file')
        and:
        1 * injector.getInstanceOf(DynamicRootControlWithResource, null) >> new DynamicRootControlWithResource()
    }

    // =============================================================================================
    // FXController.of(scene)
    //     .using(injector)
    //     .from(location)
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads contents specified in the FXML as Scene with the specified Scene class, ControllerInjector, and URL'() {
        given:
        def location = getClass().getResource 'TestScene.fxml'
        and:
        def injector = Mock ControllerInjector

        when:
        def scene = FXController.of(TestScene)
            .using(injector)
            .from(location)
            .load()

        then:
        expect scene.message, is('Label Message')
        and:
        1 * injector.getInstanceOf(TestScene, null) >> new TestScene()
    }

    def 'loads contents specified in the FXML as Scene with the specified Scene class, ControllerInjector, URL, and ResourceBundle'() {
        given:
        def location = getClass().getResource 'TestSceneWithResource.fxml'
        and:
        def injector = Mock ControllerInjector
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        def scene = FXController.of(TestSceneWithResource)
            .using(injector)
            .from(location)
            .with(resources)
            .load()

        then:
        expect scene.message, is('Label Message')
        and:
        that scene.resourceMessage, is('Resource Message')
        and:
        1 * injector.getInstanceOf(TestSceneWithResource, null) >> new TestSceneWithResource()
    }

    def 'loads contents specified in the FXML as Scene with the specified Scene class, ControllerInjector, URL, and resource base name'() {
        given:
        def location = getClass().getResource 'TestSceneWithResource.fxml'
        and:
        def injector = Mock ControllerInjector
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        def scene = FXController.of(TestSceneWithResource)
            .using(injector)
            .from(location)
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect scene.message, is('Label Message')
        and:
        that scene.resourceMessage, is('Resource Message from properties file')
        and:
        1 * injector.getInstanceOf(TestSceneWithResource, null) >> new TestSceneWithResource()
    }
}