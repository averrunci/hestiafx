/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.fxml

import spock.lang.Specification
import spock.lang.Subject

@Subject(FXController)
class FXControllerLoadingExceptionSpec extends Specification {
    def 'throws an exception when null is specified for location'() {
        given:
        def location = null

        when:
        FXController.from location load()

        then:
        thrown NullPointerException
    }

    def 'throws an exception when unknown location is specified for location'() {
        given:
        def location = new URL('file://NotFoundView.fxml')

        when:
        FXController.from location load()

        then:
        thrown FXMLLoadException
    }

    def 'throws an exception when null is specified for controller class'() {
        given:
        def controllerClass = null

        when:
        FXController.of controllerClass fromDefaultLocation() load()

        then:
        thrown NullPointerException
    }

    def 'throws an exception when the controller class is specified and null is specified for location'() {
        given:
        def controllerClass = TestViewController
        and:
        def location = null

        when:
        FXController.of controllerClass from location load()

        then:
        thrown NullPointerException
    }

    def 'throws an exception when null is specified for controller instance'() {
        given:
        def controller = null

        when:
        FXController.of controller fromDefaultLocation() load()

        then:
        thrown NullPointerException
    }

    def 'throws an exception when the controller class is specified and the FXML file for its default location does not exist'() {
        given:
        def controllerClass = DefaultNotFoundViewController

        when:
        FXController.of controllerClass fromDefaultLocation() load()

        then:
        thrown IllegalFXMLDefaultLocationException
    }
}