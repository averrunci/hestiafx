/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.fxml

import com.fievus.hestiafx.JavaFXApplication
import com.fievus.hestiafx.resources.XmlResourceBundle
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(FXController)
class FXControllerLoadingFromDefaultLocationWithAnnotationForAbsolutePathSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    // =============================================================================================
    // FXController.of(controllerClass)
    //     .fromDefaultLocation()
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads the controller specified in the FXML file from the default location annotated with the absolute path'() {
        when:
        def controller = FXController.of(AbsoluteAnnotatedTestViewController)
            .fromDefaultLocation()
            .load()

        then:
        expect controller.message, is('Label Message')
    }

    def 'loads the controller specified in the FXML file from the default location annotated with the absolute path with the specified ResourceBundle'() {
        given:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        def controller = FXController.of(AbsoluteAnnotatedTestViewWithResourceController)
            .fromDefaultLocation()
            .with(resources)
            .load()

        then:
        expect controller.message, is('Label Message')
        and:
        that controller.resourceMessage, is('Resource Message')
    }

    def 'loads the controller specified in the FXML file from the default location annotated with the absolute path with the specified resource base name'() {
        given:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        def controller = FXController.of(AbsoluteAnnotatedTestViewWithResourceController)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect controller.message, is('Label Message')
        and:
        that controller.resourceMessage, is('Resource Message from properties file')
    }

    // =============================================================================================
    // FXController.of(controllerInstance)
    //     .fromDefaultLocation()
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads the FXML contents to the specified controller from the default location annotated with the absolute path'() {
        given:
        def controller = new AbsoluteAnnotatedSimpleTestViewController()

        when:
        FXController.of(controller)
            .fromDefaultLocation()
            .load()

        then:
        expect controller.message, is('Simple Label Message')
    }

    def 'loads the FXML contents to the specified controller from the default location annotated with the absolute path with the specified ResourceBundle'() {
        given:
        def controller = new AbsoluteAnnotatedSimpleTestViewWithResourceController()
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        FXController.of(controller)
            .fromDefaultLocation()
            .with(resources)
            .load()

        then:
        expect controller.message, is('Simple Label Message')
        and:
        that controller.resourceMessage, is('Simple Resource Message')
    }

    def 'loads the FXML contents to the specified controller from the default location annotated with the absolute path with the specified resource base name'() {
        given:
        def controller = new AbsoluteAnnotatedSimpleTestViewWithResourceController()
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        FXController.of(controller)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect controller.message, is('Simple Label Message')
        and:
        that controller.resourceMessage, is('Simple Resource Message from properties file')
    }

    // =============================================================================================
    // FXController.of(controllerClass)
    //     .using(injector)
    //     .from(location)
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads the controller injected with the specified ControllerInjector from the default location annotated with the absolute path'() {
        given:
        def injector = Mock ControllerInjector

        when:
        def controller = FXController.of(AbsoluteAnnotatedSimpleTestViewController)
            .using(injector)
            .fromDefaultLocation()
            .load()

        then:
        expect controller.message, is('Simple Label Message')
        and:
        1 * injector.getInstanceOf(AbsoluteAnnotatedSimpleTestViewController, null) >> new AbsoluteAnnotatedSimpleTestViewController()
    }

    def 'loads the controller injected with the specified ControllerInjector and ResourceBundle from the default location annotated with the absolute path'() {
        given:
        def injector = Mock ControllerInjector
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        def controller = FXController.of(AbsoluteAnnotatedSimpleTestViewWithResourceController)
            .using(injector)
            .fromDefaultLocation()
            .with(resources)
            .load()

        then:
        expect controller.message, is('Simple Label Message')
        and:
        that controller.resourceMessage, is('Simple Resource Message')
        and:
        1 * injector.getInstanceOf(AbsoluteAnnotatedSimpleTestViewWithResourceController, null) >> new AbsoluteAnnotatedSimpleTestViewWithResourceController()
    }

    def 'loads the controller injected with the specified ControllerInjector and resource base name from the default location annotated with the absolute path'() {
        given:
        def injector = Mock ControllerInjector
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        def controller = FXController.of(AbsoluteAnnotatedSimpleTestViewWithResourceController)
            .using(injector)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect controller.message, is('Simple Label Message')
        and:
        that controller.resourceMessage, is('Simple Resource Message from properties file')
        and:
        1 * injector.getInstanceOf(AbsoluteAnnotatedSimpleTestViewWithResourceController, null) >> new AbsoluteAnnotatedSimpleTestViewWithResourceController()
    }

    // =============================================================================================
    // FXController.of(controllerInstance)
    //     .using(injector)
    //     .from(location)
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads the FXML contents to the the specified controller with the specified ControllerInjector from the default location annotated with the absolute path'() {
        given:
        def controller = new AbsoluteAnnotatedSimpleTestViewController()
        and:
        def injector = Mock ControllerInjector

        when:
        FXController.of(controller)
            .using(injector)
            .fromDefaultLocation()
            .load()

        then:
        expect controller.message, is('Simple Label Message')
        and:
        1 * injector.getInstanceOf(controller.class, controller) >> controller
    }

    def 'loads the FXML contents to the specified controller with the specified ControllerInjector and ResourceBundle from the default location annotated with the absolute path'() {
        given:
        def controller = new AbsoluteAnnotatedSimpleTestViewWithResourceController()
        and:
        def injector = Mock ControllerInjector
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        FXController.of(controller)
            .using(injector)
            .fromDefaultLocation()
            .with(resources)
            .load()

        then:
        expect controller.message, is('Simple Label Message')
        and:
        that controller.resourceMessage, is('Simple Resource Message')
        and:
        1 * injector.getInstanceOf(controller.class, controller) >> controller
    }

    def 'loads the FXML contents to the specified controller with the specified ControllerInjector and resource base name from the default location annotated with the absolute path'() {
        given:
        def controller = new AbsoluteAnnotatedSimpleTestViewWithResourceController()
        and:
        def injector = Mock ControllerInjector
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        FXController.of(controller)
            .using(injector)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect controller.message, is('Simple Label Message')
        and:
        that controller.resourceMessage, is('Simple Resource Message from properties file')
        and:
        1 * injector.getInstanceOf(controller.class, controller) >> controller
    }
}
