/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.fxml

import com.fievus.hestiafx.JavaFXApplication
import com.fievus.hestiafx.resources.XmlResourceBundle
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(FXController)
class FXControllerLoadingFromDefaultLocationWithAnnotationForRelativePathSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    // =============================================================================================
    // FXController.of(controllerClass)
    //     .fromDefaultLocation()
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads the controller specified in the FXML file from the default location annotated with the relative path'() {
        when:
        def controller = FXController.of(RelativeAnnotatedTestViewController)
            .fromDefaultLocation()
            .load()

        then:
        expect controller.message, is('Label Message')
    }

    def 'loads the controller specified in the FXML file from the default location annotated with the relative path with the specified ResourceBundle'() {
        given:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        def controller = FXController.of(RelativeAnnotatedTestViewWithResourceController)
            .fromDefaultLocation()
            .with(resources)
            .load()

        then:
        expect controller.message, is('Label Message')
        and:
        that controller.resourceMessage, is('Resource Message')
    }

    def 'loads the controller specified in the FXML file from the default location annotated with the relative path with the specified resource base name'() {
        given:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        def controller = FXController.of(RelativeAnnotatedTestViewWithResourceController)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect controller.message, is('Label Message')
        and:
        that controller.resourceMessage, is('Resource Message from properties file')
    }

    // =============================================================================================
    // FXController.of(controllerInstance)
    //     .fromDefaultLocation()
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads the FXML contents to the specified controller from the default location annotated with the relative path'() {
        given:
        def controller = new RelativeAnnotatedSimpleTestViewController()

        when:
        FXController.of(controller)
            .fromDefaultLocation()
            .load()

        then:
        expect controller.message, is('Simple Label Message')
    }

    def 'loads the FXML contents to the specified controller from the default location annotated with the relative path with the specified ResourceBundle'() {
        given:
        def controller = new RelativeAnnotatedSimpleTestViewWithResourceController()
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        FXController.of(controller)
            .fromDefaultLocation()
            .with(resources)
            .load()

        then:
        expect controller.message, is('Simple Label Message')
        and:
        that controller.resourceMessage, is('Simple Resource Message')
    }

    def 'loads the FXML contents to the specified controller from the default location annotated with the relative path with the specified resource base name'() {
        given:
        def controller = new RelativeAnnotatedSimpleTestViewWithResourceController()
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        FXController.of(controller)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect controller.message, is('Simple Label Message')
        and:
        that controller.resourceMessage, is('Simple Resource Message from properties file')
    }

    // =============================================================================================
    // FXController.of(controllerClass)
    //     .using(injector)
    //     .from(location)
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads the controller injected with the specified ControllerInjector from the default location annotated with the relative path'() {
        given:
        def injector = Mock ControllerInjector

        when:
        def controller = FXController.of(RelativeAnnotatedSimpleTestViewController)
            .using(injector)
            .fromDefaultLocation()
            .load()

        then:
        expect controller.message, is('Simple Label Message')
        and:
        1 * injector.getInstanceOf(RelativeAnnotatedSimpleTestViewController, null) >> new RelativeAnnotatedSimpleTestViewController()
    }

    def 'loads the controller injected with the specified ControllerInjector and ResourceBundle from the default location annotated with the relative path'() {
        given:
        def injector = Mock ControllerInjector
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        def controller = FXController.of(RelativeAnnotatedSimpleTestViewWithResourceController)
            .using(injector)
            .fromDefaultLocation()
            .with(resources)
            .load()

        then:
        expect controller.message, is('Simple Label Message')
        and:
        that controller.resourceMessage, is('Simple Resource Message')
        and:
        1 * injector.getInstanceOf(RelativeAnnotatedSimpleTestViewWithResourceController, null) >> new RelativeAnnotatedSimpleTestViewWithResourceController()
    }

    def 'loads the controller injected with the specified ControllerInjector and resource base name from the default location annotated with the relative path'() {
        given:
        def injector = Mock ControllerInjector
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        def controller = FXController.of(RelativeAnnotatedSimpleTestViewWithResourceController)
            .using(injector)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect controller.message, is('Simple Label Message')
        and:
        that controller.resourceMessage, is('Simple Resource Message from properties file')
        and:
        1 * injector.getInstanceOf(RelativeAnnotatedSimpleTestViewWithResourceController, null) >> new RelativeAnnotatedSimpleTestViewWithResourceController()
    }

    // =============================================================================================
    // FXController.of(controllerInstance)
    //     .using(injector)
    //     .from(location)
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads the FXML contents to the specified controller with the specified ControllerInjector from the default location annotated with the relative path'() {
        given:
        def controller = new RelativeAnnotatedSimpleTestViewController()
        and:
        def injector = Mock ControllerInjector

        when:
        FXController.of(controller)
            .using(injector)
            .fromDefaultLocation()
            .load()

        then:
        expect controller.message, is('Simple Label Message')
        and:
        1 * injector.getInstanceOf(controller.class, controller) >> controller
    }

    def 'loads the FXML contents to the specified controller with the specified ControllerInjector and ResourceBundle from the default location annotated with the relative path'() {
        given:
        def controller = new RelativeAnnotatedSimpleTestViewWithResourceController()
        and:
        def injector = Mock ControllerInjector
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        FXController.of(controller)
            .using(injector)
            .fromDefaultLocation()
            .with(resources)
            .load()

        then:
        expect controller.message, is('Simple Label Message')
        and:
        that controller.resourceMessage, is('Simple Resource Message')
        and:
        1 * injector.getInstanceOf(controller.class, controller) >> controller
    }

    def 'loads the FXML contents to the specified controller with the specified ControllerInjector and resource base name from the default location annotated with the relative path'() {
        given:
        def controller = new RelativeAnnotatedSimpleTestViewWithResourceController()
        and:
        def injector = Mock ControllerInjector
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        FXController.of(controller)
            .using(injector)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect controller.message, is('Simple Label Message')
        and:
        that controller.resourceMessage, is('Simple Resource Message from properties file')
        and:
        1 * injector.getInstanceOf(controller.class, controller) >> controller
    }
}