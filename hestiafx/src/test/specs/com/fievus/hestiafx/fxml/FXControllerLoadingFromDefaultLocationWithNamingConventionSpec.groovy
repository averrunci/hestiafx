/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.fxml

import com.fievus.hestiafx.JavaFXApplication
import com.fievus.hestiafx.resources.XmlResourceBundle
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(FXController)
class FXControllerLoadingFromDefaultLocationWithNamingConventionSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    // =============================================================================================
    // FXController.of(controllerClass)
    //     .fromDefaultLocation()
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    @Unroll
    def 'loads the controller specified in the FXML file from the default location which is the name removed #suffix from the end of the controller class name'() {
        when:
        def controller = FXController.of(controllerClass)
            .fromDefaultLocation()
            .load()

        then:
        expect controller.message, is('Label Message')

        where:
        suffix       | controllerClass
        'Controller' | TestViewController
        'Skin'       | STestViewSkin
    }

    @Unroll
    def 'loads the controller specified in the FXML file from the default location which is the name removed #suffix from the end of the controller class name with the specified ResourceBundle'() {
        given:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        def controller = FXController.of(controllerClass)
            .fromDefaultLocation()
            .with(resources)
            .load()

        then:
        expect controller.message, is('Label Message')
        and:
        that controller.resourceMessage, is('Resource Message')

        where:
        suffix       | controllerClass
        'Controller' | TestViewWithResourceController
        'Skin'       | STestViewWithResourceSkin
    }

    @Unroll
    def 'loads the controller specified in the FXML file from the default location which is the name removed #suffix from the end of the controller class name with the specified resource base name'() {
        given:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        def controller = FXController.of(controllerClass)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect controller.message, is('Label Message')
        and:
        that controller.resourceMessage, is('Resource Message from properties file')

        where:
        suffix       | controllerClass
        'Controller' | TestViewWithResourceController
        'Skin'       | STestViewWithResourceSkin
    }

    // =============================================================================================
    // FXController.of(controllerInstance)
    //     .fromDefaultLocation()
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    @Unroll
    def 'loads the FXML contents to the specified controller from the default location which is the name removed #suffix from the end of the controller class name'() {
        when:
        FXController.of(controller)
            .fromDefaultLocation()
            .load()

        then:
        expect controller.message, is('Simple Label Message')

        where:
        suffix       | controller
        'Controller' | new SimpleTestViewController()
        'Skin'       | new SimpleTestViewSkin()
    }

    @Unroll
    def 'loads the FXML contents to the specified controller from the default location which is the name removed #suffix from the end of the controller class name with the specified ResourceBundle'() {
        given:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        FXController.of(controller)
            .fromDefaultLocation()
            .with(resources)
            .load()

        then:
        expect controller.message, is('Simple Label Message')
        and:
        that controller.resourceMessage, is('Simple Resource Message')

        where:
        suffix       | controller
        'Controller' | new SimpleTestViewWithResourceController()
        'Skin'       | new SimpleTestViewWithResourceSkin()
    }

    @Unroll
    def 'loads the FXML contents to the specified controller from the default location which is the name removed #suffix from the end of the controller class name with the specified resource base name'() {
        given:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        FXController.of(controller)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect controller.message, is('Simple Label Message')
        and:
        that controller.resourceMessage, is('Simple Resource Message from properties file')

        where:
        suffix       | controller
        'Controller' | new SimpleTestViewWithResourceController()
        'Skin'       | new SimpleTestViewWithResourceSkin()
    }

    // =============================================================================================
    // FXController.of(controllerClass)
    //     .using(injector)
    //     .from(location)
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    @Unroll
    def 'loads the controller injected with the specified ControllerInjector from the default location which is the name removed #suffix from the end of the controller class name'() {
        given:
        def injector = Mock ControllerInjector

        when:
        def controller = FXController.of(controllerClass)
            .using(injector)
            .fromDefaultLocation()
            .load()

        then:
        expect controller.message, is('Label Message')
        and:
        1 * injector.getInstanceOf(controllerClass, null) >> controllerClass.newInstance()

        where:
        suffix       | controllerClass
        'Controller' | InjectionTestViewController
        'Skin'       | InjectionTestViewSkin
    }

    @Unroll
    def 'loads the controller injected with the specified ControllerInjector and ResourceBundle from the default location which is the name removed #suffix from the end of the controller class name'() {
        given:
        def injector = Mock ControllerInjector
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        def controller = FXController.of(controllerClass)
            .using(injector)
            .fromDefaultLocation()
            .with(resources)
            .load()

        then:
        expect controller.message, is('Label Message')
        and:
        that controller.resourceMessage, is('Resource Message')
        and:
        1 * injector.getInstanceOf(controllerClass, null) >> controllerClass.newInstance()

        where:
        suffix       | controllerClass
        'Controller' | InjectionTestViewWithResourceController
        'Skin'       | InjectionTestViewWithResourceSkin
    }

    @Unroll
    def 'loads the controller injected with the specified ControllerInjector and resource base name from the default location which is the name removed #suffix from the end of the controller class name'() {
        given:
        def injector = Mock ControllerInjector
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        def controller = FXController.of(controllerClass)
            .using(injector)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect controller.message, is('Label Message')
        and:
        that controller.resourceMessage, is('Resource Message from properties file')
        and:
        1 * injector.getInstanceOf(controllerClass, null) >> controllerClass.newInstance()

        where:
        suffix       | controllerClass
        'Controller' | InjectionTestViewWithResourceController
        'Skin'       | InjectionTestViewWithResourceSkin
    }

    // =============================================================================================
    // FXController.of(controllerInstance)
    //     .using(injector)
    //     .from(location)
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    @Unroll
    def 'loads the FXML contents to the specified controller with the specified ControllerInjector from the default location which is the name removed #suffix from the end of the controller class name'() {
        given:
        def injector = Mock ControllerInjector

        when:
        FXController.of(controller)
            .using(injector)
            .fromDefaultLocation()
            .load()

        then:
        expect controller.message, is('Label Message')
        and:
        1 * injector.getInstanceOf(controller.class, controller) >> controller

        where:
        suffix       | controller
        'Controller' | new InjectionTestViewController()
        'Skin'       | new InjectionTestViewSkin()
    }

    @Unroll
    def 'loads the FXML contents to the specified controller with the specified ControllerInjector and ResourceBundle from the default location which is the name removed #suffix from the end of the controller class name'() {
        given:
        def injector = Mock ControllerInjector
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        FXController.of(controller)
            .using(injector)
            .fromDefaultLocation()
            .with(resources)
            .load()

        then:
        expect controller.message, is('Label Message')
        and:
        that controller.resourceMessage, is('Resource Message')
        and:
        1 * injector.getInstanceOf(controller.class, controller) >> controller

        where:
        suffix       | controller
        'Controller' | new InjectionTestViewWithResourceController()
        'Skin'       | new InjectionTestViewWithResourceSkin()
    }

    @Unroll
    def 'loads the FXML contents to the specified controller with the specified ControllerInjector and resource base name from the default location which is the name removed #suffix from the end of the controller class name'() {
        given:
        def injector = Mock ControllerInjector
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        FXController.of(controller)
            .using(injector)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect controller.message, is('Label Message')
        and:
        that controller.resourceMessage, is('Resource Message from properties file')
        and:
        1 * injector.getInstanceOf(controller.class, controller) >> controller

        where:
        suffix       | controller
        'Controller' | new InjectionTestViewWithResourceController()
        'Skin'       | new InjectionTestViewWithResourceSkin()
    }
}