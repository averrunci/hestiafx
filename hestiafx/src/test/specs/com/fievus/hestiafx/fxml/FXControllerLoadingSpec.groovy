/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.fxml

import com.fievus.hestiafx.JavaFXApplication
import com.fievus.hestiafx.resources.XmlResourceBundle
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(FXController)
class FXControllerLoadingSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    // =============================================================================================
    // FXController.from(location)
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads the controller specified in the FXML file with the specified URL'() {
        given:
        def location = getClass().getResource 'TestView.fxml'

        when:
        TestViewController controller = FXController.<TestViewController>from location load()

        then:
        expect controller.message, is('Label Message')
    }

    def 'loads the controller specified in the FXML file with the specified URL and ResourceBundle'() {
        given:
        def location = getClass().getResource 'TestViewWithResource.fxml'
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        TestViewWithResourceController controller = FXController.<TestViewWithResourceController>from(location)
            .with(resources)
            .load()

        then:
        expect controller.message, is('Label Message')
        and:
        that controller.resourceMessage, is('Resource Message')
    }

    def 'loads the controller specified in the FXML file with the specified URL and resource base name'() {
        given:
        def location = getClass().getResource 'TestViewWithResource.fxml'
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        TestViewWithResourceController controller = FXController.<TestViewWithResourceController>from(location)
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect controller.message, is('Label Message')
        and:
        that controller.resourceMessage, is('Resource Message from properties file')
    }

    // =============================================================================================
    // FXController.of(controllerInstance)
    //     .from(location)
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads the FXML contents to the specified controller with the specified URL'() {
        given:
        def controller = new SimpleTestViewController()
        and:
        def location = getClass().getResource 'SimpleTestView.fxml'

        when:
        FXController.of(controller)
            .from(location)
            .load()

        then:
        expect controller.message, is('Simple Label Message')
    }

    def 'loads the FXML contents to the specified controller with the specified URL and ResourceBundle'() {
        given:
        def controller = new SimpleTestViewWithResourceController()
        and:
        def location = getClass().getResource 'SimpleTestViewWithResource.fxml'
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        FXController.of(controller)
            .from(location)
            .with(resources)
            .load()

        then:
        expect controller.message, is('Simple Label Message')
        and:
        that controller.resourceMessage, is('Simple Resource Message')
    }

    def 'loads the FXML contents to the specified controller with the specified URL and resource base name'() {
        given:
        def controller = new SimpleTestViewWithResourceController()
        and:
        def location = getClass().getResource 'SimpleTestViewWithResource.fxml'
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        FXController.of(controller)
            .from(location)
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect controller.message, is('Simple Label Message')
        and:
        that controller.resourceMessage, is('Simple Resource Message from properties file')
    }

    // =============================================================================================
    // FXController.of(controllerClass)
    //     .using(injector)
    //     .from(location)
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads the controller injected with the specified ControllerInjector and URL'() {
        given:
        def location = getClass().getResource 'InjectionTestView.fxml'
        and:
        def injector = Mock ControllerInjector

        when:
        def controller = FXController.of(InjectionTestViewController)
            .using(injector)
            .from(location)
            .load()

        then:
        expect controller.message, is('Label Message')
        and:
        1 * injector.getInstanceOf(InjectionTestViewController, null) >> new InjectionTestViewController()
    }

    def 'loads the controller injected with the specified ControllerInjector, URL, and ResourceBundle'() {
        given:
        def location = getClass().getResource 'InjectionTestViewWithResource.fxml'
        and:
        def injector = Mock ControllerInjector
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        def controller = FXController.of(InjectionTestViewWithResourceController)
            .using(injector)
            .from(location)
            .with(resources)
            .load()

        then:
        expect controller.message, is('Label Message')
        and:
        that controller.resourceMessage, is('Resource Message')
        and:
        1 * injector.getInstanceOf(InjectionTestViewWithResourceController, null) >> new InjectionTestViewWithResourceController()
    }

    def 'loads the controller injected with the specified ControllerInjector, URL, and resource base name'() {
        given:
        def location = getClass().getResource 'InjectionTestViewWithResource.fxml'
        and:
        def injector = Mock ControllerInjector
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        def controller = FXController.of(InjectionTestViewWithResourceController)
            .using(injector)
            .from(location)
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect controller.message, is('Label Message')
        and:
        that controller.resourceMessage, is('Resource Message from properties file')
        and:
        1 * injector.getInstanceOf(InjectionTestViewWithResourceController, null) >> new InjectionTestViewWithResourceController()
    }

    // =============================================================================================
    // FXController.of(controllerInstance)
    //     .using(injector)
    //     .from(location)
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads the FXML contents to the specified controller with the specified ControllerInjector and URL'() {
        given:
        def controller = new InjectionTestViewController()
        and:
        def location = getClass().getResource 'InjectionTestView.fxml'
        and:
        def injector = Mock ControllerInjector

        when:
        FXController.of(controller)
            .using(injector)
            .from(location)
            .load()

        then:
        expect controller.message, is('Label Message')
        and:
        1 * injector.getInstanceOf(InjectionTestViewController, controller) >> controller
    }

    def 'loads the FXML contents to the specified controller with the specified ControllerInjector, URL, and ResourceBundle'() {
        given:
        def controller = new InjectionTestViewWithResourceController()
        and:
        def location = getClass().getResource 'InjectionTestViewWithResource.fxml'
        and:
        def injector = Mock ControllerInjector
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        FXController.of(controller)
            .using(injector)
            .from(location)
            .with(resources)
            .load()

        then:
        expect controller.message, is('Label Message')
        and:
        that controller.resourceMessage, is('Resource Message')
        and:
        1 * injector.getInstanceOf(InjectionTestViewWithResourceController, controller) >> controller
    }

    def 'loads the FXML contents to the specified controller with the specified ControllerInjector, URL, and resource base name'() {
        given:
        def controller = new InjectionTestViewWithResourceController()
        and:
        def location = getClass().getResource 'InjectionTestViewWithResource.fxml'
        and:
        def injector = Mock ControllerInjector
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        FXController.of(controller)
            .using(injector)
            .from(location)
            .withResourceOf(resourceBaseName)
            .load()

        then:
        expect controller.message, is('Label Message')
        and:
        that controller.resourceMessage, is('Resource Message from properties file')
        and:
        1 * injector.getInstanceOf(InjectionTestViewWithResourceController, controller) >> controller
    }
}