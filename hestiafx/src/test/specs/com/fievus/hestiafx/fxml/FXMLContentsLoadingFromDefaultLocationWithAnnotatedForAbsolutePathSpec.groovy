/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.fxml

import com.fievus.hestiafx.JavaFXApplication
import com.fievus.hestiafx.resources.XmlResourceBundle
import javafx.scene.layout.StackPane
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(FXController)
class FXMLContentsLoadingFromDefaultLocationWithAnnotatedForAbsolutePathSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    // =============================================================================================
    // FXController.of(controllerClass)
    //     .fromDefaultLocation()
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads contents specified in the FXML file from the default location annotated with the absolute path'() {
        when:
        def node = FXController.of(AbsoluteAnnotatedTestViewController)
            .fromDefaultLocation()
            .loadContents()

        then:
        expect node, is(instanceOf(StackPane))
        and:
        that node.lookup('#messageLabel').text, is('Label Message')
    }

    def 'loads contents specified in the FXML file from the default location annotated with the absolute path with the specified ResourceBundle'() {
        given:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        def node = FXController.of(AbsoluteAnnotatedTestViewWithResourceController)
            .fromDefaultLocation()
            .with(resources)
            .loadContents()

        then:
        expect node, is(instanceOf(StackPane))
        and:
        that node.lookup('#messageLabel').text, is('Label Message')
        and:
        that node.lookup('#resourceMessageLabel').text, is('Resource Message')
    }

    def 'loads contents specified in the FXML file from the default location annotated with the absolute path with the specified resource base name'() {
        given:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        def node = FXController.of(AbsoluteAnnotatedTestViewWithResourceController)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .loadContents()

        then:
        expect node, is(instanceOf(StackPane))
        and:
        that node.lookup('#messageLabel').text, is('Label Message')
        and:
        that node.lookup('#resourceMessageLabel').text, is('Resource Message from properties file')
    }

    // =============================================================================================
    // FXController.of(controllerInstance)
    //     .fromDefaultLocation()
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads contents to the specified controller from the default location annotated with the absolute path'() {
        given:
        def controller = new AbsoluteAnnotatedSimpleTestViewController()

        when:
        def node = FXController.of(controller)
            .fromDefaultLocation()
            .loadContents()

        then:
        expect node, is(instanceOf(StackPane))
        and:
        that node.lookup('#messageLabel').text, is('Simple Label Message')
    }

    def 'loads contents to the specified controller from the default location annotated with the absolute path with the specified ResourceBundle'() {
        given:
        def controller = new AbsoluteAnnotatedSimpleTestViewWithResourceController()
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        def node = FXController.of(controller)
            .fromDefaultLocation()
            .with(resources)
            .loadContents()

        then:
        expect node, is(instanceOf(StackPane))
        and:
        that node.lookup('#messageLabel').text, is('Simple Label Message')
        and:
        that node.lookup('#resourceMessageLabel').text, is('Simple Resource Message')
    }

    def 'loads contents to the specified controller from the default location annotated with the absolute path with the specified resource base name'() {
        given:
        def controller = new AbsoluteAnnotatedSimpleTestViewWithResourceController()
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        def node = FXController.of(controller)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .loadContents()

        then:
        expect node, is(instanceOf(StackPane))
        and:
        that node.lookup('#messageLabel').text, is('Simple Label Message')
        and:
        that node.lookup('#resourceMessageLabel').text, is('Simple Resource Message from properties file')
    }

    // =============================================================================================
    // FXController.of(controllerClass)
    //     .using(injector)
    //     .from(location)
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads contents specified in the FXML with the specified controller class and ControllerInjector from the default location annotated with the absolute path'() {
        given:
        def injector = Mock ControllerInjector

        when:
        def node = FXController.of(AbsoluteAnnotatedSimpleTestViewController)
            .using(injector)
            .fromDefaultLocation()
            .loadContents()

        then:
        expect node, is(instanceOf(StackPane))
        and:
        that node.lookup('#messageLabel').text, is('Simple Label Message')
        and:
        1 * injector.getInstanceOf(AbsoluteAnnotatedSimpleTestViewController, null) >> new AbsoluteAnnotatedSimpleTestViewController()
    }

    def 'loads contents specified in the FXML with the specified controller class, ControllerInjector, and ResourceBundle from the default location annotated with the absolute path'() {
        given:
        def injector = Mock ControllerInjector
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        def node = FXController.of(AbsoluteAnnotatedSimpleTestViewWithResourceController)
            .using(injector)
            .fromDefaultLocation()
            .with(resources)
            .loadContents()

        then:
        expect node, is(instanceOf(StackPane))
        and:
        that node.lookup('#messageLabel').text, is('Simple Label Message')
        and:
        that node.lookup('#resourceMessageLabel').text, is('Simple Resource Message')
        and:
        1 * injector.getInstanceOf(AbsoluteAnnotatedSimpleTestViewWithResourceController, null) >> new AbsoluteAnnotatedSimpleTestViewWithResourceController()
    }

    def 'loads contents specified in the FXML with the specified controller class, ControllerInjector, and resource base name from the default location annotated with the absolute path'() {
        given:
        def injector = Mock ControllerInjector
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        def node = FXController.of(AbsoluteAnnotatedSimpleTestViewWithResourceController)
            .using(injector)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .loadContents()

        then:
        expect node, is(instanceOf(StackPane))
        and:
        that node.lookup('#messageLabel').text, is('Simple Label Message')
        and:
        that node.lookup('#resourceMessageLabel').text, is('Simple Resource Message from properties file')
        and:
        1 * injector.getInstanceOf(AbsoluteAnnotatedSimpleTestViewWithResourceController, null) >> new AbsoluteAnnotatedSimpleTestViewWithResourceController()
    }

    // =============================================================================================
    // FXController.of(controllerInstance)
    //     .using(injector)
    //     .from(location)
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .load()
    // =============================================================================================

    def 'loads contents to the specified controller with the specified ControllerInjector from the default location annotated with the absolute path'() {
        given:
        def controller = new AbsoluteAnnotatedSimpleTestViewController()
        and:
        def injector = Mock ControllerInjector

        when:
        def node = FXController.of(controller)
            .using(injector)
            .fromDefaultLocation()
            .loadContents()

        then:
        expect node, is(instanceOf(StackPane))
        and:
        that node.lookup('#messageLabel').text, is('Simple Label Message')
        and:
        1 * injector.getInstanceOf(controller.class, controller) >> controller
    }

    def 'loads contents to the specified controller with the specified ControllerInjector and ResourceBundle from the default location annotated with the absolute path'() {
        given:
        def controller = new AbsoluteAnnotatedSimpleTestViewWithResourceController()
        and:
        def injector = Mock ControllerInjector
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        def node = FXController.of(controller)
            .using(injector)
            .fromDefaultLocation()
            .with(resources)
            .loadContents()

        then:
        expect node, is(instanceOf(StackPane))
        and:
        that node.lookup('#messageLabel').text, is('Simple Label Message')
        and:
        that node.lookup('#resourceMessageLabel').text, is('Simple Resource Message')
        and:
        1 * injector.getInstanceOf(controller.class, controller) >> controller
    }

    def 'loads contents to the specified controller with the specified ControllerInjector and resource base name from the default location annotated with the absolute path'() {
        given:
        def controller = new AbsoluteAnnotatedSimpleTestViewWithResourceController()
        and:
        def injector = Mock ControllerInjector
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        def node = FXController.of(controller)
            .using(injector)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .loadContents()

        then:
        expect node, is(instanceOf(StackPane))
        and:
        that node.lookup('#messageLabel').text, is('Simple Label Message')
        and:
        that node.lookup('#resourceMessageLabel').text, is('Simple Resource Message from properties file')
        and:
        1 * injector.getInstanceOf(controller.class, controller) >> controller
    }
}