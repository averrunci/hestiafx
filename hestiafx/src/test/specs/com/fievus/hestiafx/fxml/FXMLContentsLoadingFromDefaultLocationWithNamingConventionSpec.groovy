/**
 * Copyright (c) 2015 koji
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.fxml

import com.fievus.hestiafx.JavaFXApplication
import com.fievus.hestiafx.resources.XmlResourceBundle
import javafx.scene.layout.StackPane
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(FXController)
class FXMLContentsLoadingFromDefaultLocationWithNamingConventionSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    // =============================================================================================
    // FXController.of(controllerClass)
    //     .fromDefaultLocation()
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .loadContents()
    // =============================================================================================

    @Unroll
    def 'loads contents specified in the FXML file from the default location which is the name removed #suffix from the end of the controller class name'() {
        when:
        def node = FXController.of(controllerClass)
            .fromDefaultLocation()
            .loadContents()

        then:
        expect node, is(instanceOf(StackPane))
        and:
        that node.lookup('#messageLabel').text, is('Label Message')

        where:
        suffix       | controllerClass
        'Controller' | TestViewController
        'Skin'       | STestViewSkin
    }

    @Unroll
    def 'loads contents specified in the FXML file from the default location which is the name removed #suffix from the end of the controller class name with the specified ResourceBundle'() {
        given:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        def node = FXController.of(controllerClass)
            .fromDefaultLocation()
            .with(resources)
            .loadContents()

        then:
        expect node, is(instanceOf(StackPane))
        and:
        that node.lookup('#messageLabel').text, is('Label Message')
        and:
        that node.lookup('#resourceMessageLabel').text, is('Resource Message')

        where:
        suffix       | controllerClass
        'Controller' | TestViewWithResourceController
        'Skin'       | STestViewWithResourceSkin
    }

    @Unroll
    def 'loads contents specified in the FXML file from the default location which is the name removed #suffix from the end of the controller class name with the specified resource base name'() {
        given:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        def node = FXController.of(controllerClass)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .loadContents()

        then:
        expect node, is(instanceOf(StackPane))
        and:
        that node.lookup('#messageLabel').text, is('Label Message')
        and:
        that node.lookup('#resourceMessageLabel').text, is('Resource Message from properties file')

        where:
        suffix       | controllerClass
        'Controller' | TestViewWithResourceController
        'Skin'       | STestViewWithResourceSkin
    }

    // =============================================================================================
    // FXController.of(controllerInstance)
    //     .fromDefaultLocation()
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .loadContents()
    // =============================================================================================

    @Unroll
    def 'loads contents to the specified controller from the default location which is the name removed #suffix from the end of the controller class name'() {
        when:
        def node = FXController.of(controller)
            .fromDefaultLocation()
            .loadContents()

        then:
        expect node, is(instanceOf(StackPane))
        and:
        that node.lookup('#messageLabel').text, is('Simple Label Message')

        where:
        suffix       | controller
        'Controller' | new SimpleTestViewController()
        'Skin'       | new SimpleTestViewSkin()
    }

    @Unroll
    def 'loads contents to the specified controller from the default location which is the name removed #suffix from the end of the controller class name with the specified ResourceBundle'() {
        given:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        def node = FXController.of(controller)
            .fromDefaultLocation()
            .with(resources)
            .loadContents()

        then:
        expect node, is(instanceOf(StackPane))
        and:
        that node.lookup('#messageLabel').text, is('Simple Label Message')
        and:
        that node.lookup('#resourceMessageLabel').text, is('Simple Resource Message')

        where:
        suffix       | controller
        'Controller' | new SimpleTestViewWithResourceController()
        'Skin'       | new SimpleTestViewWithResourceSkin()
    }

    @Unroll
    def 'loads contents to the specified controller from the default location which is the name removed #suffix from the end of the controller class name with the specified resource base name'() {
        given:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        def node = FXController.of(controller)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .loadContents()

        then:
        expect node, is(instanceOf(StackPane))
        and:
        that node.lookup('#messageLabel').text, is('Simple Label Message')
        and:
        that node.lookup('#resourceMessageLabel').text, is('Simple Resource Message from properties file')

        where:
        suffix       | controller
        'Controller' | new SimpleTestViewWithResourceController()
        'Skin'       | new SimpleTestViewWithResourceSkin()
    }

    // =============================================================================================
    // FXController.of(controllerClass)
    //     .using(injector)
    //     .from(location)
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .loadContents()
    // =============================================================================================

    @Unroll
    def 'loads contents specified in the FXML with the specified controller class and ControllerInjector from the default location which is the name removed #suffix from the end of the controller class name'() {
        given:
        def injector = Mock ControllerInjector

        when:
        def node = FXController.of(controllerClass)
            .using(injector)
            .fromDefaultLocation()
            .loadContents()

        then:
        expect node, is(instanceOf(StackPane))
        and:
        that node.lookup('#messageLabel').text, is('Label Message')
        and:
        1 * injector.getInstanceOf(controllerClass, null) >> controllerClass.newInstance()

        where:
        suffix       | controllerClass
        'Controller' | InjectionTestViewController
        'Skin'       | InjectionTestViewSkin
    }

    @Unroll
    def 'loads contents specified in the FXML with the specified controller class, ControllerInjector, and ResourceBundle from the default location which is the name removed #suffix from the end of the controller class name'() {
        given:
        def injector = Mock ControllerInjector
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        def node = FXController.of(controllerClass)
            .using(injector)
            .fromDefaultLocation()
            .with(resources)
            .loadContents()

        then:
        expect node, is(instanceOf(StackPane))
        and:
        that node.lookup('#messageLabel').text, is('Label Message')
        and:
        that node.lookup('#resourceMessageLabel').text, is('Resource Message')
        and:
        1 * injector.getInstanceOf(controllerClass, null) >> controllerClass.newInstance()

        where:
        suffix       | controllerClass
        'Controller' | InjectionTestViewWithResourceController
        'Skin'       | InjectionTestViewWithResourceSkin
    }

    @Unroll
    def 'loads contents specified in the FXML with the specified controller class, ControllerInjector, and resource base name from the default location which is the name removed #suffix from the end of the controller class name'() {
        given:
        def injector = Mock ControllerInjector
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        def node = FXController.of(controllerClass)
            .using(injector)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .loadContents()

        then:
        expect node, is(instanceOf(StackPane))
        and:
        that node.lookup('#messageLabel').text, is('Label Message')
        and:
        that node.lookup('#resourceMessageLabel').text, is('Resource Message from properties file')
        and:
        1 * injector.getInstanceOf(controllerClass, null) >> controllerClass.newInstance()

        where:
        suffix       | controllerClass
        'Controller' | InjectionTestViewWithResourceController
        'Skin'       | InjectionTestViewWithResourceSkin
    }

    // =============================================================================================
    // FXController.of(controllerInstance)
    //     .using(injector)
    //     .from(location)
    //   ( .with(resources) or .withResourceOf(resourceBaseName) )
    //     .loadContents()
    // =============================================================================================

    @Unroll
    def 'loads contents to the specified controller with the specified ControllerInjector from the default location which is the name removed #suffix from the end of the controller class name'() {
        given:
        def injector = Mock ControllerInjector

        when:
        def node = FXController.of(controller)
            .using(injector)
            .fromDefaultLocation()
            .loadContents()

        then:
        expect node, is(instanceOf(StackPane))
        and:
        that node.lookup('#messageLabel').text, is('Label Message')
        and:
        1 * injector.getInstanceOf(controller.class, controller) >> controller

        where:
        suffix       | controller
        'Controller' | new InjectionTestViewController()
        'Skin'       | new InjectionTestViewSkin()
    }

    @Unroll
    def 'loads contents to the specified controller with the specified ControllerInjector and ResourceBundle from the default location which is the name removed #suffix from the end of the controller class name'() {
        given:
        def injector = Mock ControllerInjector
        and:
        def resources = ResourceBundle.getBundle(
            'com.fievus.hestiafx.fxml.resources',
            Locale.default, FXController.classLoader, new XmlResourceBundle.Control()
        )

        when:
        def node = FXController.of(controller)
            .using(injector)
            .fromDefaultLocation()
            .with(resources)
            .loadContents()

        then:
        expect node, is(instanceOf(StackPane))
        and:
        that node.lookup('#messageLabel').text, is('Label Message')
        and:
        that node.lookup('#resourceMessageLabel').text, is('Resource Message')
        and:
        1 * injector.getInstanceOf(controller.class, controller) >> controller

        where:
        suffix       | controller
        'Controller' | new InjectionTestViewWithResourceController()
        'Skin'       | new InjectionTestViewWithResourceSkin()
    }

    @Unroll
    def 'loads contents to the specified controller with the specified ControllerInjector and resource base name from the default location which is the name removed #suffix from the end of the controller class name'() {
        given:
        def injector = Mock ControllerInjector
        and:
        def resourceBaseName = 'com.fievus.hestiafx.fxml.prop_resources'

        when:
        def node = FXController.of(controller)
            .using(injector)
            .fromDefaultLocation()
            .withResourceOf(resourceBaseName)
            .loadContents()

        then:
        expect node, is(instanceOf(StackPane))
        and:
        that node.lookup('#messageLabel').text, is('Label Message')
        and:
        that node.lookup('#resourceMessageLabel').text, is('Resource Message from properties file')
        and:
        1 * injector.getInstanceOf(controller.class, controller) >> controller

        where:
        suffix       | controller
        'Controller' | new InjectionTestViewWithResourceController()
        'Skin'       | new InjectionTestViewWithResourceSkin()
    }
}