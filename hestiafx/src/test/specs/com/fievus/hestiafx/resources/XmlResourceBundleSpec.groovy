/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.resources

import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(XmlResourceBundle)
class XmlResourceBundleSpec extends Specification {
    def 'gets the value from the resource of the base name with the specified the root locale'() {
        given:
        def bundle = ResourceBundle.getBundle(
            'com.fievus.hestiafx.resources.TestResources',
            Locale.ROOT,
            this.class.getClassLoader(),
            new XmlResourceBundle.Control()
        )

        expect:
        that bundle.getString('message.hello'), is('Hello, world!')
    }

    def 'gets the value from the resources of the base name with the specified the JAPAN locale'() {
        given:
        def bundle = ResourceBundle.getBundle(
            'com.fievus.hestiafx.resources.TestResources',
            Locale.JAPAN,
            this.class.getClassLoader(),
            new XmlResourceBundle.Control()
        )

        expect:
        that bundle.getString('message.hello'), is('こんにちは')
    }
}