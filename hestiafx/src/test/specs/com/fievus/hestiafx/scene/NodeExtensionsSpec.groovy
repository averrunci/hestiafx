/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene

import javafx.scene.layout.StackPane
import spock.lang.Specification

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

class NodeExtensionsSpec extends Specification {
    def 'gets the property of the specified key'() {
        given:
        def propertyKey = 'property-key'
        and:
        def property = 'propertyValue'
        and:
        def node = new StackPane()
        node.properties.put propertyKey, property

        when:
        def actualProperty = NodeExtensions.<String>getProperty node, propertyKey

        then:
        expect actualProperty.isPresent(), is(true)
        and:
        that actualProperty.get(), is(property)
    }

    def 'gets an empty Optional when the specified node does not have any properties'() {
        given:
        def node = new StackPane()

        expect:
        that node.properties.size(), is(0)

        when:
        def actualProperty = NodeExtensions.getProperty node, 'property-key'

        then:
        expect actualProperty.isPresent(), is(false)
    }

    def 'gets an empty Optional when the property of the specified key is not found'() {
        given:
        def node = new StackPane()
        node.properties.put 'property', 'propertyValue'

        when:
        def actualProperty = NodeExtensions.getProperty node, 'propertyKey'

        then:
        expect actualProperty.isPresent(), is(false)
    }

    def 'gets an empty Optional when the property value of the specified key is null'() {
        given:
        def propertyKey = 'property-key'
        and:
        def node = new StackPane()
        node.properties.put propertyKey, null

        when:
        def actualProperty = NodeExtensions.getProperty node, propertyKey

        then:
        expect actualProperty.isPresent(), is(false)
    }
}
