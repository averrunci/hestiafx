/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.chart

import com.fievus.hestiafx.JavaFXApplication
import javafx.collections.FXCollections
import javafx.scene.chart.PieChart
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(PieChartDataSource)
class PieChartDataSourceSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    PieChart chart

    def setup() {
        chart = new PieChart()
    }

    def 'binds a PieChart Data to the specified PieChart'() {
        given:
        def dataSource = new PieChartDataSource()

        expect:
        that chart.dataProperty().bound, is(false)

        when:
        dataSource.pieChart = chart

        then:
        expect chart.dataProperty().bound, is(true)

        when:
        dataSource.pieChart = null

        then:
        expect chart.dataProperty().bound, is(false)
    }

    def 'binds items with the specified namePropertyStep and pieValuePropertyStep'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(name: 'pie-1', pieValue: 100),
            new ChartDataItem(name: 'pie-2', pieValue: 200),
            new ChartDataItem(name: 'pie-3', pieValue: 300),
        )
        and:
        def dataSource = new PieChartDataSource(chart, 'name', 'pieValue')

        when:
        dataSource.items = items

        then:
        with chart, {
            expect data, hasSize(3)

            with data[0], {expect name, is('pie-1'); expect pieValue, is(100d)}
            with data[1], {expect name, is('pie-2'); expect pieValue, is(200d)}
            with data[2], {expect name, is('pie-3'); expect pieValue, is(300d)}
        }
    }

    def 'binds items with the specified namePropertySelection'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(name: 'pie-1', pieValue: 100),
            new ChartDataItem(name: 'pie-2', pieValue: 200),
            new ChartDataItem(name: 'pie-3', pieValue: 300),
        )
        and:
        def dataSource = new PieChartDataSource(
            pieChart: chart,
            pieValuePropertyStep: 'pieValue',
            namePropertySelection: {root, step -> root.nameProperty()}
        )

        when:
        dataSource.items = items

        then:
        with chart, {
            expect data, hasSize(3)

            with data[0], {expect name, is('pie-1'); expect pieValue, is(100d)}
            with data[1], {expect name, is('pie-2'); expect pieValue, is(200d)}
            with data[2], {expect name, is('pie-3'); expect pieValue, is(300d)}
        }
    }

    def 'binds items with the specified pieValuePropertySelection'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(name: 'pie-1', pieValue: 100),
            new ChartDataItem(name: 'pie-2', pieValue: 200),
            new ChartDataItem(name: 'pie-3', pieValue: 300),
        )
        and:
        def dataSource = new PieChartDataSource(
            pieChart: chart,
            namePropertyStep: 'name',
            pieValuePropertySelection: {root, step -> root.pieValueProperty()}
        )

        when:
        dataSource.items = items

        then:
        with chart, {
            expect data, hasSize(3)

            with data[0], {expect name, is('pie-1'); expect pieValue, is(100d)}
            with data[1], {expect name, is('pie-2'); expect pieValue, is(200d)}
            with data[2], {expect name, is('pie-3'); expect pieValue, is(300d)}
        }
    }

    def 'should not throw an exception and not bind for a name (a default value is set) when a namePropertyStep that is null is specified'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(name: 'pie-1', pieValue: 100),
            new ChartDataItem(name: 'pie-2', pieValue: 200),
            new ChartDataItem(name: 'pie-3', pieValue: 300),
        )
        and:
        def dataSource = new PieChartDataSource(chart, null, 'pieValue')

        when:
        dataSource.items = items

        then:
        with chart, {
            expect data, hasSize(3)

            with data[0], {expect name, is(''); expect pieValue, is(100d)}
            with data[1], {expect name, is(''); expect pieValue, is(200d)}
            with data[2], {expect name, is(''); expect pieValue, is(300d)}
        }
    }

    def 'should not throw an exception and not bind for a pieValue (a default value is set) when a pieValuePropertyStep that is null is specified'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(name: 'pie-1', pieValue: 100),
            new ChartDataItem(name: 'pie-2', pieValue: 200),
            new ChartDataItem(name: 'pie-3', pieValue: 300),
        )
        and:
        def dataSource = new PieChartDataSource(chart, 'name', null)

        when:
        dataSource.items = items

        then:
        with chart, {
            expect data, hasSize(3)

            with data[0], {expect name, is('pie-1'); expect pieValue, is(0d)}
            with data[1], {expect name, is('pie-2'); expect pieValue, is(0d)}
            with data[2], {expect name, is('pie-3'); expect pieValue, is(0d)}
        }
    }

    def 'rebinds items after changing namePropertyStep'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(name: 'pie-1', pieValue: 100, title: 'Title-1'),
            new ChartDataItem(name: 'pie-2', pieValue: 200, title: 'Title-2'),
            new ChartDataItem(name: 'pie-3', pieValue: 300, title: 'Title-3')
        )
        and:
        def dataSource = new PieChartDataSource(
            pieChart: chart,
            namePropertyStep: 'name',
            pieValuePropertyStep: 'pieValue',
            items: items
        )

        when:
        dataSource.namePropertyStep = 'title'

        then:
        with chart, {
            expect data, hasSize(3)

            with data[0], {expect name, is('Title-1'); expect pieValue, is(100d)}
            with data[1], {expect name, is('Title-2'); expect pieValue, is(200d)}
            with data[2], {expect name, is('Title-3'); expect pieValue, is(300d)}
        }
    }

    def 'rebinds items after changing pieValuePropertyStep'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(name: 'pie-1', pieValue: 100, score: 80),
            new ChartDataItem(name: 'pie-2', pieValue: 200, score: 90),
            new ChartDataItem(name: 'pie-3', pieValue: 300, score: 70)
        )
        and:
        def dataSource = new PieChartDataSource(
            pieChart: chart,
            namePropertyStep: 'name',
            pieValuePropertyStep: 'pieValue',
            items: items
        )

        when:
        dataSource.pieValuePropertyStep = 'score'

        then:
        with chart, {
            expect data, hasSize(3)

            with data[0], {expect name, is('pie-1'); expect pieValue, is(80d)}
            with data[1], {expect name, is('pie-2'); expect pieValue, is(90d)}
            with data[2], {expect name, is('pie-3'); expect pieValue, is(70d)}
        }
    }

    def 'rebinds items after changing namePropertySelection'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(name: 'pie-1', pieValue: 100, title: 'Title-1'),
            new ChartDataItem(name: 'pie-2', pieValue: 200, title: 'Title-2'),
            new ChartDataItem(name: 'pie-3', pieValue: 300, title: 'Title-3')
        )
        and:
        def dataSource = new PieChartDataSource(
            pieChart: chart,
            namePropertyStep: 'name',
            pieValuePropertyStep: 'pieValue',
            items: items
        )

        when:
        dataSource.namePropertySelection = {root, step -> root.titleProperty()}

        then:
        with chart, {
            expect data, hasSize(3)

            with data[0], {expect name, is('Title-1'); expect pieValue, is(100d)}
            with data[1], {expect name, is('Title-2'); expect pieValue, is(200d)}
            with data[2], {expect name, is('Title-3'); expect pieValue, is(300d)}
        }
    }

    def 'rebinds items after changing pieValuePropertySelection'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(name: 'pie-1', pieValue: 100, score: 80),
            new ChartDataItem(name: 'pie-2', pieValue: 200, score: 90),
            new ChartDataItem(name: 'pie-3', pieValue: 300, score: 70)
        )
        and:
        def dataSource = new PieChartDataSource(
            pieChart: chart,
            namePropertyStep: 'name',
            pieValuePropertyStep: 'pieValue',
            items: items
        )

        when:
        dataSource.pieValuePropertySelection = {root, step -> root.scoreProperty()}

        then:
        with chart, {
            expect data, hasSize(3)

            with data[0], {expect name, is('pie-1'); expect pieValue, is(80d)}
            with data[1], {expect name, is('pie-2'); expect pieValue, is(90d)}
            with data[2], {expect name, is('pie-3'); expect pieValue, is(70d)}
        }
    }

    def 'adds data to PieChart.Data when a value is added to items'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(name: 'pie-1', pieValue: 100)
        )
        and:
        def dataSource = new PieChartDataSource(
            pieChart: chart,
            namePropertyStep: 'name',
            pieValuePropertyStep: 'pieValue',
            items: items
        )

        expect:
        that chart.data, hasSize(1)

        when:
        items.add new ChartDataItem(name: 'pie-2', pieValue: 200)

        then:
        with chart, {
            expect data, hasSize(2)

            with data[0], {expect name, is('pie-1'); expect pieValue, is(100d)}
            with data[1], {expect name, is('pie-2'); expect pieValue, is(200d)}
        }
    }

    def 'removes data from PieChart.Data when a value is removed from items'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(name: 'pie-1', pieValue: 100),
            new ChartDataItem(name: 'pie-2', pieValue: 200)
        )
        and:
        def dataSource = new PieChartDataSource(
            pieChart: chart,
            namePropertyStep: 'name',
            pieValuePropertyStep: 'pieValue',
            items: items
        )

        expect:
        that chart.data, hasSize(2)

        when:
        items.remove 0

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {expect name, is('pie-2'); expect pieValue, is(200d)}
        }
    }

    def 'inserts data to PieChart.Data when a value is inserted to items'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(name: 'pie-1', pieValue: 100),
            new ChartDataItem(name: 'pie-2', pieValue: 200)
        )
        and:
        def dataSource = new PieChartDataSource(
            pieChart: chart,
            namePropertyStep: 'name',
            pieValuePropertyStep: 'pieValue',
            items: items
        )

        expect:
        that chart.data, hasSize(2)

        when:
        items.add 1, new ChartDataItem(name: 'pie-3', pieValue: 300)

        then:
        with chart, {
            expect data, hasSize(3)

            with data[0], {expect name, is('pie-1'); expect pieValue, is(100d)}
            with data[1], {expect name, is('pie-3'); expect pieValue, is(300d)}
            with data[2], {expect name, is('pie-2'); expect pieValue, is(200d)}
        }
    }

    def 'replaces data in PieChart.Data when a value is replaced'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(name: 'pie-1', pieValue: 100),
            new ChartDataItem(name: 'pie-2', pieValue: 200),
            new ChartDataItem(name: 'pie-3', pieValue: 300)
        )
        and:
        def dataSource = new PieChartDataSource(
            pieChart: chart,
            namePropertyStep: 'name',
            pieValuePropertyStep: 'pieValue',
            items: items
        )

        expect:
        that chart.data, hasSize(3)

        when:
        items[1] = new ChartDataItem(name: 'pie-2-2', pieValue: 222)

        then:
        with chart, {
            expect data, hasSize(3)

            with data[0], {expect name, is('pie-1'); expect pieValue, is(100d)}
            with data[1], {expect name, is('pie-2-2'); expect pieValue, is(222d)}
            with data[2], {expect name, is('pie-3'); expect pieValue, is(300d)}
        }
    }

    def 'updates data in PieChart.Data when a permutation happened'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(name: 'pie-1', pieValue: 100),
            new ChartDataItem(name: 'pie-2', pieValue: 200),
            new ChartDataItem(name: 'pie-3', pieValue: 300)
        )
        and:
        def dataSource = new PieChartDataSource(
            pieChart: chart,
            namePropertyStep: 'name',
            pieValuePropertyStep: 'pieValue',
            items: items
        )

        expect:
        that chart.data, hasSize(3)

        when:
        items.sort {x, y -> y.pieValue - x.pieValue}

        then:
        with chart, {
            expect data, hasSize(3)

            with data[0], {expect name, is('pie-3'); expect pieValue, is(300d)}
            with data[1], {expect name, is('pie-2'); expect pieValue, is(200d)}
            with data[2], {expect name, is('pie-1'); expect pieValue, is(100d)}
        }
    }

    def 'updates data in PieChart.Data when a value is added, removed, inserted, and replaced'() {
        given:
        def items = FXCollections.observableArrayList()
        and:
        def dataSource = new PieChartDataSource(
            pieChart: chart,
            namePropertyStep: 'name',
            pieValuePropertyStep: 'pieValue',
            items: items
        )

        expect:
        that chart.data, is(empty())

        when:
        items.add new ChartDataItem(name: 'pie-1', pieValue: 100)

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {expect name, is('pie-1'); expect pieValue, is(100d)}
        }

        when:
        items.addAll new ChartDataItem(name: 'pie-2', pieValue: 200), new ChartDataItem(name: 'pie-3', pieValue: 300)

        then:
        with chart, {
            expect data, hasSize(3)

            with data[0], {expect name, is('pie-1'); expect pieValue, is(100d)}
            with data[1], {expect name, is('pie-2'); expect pieValue, is(200d)}
            with data[2], {expect name, is('pie-3'); expect pieValue, is(300d)}
        }

        when:
        items.add 2, new ChartDataItem(name: 'pie-2-2', pieValue: 222)

        then:
        with chart, {
            expect data, hasSize(4)

            with data[0], {expect name, is('pie-1'); expect pieValue, is(100d)}
            with data[1], {expect name, is('pie-2'); expect pieValue, is(200d)}
            with data[2], {expect name, is('pie-2-2'); expect pieValue, is(222d)}
            with data[3], {expect name, is('pie-3'); expect pieValue, is(300d)}
        }

        when:
        items.remove 1, 3

        then:
        with chart, {
            expect data, hasSize(2)

            with data[0], {expect name, is('pie-1'); expect pieValue, is(100d)}
            with data[1], {expect name, is('pie-3'); expect pieValue, is(300d)}
        }

        when:
        items.set 0, new ChartDataItem(name: 'pie-0', pieValue: 10)

        then:
        with chart, {
            expect data, hasSize(2)

            with data[0], {expect name, is('pie-0'); expect pieValue, is(10d)}
            with data[1], {expect name, is('pie-3'); expect pieValue, is(300d)}
        }
    }
}
