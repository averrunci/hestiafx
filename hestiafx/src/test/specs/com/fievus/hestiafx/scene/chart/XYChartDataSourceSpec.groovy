/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.chart

import com.fievus.hestiafx.JavaFXApplication
import javafx.collections.FXCollections
import javafx.scene.chart.LineChart
import javafx.scene.chart.NumberAxis
import javafx.scene.chart.XYChart
import javafx.scene.shape.Circle
import javafx.scene.shape.Rectangle
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(XYChartDataSource)
class XYChartDataSourceSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    XYChart<Number, Number> chart
    XYChartDataSource dataSource

    def setup() {
        chart = new LineChart<>(new NumberAxis(), new NumberAxis())
        dataSource = new XYChartDataSource(chart)
    }

    def 'binds an XYChart Data to the specified XYChart'() {
        given:
        def chart = new LineChart<>(new NumberAxis(), new NumberAxis())
        and:
        def dataSource = new XYChartDataSource()

        expect:
        that chart.dataProperty().bound, is(false)

        when:
        dataSource.XYChart = chart

        then:
        expect chart.dataProperty().bound, is(true)

        when:
        dataSource.XYChart = null

        then:
        expect chart.dataProperty().bound, is(false)
    }

    def 'registers and unregisters a context to bind and unbind items'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100)
        )
        and:
        def context = new XYChartDataSource.Context(
            name: 'Series #1',
            XValuePropertyStep: 'xValue',
            YValuePropertyStep: 'yValue',
            items: items
        )

        when:
        dataSource.register context

        then:
        expect chart.data, hasSize(1)
        and:
        expect chart.data[0].data, hasSize(1)

        when:
        dataSource.unregister context

        then:
        expect chart.data, is(empty())
    }

    def 'clears contexts that are registered to XYChartDataSource'() {
        given:
        def items1 = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100)
        )
        def items2 = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 200)
        )
        and:
        def context1 = new XYChartDataSource.Context(
            name: 'Series #1',
            XValuePropertyStep: 'xValue',
            YValuePropertyStep: 'yValue',
            items: items1
        )
        def context2 = new XYChartDataSource.Context(
            name: 'Series #2',
            XValuePropertyStep: 'xValue',
            YValuePropertyStep: 'yValue',
            items: items2
        )
        and:
        dataSource.register context1
        dataSource.register context2

        expect:
        that chart.data, hasSize(2)

        when:
        dataSource.clear()

        then:
        expect chart.data, is(empty())
    }

    def 'binds items with the specified name, xValuePropertyStep, and yValuePropertyStep'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100),
            new ChartDataItem(XValue: 20, YValue: 200),
            new ChartDataItem(XValue: 30, YValue: 300)
        )
        and:
        def context = new XYChartDataSource.Context('Series #1', 'xValue', 'yValue')

        when:
        context.items = items
        dataSource.register context

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {
                expect name, is('Series #1')
                expect data, hasSize(3)

                with data[0], {expect XValue, is(10d); expect YValue, is(100d)}
                with data[1], {expect XValue, is(20d); expect YValue, is(200d)}
                with data[2], {expect XValue, is(30d); expect YValue, is(300d)}
            }
        }
    }

    def 'binds items with the specified name, xValuePropertyStep, yValuePropertyStep, extraValuePropertyStep'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100, score: 80),
            new ChartDataItem(XValue: 20, YValue: 200, score: 90),
            new ChartDataItem(XValue: 30, YValue: 300, score: 70)
        )
        and:
        def context = new XYChartDataSource.Context(
            name: 'Series #1',
            XValuePropertyStep: 'xValue',
            YValuePropertyStep: 'yValue',
            extraValuePropertyStep: 'score'
        )

        when:
        context.items = items
        dataSource.register context

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {
                expect name, is('Series #1')
                expect data, hasSize(3)

                with data[0], {expect XValue, is(10d); expect YValue, is(100d); expect extraValue, is(80d)}
                with data[1], {expect XValue, is(20d); expect YValue, is(200d); expect extraValue, is(90d)}
                with data[2], {expect XValue, is(30d); expect YValue, is(300d); expect extraValue, is(70d)}
            }
        }
    }

    def 'binds items with the specified name, xValuePropertyStep, yValuePropertyStep, nodePropertyStep'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100, node: new Circle()),
            new ChartDataItem(XValue: 20, YValue: 200, node: new Circle()),
            new ChartDataItem(XValue: 30, YValue: 300, node: new Circle())
        )
        and:
        def context = new XYChartDataSource.Context(
            name: 'Series #1',
            XValuePropertyStep: 'xValue',
            YValuePropertyStep: 'yValue',
            nodePropertyStep: 'node'
        )

        when:
        context.items = items
        dataSource.register context

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {
                expect name, is('Series #1')
                expect data, hasSize(3)

                with data[0], {expect XValue, is(10d); expect YValue, is(100d); expect node, is(items[0].node)}
                with data[1], {expect XValue, is(20d); expect YValue, is(200d); expect node, is(items[1].node)}
                with data[2], {expect XValue, is(30d); expect YValue, is(300d); expect node, is(items[2].node)}
            }
        }
    }

    def 'binds items with the specified xValuePropertySelection'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100),
            new ChartDataItem(XValue: 20, YValue: 200),
            new ChartDataItem(XValue: 30, YValue: 300)
        )
        and:
        def context = new XYChartDataSource.Context(
            name: 'Series #1',
            YValuePropertyStep: 'yValue',
            XValuePropertySelection: {root, step -> root.xValueProperty()}
        )

        when:
        context.items = items
        dataSource.register context

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {
                expect name, is('Series #1')
                expect data, hasSize(3)

                with data[0], {expect XValue, is(10d); expect YValue, is(100d)}
                with data[1], {expect XValue, is(20d); expect YValue, is(200d)}
                with data[2], {expect XValue, is(30d); expect YValue, is(300d)}
            }
        }
    }

    def 'binds items with the specified yValuePropertySelection'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100),
            new ChartDataItem(XValue: 20, YValue: 200),
            new ChartDataItem(XValue: 30, YValue: 300)
        )
        and:
        def context = new XYChartDataSource.Context(
            name: 'Series #1',
            XValuePropertyStep: 'xValue',
            YValuePropertySelection: {root, step -> root.yValueProperty()}
        )

        when:
        context.items = items
        dataSource.register context

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {
                expect name, is('Series #1')
                expect data, hasSize(3)

                with data[0], {expect XValue, is(10d); expect YValue, is(100d)}
                with data[1], {expect XValue, is(20d); expect YValue, is(200d)}
                with data[2], {expect XValue, is(30d); expect YValue, is(300d)}
            }
        }
    }

    def 'binds items with the specified extraValuePropertySelection'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100, score: 80),
            new ChartDataItem(XValue: 20, YValue: 200, score: 90),
            new ChartDataItem(XValue: 30, YValue: 300, score: 70)
        )
        and:
        def context = new XYChartDataSource.Context(
            name: 'Series #1',
            XValuePropertyStep: 'xValue',
            YValuePropertyStep: 'yValue',
            extraValuePropertySelection: {root, step -> root.scoreProperty()}
        )

        when:
        context.items = items
        dataSource.register context

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {
                expect name, is('Series #1')
                expect data, hasSize(3)

                with data[0], {expect XValue, is(10d); expect YValue, is(100d); expect extraValue, is(80d)}
                with data[1], {expect XValue, is(20d); expect YValue, is(200d); expect extraValue, is(90d)}
                with data[2], {expect XValue, is(30d); expect YValue, is(300d); expect extraValue, is(70d)}
            }
        }
    }

    def 'binds items with the specified nodePropertySelection'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100, node: new Circle()),
            new ChartDataItem(XValue: 20, YValue: 200, node: new Circle()),
            new ChartDataItem(XValue: 30, YValue: 300, node: new Circle())
        )
        and:
        def context = new XYChartDataSource.Context(
            name: 'Series #1',
            XValuePropertyStep: 'xValue',
            YValuePropertyStep: 'yValue',
            nodePropertySelection: {root, step -> root.nodeProperty()}
        )

        when:
        context.items = items
        dataSource.register context

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {
                expect name, is('Series #1')
                expect data, hasSize(3)

                with data[0], {expect XValue, is(10d); expect YValue, is(100d); expect node, is(items[0].node)}
                with data[1], {expect XValue, is(20d); expect YValue, is(200d); expect node, is(items[1].node)}
                with data[2], {expect XValue, is(30d); expect YValue, is(300d); expect node, is(items[2].node)}
            }
        }
    }

    def 'should not throw an exception an not bind for a xValue (a default value is set) when a xValuePropertyStep that is null is specified'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100),
            new ChartDataItem(XValue: 20, YValue: 200),
            new ChartDataItem(XValue: 30, YValue: 300)
        )
        and:
        def context = new XYChartDataSource.Context('Series #1', null, 'yValue')

        when:
        context.items = items
        dataSource.register context

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {
                expect name, is('Series #1')
                expect data, hasSize(3)

                with data[0], {expect XValue, is(nullValue()); expect YValue, is(100d)}
                with data[1], {expect XValue, is(nullValue()); expect YValue, is(200d)}
                with data[2], {expect XValue, is(nullValue()); expect YValue, is(300d)}
            }
        }
    }

    def 'should not throw an exception an not bind for a yValue (a default value is set) when a yValuePropertyStep that is null is specified'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100),
            new ChartDataItem(XValue: 20, YValue: 200),
            new ChartDataItem(XValue: 30, YValue: 300)
        )
        and:
        def context = new XYChartDataSource.Context('Series #1', 'xValue', null)

        when:
        context.items = items
        dataSource.register context

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {
                expect name, is('Series #1')
                expect data, hasSize(3)

                with data[0], {expect XValue, is(10d); expect YValue, is(nullValue())}
                with data[1], {expect XValue, is(20d); expect YValue, is(nullValue())}
                with data[2], {expect XValue, is(30d); expect YValue, is(nullValue())}
            }
        }
    }

    def 'rebinds items after changing xValuePropertyStep'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100, score: 81),
            new ChartDataItem(XValue: 20, YValue: 200, score: 63),
            new ChartDataItem(XValue: 30, YValue: 300, score: 75)
        )
        and:
        def context = new XYChartDataSource.Context(
            name: 'Series #1',
            XValuePropertyStep: 'xValue',
            YValuePropertyStep: 'yValue',
            items: items
        )
        dataSource.register context

        when:
        context.XValuePropertyStep = 'score'

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {
                expect name, is('Series #1')
                expect data, hasSize(3)

                with data[0], {expect XValue, is(81d); expect YValue, is(100d)}
                with data[1], {expect XValue, is(63d); expect YValue, is(200d)}
                with data[2], {expect XValue, is(75d); expect YValue, is(300d)}
            }
        }
    }

    def 'rebinds items after changing yValuePropertyStep'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100, score: 81),
            new ChartDataItem(XValue: 20, YValue: 200, score: 63),
            new ChartDataItem(XValue: 30, YValue: 300, score: 75)
        )
        and:
        def context = new XYChartDataSource.Context(
            name: 'Series #1',
            XValuePropertyStep: 'xValue',
            YValuePropertyStep: 'yValue',
            items: items
        )
        dataSource.register context

        when:
        context.YValuePropertyStep = 'score'

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {
                expect name, is('Series #1')
                expect data, hasSize(3)

                with data[0], {expect XValue, is(10d); expect YValue, is(81d)}
                with data[1], {expect XValue, is(20d); expect YValue, is(63d)}
                with data[2], {expect XValue, is(30d); expect YValue, is(75d)}
            }
        }
    }

    def 'rebinds items after changing extraValuePropertyStep'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100, score: 81, title: 'title-1'),
            new ChartDataItem(XValue: 20, YValue: 200, score: 63, title: 'title-2'),
            new ChartDataItem(XValue: 30, YValue: 300, score: 75, title: 'title-3')
        )
        and:
        def context = new XYChartDataSource.Context(
            name: 'Series #1',
            XValuePropertyStep: 'xValue',
            YValuePropertyStep: 'yValue',
            extraValuePropertyStep: 'score',
            items: items
        )
        dataSource.register context

        when:
        context.extraValuePropertyStep = 'title'

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {
                expect name, is('Series #1')
                expect data, hasSize(3)

                with data[0], {expect XValue, is(10d); expect YValue, is(100d); expect extraValue, is('title-1')}
                with data[1], {expect XValue, is(20d); expect YValue, is(200d); expect extraValue, is('title-2')}
                with data[2], {expect XValue, is(30d); expect YValue, is(300d); expect extraValue, is('title-3')}
            }
        }
    }

    def 'rebinds items after changing nodePropertyStep'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100, node: new Circle(), nextNode: new Rectangle()),
            new ChartDataItem(XValue: 20, YValue: 200, node: new Circle(), nextNode: new Rectangle()),
            new ChartDataItem(XValue: 30, YValue: 300, node: new Circle(), nextNode: new Rectangle())
        )
        and:
        def context = new XYChartDataSource.Context(
            name: 'Series #1',
            XValuePropertyStep: 'xValue',
            YValuePropertyStep: 'yValue',
            nodePropertyStep: 'node',
            items: items
        )
        dataSource.register context

        when:
        context.nodePropertyStep = 'nextNode'

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {
                expect name, is('Series #1')
                expect data, hasSize(3)

                with data[0], {expect XValue, is(10d); expect YValue, is(100d); expect node, is(items[0].nextNode)}
                with data[1], {expect XValue, is(20d); expect YValue, is(200d); expect node, is(items[1].nextNode)}
                with data[2], {expect XValue, is(30d); expect YValue, is(300d); expect node, is(items[2].nextNode)}
            }
        }
    }

    def 'rebinds items after changing xValuePropertySelection'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100, score: 81),
            new ChartDataItem(XValue: 20, YValue: 200, score: 63),
            new ChartDataItem(XValue: 30, YValue: 300, score: 75)
        )
        and:
        def context = new XYChartDataSource.Context(
            name: 'Series #1',
            XValuePropertyStep: 'xValue',
            YValuePropertyStep: 'yValue',
            items: items
        )
        dataSource.register context

        when:
        context.XValuePropertySelection = {root, step -> root.scoreProperty()}

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {
                expect name, is('Series #1')
                expect data, hasSize(3)

                with data[0], {expect XValue, is(81d); expect YValue, is(100d)}
                with data[1], {expect XValue, is(63d); expect YValue, is(200d)}
                with data[2], {expect XValue, is(75d); expect YValue, is(300d)}
            }
        }
    }

    def 'rebinds items after changing yValuePropertySelection'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100, score: 81),
            new ChartDataItem(XValue: 20, YValue: 200, score: 63),
            new ChartDataItem(XValue: 30, YValue: 300, score: 75)
        )
        and:
        def context = new XYChartDataSource.Context(
            name: 'Series #1',
            XValuePropertyStep: 'xValue',
            YValuePropertyStep: 'yValue',
            items: items
        )
        dataSource.register context

        when:
        context.YValuePropertySelection = {root, step -> root.scoreProperty()}

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {
                expect name, is('Series #1')
                expect data, hasSize(3)

                with data[0], {expect XValue, is(10d); expect YValue, is(81d)}
                with data[1], {expect XValue, is(20d); expect YValue, is(63d)}
                with data[2], {expect XValue, is(30d); expect YValue, is(75d)}
            }
        }
    }

    def 'rebinds items after changing extraValuePropertySelection'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100, score: 81, title: 'title-1'),
            new ChartDataItem(XValue: 20, YValue: 200, score: 63, title: 'title-2'),
            new ChartDataItem(XValue: 30, YValue: 300, score: 75, title: 'title-3')
        )
        and:
        def context = new XYChartDataSource.Context(
            name: 'Series #1',
            XValuePropertyStep: 'xValue',
            YValuePropertyStep: 'yValue',
            extraValuePropertyStep: 'score',
            items: items
        )
        dataSource.register context

        when:
        context.extraValuePropertySelection = {root, step -> root.titleProperty()}

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {
                expect name, is('Series #1')
                expect data, hasSize(3)

                with data[0], {expect XValue, is(10d); expect YValue, is(100d); expect extraValue, is('title-1')}
                with data[1], {expect XValue, is(20d); expect YValue, is(200d); expect extraValue, is('title-2')}
                with data[2], {expect XValue, is(30d); expect YValue, is(300d); expect extraValue, is('title-3')}
            }
        }
    }

    def 'rebinds items after changing nodePropertySelection'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100, node: new Circle(), nextNode: new Rectangle()),
            new ChartDataItem(XValue: 20, YValue: 200, node: new Circle(), nextNode: new Rectangle()),
            new ChartDataItem(XValue: 30, YValue: 300, node: new Circle(), nextNode: new Rectangle())
        )
        and:
        def context = new XYChartDataSource.Context(
            name: 'Series #1',
            XValuePropertyStep: 'xValue',
            YValuePropertyStep: 'yValue',
            nodePropertyStep: 'node',
            items: items
        )
        dataSource.register context

        when:
        context.nodePropertySelection = {root, step -> root.nextNodeProperty()}

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {
                expect name, is('Series #1')
                expect data, hasSize(3)

                with data[0], {expect XValue, is(10d); expect YValue, is(100d); expect node, is(items[0].nextNode)}
                with data[1], {expect XValue, is(20d); expect YValue, is(200d); expect node, is(items[1].nextNode)}
                with data[2], {expect XValue, is(30d); expect YValue, is(300d); expect node, is(items[2].nextNode)}
            }
        }
    }

    def 'adds data to XYChart.Data when a value is added to items'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100)
        )
        and:
        def context = new XYChartDataSource.Context('Series #1', 'xValue', "yValue")
        context.items = items
        dataSource.register context

        expect:
        that chart.data, hasSize(1)
        and:
        that chart.data[0].data, hasSize(1)

        when:
        items.add new ChartDataItem(XValue: 20, YValue: 200)

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {
                expect name, is('Series #1')
                expect data, hasSize(2)

                with data[0], {expect XValue, is(10d); expect YValue, is(100d)}
                with data[1], {expect XValue, is(20d); expect YValue, is(200d)}
            }
        }
    }

    def 'removes data from XYChart.Data when a value is removed from items'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100),
            new ChartDataItem(XValue: 20, YValue: 200)
        )
        and:
        def context = new XYChartDataSource.Context('Series #1', 'xValue', 'yValue')
        context.items = items
        dataSource.register context

        expect:
        that chart.data, hasSize(1)
        and:
        that chart.data[0].data, hasSize(2)

        when:
        items.remove 0

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {
                expect name, is('Series #1')
                expect data, hasSize(1)

                with data[0], {expect XValue, is(20d); expect YValue, is(200d)}
            }
        }
    }

    def 'inserts data to XYChart.Data when a value is inserted to items'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100),
            new ChartDataItem(XValue: 20, YValue: 200)
        )
        and:
        def context = new XYChartDataSource.Context('Series #1', 'xValue', 'yValue')
        context.items = items
        dataSource.register context

        expect:
        that chart.data, hasSize(1)
        and:
        that chart.data[0].data, hasSize(2)

        when:
        items.add 1, new ChartDataItem(XValue: 30, YValue: 300)

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {
                expect name, is('Series #1')
                expect data, hasSize(3)

                with data[0], {expect XValue, is(10d); expect YValue, is(100d)}
                with data[1], {expect XValue, is(30d); expect YValue, is(300d)}
                with data[2], {expect XValue, is(20d); expect YValue, is(200d)}
            }
        }
    }

    def 'replaces data in XYChart.Data when a value is replaced'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100),
            new ChartDataItem(XValue: 20, YValue: 200),
            new ChartDataItem(XValue: 30, YValue: 300)
        )
        and:
        def context = new XYChartDataSource.Context('Series #1', 'xValue', 'yValue')
        context.items = items
        dataSource.register context

        expect:
        that chart.data, hasSize(1)
        and:
        that chart.data[0].data, hasSize(3)

        when:
        items[1] = new ChartDataItem(XValue: 22, YValue: 222)

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {
                expect name, is('Series #1')
                expect data, hasSize(3)

                with data[0], {expect XValue, is(10d); expect YValue, is(100d)}
                with data[1], {expect XValue, is(22d); expect YValue, is(222d)}
                with data[2], {expect XValue, is(30d); expect YValue, is(300d)}
            }
        }
    }

    def 'updates data in XYChart.Data when a permutation happened'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100),
            new ChartDataItem(XValue: 20, YValue: 200),
            new ChartDataItem(XValue: 30, YValue: 300)
        )
        and:
        def context = new XYChartDataSource.Context('Series #1', 'xValue', 'yValue')
        context.items = items
        dataSource.register context

        expect:
        that chart.data, hasSize(1)
        and:
        that chart.data[0].data, hasSize(3)

        when:
        items.sort {x, y -> y.YValue.compareTo x.YValue}

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {
                expect name, is('Series #1')
                expect data, hasSize(3)

                with data[0], {expect XValue, is(30d); expect YValue, is(300d)}
                with data[1], {expect XValue, is(20d); expect YValue, is(200d)}
                with data[2], {expect XValue, is(10d); expect YValue, is(100d)}
            }
        }
    }

    def 'updates data in XYChart.Data when a value is added, removed, inserted, and replaced'() {
        given:
        def items = FXCollections.observableArrayList()
        and:
        def context = new XYChartDataSource.Context('Series #1', 'xValue', 'yValue')
        context.items = items
        dataSource.register context

        expect:
        that chart.data, hasSize(1)
        and:
        that chart.data[0].data, is(empty())

        when:
        items.add new ChartDataItem(XValue: 10, YValue: 100)

        then:
        with chart.data[0], {
            expect data, hasSize(1)

            with data[0], {expect XValue, is(10d); expect YValue, is(100d)}
        }

        when:
        items.addAll new ChartDataItem(XValue: 20, YValue: 200), new ChartDataItem(XValue: 30, YValue: 300)

        then:
        with chart.data[0], {
            expect data, hasSize(3)

            with data[0], {expect XValue, is(10d); expect YValue, is(100d)}
            with data[1], {expect XValue, is(20d); expect YValue, is(200d)}
            with data[2], {expect XValue, is(30d); expect YValue, is(300d)}
        }

        when:
        items.add 2, new ChartDataItem(XValue: 22, YValue: 222)

        then:
        with chart.data[0], {
            expect data, hasSize(4)

            with data[0], {expect XValue, is(10d); expect YValue, is(100d)}
            with data[1], {expect XValue, is(20d); expect YValue, is(200d)}
            with data[2], {expect XValue, is(22d); expect YValue, is(222d)}
            with data[3], {expect XValue, is(30d); expect YValue, is(300d)}
        }

        when:
        items.remove 1, 3

        then:
        with chart.data[0], {
            expect data, hasSize(2)

            with data[0], {expect XValue, is(10d); expect YValue, is(100d)}
            with data[1], {expect XValue, is(30d); expect YValue, is(300d)}
        }

        when:
        items.set 0, new ChartDataItem(XValue: 5, YValue: 50)

        then:
        with chart.data[0], {
            expect data, hasSize(2)

            with data[0], {expect XValue, is(5d); expect YValue, is(50d)}
            with data[1], {expect XValue, is(30d); expect YValue, is(300d)}
        }
    }

    def 'binds multi items'() {
        given:
        def items1 = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100),
            new ChartDataItem(XValue: 20, YValue: 200),
            new ChartDataItem(XValue: 30, YValue: 300)
        )
        def items2 = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 110),
            new ChartDataItem(XValue: 20, YValue: 220),
            new ChartDataItem(XValue: 30, YValue: 330)
        )
        def items3 = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 111),
            new ChartDataItem(XValue: 20, YValue: 222),
            new ChartDataItem(XValue: 30, YValue: 333)
        )
        and:
        def context1 = new XYChartDataSource.Context('Series #1', 'xValue', 'yValue')
        def context2 = new XYChartDataSource.Context('Series #2', 'xValue', 'yValue')
        def context3 = new XYChartDataSource.Context('Series #3', 'xValue', 'yValue')

        when:
        context1.items = items1
        context2.items = items2
        context3.items = items3
        and:
        dataSource.register context1
        dataSource.register context2
        dataSource.register context3

        then:
        with chart, {
            expect data, hasSize(3)

            with data[0], {
                expect name, is('Series #1')
                expect data, hasSize(3)

                with data[0], {expect XValue, is(10d); expect YValue, is(100d)}
                with data[1], {expect XValue, is(20d); expect YValue, is(200d)}
                with data[2], {expect XValue, is(30d); expect YValue, is(300d)}
            }
            with data[1], {
                expect name, is('Series #2')
                expect data, hasSize(3)

                with data[0], {expect XValue, is(10d); expect YValue, is(110d)}
                with data[1], {expect XValue, is(20d); expect YValue, is(220d)}
                with data[2], {expect XValue, is(30d); expect YValue, is(330d)}
            }
            with data[2], {
                expect name, is('Series #3')
                expect data, hasSize(3)

                with data[0], {expect XValue, is(10d); expect YValue, is(111d)}
                with data[1], {expect XValue, is(20d); expect YValue, is(222d)}
                with data[2], {expect XValue, is(30d); expect YValue, is(333d)}
            }
        }
    }
}

