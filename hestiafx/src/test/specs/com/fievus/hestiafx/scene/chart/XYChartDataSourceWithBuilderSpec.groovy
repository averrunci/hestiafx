/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.chart

import com.fievus.hestiafx.JavaFXApplication
import com.fievus.hestiafx.bean.property.ObservableValueSelection
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.scene.chart.LineChart
import javafx.scene.chart.NumberAxis
import javafx.scene.chart.XYChart
import javafx.scene.shape.Circle
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(XYChartDataSource)
class XYChartDataSourceWithBuilderSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    XYChart<Number, Number> chart
    XYChartDataSource dataSource

    def setup() {
        chart = new LineChart<>(new NumberAxis(), new NumberAxis())
        dataSource = new XYChartDataSource(chart)
    }

    def 'binds items'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100)
        )

        when:
        dataSource.newSeries() withItems items register()

        then:
        with chart, {expect data, hasSize(1); expect data[0].data, hasSize(1)}
    }

    def 'binds items that is a property'() {
        given:
        def itemsProperty = new SimpleObjectProperty<>(
            FXCollections.observableArrayList(
                new ChartDataItem(XValue: 10, YValue: 100)
            )
        )

        when:
        dataSource.newSeries() bindItems itemsProperty register()

        then:
        with chart, {expect data, hasSize(1); expect data[0].data, hasSize(1)}
    }

    def 'binds items with the specified name'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100)
        )
        and:
        def name = 'Series #1'

        when:
        dataSource.newSeries() withItems items withName name register()

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {expect name, is('Series #1'); expect data, hasSize(1)}
        }
    }

    def 'binds items with the specified name, which is a property'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100)
        )
        and:
        def nameProperty = new SimpleStringProperty('Series #1')

        when:
        dataSource.newSeries() withItems items bindName nameProperty register()

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {expect name, is('Series #1'); expect data, hasSize(1)}
        }
    }

    def 'binds items with the specified xValuePropertyStep'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100)
        )
        and:
        def xValuePropertyStep = 'xValue'

        when:
        dataSource.newSeries() withItems items withXValuePropertyStep xValuePropertyStep register()

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {expect data, hasSize(1); expect data[0].XValue, is(10d)}
        }
    }

    def 'binds items with the specified xValuePropertyStep, which is a property'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100)
        )
        and:
        def xValuePropertyStepProperty = new SimpleStringProperty('xValue')

        when:
        dataSource.newSeries() withItems items bindXValuePropertyStep xValuePropertyStepProperty register()

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {expect data, hasSize(1); expect data[0].XValue, is(10d)}
        }
    }

    def 'binds items with the specified yValuePropertyStep'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100)
        )
        and:
        def yValuePropertyStep = 'yValue'

        when:
        dataSource.newSeries() withItems items withYValuePropertyStep yValuePropertyStep register()

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {expect data, hasSize(1); expect data[0].YValue, is(100d)}
        }
    }

    def 'binds items with the specified yValuePropertyStep, which is a property'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100)
        )
        and:
        def yValuePropertyStepProperty = new SimpleStringProperty('yValue')

        when:
        dataSource.newSeries() withItems items bindYValuePropertyStep yValuePropertyStepProperty register()

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {expect data, hasSize(1); expect data[0].YValue, is(100d)}
        }
    }

    def 'binds items with the specified nodePropertyStep'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100, node: new Circle())
        )
        and:
        def nodePropertyStep = 'node'

        when:
        dataSource.newSeries() withItems items withNodePropertyStep nodePropertyStep register()

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {expect data, hasSize(1); expect data[0].node, is(items[0].node)}
        }
    }

    def 'binds items with the specified nodePropertyStep, which is a property'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100, node: new Circle())
        )
        and:
        def nodePropertyStepProperty = new SimpleStringProperty('node')

        when:
        dataSource.newSeries() withItems items bindNodePropertyStep nodePropertyStepProperty register()

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {expect data, hasSize(1); expect data[0].node, is(items[0].node)}
        }
    }

    def 'binds items with the specified extraValuePropertyStep'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100, score: 70)
        )
        and:
        def extraValuePropertyStep = 'score'

        when:
        dataSource.newSeries() withItems items withExtraValuePropertyStep extraValuePropertyStep register()

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {expect data, hasSize(1); expect data[0].extraValue, is(70d)}
        }
    }

    def 'binds items with the specified extraValuePropertyStep, which is a property'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100, score: 70)
        )
        and:
        def extraValuePropertyStepProperty = new SimpleStringProperty('score')

        when:
        dataSource.newSeries() withItems items bindExtraValuePropertyStep extraValuePropertyStepProperty register()

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {expect data, hasSize(1); expect data[0].extraValue, is(70d)}
        }
    }

    def 'binds items with the specified xValuePropertySelection'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100)
        )
        and:
        def xValuePropertySelection = {root, step -> root.xValueProperty()}

        when:
        dataSource.newSeries() withItems items withXValuePropertySelection xValuePropertySelection register()

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {expect data, hasSize(1); expect data[0].XValue, is(10d)}
        }
    }

    def 'binds items with the specified xValuePropertySelection, which is a property'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100)
        )
        and:
        def xValuePropertySelectionProperty = new SimpleObjectProperty<>({root, step -> root.xValueProperty()} as ObservableValueSelection)

        when:
        dataSource.newSeries() withItems items bindXValuePropertySelection xValuePropertySelectionProperty register()

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {expect data, hasSize(1); expect data[0].XValue, is(10d)}
        }
    }

    def 'binds items with the specified yValuePropertySelection'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100)
        )
        and:
        def yValuePropertySelection = {root, step -> root.yValueProperty()}

        when:
        dataSource.newSeries() withItems items withYValuePropertySelection yValuePropertySelection register()

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {expect data, hasSize(1); expect data[0].YValue, is(100d)}
        }
    }

    def 'binds items with the specified yValuePropertySelection, which is a property'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100)
        )
        and:
        def yValuePropertySelectionProperty = new SimpleObjectProperty<>({root, step -> root.yValueProperty()} as ObservableValueSelection)

        when:
        dataSource.newSeries() withItems items bindYValuePropertySelection yValuePropertySelectionProperty register()

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {expect data, hasSize(1); expect data[0].YValue, is(100d)}
        }
    }

    def 'binds items with the specified nodePropertySelection'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100, node: new Circle())
        )
        and:
        def nodePropertySelection = {root, step -> root.nodeProperty()}

        when:
        dataSource.newSeries() withItems items withNodePropertySelection nodePropertySelection register()

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {expect data, hasSize(1); expect data[0].node, is(items[0].node)}
        }
    }

    def 'binds items with the specified nodePropertySelection, which is property'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100, node: new Circle())
        )
        and:
        def nodePropertySelectionProperty = new SimpleObjectProperty<>({root, step -> root.nodeProperty()} as ObservableValueSelection)

        when:
        dataSource.newSeries() withItems items bindNodePropertySelection nodePropertySelectionProperty register()

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {expect data, hasSize(1); expect data[0].node, is(items[0].node)}
        }
    }

    def 'binds items with the specified extraValuePropertySelection'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100, score: 70)
        )
        and:
        def extraValuePropertySelection = {root, step -> root.scoreProperty()}

        when:
        dataSource.newSeries() withItems items withExtraValuePropertySelection extraValuePropertySelection register()

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {expect data, hasSize(1); expect data[0].extraValue, is(70d)}
        }
    }

    def 'binds items with the specified extraValuePropertySelection, which is a property'() {
        given:
        def items = FXCollections.observableArrayList(
            new ChartDataItem(XValue: 10, YValue: 100, score: 70)
        )
        and:
        def extraValuePropertySelectionProperty = new SimpleObjectProperty<>({root, step -> root.scoreProperty()} as ObservableValueSelection)

        when:
        dataSource.newSeries() withItems items bindExtraValuePropertySelection extraValuePropertySelectionProperty register()

        then:
        with chart, {
            expect data, hasSize(1)

            with data[0], {expect data, hasSize(1); expect data[0].extraValue, is(70d)}
        }
    }
}
