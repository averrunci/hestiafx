/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.control

import com.fievus.hestiafx.JavaFXApplication
import com.fievus.hestiafx.fxml.IllegalFXMLDefaultLocationException
import javafx.scene.Scene
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(FXMLSkin)
class FXMLSkinSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication;

    def 'loads contents from the FXML file which is in the default location'() {
        given:
        def view = new TestView()
        def scene = new Scene(view)

        when:
        view.applyCss()
        view.layout()

        then:
        expect view.lookup('#messageLabel').text, is('Message')
    }

    def 'loads contents from the FXML file which is in the annotated location'() {
        given:
        def view = new AnnotatedTestView()
        def scene = new Scene(view)

        when:
        view.applyCss()
        view.layout()

        then:
        expect view.lookup('#messageLabel').text, is('Message')
    }

    def 'throws an exception when the FXML file does not exist'() {
        given:
        def view = new NotFoundTestView()
        def scene = new Scene(view)

        when:
        view.applyCss()
        view.layout()

        then:
        thrown IllegalFXMLDefaultLocationException
    }
}