/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.control

import com.fievus.hestiafx.JavaFXApplication
import javafx.collections.FXCollections
import javafx.scene.control.TableView
import javafx.scene.control.cell.TextFieldTableCell
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(TableColumnGeneration)
class TableColumnGenerationCellFactoryRegistrationSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    def cleanup() {
        TableColumnGeneration.clearCellFactory()
    }

    def 'registers a cell factory for the specified class globally'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.CustomItemModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newCustomItemModel())
        )

        when:
        TableColumnGeneration.registerCellFactory TableColumnGenerationTestModels.CustomItem, { column -> new CustomTableCell()}
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(2)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())
        and:
        that tableView.columns[1].cellFactory, is(notNullValue())

        when:
        def cell0 = tableView.columns[0].cellFactory.call null
        def cell1 = tableView.columns[0].cellFactory.call null

        then:
        expect cell0, is(instanceOf(CustomTableCell))
        and:
        that cell1, is(instanceOf(CustomTableCell))
    }

    def 'overwrites the default cell factory with registered cell factory for the specified class globally'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.StringPropertyModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newStringPropertyModel())
        )

        when:
        TableColumnGeneration.registerCellFactory String.class, { column -> new CustomTableCell()}
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(CustomTableCell))
    }

    def 'unregisters the registered cell factory for the specified class globally'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.StringPropertyModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newStringPropertyModel())
        )

        when:
        TableColumnGeneration.registerCellFactory String.class, { column -> new CustomTableCell()}
        TableColumnGeneration.forTableView tableView enable()
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(CustomTableCell))

        when:
        TableColumnGeneration.unregisterCellFactory String.class
        cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(CustomTableCell))

        when:
        tableView.setItems(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newStringPropertyModel())
        )
        cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(TextFieldTableCell))
    }

    def 'throws an exception when null is specified for a cellValueClass that is an argument to register a cell factory globally'() {
        given:
        def cellValueClass = null
        and:
        def cellFactory = {column -> new CustomTableCell()}

        when:
        TableColumnGeneration.registerCellFactory cellValueClass, cellFactory

        then:
        thrown NullPointerException
    }

    def 'throws an exception when null is specified for a cellFactory that is an argument to register a cell factory globally'() {
        given:
        def cellValueClass = String.class
        and:
        def cellFactory = null

        when:
        TableColumnGeneration.registerCellFactory cellValueClass, cellFactory

        then:
        thrown NullPointerException
    }

    def 'throws an exception when null is specified for a cellValueClass that is an argument to unregister a cell factory globally'() {
        given:
        def cellValueClass = null

        when:
        TableColumnGeneration.unregisterCellFactory cellValueClass

        then:
        thrown NullPointerException
    }

    def 'registers a cell factory for the specified class locally'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.CustomItemModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newCustomItemModel())
        )

        when:
        TableColumnGeneration.forTableView(tableView)
            .with(TableColumnGenerationTestModels.CustomItem, { column -> new CustomTableCell()})
            .enable()

        then:
        expect tableView.columns, hasSize(2)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())
        and:
        that tableView.columns[1].cellFactory, is(notNullValue())

        when:
        def cell0 = tableView.columns[0].cellFactory.call null
        def cell1 = tableView.columns[0].cellFactory.call null

        then:
        expect cell0, is(instanceOf(CustomTableCell))
        and:
        that cell1, is(instanceOf(CustomTableCell))
    }

    def 'overwrites the default cell factory with registered cell factory for the specified class locally'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.StringPropertyModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newStringPropertyModel())
        )

        when:
        TableColumnGeneration.forTableView(tableView)
            .with(String.class, {column -> new CustomTableCell()})
            .enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(CustomTableCell))

        when:
        tableView = new TableView<TableColumnGenerationTestModels.StringPropertyModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newStringPropertyModel())
        )
        TableColumnGeneration.forTableView tableView enable()
        cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(TextFieldTableCell))
    }

    def 'throws an exception when null is specified for a cellValueClass that is an argument to register a cell factory locally'() {
        given:
        def tableView = new TableView()
        and:
        def cellValueClass = null
        and:
        def cellFactory = {column -> new CustomTableCell()}

        when:
        TableColumnGeneration.forTableView tableView with cellValueClass, cellFactory enable()

        then:
        thrown NullPointerException
    }

    def 'throws an exception when null is specified for a cellFactory that is an argument to register a cell factory locally'() {
        given:
        def tableView = new TableView()
        and:
        def cellValueClass = String.class
        and:
        def cellFactory = null

        when:
        TableColumnGeneration.forTableView tableView with cellValueClass, cellFactory enable()

        then:
        thrown NullPointerException
    }

    def 'throws an exception when null is specified for a tableView that is an argument to register a cell factory locally'() {
        given:
        def tableView = null
        and:
        def cellValueClass = String.class
        and:
        def cellFactory = {column -> new CustomTableCell()}

        when:
        TableColumnGeneration.forTableView tableView with cellValueClass, cellFactory enable()

        then:
        thrown NullPointerException
    }
}
