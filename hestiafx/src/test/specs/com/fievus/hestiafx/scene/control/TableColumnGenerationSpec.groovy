/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.control

import com.fievus.hestiafx.JavaFXApplication
import com.fievus.hestiafx.scene.control.cell.ColorPickerTableCell
import com.fievus.hestiafx.scene.control.cell.DatePickerTableCell
import javafx.beans.property.SimpleObjectProperty
import javafx.collections.FXCollections
import javafx.scene.control.TableView
import javafx.scene.control.cell.CheckBoxTableCell
import javafx.scene.control.cell.TextFieldTableCell
import javafx.util.converter.DateTimeStringConverter
import javafx.util.converter.DefaultStringConverter
import javafx.util.converter.DoubleStringConverter
import javafx.util.converter.FloatStringConverter
import javafx.util.converter.IntegerStringConverter
import javafx.util.converter.LongStringConverter
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(TableColumnGeneration)
class TableColumnGenerationSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    def 'generates TableColumn that has TextFieldTableCell with DefaultStringConverter when the property annotated with TableColumn is StringProperty'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.StringPropertyModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newStringPropertyModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(TextFieldTableCell))
        and:
        that cell.converter, is(instanceOf(DefaultStringConverter))
    }

    def 'generates TableColumn that has CheckBoxTableCell when the property annotated with TableColumn is BooleanProperty'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.BooleanPropertyModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newBooleanPropertyModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(CheckBoxTableCell))
        and:
        that cell.converter, is(nullValue())
    }

    def 'generates TableColumn that has TextFieldTableCell with IntegerStringConverter when the property annotated with TableColumn is IntegerProperty'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.IntegerPropertyModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newIntegerPropertyModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(TextFieldTableCell))
        and:
        that cell.converter, is(instanceOf(IntegerStringConverter))
    }

    def 'generates TableColumn that has TextFieldTableCell with LongStringConverter when the property annotated with TableColumn is LongProperty'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.LongPropertyModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newLongPropertyModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(TextFieldTableCell))
        and:
        that cell.converter, is(instanceOf(LongStringConverter))
    }

    def 'generates TableColumn that has TextFieldTableCell with FloatStringConverter when the property annotated with TableColumn is FloatProperty'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.FloatPropertyModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newFloatPropertyModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(TextFieldTableCell))
        and:
        that cell.converter, is(instanceOf(FloatStringConverter))
    }

    def 'generates TableColumn that has TextFieldTableCell with DoubleStringConverter when the property annotated with TableColumn is DoubleProperty'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.DoublePropertyModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newDoublePropertyModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(TextFieldTableCell))
        and:
        that cell.converter, is(instanceOf(DoubleStringConverter))
    }

    def 'generates TableColumn that has TextFieldTableCell with DefaultStringConverter when the property annotated with TableColumn is ObjectProperty<String>'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.ObjectPropertyStringModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newObjectPropertyStringModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(TextFieldTableCell))
        and:
        that cell.converter, is(instanceOf(DefaultStringConverter))
    }

    def 'generates TableColumn that has CheckBoxTableCell when the property annotated with TableColumn is ObjectProperty<Boolean>'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.ObjectPropertyBooleanModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newObjectPropertyBooleanModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(CheckBoxTableCell))
        and:
        that cell.converter, is(nullValue())
    }

    def 'generates TableColumn that has TextFieldTableCell with IntegerStringConverter when the property annotated with TableColumn is ObjectProperty<Integer>'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.ObjectPropertyIntegerModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newObjectPropertyIntegerModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(TextFieldTableCell))
        and:
        that cell.converter, is(instanceOf(IntegerStringConverter))
    }

    def 'generates TableColumn that has TextFieldTableCell with LongStringConverter when the property annotated with TableColumn is ObjectProperty<Long>'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.ObjectPropertyLongModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newObjectPropertyLongModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(TextFieldTableCell))
        and:
        that cell.converter, is(instanceOf(LongStringConverter))
    }

    def 'generates TableColumn that has TextFieldTableCell with FloatStringConverter when the property annotated with TableColumn is ObjectProperty<Float>'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.ObjectPropertyFloatModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newObjectPropertyFloatModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(TextFieldTableCell))
        and:
        that cell.converter, is(instanceOf(FloatStringConverter))
    }

    def 'generates TableColumn that has TextFieldTableCell with DoubleStringConverter when the property annotated with TableColumn is ObjectProperty<Double>'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.ObjectPropertyDoubleModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newObjectPropertyDoubleModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(TextFieldTableCell))
        and:
        that cell.converter, is(instanceOf(DoubleStringConverter))
    }

    def 'generates TableColumn that has TextFieldTableCell with DateTimeStringConverter when the property annotated with TableColumn is ObjectProperty<Date>'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.ObjectPropertyDateModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newObjectPropertyDateModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(TextFieldTableCell))
        and:
        that cell.converter, is(instanceOf(DateTimeStringConverter))
    }

    def 'generates TableColumn that has ColorPickerTableCell when the property annotated with TableColumn is ObjectProperty<Color>'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.ObjectPropertyColorModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newObjectPropertyColorModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(ColorPickerTableCell))
    }

    def 'generates TableColumn that has DatePickerTableCell when the property annotated with TableColumn is ObjectProperty<LocalDate>'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.ObjectPropertyLocalDateModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newObjectPropertyLocalDateModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(DatePickerTableCell))
    }

    def 'generates TableColumn that has TextFieldTableCell with DefaultStringConverter when the property annotated with TableColumn is String'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.StringItemModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newStringItemModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(TextFieldTableCell))
        and:
        that cell.converter, is(instanceOf(DefaultStringConverter))
    }

    def 'generates TableColumn that has CheckBoxTableCell when the property annotated with TableColumn is Boolean'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.BooleanItemModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newBooleanItemModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(CheckBoxTableCell))
        and:
        that cell.converter, is(nullValue())
    }

    def 'generates TableColumn that has TextFieldTableCell with IntegerStringConverter when the property annotated with TableColumn is Integer'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.IntegerItemModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newIntegerItemModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(TextFieldTableCell))
        and:
        that cell.converter, is(instanceOf(IntegerStringConverter))
    }

    def 'generates TableColumn that has TextFieldTableCell with LongStringConverter when the property annotated with TableColumn is Long'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.LongItemModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newLongItemModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(TextFieldTableCell))
        and:
        that cell.converter, is(instanceOf(LongStringConverter))
    }

    def 'generates TableColumn that has TextFieldTableCell with FloatStringConverter when the property annotated with TableColumn is Float'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.FloatItemModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newFloatItemModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(TextFieldTableCell))
        and:
        that cell.converter, is(instanceOf(FloatStringConverter))
    }

    def 'generates TableColumn that has TextFieldTableCell with DoubleStringConverter when the property annotated with TableColumn is Double'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.DoubleItemModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newDoubleItemModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(TextFieldTableCell))
        and:
        that cell.converter, is(instanceOf(DoubleStringConverter))
    }

    def 'generates TableColumn that has TextFieldTableCell with DateTimeStringConverter when the property annotated with TableColumn is Date'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.DateItemModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newDateItemModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(TextFieldTableCell))
        and:
        that cell.converter, is(instanceOf(DateTimeStringConverter))
    }

    def 'generates TableColumn that has ColorPickerTableCell when the property annotated with TableColumn is Color'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.ColorItemModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newColorItemModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(ColorPickerTableCell))
    }

    def 'generates TableColumn that has DatePickerTableCell when the property annotated with TableColumn is LocalDate'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.LocalDateItemModel>(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newLocalDateItemModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()

        then:
        expect tableView.columns, hasSize(1)
        and:
        that tableView.columns[0].cellFactory, is(notNullValue())

        when:
        def cell = tableView.columns[0].cellFactory.call null

        then:
        expect cell, is(instanceOf(DatePickerTableCell))
    }

    def 'generates TableColumn when items property is set'() {
        given:
        def tableView = new TableView(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newStringPropertyModel())
        )

        when:
        TableColumnGeneration.forTableView tableView enable()
        tableView.setItems FXCollections.observableArrayList(
            TableColumnGenerationTestModels.newTestModel(),
            TableColumnGenerationTestModels.newTestModel(),
            TableColumnGenerationTestModels.newTestModel()
        )

        then:
        expect tableView.columns, hasSize(7)
    }

    def 'generates TableColumn when initial items is empty and an item is added'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.TestModel>()

        when:
        TableColumnGeneration.forTableView tableView enable()
        tableView.getItems().add(TableColumnGenerationTestModels.newTestModel())

        then:
        expect tableView.columns, hasSize(7)
    }

    def 'generates TableColumn when items value that is empty is set and an item is added'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.TestModel>()
        def items = FXCollections.observableArrayList()

        when:
        TableColumnGeneration.forTableView tableView enable()
        tableView.setItems items
        tableView.getItems().add(TableColumnGenerationTestModels.newTestModel())

        then:
        expect tableView.columns, hasSize(7)
    }

    def 'generates TableColumn when items property is bound'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.TestModel>()
        def items = new SimpleObjectProperty(
            FXCollections.observableArrayList(
                TableColumnGenerationTestModels.newTestModel(),
                TableColumnGenerationTestModels.newTestModel(),
                TableColumnGenerationTestModels.newTestModel()
            )
        )

        when:
        TableColumnGeneration.forTableView tableView enable()
        tableView.itemsProperty().bind items

        then:
        expect tableView.columns, hasSize(7)
    }

    def 'generates TableColumn when items property that is empty is bound and an item is added'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.TestModel>()
        def items = new SimpleObjectProperty(FXCollections.observableArrayList())

        when:
        TableColumnGeneration.forTableView tableView enable()
        tableView.itemsProperty().bind items
        items.get().add TableColumnGenerationTestModels.newTestModel()

        then:
        expect tableView.columns, hasSize(7)
    }

    def 'generates TableColumn when a Class of an item is specified'() {
        given:
        def tableView = new TableView<TableColumnGenerationTestModels.TestModel>()
        
        when:
        TableColumnGeneration.forTableView tableView itemTypeOf TableColumnGenerationTestModels.TestModel enable()
        
        then:
        expect tableView.columns, hasSize(7)
    }

    def 'changes TableColumnGeneration enable/disable'() {
        given:
        def tableView = new TableView(
            FXCollections.observableArrayList(TableColumnGenerationTestModels.newStringPropertyModel())
        )

        when:
        TableColumnGeneration.setEnable tableView, true
        tableView.setItems FXCollections.observableArrayList(
            TableColumnGenerationTestModels.newTestModel()
        )

        then:
        expect TableColumnGeneration.isEnable(tableView), is(true)
        and:
        that tableView.columns, hasSize(7)

        when:
        TableColumnGeneration.setEnable tableView, false
        tableView.setItems FXCollections.observableArrayList(
            TableColumnGenerationTestModels.newStringPropertyModel()
        )

        then:
        expect TableColumnGeneration.isEnable(tableView), is(false)
        and:
        that tableView.columns, hasSize(7)
    }

    def 'throws an exception when null is specified for a tableView that is argument to get enable'() {
        given:
        def tableView = null

        when:
        TableColumnGeneration.isEnable tableView

        then:
        thrown NullPointerException
    }

    def 'throws an exception when null is specified for a tableView that is argument to set enable'() {
        given:
        def tableView = null

        when:
        TableColumnGeneration.setEnable tableView, true

        then:
        thrown NullPointerException
    }
}
