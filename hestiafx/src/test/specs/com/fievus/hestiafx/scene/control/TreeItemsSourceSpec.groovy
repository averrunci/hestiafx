/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.control

import com.fievus.hestiafx.JavaFXApplication
import javafx.event.EventHandler
import javafx.scene.control.TreeItem
import javafx.scene.control.TreeView
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(TreeItemsSource)
class TreeItemsSourceSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    TreeView treeView
    TreeItemsSource source

    def setup() {
        treeView = new TreeView()
        source = new TreeItemsSource(treeView)
    }

    def 'binds a root item to the specified TreeView'() {
        given:
        def treeView = new TreeView()
        and:
        def source = new TreeItemsSource()

        when:
        source.treeView = treeView

        then:
        expect treeView.rootProperty().bound, is(true)
    }

    def 'sets a root item that is a value of a root tree item'() {
        given:
        def rootItem = TestHierarchicalItem.asInternal()

        when:
        source.rootItem = rootItem

        then:
        expect treeView.root.value, is(rootItem)
        and:
        that treeView.root.expanded, is(rootItem.expanded)
        and:
        that treeView.root.graphic, is(rootItem.graphic)
    }

    def 'sets a root item and gets its children'() {
        given:
        def rootItem = TestHierarchicalItem.asInternal()

        when:
        source.rootItem = rootItem

        then:
        expect treeView.root.value, is(rootItem)
        and:
        that treeView.root.leaf, is(false)
        and:
        that treeView.root.children, is(not(empty()))
        and:
        that treeView.root.children[0].leaf, is(false)
        and:
        that treeView.root.children[1].leaf, is(true)
    }

    def 'adds a tree item to TreeView when an item is added to items'() {
        given:
        def rootItem = TestHierarchicalItem.asInternal()
        source.rootItem = rootItem
        and:
        def addedItem = TestHierarchicalItem.asLeaf()
        and:
        def handler = Mock EventHandler

        when:
        treeView.root.children
        treeView.root.addEventHandler TreeItem.childrenModificationEvent(), handler
        and:
        rootItem.items.add addedItem

        then:
        that treeView.root.children, hasSize(3)
        and:
        that treeView.root.children[0].leaf, is(false)
        and:
        that treeView.root.children[1].leaf, is(true)
        and:
        that treeView.root.children[2].value, is(addedItem)
        and:
        1 * handler.handle {it.treeItem.value == rootItem}
    }

    def 'removes a tree item from TreeView when an item is removed from items'() {
        given:
        def rootItem = TestHierarchicalItem.asInternal()
        source.rootItem = rootItem
        and:
        def handler = Mock EventHandler

        when:
        treeView.root.children
        treeView.root.addEventHandler TreeItem.childrenModificationEvent(), handler
        and:
        rootItem.items.remove 0

        then:
        expect treeView.root.children, hasSize(1)
        and:
        that treeView.root.children[0].leaf, is(true)
        and:
        1 * handler.handle {it.treeItem.value == rootItem}
    }

    def 'inserts a tree item to TreeView when an item is inserted to items'() {
        given:
        def rootItem = TestHierarchicalItem.asInternal()
        source.rootItem = rootItem
        and:
        def insertedItem = TestHierarchicalItem.asLeaf()
        and:
        def handler = Mock EventHandler

        when:
        treeView.root.children
        treeView.root.addEventHandler TreeItem.childrenModificationEvent(), handler
        and:
        rootItem.items.add 1, insertedItem

        then:
        that treeView.root.children, hasSize(3)
        and:
        that treeView.root.children[0].leaf, is(false)
        and:
        that treeView.root.children[1].value, is(insertedItem)
        and:
        that treeView.root.children[2].leaf, is(true)
        and:
        1 * handler.handle {it.treeItem.value == rootItem}
    }

    def 'replaces a tree item to TreeView when an item is replaced'() {
        given:
        def rootItem = TestHierarchicalItem.asInternal()
        source.rootItem = rootItem
        and:
        def replacedItem = TestHierarchicalItem.asInternal()
        and:
        def handler = Mock EventHandler

        when:
        treeView.root.children
        treeView.root.addEventHandler TreeItem.childrenModificationEvent(), handler
        and:
        rootItem.items[1] = replacedItem

        then:
        expect treeView.root.children, hasSize(2)
        and:
        that treeView.root.children[0].leaf, is(false)
        and:
        that treeView.root.children[1].leaf, is(false)
        and:
        that treeView.root.children[1].value, is(replacedItem)
        and:
        2 * handler.handle {it.treeItem.value == rootItem}
    }

    def 'updates tree items in TreeView when an permutation happened'() {
        given:
        def rootItem = TestHierarchicalItem.asInternal()
        source.rootItem = rootItem
        and:
        def handler = Mock EventHandler

        when:
        treeView.root.children
        treeView.root.addEventHandler TreeItem.childrenModificationEvent(), handler
        and:
        rootItem.items.sort {x, y -> x.hasItem() && !y.hasItem() ? 1 : -1}

        then:
        expect treeView.root.children, hasSize(2)
        and:
        that treeView.root.children[0].leaf, is(true)
        and:
        that treeView.root.children[1].leaf, is(false)
        and:
        2 * handler.handle {it.treeItem.value == rootItem}
    }

    def 'updates tree items in TreeView when an item is added, removed, inserted, and replaced'() {
        given:
        def rootItem = TestHierarchicalItem.asInternal()
        source.rootItem = rootItem

        expect:
        that treeView.root.children, hasSize(2)
        and:
        that treeView.root.children[0].leaf, is(false)
        and:
        that treeView.root.children[1].leaf, is(true)

        when:
        def addedItem = TestHierarchicalItem.asLeaf()
        rootItem.items.add addedItem

        then:
        expect treeView.root.children, hasSize(3)
        and:
        that treeView.root.children[0].leaf, is(false)
        and:
        that treeView.root.children[1].leaf, is(true)
        and:
        that treeView.root.children[2].value, is(addedItem)

        when:
        def addedItems = [TestHierarchicalItem.asInternal(), TestHierarchicalItem.asLeaf()]
        rootItem.items.addAll addedItems

        then:
        expect treeView.root.children, hasSize(5)
        and:
        that treeView.root.children[0].leaf, is(false)
        and:
        that treeView.root.children[1].leaf, is(true)
        and:
        that treeView.root.children[2].value, is(addedItem)
        and:
        that treeView.root.children[3].value, is(addedItems[0])
        and:
        that treeView.root.children[4].value, is(addedItems[1])

        when:
        def insertedItem = TestHierarchicalItem.asLeaf()
        rootItem.items.add 2, insertedItem

        then:
        expect treeView.root.children, hasSize(6)
        and:
        that treeView.root.children[0].leaf, is(false)
        and:
        that treeView.root.children[1].leaf, is(true)
        and:
        that treeView.root.children[2].value, is(insertedItem)
        and:
        that treeView.root.children[3].value, is(addedItem)
        and:
        that treeView.root.children[4].value, is(addedItems[0])
        and:
        that treeView.root.children[5].value, is(addedItems[1])

        when:
        rootItem.items.remove 1, 4

        then:
        expect treeView.root.children, hasSize(3)
        and:
        that treeView.root.children[0].leaf, is(false)
        and:
        that treeView.root.children[1].value, is(addedItems[0])
        and:
        that treeView.root.children[2].value, is(addedItems[1])

        when:
        def replacedItem = TestHierarchicalItem.asLeaf()
        rootItem.items[1] = replacedItem

        then:
        expect treeView.root.children, hasSize(3)
        and:
        that treeView.root.children[0].leaf, is(false)
        and:
        that treeView.root.children[1].value, is(replacedItem)
        and:
        that treeView.root.children[2].value, is(addedItems[1])
    }
}
