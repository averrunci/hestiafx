package com.fievus.hestiafx.scene.control

import com.fievus.hestiafx.JavaFXApplication
import javafx.scene.control.TreeItem
import javafx.scene.control.TreeView
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(TreeItemsSource)
class TreeItemsSourceTreeItemEqualitySpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    TestHierarchicalItem item
    TreeItem treeItem

    def createTreeItem() {
        return createTreeItem(item)
    }

    def createTreeItem(TestHierarchicalItem item) {
        def treeView = new TreeView()
        def source = new TreeItemsSource(treeView, item)
        return treeView.root
    }

    def setup() {
        item = TestHierarchicalItem.asInternal()
        treeItem = createTreeItem()
    }

    def 'is not equal to null'() {
        given:
        def targetTreeItem = null

        expect:
        that treeItem, is(not(targetTreeItem))
    }

    def 'is equal to an instance the reference of which is the same'() {
        given:
        def targetTreeItem = treeItem

        expect:
        that treeItem, is(targetTreeItem)
    }

    def 'is not equal to an instance the type of which is different'() {
        given:
        def targetTreeItem = new Object()

        expect:
        that treeItem, is(not(targetTreeItem))
    }

    def 'is equal to an instance the value of which is the same'() {
        given:
        def targetTreeItem = createTreeItem()

        expect:
        that treeItem, is(targetTreeItem)
    }

    def 'is not equal to an instance the value of which is different'() {
        given:
        def targetTreeItem = createTreeItem(TestHierarchicalItem.asInternal())

        expect:
        that treeItem, is(not(targetTreeItem))
    }

    def 'is equal to hashCode when the instance is equal'() {
        given:
        def targetTreeItem = createTreeItem()

        expect:
        that treeItem, is(targetTreeItem)
        and:
        that treeItem.hashCode(), is(targetTreeItem.hashCode())
    }

    def 'is reflexive for equality : for all x that is not null, x.equals(x) is always true'() {
        given:
        def x = createTreeItem()

        expect:
        that x, is(x)
    }

    def 'is symmetric for equality : for all x, y that is not null, y.equals(x) == true => x.equals(y) == true'() {
        given:
        def x = createTreeItem()
        def y = createTreeItem()
        def xx = createTreeItem()
        def yy = createTreeItem(TestHierarchicalItem.asInternal())

        expect:
        that x, is(y)
        and:
        that y, is(x)
        and:
        that xx, is(not(yy))
        and:
        that yy, is(not(xx))
    }

    def 'is transitive for equality : for all x, y, z that is not null, x.equals(y) == true && y.equals(z) => x.equals(z) == true'() {
        given:
        def x = createTreeItem()
        def y = createTreeItem()
        def z = createTreeItem()

        expect:
        that x, is(y)
        and:
        that y, is(z)
        and:
        that x, is(z)
    }

    def 'is consistent for equality : for all x, y that is not null, the multiple calls of x.equals(y) is completely true/false'() {
        given:
        def x = createTreeItem()
        def y = createTreeItem()
        def xx = createTreeItem()
        def yy = createTreeItem(TestHierarchicalItem.asInternal())

        expect:
        that x, is(y)
        and:
        that x, is(y)
        and:
        that x, is(y)
        and:
        that x, is(y)
        and:
        that x, is(y)
        and:
        that xx, is(not(yy))
        and:
        that xx, is(not(yy))
        and:
        that xx, is(not(yy))
        and:
        that xx, is(not(yy))
        and:
        that xx, is(not(yy))
    }
}
