/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.control.cell

import com.fievus.hestiafx.JavaFXApplication
import javafx.event.ActionEvent
import javafx.scene.control.Cell
import javafx.scene.control.ColorPicker
import javafx.scene.paint.Color
import javafx.scene.shape.Circle
import javafx.scene.shape.Rectangle
import javafx.util.StringConverter
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(ColorPickerCellBehavior)
class ColorPickerCellBehaviorSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    def 'updates an item that is empty'() {
        given:
        def cell = new Cell<Color>()
        and:
        def behavior = new ColorPickerCellBehavior(cell, null, null)

        when:
        behavior.updateItem null, true, null, null

        then:
        expect cell.graphic, is(nullValue())
        and:
        that cell.text, is(nullValue())
    }

    def 'updates an item'() {
        given:
        def cell = new Cell<Color>()
        and:
        def behavior = new ColorPickerCellBehavior(cell, null, null)

        when:
        behavior.updateItem Color.RED, false, null, null

        then:
        expect cell.graphic, is(instanceOf(Rectangle))
        and:
        that cell.graphic.fill, is(Color.RED)
        and:
        that cell.text, is(nullValue())
    }

    def 'updates an item with getColorProperty'() {
        given:
        def cell = new Cell<CellItem>()
        and:
        def behavior = new ColorPickerCellBehavior(cell, null, null)
        and:
        def getColorProperty = {it.colorProperty()}

        when:
        behavior.updateItem new CellItem('red', Color.RED), false, getColorProperty, null

        then:
        expect cell.graphic, is(instanceOf(Rectangle))
        and:
        that cell.graphic.fill, is(Color.RED)
        and:
        that cell.text, is(nullValue())
    }

    def 'updates an item with converter'() {
        given:
        def cell = new Cell<Color>()
        and:
        def behavior = new ColorPickerCellBehavior(cell, null, null)
        and:
        def converter = new StringConverter<Color>() {
            @Override
            String toString(Color object) {'RED'}
            @Override
            Color fromString(String string) {null}
        }

        when:
        behavior.updateItem Color.RED, false, null, converter

        then:
        expect cell.graphic, is(instanceOf(Rectangle))
        and:
        that cell.graphic.fill, is(Color.RED)
        and:
        that cell.text, is('RED')
    }

    def 'updates an item with getColorProperty and converter'() {
        given:
        def cell = new Cell<CellItem>()
        and:
        def behavior = new ColorPickerCellBehavior(cell, null, null)
        and:
        def getColorProperty = {it.colorProperty()}
        def converter = new StringConverter<CellItem>() {
            @Override
            String toString(CellItem object) {object.name}
            @Override
            CellItem fromString(String string) {null}
        }

        when:
        behavior.updateItem new CellItem('red', Color.RED), false, getColorProperty, converter

        then:
        expect cell.graphic, is(instanceOf(Rectangle))
        and:
        that cell.graphic.fill, is(Color.RED)
        and:
        that cell.text, is('red')
    }

    def 'updates an item with graphicFactory and graphicColorUpdater'() {
        given:
        def cell = new Cell<Color>()
        and:
        def graphicFactory = {new Circle()}
        def graphicColorUpdater = {circle, color -> circle.fill = color}
        and:
        def behavior = new ColorPickerCellBehavior(cell, graphicFactory, graphicColorUpdater)

        when:
        behavior.updateItem Color.RED, false, null, null

        then:
        expect cell.graphic, is(instanceOf(Circle))
        and:
        that cell.graphic.fill, is(Color.RED)
        and:
        that cell.text, is(nullValue())
    }

    def 'updates an item with getColorProperty, converter, graphicFactory, and graphicColorUpdater'() {
        given:
        def cell = new Cell<CellItem>()
        and:
        def graphicFactory = {new Circle()}
        def graphicColorUpdater = {circle, color -> circle.fill = color}
        and:
        def behavior = new ColorPickerCellBehavior(cell, graphicFactory, graphicColorUpdater)
        and:
        def getColorProperty = {it.colorProperty()}
        def converter = new StringConverter<CellItem>() {
            @Override
            String toString(CellItem object) {object.name}
            @Override
            CellItem fromString(String string) {null}
        }

        when:
        behavior.updateItem new CellItem('red', Color.RED), false, getColorProperty, converter

        then:
        expect cell.graphic, is(instanceOf(Circle))
        and:
        that cell.graphic.fill, is(Color.RED)
        and:
        that cell.text, is('red')
    }

    def 'updates an item while the cell is editing'() {
        given:
        def cell = new Cell<Color>() {
            def configure(item) {
                updateItem item, false
            }
        }
        cell.editable = true
        cell.configure Color.RED
        and:
        def behavior = new ColorPickerCellBehavior(cell, null, null)

        when:
        behavior.updateItem Color.RED, false, null, null
        cell.startEdit()
        behavior.startEdit()
        behavior.updateItem Color.BLUE, false, null, null

        then:
        expect cell.graphic, is(instanceOf(ColorPicker))
        and:
        that cell.graphic.value, is(Color.RED)
        and:
        that cell.text, is(nullValue())
    }

    def 'throws an exception when an instance is created with a cell that is null'() {
        given:
        def cell = null

        when:
        def behavior = new ColorPickerCellBehavior(cell, null, null)

        then:
        thrown NullPointerException
    }

    def 'throws an exception when an instance is created with graphicFactory that is null and graphicColorUpdater that is not null'() {
        given:
        def graphicFactory = null
        def graphicColorUpdater = {graphic, color -> }
        and:
        def cell = new Cell<Color>()

        when:
        def behavior = new ColorPickerCellBehavior(cell, graphicFactory, graphicColorUpdater)

        then:
        thrown IllegalArgumentException
    }

    def 'throws an exception when an instance is created with graphicFactory that is not null and graphicColorUpdater that is null'() {
        given:
        def graphicFactory = {}
        def graphicColorUpdater = null
        and:
        def cell = new Cell<Color>()

        when:
        def behavior = new ColorPickerCellBehavior(cell, graphicFactory, graphicColorUpdater)

        then:
        thrown IllegalArgumentException
    }

    def 'starts an edit and cancels it'() {
        given:
        def cell = new Cell<Color>(editable: true, item: Color.RED)
        and:
        def behavior = new ColorPickerCellBehavior(cell, null, null)

        when:
        behavior.updateItem Color.RED, false, null, null

        then:
        expect cell.graphic, is(instanceOf(Rectangle))
        and:
        that cell.graphic.fill, is(Color.RED)
        and:
        that cell.text, is(nullValue())

        when:
        behavior.startEdit()

        then:
        expect cell.graphic, is(instanceOf(ColorPicker))
        and:
        that cell.graphic.value, is(Color.RED)
        and:
        that cell.text, is(nullValue())

        when:
        cell.graphic.value = Color.BLUE
        behavior.cancelEdit null, null

        then:
        expect cell.graphic, is(instanceOf(Rectangle))
        and:
        that cell.graphic.fill, is(Color.RED)
        and:
        that cell.text, is(nullValue())

    }

    def 'commits an edit when a color is selected on ColorPicker'() {
        given:
        def cell = Mock Cell
        cell.editable = true
        cell.setItem Color.RED
        and:
        def behavior = new ColorPickerCellBehavior(cell, null, null)

        when:
        behavior.updateItem Color.RED, false, null, null
        behavior.startEdit()
        cell.graphic.value = Color.BLUE
        cell.graphic.onAction.handle new ActionEvent(cell.graphic, null)

        then:
        1 * cell.commitEdit(Color.BLUE)
    }

    def 'cancels an edit when ColorPicker is hidden'() {
        given:
        def cell = Mock Cell
        cell.editable = true
        cell.setItem Color.RED
        and:
        def behavior = new ColorPickerCellBehavior(cell, null, null)

        when:
        behavior.updateItem Color.RED, false, null, null
        behavior.startEdit()
        cell.graphic.onHidden.handle null

        then:
        1 * cell.cancelEdit()
    }
}
