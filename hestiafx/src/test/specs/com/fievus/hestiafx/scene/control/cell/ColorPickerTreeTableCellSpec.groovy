/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.control.cell

import com.fievus.hestiafx.JavaFXApplication
import javafx.scene.control.ColorPicker
import javafx.scene.paint.Color
import javafx.scene.shape.Circle
import javafx.scene.shape.Rectangle
import javafx.util.Callback
import javafx.util.StringConverter
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject

import java.util.function.BiConsumer

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(ColorPickerTreeTableCell)
class ColorPickerTreeTableCellSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    def 'updates an item that is empty'() {
        given:
        def cell = new ColorPickerTreeTableCell()

        when:
        cell.updateItem null, true

        then:
        expect cell.graphic, is(nullValue())
        and:
        that cell.text, is(nullValue())
    }

    def 'updates an item'() {
        given:
        def cell = new ColorPickerTreeTableCell()

        when:
        cell.updateItem Color.RED, false

        then:
        expect cell.graphic, is(instanceOf(Rectangle))
        and:
        that cell.graphic.fill, is(Color.RED)
        and:
        that cell.text, is(nullValue())
    }

    def 'updates an item with getColorProperty'() {
        given:
        def getColorProperty = {it.colorProperty()}
        and:
        def cell = new ColorPickerTreeTableCell(getColorProperty)

        when:
        cell.updateItem new CellItem('red', Color.RED), false

        then:
        expect cell.graphic, is(instanceOf(Rectangle))
        and:
        that cell.graphic.fill, is(Color.RED)
        and:
        that cell.text, is(nullValue())
        and:
        that cell.colorCallbackProperty().get(), is(getColorProperty as Callback)
    }

    def 'updates an item with converter'() {
        given:
        def converter = new StringConverter<Color>() {
            @Override
            String toString(Color object) {'RED'}
            @Override
            Color fromString(String string) {null}
        }
        and:
        def cell = new ColorPickerTreeTableCell(converter)

        when:
        cell.updateItem Color.RED, false

        then:
        expect cell.graphic, is(instanceOf(Rectangle))
        and:
        that cell.graphic.fill, is(Color.RED)
        and:
        that cell.text, is('RED')
        and:
        that cell.converterProperty().get(), is(converter as StringConverter)
    }

    def 'updates an item with getColorProperty and converter'() {
        given:
        def getColorProperty = {it.colorProperty()}
        def converter = new StringConverter<CellItem>() {
            @Override
            String toString(CellItem object) {object.name}
            @Override
            CellItem fromString(String string) {null}
        }
        and:
        def cell = new ColorPickerTreeTableCell(getColorProperty, converter)

        when:
        cell.updateItem new CellItem('red', Color.RED), false

        then:
        expect cell.graphic, is(instanceOf(Rectangle))
        and:
        that cell.graphic.fill, is(Color.RED)
        and:
        that cell.text, is('red')
    }

    def 'updates an item with graphicFactory and graphicColorUpdater'() {
        given:
        def graphicFactory = {new Circle()}
        def graphicColorUpdater = {circle, color -> circle.fill = color}
        and:
        def cell = new ColorPickerTreeTableCell(graphicFactory, graphicColorUpdater)

        when:
        cell.updateItem Color.RED, false

        then:
        expect cell.graphic, is(instanceOf(Circle))
        and:
        that cell.graphic.fill, is(Color.RED)
        and:
        that cell.text, is(nullValue())
    }

    def 'updates an item with getColorProperty, converter ,graphicFactory, and graphicColorUpdater'() {
        given:
        def graphicFactory = {new Circle()}
        def graphicColorUpdater = {circle, color -> circle.fill = color}
        def getColorProperty = {it.colorProperty()}
        def converter = new StringConverter<CellItem>() {
            @Override
            String toString(CellItem object) {object.name}
            @Override
            CellItem fromString(String string) {null}
        }
        and:
        def cell = new ColorPickerTreeTableCell(graphicFactory, graphicColorUpdater, getColorProperty, converter)

        when:
        cell.updateItem new CellItem('red', Color.RED), false

        then:
        expect cell.graphic, is(instanceOf(Circle))
        and:
        that cell.graphic.fill, is(Color.RED)
        and:
        that cell.text, is('red')
    }

    def 'throws an exception when an instance is created with graphicFactory that is null and graphicColorUpdater is not null'() {
        given:
        def graphicFactory = null
        def graphicColorUpdater = {circle, color -> }

        when:
        def cell = new ColorPickerTreeTableCell(graphicFactory, graphicColorUpdater)

        then:
        thrown IllegalArgumentException
    }

    def 'throws an exception when an instance is created with graphicFactory that is not null and graphicColorUpdater is null'() {
        given:
        def graphicFactory = {}
        def graphicColorUpdater = null as BiConsumer

        when:
        def cell = new ColorPickerTreeTableCell(graphicFactory, graphicColorUpdater)

        then:
        thrown IllegalArgumentException
    }

    def 'updates an item while the cell is editing'() {
        given:
        def cell = CellFactory.createEditableColorPickerTreeTableCell()

        when:
        cell.updateItem Color.RED, false
        cell.startEdit()
        cell.updateItem Color.BLUE, false

        then:
        expect cell.graphic, is(instanceOf(ColorPicker))
        and:
        that cell.graphic.value, is(Color.RED)
        and:
        that cell.text, is(nullValue())
    }

    def 'starts an edit and cancel it'() {
        given:
        def cell = CellFactory.createEditableColorPickerTreeTableCell()

        when:
        cell.updateItem Color.RED, false
        cell.startEdit()

        then:
        expect cell.graphic, is(instanceOf(ColorPicker))
        and:
        that cell.graphic.value, is(Color.RED)
        and:
        that cell.text, is(nullValue())

        when:
        cell.graphic.value = Color.BLUE
        cell.cancelEdit()

        then:
        expect cell.graphic, is(instanceOf(Rectangle))
        and:
        that cell.graphic.fill, is(Color.RED)
        and:
        that cell.text, is(nullValue())
    }

    def 'creates a cellFactory'() {
        when:
        def cell = ColorPickerTreeTableCell.forTreeTableColumn().call(null) as ColorPickerTreeTableCell

        then:
        expect cell.colorCallback, is(nullValue())
        and:
        that cell.converter, is(nullValue())

        when:
        cell.updateItem Color.RED, false

        then:
        expect cell.graphic, is(instanceOf(Rectangle))
        and:
        that cell.graphic.fill, is(Color.RED)
        and:
        that cell.text, is(nullValue())
    }

    def 'creates a cellFactory with converter'() {
        given:
        def converter = new StringConverter<Color>() {
            @Override
            String toString(Color object) {'red'}
            @Override
            Color fromString(String string) {null}
        }

        when:
        def cell = ColorPickerTreeTableCell.forTreeTableColumn(converter).call(null) as ColorPickerTreeTableCell

        then:
        expect cell.colorCallback, is(nullValue())
        and:
        that cell.converter, is(converter as StringConverter)

        when:
        cell.updateItem Color.RED, false

        then:
        expect cell.graphic, is(instanceOf(Rectangle))
        and:
        that cell.graphic.fill, is(Color.RED)
        and:
        that cell.text, is('red')
    }

    def 'creates a cellFactory with getColorProperty'() {
        given:
        def getColorProperty = {it.colorProperty()}

        when:
        def cell = ColorPickerTreeTableCell.forTreeTableColumn(getColorProperty).call(null) as ColorPickerTreeTableCell

        then:
        expect cell.colorCallback, is(getColorProperty as Callback)
        and:
        that cell.converter, is(nullValue())

        when:
        cell.updateItem new CellItem('red', Color.RED), false

        then:
        expect cell.graphic, is(instanceOf(Rectangle))
        and:
        that cell.graphic.fill, is(Color.RED)
        and:
        that cell.text, is(nullValue())
    }

    def 'creates a cellFactory with getColorProperty and converter'() {
        given:
        def getColorProperty = {it.colorProperty()} as Callback
        def converter = new StringConverter<CellItem>() {
            @Override
            String toString(CellItem object) {object.name}
            @Override
            CellItem fromString(String string) {null}
        }

        when:
        def cell = ColorPickerTreeTableCell.forTreeTableColumn(getColorProperty, converter).call(null) as ColorPickerTreeTableCell

        then:
        expect cell.colorCallback, is(getColorProperty)
        and:
        that cell.converter, is(converter as StringConverter)

        when:
        cell.updateItem new CellItem('red', Color.RED), false

        then:
        expect cell.graphic, is(instanceOf(Rectangle))
        and:
        that cell.graphic.fill, is(Color.RED)
        and:
        that cell.text, is('red')
    }

    def 'creates a cellFactory with graphicFactory and graphicColorUpdater'() {
        given:
        def graphicFactory = {new Circle()}
        def graphicColorUpdater = {circle, color -> circle.fill = color}

        when:
        def cell = ColorPickerTreeTableCell.forTreeTableColumn(graphicFactory, graphicColorUpdater).call(null) as ColorPickerTreeTableCell

        then:
        expect cell.colorCallback, is(nullValue())
        and:
        that cell.converter, is(nullValue())

        when:
        cell.updateItem Color.RED, false

        then:
        expect cell.graphic, is(instanceOf(Circle))
        and:
        that cell.graphic.fill, is(Color.RED)
        and:
        that cell.text, is(nullValue())
    }

    def 'creates a cellFactory with graphicFactory, graphicColorUpdater, getColorProperty, and converter'() {
        given:
        def graphicFactory = {new Circle()} as Callback
        def graphicColorUpdater = {circle, color -> circle.fill = color}
        def getColorProperty = {it.colorProperty()} as Callback
        def converter = new StringConverter<CellItem>() {
            @Override
            String toString(CellItem object) {object.name}
            @Override
            CellItem fromString(String string) {null}
        }

        when:
        def cell = ColorPickerTreeTableCell.forTreeTableColumn(graphicFactory, graphicColorUpdater, getColorProperty, converter).call(null) as ColorPickerTreeTableCell

        then:
        expect cell.colorCallback, is(getColorProperty)
        and:
        that cell.converter, is(converter as StringConverter)

        when:
        cell.updateItem new CellItem('red', Color.RED), false

        then:
        expect cell.graphic, is(instanceOf(Circle))
        and:
        that cell.graphic.fill, is(Color.RED)
        and:
        that cell.text, is('red')
    }
}
