/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.control.cell

import com.fievus.hestiafx.JavaFXApplication
import javafx.beans.property.SimpleObjectProperty
import javafx.event.ActionEvent
import javafx.scene.control.Cell
import javafx.scene.control.DatePicker
import javafx.util.Callback
import javafx.util.converter.LocalDateStringConverter
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject

import java.time.LocalDate
import java.time.chrono.Chronology

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(DatePickerCellBehavior)
class DatePickerCellBehaviorSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    def 'updates an item that is empty'() {
        given:
        def cell = new Cell<LocalDate>()
        and:
        def converter = new SimpleObjectProperty(new LocalDateStringConverter())
        and:
        def behavior = new DatePickerCellBehavior(cell, converter, null)

        when:
        behavior.updateItem null, true, null

        then:
        expect cell.graphic, is(nullValue())
        and:
        that cell.text, is(nullValue())
    }

    def 'updates an item'() {
        given:
        def cell = new Cell<LocalDate>()
        and:
        def converter = new SimpleObjectProperty(new LocalDateStringConverter())
        and:
        def behavior = new DatePickerCellBehavior(cell, converter, null)

        when:
        behavior.updateItem LocalDate.of(2016, 1, 1), false, null

        then:
        expect cell.graphic, is(nullValue())
        and:
        that cell.text, is('2016/01/01')
    }

    def 'updates an item with getDateProperty'() {
        given:
        def cell = new Cell<LocalDate>()
        and:
        def converter = new SimpleObjectProperty(new LocalDateStringConverter())
        and:
        def behavior = new DatePickerCellBehavior(cell, converter, null)
        and:
        def getDateProperty = {it.dateProperty()}

        when:
        behavior.updateItem new CellItem('New Year Day', LocalDate.of(2016, 1, 1)), false, getDateProperty

        then:
        expect cell.graphic, is(nullValue())
        and:
        that cell.text, is('2016/01/01')
    }

    def 'updates an item while the cell is editing'() {
        given:
        def cell = new Cell<LocalDate>() {
            def configure(item) {
                updateItem item, false
            }
        }
        cell.editable = true
        cell.configure LocalDate.of(2016, 1, 1)
        and:
        def converter = new SimpleObjectProperty(new LocalDateStringConverter())
        and:
        def behavior = new DatePickerCellBehavior(cell, converter, null)

        when:
        behavior.updateItem LocalDate.of(2016, 1, 1), false, null
        cell.startEdit()
        behavior.startEdit()
        behavior.updateItem LocalDate.of(2016, 2, 3), false, null

        then:
        expect cell.graphic, is(instanceOf(DatePicker))
        and:
        that cell.graphic.value, is(LocalDate.of(2016, 1, 1))
        and:
        that cell.text, is(nullValue())
    }

    def 'throws an exception when an instance is created with a cell that is null'() {
        given:
        def cell = null
        and:
        def converter = new SimpleObjectProperty(new LocalDateStringConverter())

        when:
        def behavior = new DatePickerCellBehavior(cell, converter, null)

        then:
        thrown NullPointerException
    }

    def 'throws an exception when an instance is created with a converter that is null'() {
        given:
        def cell = new Cell<LocalDate>()
        and:
        def converter = null

        when:
        def behavior = new DatePickerCellBehavior(cell, converter, null)

        then:
        thrown NullPointerException
    }

    def 'starts an edit and cancels it'() {
        given:
        def cell = new Cell<LocalDate>(editable: true, item: LocalDate.of(2016, 1, 1))
        and:
        def converter = new SimpleObjectProperty(new LocalDateStringConverter())
        and:
        def behavior = new DatePickerCellBehavior(cell, converter, null)

        when:
        behavior.updateItem LocalDate.of(2016, 1, 1), false, null

        then:
        expect cell.graphic, is(nullValue())
        and:
        that cell.text, is('2016/01/01')

        when:
        behavior.startEdit()

        then:
        expect cell.graphic, is(instanceOf(DatePicker))
        and:
        that cell.text, is(nullValue())

        when:
        cell.graphic.value = LocalDate.of(2016, 1, 2)
        behavior.cancelEdit null

        then:
        expect cell.graphic, is(nullValue())
        and:
        that cell.text, is('2016/01/01')
    }

    def 'starts an edit with datePrickerFactory'() {
        given:
        def chronology = Chronology.ofLocale Locale.getDefault(Locale.Category.FORMAT)
        def converter = new SimpleObjectProperty(new LocalDateStringConverter())
        def dayCellFactory = {datePicker, cell -> } as Callback
        def showWeekNumbers = false
        and:
        def datePickerFactory = {
            def datePicker = new DatePicker()
            datePicker.chronology = chronology
            datePicker.converterProperty().bind converter
            datePicker.dayCellFactory = dayCellFactory
            datePicker.showWeekNumbers = showWeekNumbers
            return datePicker
        }
        def cell = new Cell<LocalDate>(editable: true, item: LocalDate.of(2016, 1, 1))
        and:
        def behavior = new DatePickerCellBehavior(cell, converter, datePickerFactory)

        when:
        behavior.updateItem LocalDate.of(2016, 1, 1), false, null
        behavior.startEdit()

        then:
        expect cell.graphic, is(instanceOf(DatePicker))
        and:
        that cell.graphic.chronology, is(chronology)
        and:
        that cell.graphic.converter, is(converter.get())
        and:
        that cell.graphic.dayCellFactory, is(dayCellFactory)
        and:
        that cell.graphic.showWeekNumbers, is(showWeekNumbers)
        and:
        that cell.text, is(nullValue())
    }

    def 'commits an edit when a date is selected on DatePicker'() {
        given:
        def cell = Mock Cell
        cell.editable = true
        cell.setItem LocalDate.of(2016, 1, 1)
        and:
        def converter = new SimpleObjectProperty(new LocalDateStringConverter())
        and:
        def behavior = new DatePickerCellBehavior(cell, converter, null)

        when:
        behavior.updateItem LocalDate.of(2016, 1, 1), false, null
        behavior.startEdit()
        cell.graphic.value = LocalDate.of(2016, 1, 2)
        cell.graphic.onAction.handle new ActionEvent(cell.graphic, null)

        then:
        1 * cell.commitEdit(LocalDate.of(2016, 1, 2))
    }

    def 'cancels an edit when DatePicker is hidden'() {
        given:
        def cell = Mock Cell
        cell.editable = true
        cell.setItem LocalDate.of(2016, 1, 1)
        and:
        def converter = new SimpleObjectProperty(new LocalDateStringConverter())
        and:
        def behavior = new DatePickerCellBehavior(cell, converter, null)

        when:
        behavior.updateItem LocalDate.of(2016, 1, 1), false, null
        behavior.startEdit()
        cell.graphic.onHidden.handle null

        then:
        1 * cell.cancelEdit()
    }
}
