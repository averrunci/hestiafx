/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.control.cell

import com.fievus.hestiafx.JavaFXApplication
import javafx.scene.control.DatePicker
import javafx.scene.control.ListView
import javafx.util.Callback
import javafx.util.StringConverter
import javafx.util.converter.LocalDateStringConverter
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject

import java.time.LocalDate
import java.time.chrono.Chronology
import java.time.format.DateTimeFormatter
import java.util.function.Supplier

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(DatePickerListCell)
class DatePickerListCellSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    def 'updates an item that is empty'() {
        given:
        def cell = new DatePickerListCell()

        when:
        cell.updateItem null, true

        then:
        expect cell.graphic, is(nullValue())
        and:
        that cell.text, is(nullValue())
    }

    def 'updates an item'() {
        given:
        def cell = new DatePickerListCell()

        when:
        cell.updateItem LocalDate.of(2016, 1, 1), false

        then:
        expect cell.graphic, is(nullValue())
        and:
        that cell.text, is('2016/01/01')
    }

    def 'updates an item with getDateProperty'() {
        given:
        def getDateProperty = {it.dateProperty()} as Callback
        and:
        def cell = new DatePickerListCell(getDateProperty)

        when:
        cell.updateItem new CellItem('New Year Day', LocalDate.of(2016, 1, 1)), false

        then:
        expect cell.graphic, is(nullValue())
        and:
        that cell.text, is('2016/01/01')
        and:
        that cell.dateCallbackProperty().get(), is(getDateProperty)
    }

    def 'updates an item with converter'() {
        given:
        def converter = new LocalDateStringConverter(DateTimeFormatter.ofPattern('yyyy-MM-dd'), null)
        and:
        def cell = new DatePickerListCell(converter)

        when:
        cell.updateItem LocalDate.of(2016, 1, 1), false

        then:
        expect cell.graphic, is(nullValue())
        and:
        that cell.text, is('2016-01-01')
        and:
        that cell.converterProperty().get(), is(converter)
    }

    def 'updates an item with getDateProperty and converter'() {
        given:
        def getDateProperty = {it.dateProperty()}
        def converter = new LocalDateStringConverter(DateTimeFormatter.ofPattern('yyyy-MM-dd'), null)
        and:
        def cell = new DatePickerListCell(getDateProperty, converter)

        when:
        cell.updateItem new CellItem('New Year Day', LocalDate.of(2016, 1, 1)), false

        then:
        expect cell.graphic, is(nullValue())
        and:
        that cell.text, is('2016-01-01')
    }

    def 'updates an item while the cell is editing'() {
        given:
        def cell = new DatePickerListCell(editable: true)
        cell.updateListView new ListView(editable: true)

        when:
        cell.updateItem LocalDate.of(2016, 1, 1), false
        cell.startEdit()
        cell.updateItem LocalDate.of(2016, 1, 2), false

        then:
        expect cell.graphic, is(instanceOf(DatePicker))
        and:
        that cell.graphic.value, is(LocalDate.of(2016, 1, 1))
        and:
        that cell.text, is(nullValue())
    }

    def 'starts an edit and cancels it'() {
        given:
        def cell = new DatePickerListCell(editable: true)
        cell.updateListView new ListView(editable: true)

        when:
        cell.updateItem LocalDate.of(2016, 1, 1), false
        cell.startEdit()

        then:
        expect cell.graphic, is(instanceOf(DatePicker))
        and:
        that cell.graphic.value, is(LocalDate.of(2016, 1, 1))
        and:
        that cell.text, is(nullValue())

        when:
        cell.graphic.value = LocalDate.of(2016, 1, 2)
        cell.cancelEdit()

        then:
        expect cell.graphic, is(nullValue())
        and:
        that cell.text, is('2016/01/01')
    }

    def 'starts an edit with datePickerFactory'() {
        given:
        def chronology = Chronology.ofLocale Locale.getDefault(Locale.Category.FORMAT)
        def converter = new LocalDateStringConverter()
        def dayCellFactory = {datePicker, cell -> } as Callback
        def showWeekNumbers = false
        and:
        def datePickerFactory = {
            def datePicker = new DatePicker()
            datePicker.chronology = chronology
            datePicker.converter = converter
            datePicker.dayCellFactory = dayCellFactory
            datePicker.showWeekNumbers = showWeekNumbers
            return datePicker
        } as Supplier
        and:
        def cell = new DatePickerListCell(datePickerFactory)
        cell.editable = true
        cell.updateListView new ListView(editable: true)

        when:
        cell.updateItem LocalDate.of(2016, 1, 1), false
        cell.startEdit()

        then:
        expect cell.graphic, is(instanceOf(DatePicker))
        and:
        that cell.graphic.chronology, is(chronology)
        and:
        that cell.graphic.converter, is(converter)
        and:
        that cell.graphic.dayCellFactory, is(dayCellFactory)
        and:
        that cell.graphic.showWeekNumbers, is(showWeekNumbers)
        and:
        that cell.text, is(nullValue())
    }

    def 'creates a cellFactory'() {
        when:
        def cell = DatePickerListCell.forListView().call(null) as DatePickerListCell

        then:
        expect cell.dateCallback, is(nullValue())
        and:
        that cell.converter, is(not(nullValue()))

        when:
        cell.updateItem LocalDate.of(2016, 1, 1), false

        then:
        expect cell.graphic, is(nullValue())
        and:
        that cell.text, is('2016/01/01')
    }

    def 'creates a cellFactory with converter'() {
        given:
        def converter = new LocalDateStringConverter(DateTimeFormatter.ofPattern('yyyy-MM-dd'), null)

        when:
        def cell = DatePickerListCell.forListView(converter).call(null) as DatePickerListCell

        then:
        expect cell.dateCallback, is(nullValue())
        and:
        that cell.converter, is(converter)

        when:
        cell.updateItem LocalDate.of(2016, 1, 1), false

        then:
        expect cell.graphic, is(nullValue())
        and:
        that cell.text, is('2016-01-01')
    }

    def 'creates a cellFactory with getDateProperty'() {
        given:
        def getDateProperty = {it.dateProperty()} as Callback

        when:
        def cell = DatePickerListCell.forListView(getDateProperty).call(null) as DatePickerListCell

        then:
        expect cell.dateCallback, is(getDateProperty)
        and:
        that cell.converter, is(not(nullValue()))

        when:
        cell.updateItem new CellItem('New Year Day', LocalDate.of(2016, 1, 1)), false

        then:
        expect cell.graphic, is(nullValue())
        and:
        that cell.text, is('2016/01/01')
    }

    def 'creates a cellFactory with getDateProperty and converter'() {
        given:
        def getDateProperty = {it.dateProperty()} as Callback
        def converter = new LocalDateStringConverter(DateTimeFormatter.ofPattern('yyyy-MM-dd'), null)

        when:
        def cell = DatePickerListCell.forListView(getDateProperty, converter).call(null) as DatePickerListCell

        then:
        expect cell.dateCallback, is(getDateProperty)
        and:
        that cell.converter, is(converter as StringConverter)

        when:
        cell.updateItem new CellItem('New Year Day', LocalDate.of(2016, 1, 1)), false

        then:
        expect cell.graphic, is(nullValue())
        and:
        that cell.text, is('2016-01-01')
    }

    def 'creates a cellFactory with datePickerFactory'() {
        given:
        def datePickerFactory = {} as Supplier

        when:
        def cell = DatePickerListCell.forListView(datePickerFactory).call(null) as DatePickerListCell

        then:
        expect cell.dateCallback, is(nullValue())
        and:
        that cell.converter, is(not(nullValue()))

        when:
        cell.updateItem LocalDate.of(2016, 1, 1), false

        then:
        expect cell.graphic, is(nullValue())
        and:
        that cell.text, is('2016/01/01')
    }

    def 'creates a cellFactory with datePickerFactory, getDateProperty, and converter'() {
        given:
        def datePickerFactory = {} as Supplier
        def getDateProperty = {it.dateProperty()} as Callback
        def converter = new LocalDateStringConverter(DateTimeFormatter.ofPattern('yyyy-MM-dd'), null)

        when:
        def cell = DatePickerListCell.forListView(datePickerFactory, getDateProperty, converter).call(null) as DatePickerListCell

        then:
        expect cell.dateCallback, is(getDateProperty)
        and:
        that cell.converter, is(converter)

        when:
        cell.updateItem new CellItem('New Year Day', LocalDate.of(2016, 1, 1)), false

        then:
        expect cell.graphic, is(nullValue())
        and:
        that cell.text, is('2016-01-01')
    }
}
