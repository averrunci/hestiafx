/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.input

import com.fievus.hestiafx.JavaFXApplication
import javafx.scene.Scene
import javafx.scene.layout.Region
import javafx.scene.layout.StackPane
import javafx.scene.shape.Rectangle
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(DragContext)
class DraggingNodeWithDragContextSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication;

    def 'sets the location of the specified node when the mouse moves to the location that is in Scene'() {
        given:
        def node = new Rectangle(0, 0, 50, 25)
        node.layoutX = 200
        node.layoutY = 100
        and:
        def scene = new Scene(new StackPane(node), 480, 360);

        when:
        DragContext.of(node)
            .atMouseLocation(220, 110)
            .dragNodeTo(479, 120)

        then:
        expect node.layoutX, is(equalTo(459d))
        and:
        that node.layoutY, is(equalTo(110d))
    }

    def 'sets the location of the specified node when the mouse moves to the location that is on the boundary of Scene'() {
        given:
        def node = new Rectangle(0, 0, 50, 25)
        node.layoutX = 200
        node.layoutY = 100
        and:
        def scene =new Scene(new StackPane(node), 480, 360)

        when:
        DragContext.of(node)
            .atMouseLocation(220, 110)
            .dragNodeTo(480, 120)

        then:
        expect node.layoutX, is(equalTo(460d))
        and:
        that node.layoutY, is(equalTo(110d))
    }

    def 'should not change the location of the specified node when the mouse moves to the location that is out of Scene'() {
        given:
        def node = new Rectangle(0, 0, 50, 25)
        node.layoutX = 200
        node.layoutY = 100
        and:
        def scene = new Scene(new StackPane(node), 480, 360)

        when:
        DragContext.of(node)
            .atMouseLocation(220, 110)
            .dragNodeTo(481, 120)

        then:
        expect node.layoutX, is(equalTo(200d))
        and:
        that node.layoutY, is(equalTo(100d))
    }

    def 'sets the location of the specified node when the mouse moves to the location that is in the bounding node'() {
        given:
        def node = new Rectangle(0, 0, 50, 25)
        node.layoutX = 200
        node.layoutY = 100
        and:
        def boundingNode = new Region()
        boundingNode.resizeRelocate 100, 50, 200, 100
        and:
        def scene = new Scene(new StackPane(boundingNode, node), 480, 360)

        when:
        DragContext.of(node)
            .boundTo(boundingNode)
            .atMouseLocation(220, 110)
            .dragNodeTo(101, 120)

        then:
        expect node.layoutX, is(equalTo(81d))
        and:
        that node.layoutY, is(equalTo(110d))
    }

    def 'sets the location of the specified node when the mouse moves to the location that is on the boundary of the bounding node'() {
        given:
        def node = new Rectangle(0, 0, 50, 25)
        node.layoutX = 200
        node.layoutY = 100
        and:
        def boundingNode = new Region()
        boundingNode.resizeRelocate 100, 50, 200, 100
        and:
        def scene = new Scene(new StackPane(boundingNode, node), 480, 360)

        when:
        DragContext.of(node)
            .boundTo(boundingNode)
            .atMouseLocation(220, 110)
            .dragNodeTo(100, 120)

        then:
        expect node.layoutX, is(equalTo(80d))
        and:
        that node.layoutY, is(equalTo(110d))
    }

    def 'should not change the location of the specified node when the mouse moves to the location that is out of the bounding node'() {
        given:
        def node = new Rectangle(0, 0, 50, 25)
        node.layoutX = 200
        node.layoutY = 100
        and:
        def boundingNode = new Region()
        boundingNode.resizeRelocate 100, 50, 200, 100
        and:
        def scene = new Scene(new StackPane(boundingNode, node), 480, 360)

        when:
        DragContext.of(node)
            .boundTo(boundingNode)
            .atMouseLocation(220, 110)
            .dragNodeTo(99, 120)

        then:
        expect node.layoutX, is(equalTo(200d))
        and:
        that node.layoutY, is(equalTo(100d))
    }

    def 'throws an exception when null is specified for the drag node'() {
        given:
        def node = null

        when:
        DragContext.of(node)

        then:
        thrown NullPointerException
    }
}