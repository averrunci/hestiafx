/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.scene.input

import com.fievus.hestiafx.JavaFXApplication
import javafx.scene.Scene
import javafx.scene.layout.Pane
import javafx.scene.layout.Region
import javafx.scene.layout.StackPane
import javafx.scene.shape.Rectangle
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Subject

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

@Subject(Draggable)
class DraggingNodeWithDraggableSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    def 'drags the specified node when the mouse moves to the location that is in Scene'() {
        given:
        def node = new Rectangle(0, 0, 50, 25)
        node.layoutX = 200
        node.layoutY = 100
        and:
        def scene = new Scene(new StackPane(node), 480, 360)

        when:
        Draggable.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(300)
            .sceneOffsetY(200)
            .startDragAt(220, 110)
            .dragTo(325, 113)
            .dragTo(418, 118)
            .dragTo(479, 120)
            .release()

        then:
        expect node.layoutX, is(equalTo(459d))
        and:
        that node.layoutY, is(equalTo(110d))
    }

    def 'drags the specified node when the mouse moves to the location that is out of Scene'() {
        given:
        def node = new Rectangle(0, 0, 50, 25)
        node.layoutX = 200
        node.layoutY = 100
        and:
        def scene = new Scene(new StackPane(node), 480, 360)

        when:
        Draggable.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(300)
            .sceneOffsetY(200)
            .startDragAt(220, 110)
            .dragTo(325, 113)
            .dragTo(418, 118)
            .dragTo(480, 120)
            .dragTo(490, 130)
            .release()

        then:
        expect node.layoutX, is(equalTo(460d))
        and:
        that node.layoutY, is(equalTo(110d))
    }

    def 'drags the specified node when the mouse moves to the location that is in the specified bounding node'() {
        given:
        def node = new Rectangle(0, 0, 50, 25)
        node.layoutX = 200
        node.layoutY = 100
        and:
        def boundingNode = new Region()
        boundingNode.resizeRelocate 100, 50, 200, 100
        and:
        def scene = new Scene(new StackPane(boundingNode, node), 480, 360)

        when:
        Draggable.to node boundTo boundingNode enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(300)
            .sceneOffsetY(200)
            .startDragAt(220, 110)
            .dragTo(185, 113)
            .dragTo(134, 118)
            .dragTo(101, 120)
            .release()

        then:
        expect node.layoutX, is(equalTo(81d))
        and:
        that node.layoutY, is(equalTo(110d))
    }

    def 'drags the specified node when the mouse moves to the location that is out of the specified bounding node'() {
        given:
        def node = new Rectangle(0, 0, 50, 25)
        node.layoutX = 200
        node.layoutY = 100
        and:
        def boundingNode = new Region()
        boundingNode.resizeRelocate 100, 50, 200, 100
        and:
        def scene = new Scene(new StackPane(boundingNode, node), 480, 360)

        when:
        Draggable.to node boundTo boundingNode enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(300)
            .sceneOffsetY(200)
            .startDragAt(220, 110)
            .dragTo(185, 113)
            .dragTo(134, 118)
            .dragTo(100, 120)
            .dragTo(90, 130)
            .release()

        then:
        expect node.layoutX, is(equalTo(80d))
        and:
        that node.layoutY, is(equalTo(110d))
    }

    def 'drags the specified node when the mouse moves to the location that is in the parent of the specified node'() {
        given:
        def node = new Rectangle(0, 0, 50, 25)
        node.layoutX = 100
        node.layoutY = 50
        and:
        def parent = new Pane()
        parent.resizeRelocate 100, 50, 200, 100
        parent.children.add node
        and:
        def scene = new Scene(new StackPane(parent), 480, 360)
        scene.getRoot().requestLayout()

        when:
        Draggable.to node boundToParent() enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(300)
            .sceneOffsetY(200)
            .startDragAt(220, 110)
            .dragTo(185, 113)
            .dragTo(134, 118)
            .dragTo(101, 120)
            .release()

        then:
        expect node.layoutX, is(equalTo(-19d))
        and:
        that node.layoutY, is(equalTo(60d))
    }

    def 'drags the specified node when the mouse moves to the location that is out of the parent of the specified node'() {
        given:
        def node = new Rectangle(0, 0, 50, 25)
        node.layoutX = 100
        node.layoutY = 50
        and:
        def parent = new Pane()
        parent.resizeRelocate 100, 50, 200, 100
        parent.children.add node
        and:
        def scene = new Scene(new StackPane(parent), 480, 360)
        scene.getRoot().requestLayout()

        when:
        Draggable.to node boundToParent() enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(300)
            .sceneOffsetY(200)
            .startDragAt(220, 110)
            .dragTo(185, 113)
            .dragTo(134, 118)
            .dragTo(100, 120)
            .dragTo(75, 143)
            .release()

        then:
        expect node.layoutX, is(equalTo(-20d))
        and:
        that node.layoutY, is(equalTo(60d))
    }
}