/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.stage

import com.fievus.hestiafx.JavaFXApplication
import javafx.geometry.Insets
import javafx.scene.Cursor
import javafx.stage.Stage
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Unroll

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

class WindowCursorPositionSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    @Unroll
    def 'gets a window cursor position (#windowCursorPosition) with the specified window (#windowX, #windowY), (#windowWidth x #windowHeight) and screen location (#screenX, #screenY)'() {
        given:
        def window = new Stage(x: windowX, y: windowY, width: windowWidth, height: windowHeight)

        when:
        def position = WindowCursorPosition.of window at screenX, screenY

        then:
        expect position, is(windowCursorPosition)
        and:
        that position.cursor, is(cursor)

        where:
        windowX | windowY | windowWidth | windowHeight | screenX | screenY  || windowCursorPosition            | cursor           | canCreateWindowResizer
        10      | 20      | 100         | 200          | 50      | 19       || WindowCursorPosition.CENTER     | Cursor.DEFAULT   | false
        10      | 20      | 100         | 200          | 50      | 20       || WindowCursorPosition.NORTH      | Cursor.N_RESIZE  | true
        10      | 20      | 100         | 200          | 50      | 21       || WindowCursorPosition.NORTH      | Cursor.N_RESIZE  | true
        10      | 20      | 100         | 200          | 50      | 30       || WindowCursorPosition.NORTH      | Cursor.N_RESIZE  | true
        10      | 20      | 100         | 200          | 50      | 31       || WindowCursorPosition.CENTER     | Cursor.DEFAULT   | false

        10      | 20      | 100         | 200          | 99      | 150      || WindowCursorPosition.CENTER     | Cursor.DEFAULT   | false
        10      | 20      | 100         | 200          | 100     | 150      || WindowCursorPosition.EAST       | Cursor.E_RESIZE  | true
        10      | 20      | 100         | 200          | 101     | 150      || WindowCursorPosition.EAST       | Cursor.E_RESIZE  | true
        10      | 20      | 100         | 200          | 110     | 150      || WindowCursorPosition.EAST       | Cursor.E_RESIZE  | true
        10      | 20      | 100         | 200          | 111     | 150      || WindowCursorPosition.CENTER     | Cursor.DEFAULT   | false

        10      | 20      | 100         | 200          | 50      | 209      || WindowCursorPosition.CENTER     | Cursor.DEFAULT   | false
        10      | 20      | 100         | 200          | 50      | 210      || WindowCursorPosition.SOUTH      | Cursor.S_RESIZE  | true
        10      | 20      | 100         | 200          | 50      | 211      || WindowCursorPosition.SOUTH      | Cursor.S_RESIZE  | true
        10      | 20      | 100         | 200          | 50      | 220      || WindowCursorPosition.SOUTH      | Cursor.S_RESIZE  | true
        10      | 20      | 100         | 200          | 50      | 221      || WindowCursorPosition.CENTER     | Cursor.DEFAULT   | false

        10      | 20      | 100         | 200          | 9       | 150      || WindowCursorPosition.CENTER     | Cursor.DEFAULT   | false
        10      | 20      | 100         | 200          | 10      | 150      || WindowCursorPosition.WEST       | Cursor.W_RESIZE  | true
        10      | 20      | 100         | 200          | 11      | 150      || WindowCursorPosition.WEST       | Cursor.W_RESIZE  | true
        10      | 20      | 100         | 200          | 20      | 150      || WindowCursorPosition.WEST       | Cursor.W_RESIZE  | true
        10      | 20      | 100         | 200          | 21      | 150      || WindowCursorPosition.CENTER     | Cursor.DEFAULT   | false

        10      | 20      | 100         | 200          | 105     | 19       || WindowCursorPosition.CENTER     | Cursor.DEFAULT   | false
        10      | 20      | 100         | 200          | 105     | 20       || WindowCursorPosition.NORTH_EAST | Cursor.NE_RESIZE | true
        10      | 20      | 100         | 200          | 105     | 21       || WindowCursorPosition.NORTH_EAST | Cursor.NE_RESIZE | true
        10      | 20      | 100         | 200          | 105     | 30       || WindowCursorPosition.NORTH_EAST | Cursor.NE_RESIZE | true
        10      | 20      | 100         | 200          | 105     | 31       || WindowCursorPosition.EAST       | Cursor.E_RESIZE  | true
        10      | 20      | 100         | 200          | 99      | 25       || WindowCursorPosition.NORTH      | Cursor.N_RESIZE  | true
        10      | 20      | 100         | 200          | 100     | 25       || WindowCursorPosition.NORTH_EAST | Cursor.NE_RESIZE | true
        10      | 20      | 100         | 200          | 101     | 25       || WindowCursorPosition.NORTH_EAST | Cursor.NE_RESIZE | true
        10      | 20      | 100         | 200          | 110     | 25       || WindowCursorPosition.NORTH_EAST | Cursor.NE_RESIZE | true
        10      | 20      | 100         | 200          | 111     | 25       || WindowCursorPosition.CENTER     | Cursor.DEFAULT   | false

        10      | 20      | 100         | 200          | 105     | 209      || WindowCursorPosition.EAST       | Cursor.E_RESIZE  | true
        10      | 20      | 100         | 200          | 105     | 210      || WindowCursorPosition.SOUTH_EAST | Cursor.SE_RESIZE | true
        10      | 20      | 100         | 200          | 105     | 211      || WindowCursorPosition.SOUTH_EAST | Cursor.SE_RESIZE | true
        10      | 20      | 100         | 200          | 105     | 220      || WindowCursorPosition.SOUTH_EAST | Cursor.SE_RESIZE | true
        10      | 20      | 100         | 200          | 105     | 221      || WindowCursorPosition.CENTER     | Cursor.DEFAULT   | false
        10      | 20      | 100         | 200          | 99      | 215      || WindowCursorPosition.SOUTH      | Cursor.S_RESIZE  | true
        10      | 20      | 100         | 200          | 100     | 215      || WindowCursorPosition.SOUTH_EAST | Cursor.SE_RESIZE | true
        10      | 20      | 100         | 200          | 101     | 215      || WindowCursorPosition.SOUTH_EAST | Cursor.SE_RESIZE | true
        10      | 20      | 100         | 200          | 110     | 215      || WindowCursorPosition.SOUTH_EAST | Cursor.SE_RESIZE | true
        10      | 20      | 100         | 200          | 111     | 215      || WindowCursorPosition.CENTER     | Cursor.DEFAULT   | false

        10      | 20      | 100         | 200          | 15      | 209      || WindowCursorPosition.WEST       | Cursor.W_RESIZE  | true
        10      | 20      | 100         | 200          | 15      | 210      || WindowCursorPosition.SOUTH_WEST | Cursor.SW_RESIZE | true
        10      | 20      | 100         | 200          | 15      | 211      || WindowCursorPosition.SOUTH_WEST | Cursor.SW_RESIZE | true
        10      | 20      | 100         | 200          | 15      | 220      || WindowCursorPosition.SOUTH_WEST | Cursor.SW_RESIZE | true
        10      | 20      | 100         | 200          | 15      | 221      || WindowCursorPosition.CENTER     | Cursor.DEFAULT   | false
        10      | 20      | 100         | 200          | 9       | 215      || WindowCursorPosition.CENTER     | Cursor.DEFAULT   | false
        10      | 20      | 100         | 200          | 10      | 215      || WindowCursorPosition.SOUTH_WEST | Cursor.SW_RESIZE | true
        10      | 20      | 100         | 200          | 11      | 215      || WindowCursorPosition.SOUTH_WEST | Cursor.SW_RESIZE | true
        10      | 20      | 100         | 200          | 20      | 215      || WindowCursorPosition.SOUTH_WEST | Cursor.SW_RESIZE | true
        10      | 20      | 100         | 200          | 21      | 215      || WindowCursorPosition.SOUTH      | Cursor.S_RESIZE  | true

        10      | 20      | 100         | 200          | 15      | 19       || WindowCursorPosition.CENTER     | Cursor.DEFAULT   | false
        10      | 20      | 100         | 200          | 15      | 20       || WindowCursorPosition.NORTH_WEST | Cursor.NW_RESIZE | true
        10      | 20      | 100         | 200          | 15      | 21       || WindowCursorPosition.NORTH_WEST | Cursor.NW_RESIZE | true
        10      | 20      | 100         | 200          | 15      | 30       || WindowCursorPosition.NORTH_WEST | Cursor.NW_RESIZE | true
        10      | 20      | 100         | 200          | 15      | 31       || WindowCursorPosition.WEST       | Cursor.W_RESIZE  | true
        10      | 20      | 100         | 200          | 9       | 25       || WindowCursorPosition.CENTER     | Cursor.DEFAULT   | false
        10      | 20      | 100         | 200          | 10      | 25       || WindowCursorPosition.NORTH_WEST | Cursor.NW_RESIZE | true
        10      | 20      | 100         | 200          | 11      | 25       || WindowCursorPosition.NORTH_WEST | Cursor.NW_RESIZE | true
        10      | 20      | 100         | 200          | 20      | 25       || WindowCursorPosition.NORTH_WEST | Cursor.NW_RESIZE | true
        10      | 20      | 100         | 200          | 21      | 25       || WindowCursorPosition.NORTH      | Cursor.N_RESIZE  | true
    }

    def 'gets a window cursor position with the specified resize edge'() {
        given:
        def window = new Stage(x: windowX, y: windowY, width: windowWidth, height: windowHeight)

        when:
        def position = WindowCursorPosition.of(window).in resizeEdge at screenX, screenY

        then:
        expect position, is(windowCursorPosition)

        where:
        windowX | windowY | windowWidth | windowHeight | resizeEdge               | screenX | screenY || windowCursorPosition
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 50      | 23      || WindowCursorPosition.NORTH
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 50      | 24      || WindowCursorPosition.NORTH
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 50      | 25      || WindowCursorPosition.CENTER

        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 101     | 150     || WindowCursorPosition.CENTER
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 102     | 150     || WindowCursorPosition.EAST
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 103     | 150     || WindowCursorPosition.EAST

        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 50      | 207     || WindowCursorPosition.CENTER
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 50      | 208     || WindowCursorPosition.SOUTH
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 50      | 209     || WindowCursorPosition.SOUTH

        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 29      | 150     || WindowCursorPosition.WEST
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 30      | 150     || WindowCursorPosition.WEST
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 31      | 150     || WindowCursorPosition.CENTER

        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 105     | 23      || WindowCursorPosition.NORTH_EAST
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 105     | 24      || WindowCursorPosition.NORTH_EAST
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 105     | 25      || WindowCursorPosition.EAST
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 101     | 22      || WindowCursorPosition.NORTH
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 102     | 22      || WindowCursorPosition.NORTH_EAST
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 103     | 22      || WindowCursorPosition.NORTH_EAST

        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 105     | 207     || WindowCursorPosition.EAST
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 105     | 208     || WindowCursorPosition.SOUTH_EAST
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 105     | 209     || WindowCursorPosition.SOUTH_EAST
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 101     | 210     || WindowCursorPosition.SOUTH
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 102     | 210     || WindowCursorPosition.SOUTH_EAST
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 103     | 210     || WindowCursorPosition.SOUTH_EAST

        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 20      | 207     || WindowCursorPosition.WEST
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 20      | 208     || WindowCursorPosition.SOUTH_WEST
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 20      | 209     || WindowCursorPosition.SOUTH_WEST
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 29      | 210     || WindowCursorPosition.SOUTH_WEST
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 30      | 210     || WindowCursorPosition.SOUTH_WEST
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 31      | 210     || WindowCursorPosition.SOUTH

        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 20      | 23      || WindowCursorPosition.NORTH_WEST
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 20      | 24      || WindowCursorPosition.NORTH_WEST
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 20      | 25      || WindowCursorPosition.WEST
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 29      | 22      || WindowCursorPosition.NORTH_WEST
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 30      | 22      || WindowCursorPosition.NORTH_WEST
        10      | 20      | 100         | 200          | new Insets(4, 8, 12, 20) | 31      | 22      || WindowCursorPosition.NORTH
    }

    @Unroll
    def 'gets an appropriate window resizer whose builder is #windowResizerBuilderType for the specified cursor position (#windowCursorPosition)'() {
        given:
        def window = new Stage()

        when:
        def builder = windowCursorPosition.windowResizerOf window

        then:
        expect builder.isPresent(), is(true)
        and:
        that builder.get(), is(instanceOf(windowResizerBuilderType))

        where:
        windowCursorPosition            || windowResizerBuilderType
        WindowCursorPosition.NORTH      || WindowNorthResizer.Builder
        WindowCursorPosition.EAST       || WindowEastResizer.Builder
        WindowCursorPosition.SOUTH      || WindowSouthResizer.Builder
        WindowCursorPosition.WEST       || WindowWestResizer.Builder
        WindowCursorPosition.NORTH_EAST || WindowNorthEastResizer.Builder
        WindowCursorPosition.SOUTH_EAST || WindowSouthEastResizer.Builder
        WindowCursorPosition.SOUTH_WEST || WindowSouthWestResizer.Builder
        WindowCursorPosition.NORTH_WEST || WindowNorthWestResizer.Builder
    }

    def 'throws an exception when an instance is created with the specified window, which is null'() {
        given:
        def window = null

        when:
        WindowCursorPosition.of window at 0, 0

        then:
        thrown NullPointerException
    }
}
