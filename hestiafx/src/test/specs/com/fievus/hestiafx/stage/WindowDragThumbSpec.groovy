/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.stage

import com.fievus.hestiafx.JavaFXApplication
import com.fievus.hestiafx.scene.input.MouseDragOperation
import javafx.geometry.Insets
import javafx.scene.Scene
import javafx.scene.layout.StackPane
import javafx.scene.shape.Rectangle
import javafx.stage.Stage
import javafx.stage.Window
import org.junit.Rule
import spock.lang.Specification

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

class WindowDragThumbSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    Window window
    javafx.scene.Node node

    def setup() {
        node = new Rectangle(x: 0, y: 0, width: 50, height: 100, layoutX: 200, layoutY: 100)
        window = new Stage(x : 100, y: 200, scene: new Scene(new StackPane(node), 480, 360))
    }

    def 'drags the specified window when the drag detected location is in the draggable area of the target node'() {
        when:
        WindowDragThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(100)
            .sceneOffsetY(200)
            .startDragAt(220, 110)
            .dragTo(325, 113)
            .dragTo(418, 118)
            .dragTo(479, 120)
            .release()

        then:
        expect window.x, is(equalTo(359d))
        and:
        that window.y, is(equalTo(210d))
    }

    def 'should not move the specified window when the drag detected location is out of the draggable area of the target node (left)'() {
        given:
        def padding = new Insets(10)

        when:
        WindowDragThumb.to node padBy padding enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(100)
            .sceneOffsetY(200)
            .startDragAt(205, 150)
            .dragTo(225, 113)
            .dragTo(318, 118)
            .dragTo(379, 120)
            .release()

        then:
        expect window.x, is(equalTo(100d))
        and:
        that window.y, is(equalTo(200d))
    }

    def 'should not move the specified window when the drag detected location is out of the draggable area of the target node (top)'() {
        given:
        def padding = new Insets(10)

        when:
        WindowDragThumb.to node padBy padding enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(100)
            .sceneOffsetY(200)
            .startDragAt(225, 105)
            .dragTo(275, 113)
            .dragTo(318, 118)
            .dragTo(379, 120)
            .release()

        then:
        expect window.x, is(equalTo(100d))
        and:
        that window.y, is(equalTo(200d))
    }

    def 'should not move the specified window when the drag detected location is out of the draggable area of the target node (right)'() {
        given:
        def padding = new Insets(10)

        when:
        WindowDragThumb.to node padBy padding enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(100)
            .sceneOffsetY(200)
            .startDragAt(245, 150)
            .dragTo(275, 113)
            .dragTo(318, 118)
            .dragTo(379, 120)
            .release()

        then:
        expect window.x, is(equalTo(100d))
        and:
        that window.y, is(equalTo(200d))
    }

    def 'should not move the specified window when the drag detected location is out of the draggable area of the target node (bottom)'() {
        given:
        def padding = new Insets(10)

        when:
        WindowDragThumb.to node padBy padding enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(100)
            .sceneOffsetY(200)
            .startDragAt(225, 195)
            .dragTo(275, 183)
            .dragTo(318, 178)
            .dragTo(379, 160)
            .release()

        then:
        expect window.x, is(equalTo(100d))
        and:
        that window.y, is(equalTo(200d))
    }

    def 'should not throw an exception when a window is not specified'() {
        given:
        def node = new Rectangle(x: 0, y: 0, width: 50, height: 100, layoutX: 200, layoutY: 100)
        and:
        def scene = new Scene(new StackPane(node), 480, 360)

        when:
        WindowDragThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(100)
            .sceneOffsetY(200)
            .startDragAt(220, 110)
            .dragTo(325, 113)
            .dragTo(418, 118)
            .dragTo(479, 120)
            .release()

        then:
        notThrown Exception
    }

    def 'should not throw an exception when a scene is not specified'() {
        given:
        def node = new Rectangle(x: 0, y: 0, width: 50, height: 100, layoutX: 200, layoutY: 100)

        when:
        WindowDragThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(100)
            .sceneOffsetY(200)
            .startDragAt(220, 110)
            .dragTo(325, 113)
            .dragTo(418, 118)
            .dragTo(479, 120)
            .release()

        then:
        notThrown Exception
    }

    def 'throws an exception when a target node that is null is specified'() {
        given:
        def node = null

        when:
        WindowDragThumb.to node enable()

        then:
        thrown NullPointerException
    }

    def 'throws an exception when a padding that is null is specified'() {
        given:
        def node = new Rectangle(0, 0, 50, 100)
        and:
        def padding = null

        when:
        WindowDragThumb.to node padBy padding enable()

        then:
        thrown NullPointerException
    }
}
