/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.stage

import com.fievus.hestiafx.JavaFXApplication
import javafx.stage.Stage
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Unroll

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

class WindowRelocatorSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    @Unroll
    def 'relocates the specified window, the location of which is (#initialWindowX, #initialWindowY), to (#windowX, #windowY) when the window, the initial location of which is (#initialX, #initialY), moves to (#x, #y)'() {
        given:
        def window = new Stage(x: initialWindowX, y: initialWindowY)
        and:
        def relocator = WindowRelocator.of window at initialX, initialY

        when:
        relocator.relocateFor x, y

        then:
        expect window.x, is(equalTo(windowX))
        and:
        that window.y, is(equalTo(windowY))

        where:
        initialX | initialY | initialWindowX | initialWindowY |  x |  y || windowX | windowY
         0       |  0       |  0             |  0             |  3 |  5 ||  3d     |  5d

         0       |  0       |  3             |  5             |  2 |  3 ||  5d     |  8d
         0       |  0       |  3             |  5             | -2 | -3 ||  1d     |  2d
         0       |  0       |  3             |  5             | -4 | -8 || -1d     | -3d

         0       |  0       | -3             | -5             |  2 |  3 || -1d     | -2d
         0       |  0       | -3             | -5             | -2 | -3 || -5d     | -8d
         0       |  0       | -3             | -5             |  4 |  8 ||  1d     |  3d

         4       |  7       |  3             |  5             |  5 |  9 ||  4d     |  7d
         4       |  7       |  3             |  5             |  2 |  4 ||  1d     |  2d
         4       |  7       |  3             |  5             |  0 | -1 || -1d     | -3d

        -4       | -7       | -3             | -5             | -2 | -4 || -1d     | -2d
        -4       | -7       | -3             | -5             | -5 | -9 || -4d     | -7d
        -4       | -7       | -3             | -5             |  0 |  1 ||  1d     |  3d
    }

    def 'throws an exception when an instance is created with the specified window, which is null'() {
        given:
        def window = null

        when:
        WindowRelocator.of window at 0, 0

        then:
        thrown NullPointerException
    }
}
