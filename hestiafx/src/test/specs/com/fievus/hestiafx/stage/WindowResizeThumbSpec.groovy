/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.stage

import com.fievus.hestiafx.JavaFXApplication
import com.fievus.hestiafx.scene.input.MouseDragOperation
import javafx.geometry.Insets
import javafx.scene.Scene
import javafx.scene.layout.StackPane
import javafx.stage.Stage
import javafx.stage.Window
import org.junit.Rule
import spock.lang.Specification

import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

class WindowResizeThumbSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    Window window
    StackPane node

    def setup() {
        node = new StackPane()
        window = new Stage(x: 10, y: 20, width: 100, height: 200, scene: new Scene(node))
    }

    def 'resizes the specified window in the direction of NORTH(positive) when the the drag detected location is in the north edge'() {
        when:
        WindowResizeThumb.to window enable()
        and:
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(50, 5)
            .dragTo(55, 3)
            .dragTo(58, 1)
            .dragTo(64, -2)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(13d))
        and:
        that window.width, is(equalTo(100d))
        and:
        that window.height, is(equalTo(207d))
    }

    def 'resizes the specified window in the direction of NORTH(negative) when the the drag detected location is in the north edge'() {
        when:
        WindowResizeThumb.to window enable()
        and:
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(50, 5)
            .dragTo(55, 8)
            .dragTo(58, 10)
            .dragTo(64, 13)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(28d))
        and:
        that window.width, is(equalTo(100d))
        and:
        that window.height, is(equalTo(192d))
    }

    def 'resizes the specified window in the direction of EAST(positive) when the drag detected location is in the east edge'() {
        when:
        WindowResizeThumb.to window enable()
        and:
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(95, 150)
            .dragTo(99, 153)
            .dragTo(101, 158)
            .dragTo(104, 162)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(109d))
        and:
        that window.height, is(equalTo(200d))
    }

    def 'resizes the specified window in the direction of EAST(negative) when the drag detected location is in the east edge'() {
        when:
        WindowResizeThumb.to window enable()
        and:
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(95, 150)
            .dragTo(91, 153)
            .dragTo(83, 158)
            .dragTo(78, 162)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(83d))
        and:
        that window.height, is(equalTo(200d))
    }

    def 'resizes the specified window in the direction of SOUTH(positive) when the drag detected location is in the south edge'() {
        when:
        WindowResizeThumb.to window enable()
        and:
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(50, 195)
            .dragTo(42, 198)
            .dragTo(38, 205)
            .dragTo(33, 209)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(100d))
        and:
        that window.height, is(equalTo(214d))
    }

    def 'resizes the specified window in the direction of SOUTH(negative) when the drag detected location is in the south edge'() {
        when:
        WindowResizeThumb.to window enable()
        and:
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(50, 195)
            .dragTo(42, 191)
            .dragTo(38, 189)
            .dragTo(33, 183)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(100d))
        and:
        that window.height, is(equalTo(188d))
    }

    def 'resizes the specified window in the direction of WEST(positive) when the drag detected location is in the west edge'() {
        when:
        WindowResizeThumb.to window enable()
        and:
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(5, 150)
            .dragTo(9, 153)
            .dragTo(11, 158)
            .dragTo(13, 162)
            .release()

        then:
        expect window.x, is(equalTo(18d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(92d))
        and:
        that window.height, is(equalTo(200d))
    }

    def 'resizes the specified window in the direction of WEST(negative) when the drag detected location is in the west edge'() {
        when:
        WindowResizeThumb.to window enable()
        and:
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(5, 150)
            .dragTo(3, 153)
            .dragTo(-1, 158)
            .dragTo(-5, 162)
            .release()

        then:
        expect window.x, is(equalTo(0d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(110d))
        and:
        that window.height, is(equalTo(200d))
    }

    def 'resizes the specified window in the direction of NORTH_EAST(positive-positive) when the drag detected location in in the north east edge'() {
        when:
        WindowResizeThumb.to window enable()
        and:
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(95, 5)
            .dragTo(98, 6)
            .dragTo(101, 8)
            .dragTo(103, 9)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(24d))
        and:
        that window.width, is(equalTo(108d))
        and:
        that window.height, is(equalTo(196d))
    }

    def 'resizes the specified window in the direction of NORTH_EAST(negative-positive) when the drag detected location in in the north east edge'() {
        when:
        WindowResizeThumb.to window enable()
        and:
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(95, 5)
            .dragTo(98, 1)
            .dragTo(101, -3)
            .dragTo(103, -9)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(6d))
        and:
        that window.width, is(equalTo(108d))
        and:
        that window.height, is(equalTo(214d))
    }

    def 'resizes the specified window in the direction of NORTH_EAST(positive-negative) when the drag detected location in in the north east edge'() {
        when:
        WindowResizeThumb.to window enable()
        and:
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(95, 5)
            .dragTo(92, 6)
            .dragTo(88, 8)
            .dragTo(84, 9)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(24d))
        and:
        that window.width, is(equalTo(89d))
        and:
        that window.height, is(equalTo(196d))
    }

    def 'resizes the specified window in the direction of NORTH_EAST(negative-negative) when the drag detected location in in the north east edge'() {
        when:
        WindowResizeThumb.to window enable()
        and:
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(95, 5)
            .dragTo(92, 1)
            .dragTo(88, -3)
            .dragTo(84, -9)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(6d))
        and:
        that window.width, is(equalTo(89d))
        and:
        that window.height, is(equalTo(214d))
    }

    def 'resizes the specified window in the direction of SOUTH_EAST(positive-positive) when the drag detected location is in the south east edge'() {
        when:
        WindowResizeThumb.to window enable()
        and:
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(95, 195)
            .dragTo(98, 199)
            .dragTo(101, 205)
            .dragTo(103, 211)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(108d))
        and:
        that window.height, is(equalTo(216d))
    }

    def 'resizes the specified window in the direction of SOUTH_EAST(negative-positive) when the drag detected location is in the south east edge'() {
        when:
        WindowResizeThumb.to window enable()
        and:
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(95, 195)
            .dragTo(98, 193)
            .dragTo(101, 188)
            .dragTo(103, 182)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(108d))
        and:
        that window.height, is(equalTo(187d))
    }

    def 'resizes the specified window in the direction of SOUTH_EAST(positive-negative) when the drag detected location is in the south east edge'() {
        when:
        WindowResizeThumb.to window enable()
        and:
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(95, 195)
            .dragTo(92, 199)
            .dragTo(88, 205)
            .dragTo(84, 211)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(89d))
        and:
        that window.height, is(equalTo(216d))
    }

    def 'resizes the specified window in the direction of SOUTH_EAST(negative-negative) when the drag detected location is in the south east edge'() {
        when:
        WindowResizeThumb.to window enable()
        and:
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(95, 195)
            .dragTo(92, 193)
            .dragTo(88, 188)
            .dragTo(84, 182)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(89d))
        and:
        that window.height, is(equalTo(187d))
    }

    def 'resizes the specified window in the direction of SOUTH_WEST(positive-positive) when the drag detected location is in the south west edge'() {
        when:
        WindowResizeThumb.to window enable()
        and:
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(5, 195)
            .dragTo(11, 198)
            .dragTo(13, 201)
            .dragTo(19, 204)
            .release()

        then:
        expect window.x, is(equalTo(24d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(86d))
        and:
        that window.height, is(equalTo(209d))
    }

    def 'resizes the specified window in the direction of SOUTH_WEST(negative-positive) when the drag detected location is in the south west edge'() {
        when:
        WindowResizeThumb.to window enable()
        and:
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(5, 195)
            .dragTo(11, 192)
            .dragTo(13, 184)
            .dragTo(19, 179)
            .release()

        then:
        expect window.x, is(equalTo(24d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(86d))
        and:
        that window.height, is(equalTo(184d))
    }

    def 'resizes the specified window in the direction of SOUTH_WEST(positive-negative) when the drag detected location is in the south west edge'() {
        when:
        WindowResizeThumb.to window enable()
        and:
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(5, 195)
            .dragTo(3, 198)
            .dragTo(-1, 201)
            .dragTo(-8, 204)
            .release()

        then:
        expect window.x, is(equalTo(-3d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(113d))
        and:
        that window.height, is(equalTo(209d))
    }

    def 'resizes the specified window in the direction of SOUTH_WEST(negative-negative) when the drag detected location is in the south west edge'() {
        when:
        WindowResizeThumb.to window enable()
        and:
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(5, 195)
            .dragTo(3, 192)
            .dragTo(-1, 184)
            .dragTo(-8, 179)
            .release()

        then:
        expect window.x, is(equalTo(-3d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(113d))
        and:
        that window.height, is(equalTo(184d))
    }

    def 'resizes the specified window in the direction of NORTH_WEST(positive-positive) when the drag detected location is in the north west edge'() {
        when:
        WindowResizeThumb.to window enable()
        and:
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(5, 5)
            .dragTo(8, 9)
            .dragTo(11, 13)
            .dragTo(18, 21)
            .release()

        then:
        expect window.x, is(equalTo(23d))
        and:
        that window.y, is(equalTo(36d))
        and:
        that window.width, is(equalTo(87d))
        and:
        that window.height, is(equalTo(184d))
    }

    def 'resizes the specified window in the direction of NORTH_WEST(negative-positive) when the drag detected location is in the north west edge'() {
        when:
        WindowResizeThumb.to window enable()
        and:
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(5, 5)
            .dragTo(8, 1)
            .dragTo(11, -3)
            .dragTo(18, -6)
            .release()

        then:
        expect window.x, is(equalTo(23d))
        and:
        that window.y, is(equalTo(9d))
        and:
        that window.width, is(equalTo(87d))
        and:
        that window.height, is(equalTo(211d))
    }

    def 'resizes the specified window in the direction of NORTH_WEST(positive-negative) when the drag detected location is in the north west edge'() {
        when:
        WindowResizeThumb.to window enable()
        and:
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(5, 5)
            .dragTo(3, 9)
            .dragTo(-1, 13)
            .dragTo(-4, 21)
            .release()

        then:
        expect window.x, is(equalTo(1d))
        and:
        that window.y, is(equalTo(36d))
        and:
        that window.width, is(equalTo(109d))
        and:
        that window.height, is(equalTo(184d))
    }

    def 'resizes the specified window in the direction of NORTH_WEST(negative-negative) when the drag detected location is in the north west edge'() {
        when:
        WindowResizeThumb.to window enable()
        and:
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(5, 5)
            .dragTo(3, 1)
            .dragTo(-1, -3)
            .dragTo(-4, -6)
            .release()

        then:
        expect window.x, is(equalTo(1d))
        and:
        that window.y, is(equalTo(9d))
        and:
        that window.width, is(equalTo(109d))
        and:
        that window.height, is(equalTo(211d))
    }

    def 'resizes the specified window with a custom resize edge'() {
        when:
        WindowResizeThumb.to window with new Insets(4, 8, 12, 16) enable()
        and: 'moves a north-west corner'
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(16, 4)
            .dragTo(14, 1)
            .release()
        and: 'moves a north-east corner'
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(8)
            .sceneOffsetY(17)
            .startDragAt(94, 4)
            .dragTo(97, 2)
            .release()
        and: 'moves a south-east corner'
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(8)
            .sceneOffsetY(15)
            .startDragAt(97, 193)
            .dragTo(101, 198)
            .release()
        and: 'moves a south-west corner'
        MouseDragOperation.forWindow(window)
            .sceneOffsetX(8)
            .sceneOffsetY(15)
            .startDragAt(16, 198)
            .dragTo(11, 201)
            .release()

        then:
        expect window.x, is(equalTo(3d))
        and:
        that window.y, is(equalTo(15d))
        and:
        that window.width, is(equalTo(114d))
        and:
        that window.height, is(equalTo(213d))
    }

    def 'throws an exception when a target window that is null is specified'() {
        given:
        def window = null

        when:
        WindowResizeThumb.to window as Window enable()

        then:
        thrown NullPointerException
    }

    def 'resizes a window in which the specified node is in the direction of NORTH(positive) when the the drag detected location is in the north edge'() {
        when:
        WindowResizeThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(50, 5)
            .dragTo(55, 3)
            .dragTo(58, 1)
            .dragTo(64, -2)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(13d))
        and:
        that window.width, is(equalTo(100d))
        and:
        that window.height, is(equalTo(207d))
    }

    def 'resizes a window in which the specified node is in the direction of NORTH(negative) when the the drag detected location is in the north edge'() {
        when:
        WindowResizeThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(50, 5)
            .dragTo(55, 8)
            .dragTo(58, 10)
            .dragTo(64, 13)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(28d))
        and:
        that window.width, is(equalTo(100d))
        and:
        that window.height, is(equalTo(192d))
    }

    def 'resizes a window in which the specified node is in the direction of EAST(positive) when the drag detected location is in the east edge'() {
        when:
        WindowResizeThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(95, 150)
            .dragTo(99, 153)
            .dragTo(101, 158)
            .dragTo(104, 162)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(109d))
        and:
        that window.height, is(equalTo(200d))
    }

    def 'resizes a window in which the specified node is in the direction of EAST(negative) when the drag detected location is in the east edge'() {
        when:
        WindowResizeThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(95, 150)
            .dragTo(91, 153)
            .dragTo(83, 158)
            .dragTo(78, 162)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(83d))
        and:
        that window.height, is(equalTo(200d))
    }

    def 'resizes a window in which the specified node is in the direction of SOUTH(positive) when the drag detected location is in the south edge'() {
        when:
        WindowResizeThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(50, 195)
            .dragTo(42, 198)
            .dragTo(38, 205)
            .dragTo(33, 209)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(100d))
        and:
        that window.height, is(equalTo(214d))
    }

    def 'resizes a window in which the specified node is in the direction of SOUTH(negative) when the drag detected location is in the south edge'() {
        when:
        WindowResizeThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(50, 195)
            .dragTo(42, 191)
            .dragTo(38, 189)
            .dragTo(33, 183)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(100d))
        and:
        that window.height, is(equalTo(188d))
    }

    def 'resizes a window in which the specified node is in the direction of WEST(positive) when the drag detected location is in the west edge'() {
        when:
        WindowResizeThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(5, 150)
            .dragTo(9, 153)
            .dragTo(11, 158)
            .dragTo(13, 162)
            .release()

        then:
        expect window.x, is(equalTo(18d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(92d))
        and:
        that window.height, is(equalTo(200d))
    }

    def 'resizes a window in which the specified node is in the direction of WEST(negative) when the drag detected location is in the west edge'() {
        when:
        WindowResizeThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(5, 150)
            .dragTo(3, 153)
            .dragTo(-1, 158)
            .dragTo(-5, 162)
            .release()

        then:
        expect window.x, is(equalTo(0d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(110d))
        and:
        that window.height, is(equalTo(200d))
    }

    def 'resizes a window in which the specified node is in the direction of NORTH_EAST(positive-positive) when the drag detected location in in the north east edge'() {
        when:
        WindowResizeThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(95, 5)
            .dragTo(98, 6)
            .dragTo(101, 8)
            .dragTo(103, 9)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(24d))
        and:
        that window.width, is(equalTo(108d))
        and:
        that window.height, is(equalTo(196d))
    }

    def 'resizes a window in which the specified node is in the direction of NORTH_EAST(negative-positive) when the drag detected location in in the north east edge'() {
        when:
        WindowResizeThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(95, 5)
            .dragTo(98, 1)
            .dragTo(101, -3)
            .dragTo(103, -9)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(6d))
        and:
        that window.width, is(equalTo(108d))
        and:
        that window.height, is(equalTo(214d))
    }

    def 'resizes a window in which the specified node is in the direction of NORTH_EAST(positive-negative) when the drag detected location in in the north east edge'() {
        when:
        WindowResizeThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(95, 5)
            .dragTo(92, 6)
            .dragTo(88, 8)
            .dragTo(84, 9)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(24d))
        and:
        that window.width, is(equalTo(89d))
        and:
        that window.height, is(equalTo(196d))
    }

    def 'resizes a window in which the specified node is in the direction of NORTH_EAST(negative-negative) when the drag detected location in in the north east edge'() {
        when:
        WindowResizeThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(95, 5)
            .dragTo(92, 1)
            .dragTo(88, -3)
            .dragTo(84, -9)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(6d))
        and:
        that window.width, is(equalTo(89d))
        and:
        that window.height, is(equalTo(214d))
    }

    def 'resizes a window in which the specified node is in the direction of SOUTH_EAST(positive-positive) when the drag detected location is in the south east edge'() {
        when:
        WindowResizeThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(95, 195)
            .dragTo(98, 199)
            .dragTo(101, 205)
            .dragTo(103, 211)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(108d))
        and:
        that window.height, is(equalTo(216d))
    }

    def 'resizes a window in which the specified node is in the direction of SOUTH_EAST(negative-positive) when the drag detected location is in the south east edge'() {
        when:
        WindowResizeThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(95, 195)
            .dragTo(98, 193)
            .dragTo(101, 188)
            .dragTo(103, 182)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(108d))
        and:
        that window.height, is(equalTo(187d))
    }

    def 'resizes a window in which the specified node is in the direction of SOUTH_EAST(positive-negative) when the drag detected location is in the south east edge'() {
        when:
        WindowResizeThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(95, 195)
            .dragTo(92, 199)
            .dragTo(88, 205)
            .dragTo(84, 211)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(89d))
        and:
        that window.height, is(equalTo(216d))
    }

    def 'resizes a window in which the specified node is in the direction of SOUTH_EAST(negative-negative) when the drag detected location is in the south east edge'() {
        when:
        WindowResizeThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(95, 195)
            .dragTo(92, 193)
            .dragTo(88, 188)
            .dragTo(84, 182)
            .release()

        then:
        expect window.x, is(equalTo(10d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(89d))
        and:
        that window.height, is(equalTo(187d))
    }

    def 'resizes a window in which the specified node is in the direction of SOUTH_WEST(positive-positive) when the drag detected location is in the south west edge'() {
        when:
        WindowResizeThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(5, 195)
            .dragTo(11, 198)
            .dragTo(13, 201)
            .dragTo(19, 204)
            .release()

        then:
        expect window.x, is(equalTo(24d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(86d))
        and:
        that window.height, is(equalTo(209d))
    }

    def 'resizes a window in which the specified node is in the direction of SOUTH_WEST(negative-positive) when the drag detected location is in the south west edge'() {
        when:
        WindowResizeThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(5, 195)
            .dragTo(11, 192)
            .dragTo(13, 184)
            .dragTo(19, 179)
            .release()

        then:
        expect window.x, is(equalTo(24d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(86d))
        and:
        that window.height, is(equalTo(184d))
    }

    def 'resizes a window in which the specified node is in the direction of SOUTH_WEST(positive-negative) when the drag detected location is in the south west edge'() {
        when:
        WindowResizeThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(5, 195)
            .dragTo(3, 198)
            .dragTo(-1, 201)
            .dragTo(-8, 204)
            .release()

        then:
        expect window.x, is(equalTo(-3d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(113d))
        and:
        that window.height, is(equalTo(209d))
    }

    def 'resizes a window in which the specified node is in the direction of SOUTH_WEST(negative-negative) when the drag detected location is in the south west edge'() {
        when:
        WindowResizeThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(5, 195)
            .dragTo(3, 192)
            .dragTo(-1, 184)
            .dragTo(-8, 179)
            .release()

        then:
        expect window.x, is(equalTo(-3d))
        and:
        that window.y, is(equalTo(20d))
        and:
        that window.width, is(equalTo(113d))
        and:
        that window.height, is(equalTo(184d))
    }

    def 'resizes a window in which the specified node is in the direction of NORTH_WEST(positive-positive) when the drag detected location is in the north west edge'() {
        when:
        WindowResizeThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(5, 5)
            .dragTo(8, 9)
            .dragTo(11, 13)
            .dragTo(18, 21)
            .release()

        then:
        expect window.x, is(equalTo(23d))
        and:
        that window.y, is(equalTo(36d))
        and:
        that window.width, is(equalTo(87d))
        and:
        that window.height, is(equalTo(184d))
    }

    def 'resizes a window in which the specified node is in the direction of NORTH_WEST(negative-positive) when the drag detected location is in the north west edge'() {
        when:
        WindowResizeThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(5, 5)
            .dragTo(8, 1)
            .dragTo(11, -3)
            .dragTo(18, -6)
            .release()

        then:
        expect window.x, is(equalTo(23d))
        and:
        that window.y, is(equalTo(9d))
        and:
        that window.width, is(equalTo(87d))
        and:
        that window.height, is(equalTo(211d))
    }

    def 'resizes a window in which the specified node is in the direction of NORTH_WEST(positive-negative) when the drag detected location is in the north west edge'() {
        when:
        WindowResizeThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(5, 5)
            .dragTo(3, 9)
            .dragTo(-1, 13)
            .dragTo(-4, 21)
            .release()

        then:
        expect window.x, is(equalTo(1d))
        and:
        that window.y, is(equalTo(36d))
        and:
        that window.width, is(equalTo(109d))
        and:
        that window.height, is(equalTo(184d))
    }

    def 'resizes a window in which the specified node is in the direction of NORTH_WEST(negative-negative) when the drag detected location is in the north west edge'() {
        when:
        WindowResizeThumb.to node enable()
        and:
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(5, 5)
            .dragTo(3, 1)
            .dragTo(-1, -3)
            .dragTo(-4, -6)
            .release()

        then:
        expect window.x, is(equalTo(1d))
        and:
        that window.y, is(equalTo(9d))
        and:
        that window.width, is(equalTo(109d))
        and:
        that window.height, is(equalTo(211d))
    }

    def 'resizes a window in which the specified node is with a custom resize edge'() {
        when:
        WindowResizeThumb.to node with new Insets(4, 8, 12, 16) enable()
        and: 'moves a north-west corner'
        MouseDragOperation.forNode(node)
            .sceneOffsetX(10)
            .sceneOffsetY(20)
            .startDragAt(16, 4)
            .dragTo(14, 1)
            .release()
        and: 'moves a north-east corner'
        MouseDragOperation.forNode(node)
            .sceneOffsetX(8)
            .sceneOffsetY(17)
            .startDragAt(94, 4)
            .dragTo(97, 2)
            .release()
        and: 'moves a south-east corner'
        MouseDragOperation.forNode(node)
            .sceneOffsetX(8)
            .sceneOffsetY(15)
            .startDragAt(97, 193)
            .dragTo(101, 198)
            .release()
        and: 'moves a south-west corner'
        MouseDragOperation.forNode(node)
            .sceneOffsetX(8)
            .sceneOffsetY(15)
            .startDragAt(16, 198)
            .dragTo(11, 201)
            .release()

        then:
        expect window.x, is(equalTo(3d))
        and:
        that window.y, is(equalTo(15d))
        and:
        that window.width, is(equalTo(114d))
        and:
        that window.height, is(equalTo(213d))
    }

    def 'throws an exception when a target node that is null is specified'() {
        given:
        def node = null

        when:
        WindowResizeThumb.to node as javafx.scene.Node enable()

        then:
        thrown NullPointerException
    }

    def 'throws an exception when a target node that is not in a Scene'() {
        given:
        def node = new StackPane()

        when:
        WindowResizeThumb.to node enable()

        then:
        thrown IllegalArgumentException
    }

    def 'throws an exception when a target node that is not in a Window'() {
        given:
        def node = new StackPane()
        def scene = new Scene(node)

        when:
        WindowResizeThumb.to node enable()

        then:
        thrown IllegalArgumentException
    }
}
