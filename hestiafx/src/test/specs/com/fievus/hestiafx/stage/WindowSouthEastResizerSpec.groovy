/**
 * Copyright (c) 2016 Fievus
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
package com.fievus.hestiafx.stage

import com.fievus.hestiafx.JavaFXApplication
import javafx.stage.Stage
import org.junit.Rule
import spock.lang.Specification
import spock.lang.Unroll

import static com.fievus.hestiafx.stage.WindowSouthEastResizer.*
import static org.hamcrest.Matchers.*
import static spock.util.matcher.HamcrestSupport.*

class WindowSouthEastResizerSpec extends Specification {
    @Rule
    JavaFXApplication javaFXApplication

    @Unroll
    def 'resizes the specified window, the width of which is #initialWindowWidth, to #windowWidth when the window, the location of which is (#initialX, #initialY), moves to (#x, #y)'() {
        given:
        def window = new Stage(x: initialWindowX, y: initialWindowY, width: initialWindowWidth, height: initialWindowHeight)
        and:
        def resizer = of window at initialX, initialY

        when:
        resizer.resizeFor x, y

        then:
        expect window.x, is(equalTo(windowX))
        and:
        that window.y, is(equalTo(windowY))
        and:
        that window.width, is(equalTo(windowWidth))
        and:
        that window.height, is(equalTo(windowHeight))

        where:
        initialX | initialY | initialWindowX | initialWindowY | initialWindowWidth | initialWindowHeight |  x |  y || windowX | windowY | windowWidth | windowHeight
        0        | 0        | 10             | 20             | 100                | 200                 |  3 |  5 || 10d     | 20d     | 103d        | 205d
        0        | 0        | 10             | 20             | 100                | 200                 | -3 |  5 || 10d     | 20d     | 97d         | 205d
        0        | 0        | 10             | 20             | 100                | 200                 |  3 | -5 || 10d     | 20d     | 103d        | 195d
        0        | 0        | 10             | 20             | 100                | 200                 | -3 | -5 || 10d     | 20d     | 97d         | 195d
    }

    def 'should not resize smaller than the specified window min size or larger than the specified window max size'() {
        given:
        def window = new Stage(
            x: initialWindowX, y: initialWindowY, width: initialWindowWidth, height: initialWindowHeight,
            minWidth: windowMinWidth, maxWidth: windowMaxWidth, minHeight: windowMinHeight, maxHeight: windowMaxHeight
        )
        and:
        def resizer = of window at initialX, initialY

        when:
        resizer.resizeFor x, y

        then:
        expect window.x, is(equalTo(windowX))
        and:
        that window.y, is(equalTo(windowY))
        and:
        that window.width, is(equalTo(windowWidth))
        and:
        that window.height, is(equalTo(windowHeight))

        where:
        initialX | initialY | initialWindowX | initialWindowY | initialWindowWidth | initialWindowHeight | windowMinWidth | windowMaxWidth | windowMinHeight | windowMaxHeight |  x |  y || windowX | windowY | windowWidth | windowHeight
        0        | 0        | 10             | 20             | 100                | 200                 | 100            | 100            | 200             | 200             |  3 |  5 || 10d     | 20d     | 100d        | 200d
        0        | 0        | 10             | 20             | 100                | 200                 | 100            | 100            | 200             | 200             | -3 |  5 || 10d     | 20d     | 100d        | 200d
        0        | 0        | 10             | 20             | 100                | 200                 | 100            | 100            | 200             | 200             |  3 | -5 || 10d     | 20d     | 100d        | 200d
        0        | 0        | 10             | 20             | 100                | 200                 | 100            | 100            | 200             | 200             | -3 | -5 || 10d     | 20d     | 100d        | 200d
    }

    def 'throws an exception when an instance is created with the specified window, which is null'() {
        given:
        def window = null

        when:
        of window at 0, 0

        then:
        thrown NullPointerException
    }
}
